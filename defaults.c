#include "buffer.h"
#include "commands.h"
#include "context.h"
#include "defaults.h"
#include "dock.h"
#include "events.h"
#include "functions.h"
#include "labels.h"
#include "options.h"
#include "registers.h"
#include "tile.h"
#include "undo.h"

#include <ctype.h>

static KeyBinding exBindings[] = {
    {MODE_EX, ':',         .func = focusCmdWindow},
    {MODE_EX, '\n',        .func = appendChar,        .arg.c = '\n', .flags = BF_NO_ABORT_ON_MATCH},
    {MODE_EX, '\n',        .func = enterNormalModeIf, .arg.s=".\n"},
    {MODE_EX, CTRL('m'),   .func = typeChar,          .arg.c = '\n'},
    {MODE_EX, ANY_CHAR,    .func = appendChar,        .flags = BF_USE_EVENT_CHAR_AS_ARG },
};

static KeyBinding defaultBindings[] = {

    {MODE_ANY, '\n',           .func = typeChar,        .arg.c = CTRL('m')},
    {MODE_INSERT, '\t',        .func = appendTab},

    {MODE_NORMAL, ':',         .func = focusCmdWindow},
    {MODE_ANY, CTRL('m'),      .func = submitCommand, .fileType = FT_WEE_CMD},
    {MODE_NORMAL, ESC,         .func = unfocusCmdWindow, .fileType = FT_WEE_CMD},

    {MODE_NORMAL, 'i',         .func = setMode,         .arg = {.i = MODE_INSERT}},
    {MODE_INSERT, .str = "jj", .func = setMode,         .arg = {.i = MODE_NORMAL}},
    {MODE_INSERT, CTRL('['),   .func = setMode,         .arg = {.i = MODE_NORMAL}}, // Escape
    {MODE_INSERT, CTRL('m'),   .func = appendChar,      .arg = {.c = '\n'}   },

    {MODE_NORMAL, .testFunc = isdigit, .func = updateCount, .flags = BF_IGNORE_OTHER_COMMANDS | BF_USE_EVENT_CHAR_AS_ARG | BF_NO_ABORT_ON_SUCCESS},
    {MODE_NORMAL, '0',         .func = jumpToCol,       .arg = {.i = 0}, .flags = BF_MOTION_CMD},

    {MODE_NORMAL, CTRL('f'),   .func = movePage,        .arg = {.i = UP},    .flags = BF_MOTION_CMD},
    {MODE_NORMAL, PGUP,        .func = movePage,        .arg = {.i = UP},    .flags = BF_MOTION_CMD},
    {MODE_NORMAL, CTRL('b'),   .func = movePage,        .arg = {.i = DOWN},  .flags = BF_MOTION_CMD},
    {MODE_NORMAL, PGDN,        .func = movePage,        .arg = {.i = DOWN},  .flags = BF_MOTION_CMD},

    {MODE_NORMAL, .str = "gg", .func = gotoLine,        .arg.i = 0},
    {MODE_NORMAL, 'G',         .func = gotoLine,        .arg.i = -1},

    {MODE_NORMAL, 'k',         .func = moveVert,        .arg = {.i = UP  },  .flags = BF_MOTION_CMD},
    {MODE_NORMAL, 'j',         .func = moveVert,        .arg = {.i = DOWN},  .flags = BF_MOTION_CMD},
    {MODE_NORMAL, 'h',         .func = moveHor,         .arg = {.i = LEFT},  .flags = BF_MOTION_CMD},
    {MODE_NORMAL, 'l',         .func = moveHor,         .arg = {.i = RIGHT}, .flags = BF_MOTION_CMD},

    {MODE_NORMAL, 'f',         .func = jumpToCharInLine,               .flags = BF_USE_NEXT_EVENT_CHAR_AS_ARG | BF_MOTION_INCLUSIVE_CMD},
    {MODE_NORMAL, 'F',         .func = jumpToCharInLineBackwards,      .flags = BF_USE_NEXT_EVENT_CHAR_AS_ARG | BF_MOTION_INCLUSIVE_CMD},
    {MODE_NORMAL, 't',         .func = jumpToBeforeCharInLine,         .flags = BF_USE_NEXT_EVENT_CHAR_AS_ARG | BF_MOTION_INCLUSIVE_CMD},
    {MODE_NORMAL, 'T',         .func = jumpToAfterCharInLineBackwards, .flags = BF_USE_NEXT_EVENT_CHAR_AS_ARG | BF_MOTION_CMD},
    {MODE_NORMAL, '$',         .func = jumpToEndOfLine, .arg.i = 1,    .flags = BF_MOTION_CMD},

    {MODE_NORMAL, 'w',         .func = jumpToBoundary,   .arg={.index = word_regex_index,    .dir =  1, .end = 0},    .flags = BF_MOTION_CMD},
    {MODE_NORMAL, 'e',         .func = jumpToBoundary,   .arg={.index = word_regex_index,    .dir =  1, .end = 1},    .flags = BF_MOTION_CMD},
    {MODE_NORMAL, 'b',         .func = jumpToBoundary,   .arg={.index = word_regex_index,    .dir = -1, .end = 1},    .flags = BF_MOTION_CMD},

    {MODE_NORMAL, 'W',         .func = jumpToBoundary,   .arg={.index = bigword_regex_index, .dir =  1, .end = 0},    .flags = BF_MOTION_CMD},
    {MODE_NORMAL, 'E',         .func = jumpToBoundary,   .arg={.index = bigword_regex_index, .dir =  1, .end = 1},    .flags = BF_MOTION_CMD},
    {MODE_NORMAL, 'B',         .func = jumpToBoundary,   .arg={.index = bigword_regex_index, .dir = -1, .end = 1},    .flags = BF_MOTION_CMD},

    {MODE_NORMAL, .str="[[",   .func = jumpToBoundary,   .arg={.index = section_regex_index, .dir = -1},   .flags = BF_MOTION_CMD},
    {MODE_NORMAL, .str="]]",   .func = jumpToBoundary,   .arg={.index = section_regex_index, .dir =  1},   .flags = BF_MOTION_CMD},

    {MODE_NORMAL, '{',         .func = jumpToBoundary,   .arg={.index = paragraph_regex_index, .dir = -1}, .flags = BF_MOTION_CMD},
    {MODE_NORMAL, '}',         .func = jumpToBoundary,   .arg={.index = paragraph_regex_index, .dir =  1}, .flags = BF_MOTION_CMD},

    {MODE_NORMAL, '(',         .func = jumpToBoundary,   .arg={.index = sentence_regex_index, .dir = -1},   .flags = BF_MOTION_CMD},
    {MODE_NORMAL, ')',         .func = jumpToBoundary,   .arg={.index = sentence_regex_index, .dir =  1},   .flags = BF_MOTION_CMD},

    {MODE_NORMAL, 'u',         .func = undo},
    {MODE_NORMAL, CTRL('R'),   .func = redo},

    {MODE_NORMAL, .str = "dd", .func = deleteCurrentLine, .arg = {.i = 1}  },
    {MODE_NORMAL, 'd',         .func = deleteChar,        .arg = {.i = 1}, .flags = BF_REQUIRES_MOTION_CMD},

    {MODE_NORMAL, .str = "yy", .func = yankCurrentLine, .arg = {.i = 1}  },
    {MODE_NORMAL, 'y',         .func = yankRange,        .arg = {.i = 1}, .flags = BF_REQUIRES_MOTION_CMD},

    {MODE_NORMAL, 'P',         .func = jumpToEndOfLine, .arg.i = -1, .flags = BF_FALLTHROUGH},
    {MODE_NORMAL, 'P',         .func = pasteText,        .arg = {.i = 1}},
    {MODE_NORMAL, 'p',         .func = pasteText,        .arg = {.i = 1}},


    {MODE_INSERT, CTRL('H'),   .func = deleteCharWithinLine,      .arg = {.i = -1}}, // backspace
    {MODE_INSERT, 0x7F,        .func = deleteCharWithinLine,      .arg = {.i = -1}}, // backspace
    {MODE_NORMAL, 'X',         .func = deleteCharWithinLine,      .arg = {.i = -1}},

    {MODE_ANY, DELETE,         .func = deleteCharWithinLine,      .arg = {.i =  1}}, // delete
    {MODE_NORMAL, 'x',         .func = deleteCharWithinLine,      .arg = {.i =  1}},

    {MODE_NORMAL, CTRL('c'),   .func = shutdownContext,         },
    {MODE_NORMAL, CTRL('z'),   .func = suspendContext,         },

    // Combo commands
    {MODE_NORMAL, 'a',         .func = setMode,         .arg.m = MODE_INSERT, .flags = BF_IGNORE_OTHER_COMMANDS | BF_FALLTHROUGH},
    {MODE_NORMAL, 'a',         .func = moveCursor,      .arg.i = RIGHT},
    {MODE_NORMAL, 'A',         .func = setMode,         .arg.m = MODE_INSERT, .flags = BF_IGNORE_OTHER_COMMANDS | BF_FALLTHROUGH},
    {MODE_NORMAL, 'A',         .func = jumpToEndOfLine, .arg.i = 1},

    {MODE_NORMAL, 'o',         .func = jumpToEndOfLine, .arg.i =  1, .flags = BF_NO_ABORT_ON_MATCH},
    {MODE_NORMAL, 'O',         .func = jumpToEndOfLine, .arg.i = -1, .flags = BF_FALLTHROUGH},
    {MODE_NORMAL, 'o',         .func = setMode,         .arg.m = MODE_INSERT, .flags = BF_FALLTHROUGH},
    {MODE_NORMAL, 'o',         .func = appendChar,      .arg.c = '\n'},

    {MODE_NORMAL, 'J',         .func = jumpToEndOfLine, .arg.i = 1, .flags = BF_NO_ABORT_ON_MATCH},
    {MODE_NORMAL, 'J',         .func = deleteChar,      .arg.i = 1},

    {MODE_NORMAL, 'r',         .func = replaceChar,      .flags = BF_USE_EVENT_CHAR_AS_ARG},
    {MODE_NORMAL, 'R',         .func = setMode,          .arg = {.i = MODE_INSERT | MODE_REPLACE}},
    {MODE_REPLACE, ANY_CHAR,   .func = replaceChar,      .flags = BF_USE_EVENT_CHAR_AS_ARG},

    {MODE_INSERT, ANY_CHAR,    .func = appendChar,      .flags = BF_USE_EVENT_CHAR_AS_ARG },
};

static StyleScheme defaultStyleScheme[] = {
    {LABEL_MACRO,   COLOR_MAGENTA},
    {LABEL_KEYWORD, COLOR_LIME_GREEN},
    {LABEL_LINENO,  COLOR_YELLOW},
    {LABEL_STRING,  COLOR_BROWN},
    {LABEL_INCLUDE, {COLOR_BROWN, .style = STYLE_UNDERLINE}},
    {LABEL_LINK,    {COLOR_BLUE, .style = STYLE_UNDERLINE}},
    {LABEL_LITERAL, COLOR_RED},
    {LABEL_STATUS,  {.fg = COLOR_BLACK, .bg = COLOR_WHITE}},
};

STATIC_BUFFER(defaultBindingsBuffer, defaultBindings);
STATIC_BUFFER(exBindingsBuffer, exBindings);
STATIC_BUFFER(defaultStyleBuffer, defaultStyleScheme);

void addDefaults(Context* context) {
    addStateBuffer(context, AUTOCOMMAND_INDEX, getDefaultFileTypeAutoCommands());
    addStateBuffer(context, AUTOCOMMAND_INDEX, getLabelAutocommands());
    addStateBuffer(context, AUTOCOMMAND_INDEX, getUndoAutocommands());
    addStateBuffer(context, AUTOCOMMAND_INDEX, getRegistersAutocommands());
    addStateBuffer(context, AUTOCOMMAND_INDEX, getOptionAutocommands());

    addStateBuffer(context, BINDING_INDEX, &defaultBindingsBuffer);
    addStateBuffer(context, BINDING_INDEX, &exBindingsBuffer);
    addStateBuffer(context, COLOR_SCHEME_INDEX, &defaultStyleBuffer);
    addStateBuffer(context, COMMAND_INDEX, getDefaultCommands());
    addStateBuffer(context, DOCK_INDEX, getDefaultDocks());
    addStateBuffer(context, RULE_INDEX, getDefaultRules());
}
