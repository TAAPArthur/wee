#ifndef DOCK_H
#define DOCK_H

#include "buffer.h"
#include "settings.h"
#include "typedefs.h"

typedef enum DockType {
    DOCK_LEFT,
    DOCK_RIGHT,
    DOCK_TOP,
    DOCK_BOTTOM,
    NUM_DOCK_TYPES
} DockType;

typedef struct DockInfo {
    unsigned int lineno;
} DockInfo;

typedef struct Dock {
    DockType dockType;
    SettingsIndex settingIndex;
    int(*func)(const Window* win, const ViewSettings * settings, const DockInfo* info, char* str, int maxLen, Buffer* labelRanges);
} Dock;

int getDockText(const Window* win, const ViewSettings * settings, const DockInfo* info, const Dock* dock, char* buffer, int bufferLen, Buffer* labelRanges);

void drawDocks(const Window* win, const DockInfo* info, DockType type, int x, int y);

int isDockEnabledForType(const ViewSettings* settings, const Dock* dock, DockType type);

const Buffer* getDefaultDocks();

DockType getEffectiveDockType(const ViewSettings* settings, const Dock* dock);
#endif

