#ifndef COMMANDS_H
#define COMMANDS_H

#include "events.h"
#include "typedefs.h"

#define ALT_FLAG      (1<<0)
#define ALL_FLAG      (1<<1)
#define ADDR1_FLAG    (1<<2)
#define ADDR2_FLAG    (1<<3)
#define USE_ARG_FLAG  (1<<4)
#define FLAGS_FLAG    (1<<6)
#define COUNT_FLAG    (1<<6)

typedef struct Command {
    const char* name;
    int (*func)(Context*, Arg);
    int flags;
    Arg arg;
} Command;

int runCommand(Context* context, const char* cmd);

const Buffer* getDefaultCommands();


#endif
