#ifndef UTILITY_H
#define UTILITY_H

#include "typedefs.h"
#include "window.h"


/**
 * Returns the line number of the target where
 * target is in one of the buffers of win
 * or current buffer
 */
int getLineNumberOfLine(const Window* win, const char* target);

/*
 * Returns the line number of cursor in the window
 */
int getCurrentLineNumber(const Window* window);

/**
 * Returns the line number of the last line in the buffer
 */
int getLastLineNumber(const Window* window);

//int getColNumberOfLine(const DataRange buffers[], int numBuffers, const char* pos);

/*
 * Returns the offset pos is in the current line
 */
int getOffsetIntoLine(const Window* win, const char* pos);

unsigned long getCursorCol(const Window* win);
const char* getCursorLine(const Window* win, unsigned long* len);
const char* getLine(const Window* win, const char* pos, unsigned long* len);

unsigned long clampToCursorLine(const Window* win, unsigned long offset);
unsigned long clampToLine(const Window* win, const char* pos, unsigned long offset);


/**
 *
 * Returns the next/prev line after the cursor line or NULL
 */
const char* getNextLineAfterCursor(const Window*win, int dir);

int jumpToLine(Window* win, unsigned targetLineNumber);
const char* getNthLine(const Window* win, unsigned int targetLineNumber);

int jumpToCharInCurrentLineOfWindow(Window* win, int dir, char c);
const char* getNextCharInCurrentLineOfWindow(Window* win, int dir, char c);



typedef struct CountCharResult {
    unsigned short xDelta;
    unsigned char yDelta;
    char scratchSpace[7];
    unsigned long len;
    unsigned long bytesUsed;
} CountCharResult;

/*
 * This function determines how many characters of the input buffer p are need to display text on the screen and also
 * how wide that text is and if it causes a line break;
 *
 * The return value will be one of
 * NULL - which indicates that nothing should be displayed for this char
 * p - in which case use result->bytesUsed bytes of p to display text
 * result->scratchSpace - the first char indicates the len and next len chars should be written to the screen. If there aren't enough
 * compared to xDelta, cycle through the list. If this cse result->len will equal 0
 *
 */
const char* getNumCharsUsed(const char * p, const char* end, const ViewSettings* settings, CountCharResult * result);

#endif
