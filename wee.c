#include "logger.h"
#include "setup.h"
#include "system.h"

void onStartup(Context* context);

int main (int argc, char *argv[]) {
#ifndef DEBUG
   setLoggingFd(openFile("output.log", OPEN_IO_TRUNCATE | OPEN_IO_CREATE | OPEN_IO_WRITE));
#endif
    extern Context* globalContext;
    return parseArgsAndRuninitWee(argc, argv, globalContext, onStartup);
}
