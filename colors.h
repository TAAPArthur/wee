#ifndef COLORS_H
#define COLORS_H

#include "labels.h"
#include <stdint.h>

#define COLOR_BLACK       0x1000000

#define COLOR_BLUE        0x00000FF
#define COLOR_NAVY_BLUE   0x0000077

#define COLOR_LIME_GREEN  0x000FF00
#define COLOR_GREEN       0x0007700

#define COLOR_CYAN        0x000FFFF
#define COLOR_TEAL        0x0007777

#define COLOR_RED         0x0FF0000
#define COLOR_MAROON      0x0770000

#define COLOR_MAGENTA     0x0FF00FF
#define COLOR_PURPLE      0x0770077

#define COLOR_YELLOW      0x0FFFF00
#define COLOR_BROWN       0x0777700

#define COLOR_WHITE       0x0FFFFFF
#define COLOR_LIGHT_GRAY  0x0C0C0C0
#define COLOR_GRAY        0x0777777

typedef uint32_t color_t;

typedef enum Style {
    STYLE_DEFAULT      = 0,
    STYLE_BOLD         = 1 << 0,
    STYLE_FAINT        = 1 << 1,
    STYLE_ITALIC       = 1 << 2,
    STYLE_UNDERLINE    = 1 << 3,
    STYLE_BLINK        = 1 << 4,
    STYLE_REVERSE      = 1 << 6,
    STYLE_HIDDEN       = 1 << 7,
    STYLE_STRIKETHOUGH = 1 << 8,
    NUM_STYLES
} Style;

typedef struct {
    color_t fg;
    color_t bg;
    Style style;
} StyleTuple;

typedef struct {
    color_t fg;
    color_t bg;
    Style style;
} RenderTuple;

typedef struct {
    Label label;
    StyleTuple tuple;
} StyleScheme;

void copyStyle(RenderTuple * dest, const StyleTuple* src);

int get8BitColor(color_t color);
#endif
