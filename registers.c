#include "autocommands.h"
#include "buffer.h"
#include "logger.h"
#include "registers.h"
#include "view.h"
#include <assert.h>
#include <stddef.h>

// There are 10 types of register in vim. We don't support all of them
typedef struct Registers {
    // 1. Unnamed register "" - points to the last used register (default)
    Buffer* unamedRegister;

    // 2. Numbered registers "0 to "9
    // Number register 0 - Store the text from the most recent yank command
    // Number register 1 - 9 -  stores the most recent delete/replace text.
    Buffer numberRegister0;
    Buffer numberRegisters[9];
    // The numberRegister is a ring buffer where startingIndex has the most recent
    // change and startingIndex - 1 (with wrapping) holds the least recent
    unsigned char startingIndex;
    // 3. Small delete register "- There is no small delete register as we don't care if a deletion went across a line boundary

    // 4. Named registers "a to "z or "A to "Z
    // Only used when explicitly requested
    Buffer namedRegisters[26];

    // 5. Read-only registers ":, ". and "%
    // ". Last inserted text; Might be supported in the future
    // "% - name of the current file; Might be supported in the future. Is this actually useful?
    // ": - most recently executed command; Not supported
    // These are fake and don't require explicit memory here

    // 6. Alternate file register "# isn't supported
    // 7. Expression register "= isn't supported
    // 8. Selection and drop registers "*, "+ and "~ as GUI clipboard is a non feature

    // 9. Black hole register "_ is fake and doesn't require explicit memory here
    Buffer blackHoleRegister;
    // 10. Last search pattern register	"/ TODO

} RegisterData;

static RegisterData _registerData = {};
static RegisterData* registerData = &_registerData;

void clearAllRegisterData() {
    freeBufferData(&registerData->numberRegister0);
    for (int i = 0; i < LEN(registerData->numberRegisters); i++) {
        freeBufferData(registerData->numberRegisters + i);
    }
    for (int i = 0; i < LEN(registerData->namedRegisters); i++) {
        freeBufferData(registerData->namedRegisters + i);
    }
}

Buffer* getRegisterBuffer(RegisterData* registerData, Register reg, int* append) {
    *append = 0;
    if (reg == REGISTER_UNAMED) {
        TRACE("REturning default register %p\n", registerData->unamedRegister);
        return registerData->unamedRegister;
    } else if (reg == REGISTER_0) {
        return &registerData->numberRegister0;
    } else if (REGISTER_1 <= reg && reg < REGISTER_1 + LEN(registerData->numberRegisters)) {
        int i =  (REGISTER_1 - reg + registerData->startingIndex) % LEN(registerData->numberRegisters);
        return registerData->numberRegisters + (REGISTER_1 - reg + registerData->startingIndex) % LEN(registerData->numberRegisters);
    } else if (REGISTER_A <= reg && reg < REGISTER_A + LEN(registerData->namedRegisters)) {
        return registerData->namedRegisters + reg - REGISTER_A;
    } else if (REGISTER_A_APPEND <= reg && reg < REGISTER_A_APPEND + LEN(registerData->namedRegisters)) {
        *append = 1;
        return registerData->namedRegisters + reg - REGISTER_A_APPEND ;
    } else {
        TRACE("HERE '%c'\n", reg);
        assert(reg == REGSITER_BLACKHOLE);
        return &registerData->blackHoleRegister;
    }
}

static int insertIntoRegister(RegisterData* registerData, Register reg, Register defaultRegister, const char* str, unsigned long len) {
    int append;
    reg = reg != REGISTER_UNAMED ? reg : defaultRegister;
    Buffer* dest = getRegisterBuffer(registerData, reg, &append);
    assert(dest);
    if (!append)
        freeBufferData(dest);
    if (appendToBuffer(dest, str, len) == -1)
        return -1;
    registerData->unamedRegister = dest;
    return 0;
}

int yankText(Register reg, const char* str, unsigned long len) {
    return insertIntoRegister(registerData, reg, REGISTER_0, str, len);
}

int storeDeletedText(Register reg, const char* str, unsigned long len) {
    registerData->startingIndex++;
    if (insertIntoRegister(registerData, reg, REGISTER_1, str, len) == -1) {
        registerData->startingIndex--;
        return -1;
    }
    return 0;
}

const Buffer* getRegisterBufferConst(Register reg) {
    int append;
    return getRegisterBuffer(registerData, reg, &append);
}

static int storeDeletedTextAutocommand(AutoCommandType type, const ViewAutoParams * params) {
    return storeDeletedText(REGISTER_DEFAULT, getCurrentBuffer(params->view)->readOnlyData + params->bufferParams.offset, params->bufferParams.len);
}

static AutocommandCallback registersCallbacks[] = {
    {AUTO_CMD_BUFFER_PRE_DELETE_CHAR, storeDeletedTextAutocommand},
};

STATIC_BUFFER(registersCallbacksBuffer, registersCallbacks);

const Buffer* getRegistersAutocommands() {
    return &registersCallbacksBuffer;
}
