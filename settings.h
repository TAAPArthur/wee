#ifndef SETTINGS_H
#define SETTINGS_H
#include "settings_list.h"


typedef enum {
    SETTING_BOOL,
    SETTING_CHAR,
    SETTING_INT,
    SETTING_LONG,
    SETTING_ENUM,
    SETTING_STR,
    SETTING_ENV_STR,
    SETTING_ENV_INT,
    SETTING_POINTER,
    NUM_SETTING_TYPES,
} SettingType;


typedef char  SETTING_BOOL_t[0];
typedef char  SETTING_CHAR_t;
typedef int SETTING_INT_t;
typedef long SETTING_LONG_t;
typedef int SETTING_ENUM_t;
typedef const char* SETTING_STR_t;
typedef SETTING_STR_t SETTING_ENV_STR_t[0];
typedef SETTING_INT_t SETTING_ENV_INT_t[0];
typedef const struct void* SETTING_POINTER_t;

#define INIT_SETTING(T, N, ...) N ## _index,
#define ALIAS_SETTING(N, S, M) N ## _index = M ## _index,
typedef enum {
    SETTINGS_LIST
    NUM_SETTINGS,
    ALIAS_LIST
} SettingsIndex;
#undef INIT_SETTING
#undef ALIAS_SETTING

typedef struct Settings Settings;

// Allocates a Settings. If src is non-null it will be copied to the new setting.
// Otherwise a setting will be created with default values
Settings* allocGlobalSettings();
Settings* allocWindowSettings(const Settings* src, Settings* const* viewSettings);
Settings* allocViewSettings(const Settings* src);


// Free memory allocated by settings
void freeSettings(Settings* settings);

/*
 * Sets settings to their default values
 */
void setDefaultSettings(Settings * settings);

/**
 * Setting name to SettingIndex conversion
 * returns NUM_SETTINGS if str is unknown
 */
SettingsIndex getSettingsIndexFromString(const char* str, int len);

/*
 * Marks a setting as enabled/disabled. Note this is probably only useful for SETTING_STR types
 * For SETTING_BOOL it is the same as calling setNumericSetting
 */
int setEnabled(Settings* settings, SettingsIndex index, int set);
/*
 * Test if a function is enabled. Note this is probably only useful for SETTING_STR types
 * For SETTING_BOOL it is the same as calling getNumericSetting
 */
char isEnabled(const Settings* settings, SettingsIndex index);

/**
 * Effectively calls setEnabled(settings, index, 0) for all SettingsIndexes
 */
void disableAllSettings(Settings * settings);

/**
 * Many setting have an alternative function. These helpers are use to test and toggle it
 */
char isAltVersion(const Settings* settings, SettingsIndex index);
int setAltVersion(Settings* settings, SettingsIndex index, int set);

// Prevents the enabled/alt status of the setting from being modified until the value is cleared
int setFixed(Settings* settings, SettingsIndex index, int set);
int setEnabledForced(Settings* settings, SettingsIndex index, int set);

/**
 * Settings the given setting from the given non-null value
 */
int setSettings(Settings* settings, SettingsIndex index, const char* s);

/**
 * Returns the value of the specified setting assume it is a string
 */
SETTING_STR_t getStringSettings(const Settings* settings, SettingsIndex index);

/**
 * Returns the value of the specified setting assume it is numeric
 */
long getNumericSettings(const Settings* settings, SettingsIndex index);

static inline long getNumericSettingsOrDefault(const Settings* settings, SettingsIndex index, long defaultValue) {
    return isEnabled(settings, index) && getNumericSettings(settings, index) ? getNumericSettings(settings, index) : defaultValue;
}

/**
 * Sets the value of the specified setting to i assuming it is numeric
 */
void setNumericSettings(Settings* settings, SettingsIndex index, int i);

int setStringSettings(Settings* settings, SettingsIndex index, const char* s);

/**
 * Helper to OR flag to a numeric setting
 */
static inline void addSettingsFlag(Settings* settings, SettingsIndex index, int flag) {
    setNumericSettings(settings, index, getNumericSettings(settings, index) | flag);
}

static inline void incrementSettings(Settings* settings, SettingsIndex index) {
    setNumericSettings(settings, index, getNumericSettings(settings, index) + 1);
}

void setPointerSettings(Settings* settings, SettingsIndex index, const void* buffer);
const void* getPointerSettings(Settings* settings, SettingsIndex index);

/**
 * Return the setting name
 */
const char* getSettingsName(SettingsIndex index);
/**
 * Returns how the setting at the given index will be interpreted
 */
SettingType getSettingsType(SettingsIndex index);

int parseAndSetSetting(Settings* settings, const char* s) ;

int isNumericSetting(SettingsIndex index);

void setupBatchMode(Settings* settings);

#endif
