#include "autocommands.h"
#include "buffer.h"
#include "filetypes.h"
#include "logger.h"
#include "memory.h"
#include "settings.h"
#include "system.h"
#include "undo.h"
#include "view.h"
#include <assert.h>


#define RUN_AUTOCMD_RETURN(VIEW, TYPE, RET, ...) do { if (runAutoCommands(getPointerSettings(getSettings((View*)VIEW), autocommand_index), TYPE, (AutocommandParams){.viewParams = {.view = VIEW, __VA_ARGS__}})) return RET; } while(0)
#define RUN_AUTOCMD_RETURN_VOID(VIEW, TYPE, ...) RUN_AUTOCMD_RETURN(VIEW, TYPE, , __VA_ARGS__)
#define RUN_AUTOCMD(VIEW, TYPE, ...) RUN_AUTOCMD_RETURN(VIEW, TYPE, -1, __VA_ARGS__)

#define MAX_WIN_PER_VIEW 16

/**
 * Holds buffers
 * A view is associated with at most one file.
 */
typedef struct View {
    // User defined name of the View
    char* name;
    const Buffer* rules;
    Settings* settings;
    // Stores the current state of the data
    Buffer current;
    Buffer stateBuffers[NUM_VIEW_STATE];
    Buffer offsets;
    unsigned char updateCounter;
    unsigned int modifiedCount;
} View;

long getViewId(const View* view) {
    return (long) view;
}

int getUpdateCounter(const View* view) {
    return view->updateCounter;
}

static void recordModification(View* view) {
    view->modifiedCount;
    setEnabled(getSettings(view), modified_index, 1);
}

unsigned long getModifiedCounter(const View* view) {
    return view->modifiedCount;
}

View* allocView(Settings* settings) {
    View * view = wee_malloc(ALLOCATION_VIEW, sizeof(View));
    *view = (View) { .settings = allocViewSettings(settings)};
    return view;
}

void freeView(View* view) {
    assert(view);
    for (int i = 0; i < NUM_VIEW_STATE; i++) {
        switch(i) {
            case UNDO_INDEX:
                freeUndoState(view->stateBuffers + i);
                break;
            default:
                freeBufferData(view->stateBuffers + i);
        }
    }

    freeSettings(view->settings);
    freeBufferData(&view->current);
    if (view->name)
        wee_free(view->name);
    wee_free(view);
}

int reserveView(View* view, int n, unsigned long offset[n]) {
    long end = view->offsets.size / sizeof(long*);
    if (appendToBuffer(&view->offsets, NULL, sizeof(long*) * n) == -1) {
        return -1;
    }
    for (int i = 0; i < n; i++) {
        view->offsets.p[end + i]  = offset + i;
    }
    return 0;
}

void releaseView(View* view, int n, unsigned long offset[n]) {
    for (int i = n  - 1; i >= 0; i--) {
        removeElement(&view->offsets, offset + i);
    }
}

static int getUpdatedOffset(int pos, int delta, int offset) {
    if (delta < 0 && pos < offset) {
        return MAX( pos - offset, delta);
    } else if (delta > 0 && pos <= offset)
        return delta;
    return 0;
}

static void updateCursorPositionAfterViewChange(View* view, int pos, int delta) {
    view->updateCounter++;
    for (int i = 0; i <getLen(&view->offsets); i++) {
        unsigned long * offset = getElementAt(&view->offsets, i);
        int adjustment = getUpdatedOffset(pos, delta, *offset);
        if (adjustment) {
            *offset = *offset + adjustment;
        }
    }
}

Buffer* getViewStateBuffer(View* view, ViewStateIndex index) {
    return view->stateBuffers + index;
}

const Buffer* getViewStateBufferConst(const View* view, ViewStateIndex index) {
    return view->stateBuffers + index;
}

const Buffer* getRulesBuffer(const View * view) {
    return view->rules;
}

void setRulesBuffer(View * view, const Buffer * rulesBuffer) {
    view->rules = rulesBuffer;
}

const Buffer* getCurrentBuffer(const View* view) {
    assert(view);
    return &view->current;
}

int deleteFromView(View* view, int offset, int len) {
    len = MIN(getCurrentBuffer(view)->size, offset + len) - offset;
    RUN_AUTOCMD(view, AUTO_CMD_BUFFER_PRE_DELETE_CHAR, .bufferParams = {.offset = offset, .len = len});
    int ret = removeFromBuffer(&view->current, offset, len);
    if (ret == -1)
        return  ret;
    RUN_AUTOCMD(view, AUTO_CMD_BUFFER_POST_CHANGE_CHAR, .bufferParams = {.offset = offset, .len = len});
    recordModification(view);
    updateCursorPositionAfterViewChange(view, offset, -len);
    return ret;
}

int insertInView(View* view, const char* data, unsigned int offset, int len) {
    int ret = insertInBuffer(&view->current, data, offset, len);
    if (ret == -1 )
        return -1;
    RUN_AUTOCMD(view, AUTO_CMD_BUFFER_POST_APPEND_CHAR, .bufferParams = {.offset = offset, .len = len, .data = data});
    RUN_AUTOCMD(view, AUTO_CMD_BUFFER_POST_CHANGE_CHAR, .bufferParams = {.offset = offset, .len = len});

    recordModification(view);
    updateCursorPositionAfterViewChange(view, offset, len);
    return ret;
}

int appendToView(View* view, const char* data, int len) {
    return insertInView(view, data, view->current.size, len);
}

int replaceInView(View* view, const char* data, unsigned int offset, int len, int deletionLen) {
    deletionLen = MIN(getCurrentBuffer(view)->size, offset + deletionLen) - offset;
    RUN_AUTOCMD(view, AUTO_CMD_BUFFER_PRE_DELETE_CHAR, .bufferParams = {.offset = offset, .len = deletionLen});
    int ret = replaceInBuffer(&view->current, data, offset, len, deletionLen);
    if (ret == -1)
        return -1;
    RUN_AUTOCMD(view, AUTO_CMD_BUFFER_POST_APPEND_CHAR, .bufferParams = {.offset = offset, .len = len, .data = data});
    RUN_AUTOCMD(view, AUTO_CMD_BUFFER_POST_CHANGE_CHAR, .bufferParams = {.offset = offset, .len = len});

    long delta = len - deletionLen;
    recordModification(view);
    if (len >= deletionLen) {
        updateCursorPositionAfterViewChange(view, offset + deletionLen, delta);
    } else {
        updateCursorPositionAfterViewChange(view, offset, delta);
    }
    return 0;
}

Settings* getSettings(const View* view) {
    return view->settings;
}

void setName(View* view, const char* name) {
    if (view->name)
        wee_free(view->name);
    view->name = wee_strdup(name);
    RUN_AUTOCMD_RETURN_VOID(view, AUTO_CMD_SET_NAME);
}

const char * getName(const View* view) {
    return view->name;
}

static int onReadComplete(CompletionStatus* status) {
    recordModification(status->userData);
    RUN_AUTOCMD(status->userData, AUTO_CMD_READ_END, .fileParams = {.ret = status->exitStatus});
    RUN_AUTOCMD(status->userData, AUTO_CMD_BUFFER_POST_CHANGE_CHAR);
    return 0;
}

static int onReadProgress(ProgressStatus* status) {
    updateCursorPositionAfterViewChange(status->userData, status->offset, status->len);
    return 0;
}

int appendCmdIntoView(View* view, const char* cmd) {
    RUN_AUTOCMD(view, AUTO_CMD_READ_CMD_START, .fileParams = {.cmd = cmd});
    readFromCmd(cmd, &view->current, view->current.size, onReadProgress, onReadComplete, view);
    return 0;
}

static struct {
    SettingsIndex index;
    char flags;
} settingsIndexToOpenFlag[] = {
    {nonblocking_index,   OPEN_IO_READ_WRITE | OPEN_IO_NON_BLOCKING},
    {closeonexec_index,   OPEN_IO_READ_WRITE | OPEN_IO_CLOSE_ON_EXEC},
    {autocreate_index,    OPEN_IO_WRITE      | OPEN_IO_CREATE},
    {autocreatedir_index, OPEN_IO_WRITE      | OPEN_IO_CREATE_DIR},
    {opentrunc_index,     OPEN_IO_WRITE      | OPEN_IO_TRUNCATE},
    {appendonly_index,    OPEN_IO_WRITE      | OPEN_IO_APPEND},
};

static int getOpenFlagsFromSettings(int base, const Settings* settings) {
    assert((base & ~OPEN_IO_READ_WRITE) == 0);
    for (int i = 0; i < LEN(settingsIndexToOpenFlag); i++) {
        if (isEnabled(settings, settingsIndexToOpenFlag[i].index)) {
            if (base & settingsIndexToOpenFlag[i].flags || isAltVersion(settings, settingsIndexToOpenFlag[i].index)) {
                base |= settingsIndexToOpenFlag[i].flags & ~OPEN_IO_READ_WRITE;
            }
        }
    }
    return base;
}

int openFDForView(const View* view, const char* name, int baseFlag) {
    return openFile(name, getOpenFlagsFromSettings(baseFlag, view->settings));
}

int appendFileIntoView(View* view, const char* name) {
    RUN_AUTOCMD(view, AUTO_CMD_READ_FILE_START, .fileParams = {.fileName = name});
    int fd = openFDForView(view, name, OPEN_IO_READ);
    if (fd == -1) {
        return -1;
    }
    readDataFromFd(fd, &view->current, view->current.size, onReadProgress, onReadComplete, view);
    return 0;
}

static int onSaveComplete(CompletionStatus* status) {
    setEnabled(getSettings(status->userData), modified_index, 0);
    RUN_AUTOCMD(status->userData, AUTO_CMD_SAVE, .fileParams = {.ret = status->exitStatus});
    return 0;
}

int saveView(View* view, const char* name) {
    if(!name || !name[0])
        name = view->name;
    RUN_AUTOCMD(view, AUTO_CMD_WRITE_FILE_START, .fileParams = {.fileName = name});
    int fd = openFDForView(view, name, OPEN_IO_WRITE);
    if (fd == -1) {
        return -1;
    }
    return writeDataToFd(fd, &view->current, 0, view->current.size, NULL, onSaveComplete, view);
}
