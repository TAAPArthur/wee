#ifndef UNDO_H
#define UNDO_H

#include "buffer.h"
#include "typedefs.h"

int undoLast(View* view);
int redoLast(View* view);

const Buffer* getUndoAutocommands();
void freeUndoState(Buffer* state);
#endif
