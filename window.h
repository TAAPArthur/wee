#ifndef WINDOW_H
#define WINDOW_H

#include "labels.h"
#include "typedefs.h"

/**
 * Allocates a new Window object
 */
Window* allocWindow(const WindowSettings* windowSettings);

/*
 * Frees resources associated with win
 * Returns the parent or if NULL, child window
 */
void freeWindow(Window* win);

WindowSettings* getWindowSettings(const Window* win);

/**
 * Sets the current view for the window
 */
void setViewForWindow(Window* window, View* view);

int setSubViewForWindow(Window* window, long start, long end);

/**
 * Returns the view associated with the window or NULL
 */
long getViewIdOfWindow(const Window*);

// TODO turn into a settings
void setAutoAudjustViewportForCursor(Window* win, int enable);
int isAutoAudjustViewportForCursor(Window* win);


/*
 * Sets the cursor to the given position.
 *
 * pos must be in the currentBuffer of win or NULL if win is empty
 *
 * This is a wrapper around setCursorOffset
 */
void setCursorPosition(Window* win, const char* pos);

/*
 * Sets the cursor to the given offset. The offset is clamped the valid offsets within the view
 */
void setCursorOffset(Window* win, unsigned long offset);

/*
 * Returns a pointer the cursor's position in the buffer
 * This is a wrapper around getCursorOffset
 */
const char* getCursorPosition(const Window* window);

/*
 * Returns the offset of the cursor position in the window's main buffer
 * This means that auxiliary buffers like the insertionBuffer are ignored
 */
unsigned long getCursorOffset(const Window* win);
void setEffectiveCursorOffset(Window* win, unsigned long offset);

// Shifts the cursor position over by delta
void updateCursorPosition(Window* win, int delta);

/**
 * Returns the offset last stored via setStartOfViewportEffectiveOffset. The default value is the lowest valid offset for the window
 */
const char * getStartOfViewport(const Window* win, int * col);

/**
 * Stores the offset so it can be retrieved with getStartOfViewportEffectiveOffset
 */
void setStartOfViewport(Window* win, const char * c, int col);




/*
 * Returns the last saved cursor col given the underlying view hasn't modified the current cursor position.
 * If it has then, this method returns -1
 */
long getSavedCursorCol(Window* win);
/**
 * Stores the cursor position so it can be retrieved with getSavedCursorCol
 */
void setSavedCursorCol(Window* win, long col);

/**
 * Merge the insertion buffer to the current buffer
 */
int commitInsertion(Window* win);

/**
 * Appends data to the end of the insertion buffer
 */
int appendToInsertionBuffer(Window* win, const char* data, int len);
/**
 * Inserts data directly into the view and completely bypasses the insertion buffer
 */
int insertAtCurrentPosition(Window* win, const char* data, int len);
/*
 * Removes data from the end of the insertion buffer.
 * If the insertion buffer was already or becomes empty the remaining data is deleted from the current buffer
 *
 */
int removeAtCurrentPosition(Window* win, int len);
/**
 * Replaces the data at the given cursor position.
 * This method is effectively the same as calling
 * removeAtCurrentPosition(win, deletionLen) followed by
 *insertAtCurrentPosition(win, data, len)
 */
int replaceAtCurrentPosition(Window* win, const char* data, int len, int deletionLen);

int appendEffectiveRangeToInsertionBuffer(Window * win, unsigned long startOffset, unsigned long endOffset);

/**
 * Returns the buffer pending insertion into the window
 */
const Buffer * getInsertionBuffer(const Window* win);

/**
 * run - controls where str is executed as a shell command or opened as a file name
 */
int readIntoCurrentPosition(Window* win, int run, const char* str);

/*
 * Saves the underlying view to disk as name.
 * if name is NULL, the name of the view is used
 */
int saveWindow(Window* win, const char* name);

/**
 * Returns the name of the window
 */
const char* getWindowName(const Window* win);

/**
 * Returns the offset if all the window's buffers were combined
 */
unsigned long getEffectiveOffsetOfPosition(const Window* win, const char* pos);
/*
 * Inverse of getEffectiveOffsetOfPosition
 */
const char* getPositionFromEffectiveOffset(const Window* win, unsigned long effectiveOffset);

/*
 * Returns the effective offset of the last position the cursor can be set to
 */
const char* getEffectiveEndPosition(const Window* win);

/**
 * Returns the total amount of data contained in the window
 */
unsigned long getWindowDataLen(const Window* win);

int getSectionBounds(const Window* win, const char* pos, const char** start, const char** end, const char** next);

int getLabelsAtEffectiveOffset(const Window* win, unsigned long effectiveOffset, int n, LabelRange ranges[n]);
int getLabelsAtPosition(const Window* win, const char* pos, int n, LabelRange ranges[n]);
Label getLabelAtPosition(const Window* win, const char* pos);

const Buffer* getSearchBuffer(const Window* win);

int isCursorAtStart(const Window* win);
#endif
