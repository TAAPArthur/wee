#include "wee_string.h"

#include <assert.h>
#include <errno.h>
#include <stddef.h>
#include <stdlib.h>

const char* findChar(const char* pos, const char* end, char needle, int dir) {
    assert(!dir || dir < 0 && end <= pos || dir > 0 && pos <= end);
    while (pos != end) {
        if(*pos == needle)
            return pos;
        pos += dir;
    }
    return NULL;
}

int countInstancesOfChar(const char* start, const char*end, char target) {
    int count = 0;
    while (start < end) {
        if (*start == target)
            count++;
        start++;
    }
    return count;
}

const char* parseAsNumber(const char* start, long* num) {
    errno = 0;
    char* s;
    long delta = strtol(start, &s, 0);
    *num = delta;
    if (errno) {
        if (*start == '+' || *start == '-') {
            *num = (*start == '+' ? 1 : -1);
            return start + 1;
        }
    }
    return s;
}
