#include "context.h"
#include "events.h"
#include "functions.h"
#include "logger.h"
#include "render.h"
#include "screen.h"
#include "settings.h"
#include "system.h"
#include "wee_string.h"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

extern Context context;

void onStartup(Context* context);

static void usage() {
    printf("ex [-rR] [-e|-s|-v] [-c command] [-t tagstring] [-w size] [file...]");
}

static const char* optstring = "erRsvc:t:w:h";

static int parseArgs(Context* context, Settings* settings, int argc, char *argv[]) {
    char opt;
    long num;
    while ((opt = getopt(argc, argv, optstring)) != -1) {
        switch (opt) {
            case 'R':
                setEnabled(settings, readonly_index, 1);
                break;
            case 'c':
                // handled in parseArgsPost
                break;
            case 'e':
                focusCommandWindow(context);
                break;
            case 'h':
                usage();
                return 0;
            case 'r':
                // handled in parseArgsPost
                break;
            case 's':
                setupBatchMode(settings);
                break;
            case 't':
                // handled in parseArgsPost
                break;
            case 'v':
                restoreFocus(context);
                break;
            case 'w':
                setEnabled(settings, window_index, 1);
                parseAsNumber(optarg, &num);
                setNumericSettings(settings, window_index, num);
                break;
            default:
                usage();
                return -1;
        }
    }
    return optind;
}

static void parseArgsPost(Context* context, int argc, char *argv[]) {
    optind = 1;
    char opt;
    while ((opt = getopt(argc, argv, optstring)) != -1) {
        switch (opt) {
            case 'c':
                // TODO ex command
                break;
            case 't':
                // TODO goto tagstring
                break;
            case 'e':
            case 's':
                gotoLine(context, (Arg){.i = -1});
        }
    }
}

int parseArgsAndRuninitWee(int argc, char *argv[], Context* context, void onStartup(Context*)) {
    initContext(context);
    int argIndex = parseArgs(context, getGlobalSettings(context), argc, argv);
    if (argIndex == 0 || argIndex == -1) {
        shutdownContext(context);
        return !!argIndex;
    }
    if (!isTTY()) {
        TRACE("Enabling batch mode\n");
        setupBatchMode(getGlobalSettings(context));
    } else {
        enableTermboxImpl();
    }
    onStartup(context);
    TRACE("Opening %d files\n", argc - argIndex);
    if (argc == argIndex) {
            openFileInNewWindow(context, (Arg){.s = NULL});
    } else {
        for(int i = argIndex; i < argc; i++) {
            openFileInNewWindow(context, (Arg){.s = argv[i]});
        }
    }
    parseArgsPost(context, argc, argv);

    if (getRenderBackend() && getRenderBackend()->init)
        getRenderBackend()->init(context);
    int ret = eventLoop(context);
    if (getRenderBackend() && getRenderBackend()->shutdown)
        getRenderBackend()->shutdown(context);
    shutdownContext(context);
    return ret;
}
