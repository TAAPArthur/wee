#ifndef VIEW_H
#define VIEW_H

#include "buffer.h"
#include "filetypes.h"
#include "typedefs.h"

typedef enum {
    UNDO_INDEX,
    LABEL_INDEX,
    NUM_VIEW_STATE,
} ViewStateIndex;

typedef struct CursorInfo {
    // The starting position of cursor
    unsigned long cursorOffset;
    // The column of the cursor
    unsigned long cursorCol;
} CursorInfo;


/**
 * Allocates a new view with the specified settings
 */
View* allocView(ViewSettings* settings);

/**
 * Frees resources owned by the view
 */
void freeView(View* view);

int reserveView(View* view, int n, unsigned long offset[n]);

void releaseView(View* view, int n, unsigned long offset[n]);

/*
 * Set the name of the view
 */
void setName(View* view, const char* name);
/**
 * Returns the previously set name of the view
 */
const char* getName(const View* view);

/* Returns the settings associated with this view */
ViewSettings* getSettings(const View* view);

/**
 * Returns the buffer indicate by index containing external state
 * associated with the view
 */
Buffer* getViewStateBuffer(View* view, ViewStateIndex index);
const Buffer* getViewStateBufferConst(const View* view, ViewStateIndex index);

/**
 * Returns the current buffer of the view
 */
const Buffer * getCurrentBuffer(const View* view);

/**
 * Runs cmd and inserts the contents at the given offset
 */
int appendCmdIntoView(View* view, const char* cmd);

/**
 * Opens the file denoted by name and inserts the contents at the given offset
 */
int appendFileIntoView(View* view, const char* name);
/*
 * Save the contents of view to the file denoted by name.
 * If name is null or empty, the name of the window/view will be used
 */
int saveView(View* window, const char* name);

int deleteFromView(View* view, int offset, int len);
int appendToView(View* view, const char* data, int len);
int insertInView(View* view, const char* data, unsigned int offset, int len);
int replaceInView(View* view, const char* data, unsigned int offset, int len, int deletionLen);

int getUpdateCounter(const View* view);
unsigned long getModifiedCounter(const View* view);
const Buffer* getRulesBuffer(const View * view);

void setRulesBuffer(View * view, const Buffer * rulesBuffer);

int openFDForView(const View* view, const char* name, int baseFlag);

long getViewId(const View*);
#endif
