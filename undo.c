#include "autocommands.h"
#include "context.h"
#include "memory.h"
#include "undo.h"
#include "view.h"

#include <assert.h>

typedef enum {
    UNDO_APPEND, UNDO_DELETE
} UndoAction;

typedef struct UndoNodeInfo {
    UndoAction action;
    char* data;
    unsigned int changeOffset;
    unsigned int len;
} UndoNodeInfo;

typedef unsigned int NodeId;
typedef struct UndoNode{
    NodeId parentId;
    unsigned int creationTime;
    unsigned int eventId;
    unsigned int modifiedId;
    unsigned int depth;
    UndoNodeInfo info;
} UndoNode;

typedef struct {
    NodeId currentId;
    NodeId headId;
} UndoStateHeader;

static int getNodeId(const Buffer* buffer, const UndoNode* node) {
    return node ? (node - (UndoNode*)(buffer->raw + sizeof(UndoStateHeader))) : -1;
}

static UndoNode* getNodeFromId(const Buffer* buffer, NodeId nodeId) {
    if (buffer->size == 0 || nodeId == -1)
        return NULL;
    UndoNode* base = (buffer->raw + sizeof(UndoStateHeader));
    assert((void*)(base + nodeId) < buffer->raw + buffer->size);
    return base + nodeId;
}

static UndoNode* getParent(const Buffer* buffer, const UndoNode* node) {
    assert(node);
    UndoNode* base = (buffer->raw + sizeof(UndoStateHeader));
    UndoNode* parent = node->parentId != -1 ? getNodeFromId(buffer, getNodeId(buffer, base + node->parentId)) : NULL;
    return parent;
}

static UndoStateHeader* getUndoStateHeader(const Buffer* buffer) {
    return buffer->raw;
}

static UndoNode* getHead(const Buffer* buffer) {
    return buffer->size ? getNodeFromId(buffer, getUndoStateHeader(buffer)->headId) : NULL;
}

static void setHead(Buffer* buffer, UndoNode* node) {
    ((UndoStateHeader*)buffer->raw)->headId = getNodeId(buffer, node);
}

static UndoNode* getCurrent(const Buffer* buffer) {
    return buffer->size ? getNodeFromId(buffer, getUndoStateHeader(buffer)->currentId) : NULL;
}

static void setCurrent(const Buffer* buffer, UndoNode* node) {
    ((UndoStateHeader*)buffer->raw)->currentId = getNodeId(buffer, node);
}

static UndoNode* initNode(Buffer* buffer, const ViewAutoParams * params, UndoNode* newNode, UndoAction action) {
    *newNode = (UndoNode){0};
    newNode->eventId = getNumericSettings(getSettings(params->view), event_counter_index);

    UndoNode* current = getCurrent(buffer);
    if (current) {
        newNode->parentId = getNodeId(buffer, current);
        newNode->depth = current->depth + (newNode->eventId != current->eventId);
    } else {
        newNode->parentId = -1;
    }

    newNode->info.len = params->bufferParams.len;
    newNode->info.changeOffset = params->bufferParams.offset;
    newNode->info.action = action;
    const void* data = params->bufferParams.data ? params->bufferParams.data : getCurrentBuffer(params->view)->readOnlyData + newNode->info.changeOffset;

    unsigned long copyOffset = 0;
    if (current && newNode->eventId == current->eventId && current->info.action == action && current->info.action + current->info.len == newNode->info.changeOffset) {
        newNode->info.data = wee_realloc(ALLOCATION_EXT, current->info.data, current->info.len + newNode->info.len);
        if (newNode->info.data) {
            copyOffset = current->info.len;
            current->info.len += newNode->info.len;
            current->info.data = newNode->info.data;
            newNode = current;
        }
    } else {
        newNode->info.data = wee_malloc(ALLOCATION_EXT, newNode->info.len);
    }
    if (!newNode->info.data) {
        return NULL;
    }
    memcpy(newNode->info.data + copyOffset, data, newNode->info.len - copyOffset);
    return newNode;
}

static int saveAction(AutoCommandType type, const ViewAutoParams * params) {
    Buffer* buffer = getViewStateBuffer(params->view, UNDO_INDEX);
    UndoStateHeader* header;
    if (params->bufferParams.len == 0)
        return 0;
    int offset = buffer->size;
    if (buffer->size == 0) {
        appendToBuffer(buffer, NULL, sizeof(UndoStateHeader));
        header = getUndoStateHeader(buffer);
        *header = (UndoStateHeader){.currentId = -1, .headId = -1};
        offset = sizeof(UndoStateHeader);
    } else {
        header = getUndoStateHeader(buffer);
        if (header->headId == -1)
            return 0;
    }

    UndoNode temp;
    UndoAction action = type == AUTO_CMD_BUFFER_POST_APPEND_CHAR ? UNDO_APPEND : UNDO_DELETE;
    UndoNode* newNode = initNode(buffer, params, &temp, action);
    if (newNode == &temp) {
        appendToBuffer(buffer, &temp, sizeof(UndoNode));
        newNode = buffer->raw + offset;
    } else if (!newNode) {
        return -1;
    }

    setCurrent(buffer, newNode);
    setHead(buffer, newNode);

    ALL("Saving undo state %d\n", getNodeId(buffer, newNode));
    return 0;
}

static const UndoNode* findCommonAncestor(const Buffer* buffer, const UndoNode* node1, const UndoNode* node2) {
    while(node1 != node2) {
        if (node1 < node2){
            node2 = getParent(buffer, node2);
        } else {
            node1 = getParent(buffer, node1);
        }
    }
    return node1;
}

static void undoAction(View* view, const UndoNode* node, int redo) {
    UndoAction action = node->info.action;
    if (!redo) {
        switch (action) {
            case UNDO_APPEND:
                action = UNDO_DELETE;
                break;
            case UNDO_DELETE:
                action = UNDO_APPEND;
                break;
        }
    }
    switch (action) {
        case UNDO_APPEND:
            insertInView(view, node->info.data, node->info.changeOffset, node->info.len);
            break;
        case UNDO_DELETE:
            deleteFromView(view, node->info.changeOffset, node->info.len);
            break;
    }
}

static void undoUntil(View* view, const Buffer* buffer, const UndoNode* current, const UndoNode* target) {
    while (current != target) {
        undoAction(view, current, 0);
        current = getParent(buffer, current);
    }
}

static void redoUntil(View* view, const Buffer* buffer, const UndoNode* current, const UndoNode* target) {
    while (current != target) {
        const UndoNode* child = target;
        while (getParent(buffer, child) != current) {
            child = getParent(buffer, child);
        }
        undoAction(view, child, 1);
        current = child;
    }
}

static int jumpToState(View* view, const Buffer* buffer, UndoNode* target) {
    const UndoNode* current = getCurrent(buffer);
    UndoStateHeader* header = getUndoStateHeader(buffer);
    NodeId savedHead = header->headId;
    // Set the head to an invalid value so we don't track the state changes we are about to make
    header->headId = -1;
    const UndoNode* base = findCommonAncestor(buffer, current, target);
    undoUntil(view, buffer, current, base);
    redoUntil(view, buffer, base, target);
    setCurrent(buffer, target);
    header->headId = savedHead;
    return 0;
}

int undoLast(View* view) {
    Buffer* buffer = getViewStateBuffer(view, UNDO_INDEX);
    const UndoNode* current = getCurrent(buffer);
    if (!current)
        return -1;
    UndoNode* parent = getParent(buffer, current);

    if (parent) {
        int eventId = parent->eventId;
        UndoNode* originalParent = parent;
        while (parent) {
            UndoNode* grandParent = getParent(buffer, parent);
            if (!grandParent && parent != originalParent || grandParent && grandParent->eventId == eventId) {
                parent = grandParent;
                continue;
            }
            break;
        }
    }

    return jumpToState(view, buffer, parent);
}

int redoLast(View* view) {
    Buffer* buffer = getViewStateBuffer(view, UNDO_INDEX);
    UndoNode* head = getHead(buffer);
    if (!head)
        return -1;
    UndoNode* child = head;
    UndoNode* eventGroupHead = head;
    UndoNode* current = getCurrent(buffer);

    while (getParent(buffer, child) != current) {
        child = getParent(buffer, child);
        if (!child)
            break;
        if (child->eventId != eventGroupHead->eventId ) {
            eventGroupHead = child;
        }
    }
    assert(!child || child->eventId == eventGroupHead->eventId);
    return child ? jumpToState(view, buffer, eventGroupHead) : 0;
}

int jumpToUndoId(View* view, int id) {
    Buffer* buffer = getViewStateBuffer(view, UNDO_INDEX);
    for (int i = sizeof(UndoStateHeader); i < buffer->size; i += sizeof(UndoNode)) {
        UndoNode* node = (void*)buffer->p + i;
        if (getNodeId(buffer, node) == id) {
            return jumpToState(view, buffer, node);
        }
    }
    return -1;
}

static AutocommandCallback undoCallbacks[] = {
    {AUTO_CMD_BUFFER_PRE_DELETE_CHAR, saveAction},
    {AUTO_CMD_BUFFER_POST_APPEND_CHAR, saveAction},
};

STATIC_BUFFER(undoCallbacksBuffer, undoCallbacks);

const Buffer* getUndoAutocommands() {
    return &undoCallbacksBuffer;
}

void freeUndoState(Buffer* state) {
    FOR_EACH(UndoNode*, node, state, sizeof(UndoStateHeader)) {
        if (node->info.data)
            wee_free((void*)node->info.data);
    }
    freeBufferData(state);
}
