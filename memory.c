#include "enum_conversion.h"
#include "logger.h"
#include "memory.h"
#include <assert.h>
#include <stdio.h>

struct AllocationInfo {
    // type of allocation
    AllocationTypes type;
    // pointer returned by malloc/realloc
    void* ptr;
    // size of allocation
    size_t size;
    // Where the allocation was made
    const char* label;
};

struct AllocationInfo * allocations;
static int numAllocations;
static int allocSize;

static int outstandingAllocations[NUM_ALLOCATION_TYPES];

/**
 * Prints a summary of the outstanding allocated memory
 */
static void printMemorySummary() {
    int totalSize[NUM_ALLOCATION_TYPES] = {};
    INFO("Allocation List\n");
    for(int i = 0; i < numAllocations; i++) {
        struct AllocationInfo * info = allocations + i;
        INFO("Type %d %s ptr %p size 0x%lX label '%s'\n", info->type, toAllocationTypesStr(info->type), info->ptr, info->size, info->label);
        totalSize[info->type] += info->size;
    }
    INFO("Summary\n");
    for(int i = 0; i < NUM_ALLOCATION_TYPES; i++) {
        INFO("Type %d %s; Total memory 0x%03X; Total Outstanding Allocations %d; Avg allocation size 0x%03X\n",
                i, toAllocationTypesStr(i), totalSize[i], outstandingAllocations[i], outstandingAllocations[i] ? totalSize[i]/ outstandingAllocations[i]: 0 );
    }
}

#ifndef NDEBUG
#define assert_with_logging(X, LABEL) do { if(!(X)){printMemorySummary(); printf("%s\n", LABEL);} assert(X); } while(0)
#else
#define assert_with_logging(X, LABEL) assert(X)
#endif

static char sentinel = '\0';
/**
 * Records the allocation so we can check for leaks
 */
void* recordAllocation(AllocationTypes type, void* old, void* new, size_t size, const char* label) {
    int index = -1;
    assert(!new || size);
    if (old) {
        assert(numAllocations);
        for(int i = 0; i < numAllocations ; i++) {
            if(allocations[i].ptr == old) {
                if (new && size > allocations[i].size) {
                        // if growing buffer
                        memset(new + allocations[i].size, sentinel, size - allocations[i].size);
                } else if (!new){
                    memset(old, sentinel, allocations[i].size);
                }
                if(new) {
                    index = i;
                    break;
                } else {
                    AllocationTypes oldType = allocations[i].type;
                    assert_with_logging(outstandingAllocations[oldType], label);
                    allocations[i] = allocations[--numAllocations];
                    outstandingAllocations[oldType]--;
                    return NULL;
                }
            }
        }
    } else {
        assert_with_logging(size, label);
        assert_with_logging(!!new, label);
        if(numAllocations == allocSize) {
            allocSize = allocSize ? allocSize * 2 : 4;
            allocations = realloc(allocations, sizeof(struct AllocationInfo) * allocSize);
            assert(allocations);
        }
        index = numAllocations++;
        outstandingAllocations[type]++;
        memset(new, sentinel, size);
    }
    assert_with_logging(index != -1, label);
    allocations[index] = (struct AllocationInfo){type, new, size, label};
    return new;
}

/*
 * Checks to see if there is any outstanding allocated memory
 */
int checkForLeaks() {
    for(int i = 0; i < NUM_ALLOCATION_TYPES; i++)
        if (outstandingAllocations[i]) {
            TRACE("Detected leak with allocator type %d\n", i);
            printMemorySummary();
            return 1;
        }
    return 0;
}
