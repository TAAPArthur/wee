#include "buffer.h"
#include "dock.h"
#include "memory.h"
#include "rect.h"
#include "render.h"
#include "screen.h"
#include "settings.h"
#include "typedefs.h"
#include "utility.h"
#include "view.h"
#include "window.h"

#include <assert.h>
#include <stddef.h>

typedef struct ScreenHeader {
    unsigned short rows;
    unsigned short cols;
    unsigned short cursorX;
    unsigned short cursorY;
    Buffer dockSpace;
} ScreenHeader;

typedef struct {
    unsigned long effectiveOffset;
    const char* pos;
    char c;
    // if width is 1, then the data is stored in c
    // otherwise query the getPositionFromEffectiveOffset(win, effectiveOffset)
    char width;
    RenderTuple style;
} ScreenCell;

typedef struct Screen {
    ScreenHeader header;
    ScreenCell data[];
} Screen;

typedef struct DockParameters {
    const Rect originalRect;
    DockInfo* dockInfo;
    const Window* win;
    const Buffer* styleSchemes;
    const Buffer* docks;
} DockParameters;

void clearScreen(Screen * screen, const Rect * rect) {
    if (!rect) {
        memset(screen->data, 0x0, sizeof(ScreenCell) * screen->header.cols * screen->header.rows);
        return;
    }
    for (int i = rect->y; i < rect->height; i++) {
        memset(screen->data + screen->header.cols * i, 0x0, sizeof(ScreenCell) * (rect->width - rect->x));
    }
}

Screen* resizeScreen(Screen* screen, unsigned short width, unsigned short height) {
    if (screen) {
        TRACE("Resizing screen %d %d\n", width, height);
        int delta = screen->header.dockSpace.maxSize - width * height;
        if (delta < 0)
            growBuffer(&screen->header.dockSpace, -delta);
    }
    screen = wee_realloc(ALLOCATION_SCREEN, screen, sizeof(Screen) + sizeof(ScreenCell) * width * height);
    if (screen) {
        screen->header.rows = height;
        screen->header.cols = width;
    }
    return screen;
}

Screen* allocScreen() {
    Screen* screen = wee_malloc(ALLOCATION_SCREEN, sizeof(Screen) + sizeof(ScreenCell));
    memset(&screen->header, 0x0, sizeof(screen->header));
    return screen;
}

void freeScreen(Screen* screen) {
    freeBufferData(&screen->header.dockSpace);
    wee_free(screen);
}

static inline int getCellIndex(const Screen* screen, int x, int y) {
    assert(x < screen->header.cols);
    assert(y < screen->header.rows);
    return y * screen->header.cols + x;
}

static void setCell(Screen* screen, int x, int y, unsigned long effectiveOffset, const char* pos, char c, int width) {
    ScreenCell* cell = &screen->data[getCellIndex(screen, x, y)];
    assert(!cell->width);
    cell->effectiveOffset = effectiveOffset;
    cell->c = c;
    cell->pos = pos ? pos : &cell->c;
    cell->width = width;
}

static inline const ScreenCell * getCell(const Screen* screen, int x, int y) {
    return &screen->data[getCellIndex(screen, x, y)];
}

static inline const char * getData(const ScreenCell* cell) {
    return cell->pos;
}

StyleTuple* findStyleTupleForLabel(const Buffer* styleSchemes, Label label) {
        FOR_EACH_BUFFER(StyleScheme*, styleScheme, styleSchemes) {
            if (styleScheme->label == label) {
                return &styleScheme->tuple;
            }
        }
        return NULL;
}

void updateScreenStyle(Screen* screen, const Rect* rect, const Window* win, const Buffer* labelBuffer, const Buffer* styleSchemes) {
    int n = 16;
    LabelRange labelRanges[n];
    for (int y = rect->y; y < rect->y + rect->height && y < screen->header.rows; y++) {
        for (int x = rect->x; x < rect->x + rect->width && x < screen->header.cols; x++) {
            const ScreenCell* cell = getCell(screen, x, y);
            if (win) {
                int numLables = getLabelsAtEffectiveOffset(win, cell->effectiveOffset, n, labelRanges);
                for (int i = 0; i < numLables; i++) {
                    StyleTuple * tuple = findStyleTupleForLabel(styleSchemes, labelRanges[i].label);
                    if (tuple) {
                        copyStyle(&screen->data[getCellIndex(screen, x, y)].style, tuple);
                    }
                }
            } else {
                FOR_EACH(LabelRange*, rule, labelBuffer, 0) {
                    StyleTuple * tuple = findStyleTupleForLabel(styleSchemes, rule->label);
                    if (tuple) {
                        copyStyle(&screen->data[getCellIndex(screen, x, y)].style, tuple);
                    }
                }
            }
        }
    }
}

static void drawDocksHelper(const DockParameters * dockParams, const WindowSettings* settings, Screen* screen, DockType type, int x, int y, int width, int height);

typedef struct {
    int x;
    int y;
    char set;
} TargetSet;

const char * updateScreenContent(Screen* screen, const Window* win, const WindowSettings* settings, DockParameters * dockParameters, Rect rect, const char* cursor, const char* pos, const char* end, int startingCol, TargetSet * targetSet) {
    int labelRangePos = 0;
    int newLine = 1;
    const char* endOfSection = NULL;
    const char* startOfSection = NULL;
    unsigned long effectiveOffset = win ? getEffectiveOffsetOfPosition(win, pos) : 0;
    unsigned long len = win ? getWindowDataLen(win) : 0;

    const char* startOfNextSection;
    if (win) {
        getSectionBounds(win, pos, &startOfSection, &endOfSection, &startOfNextSection);
    }
    assert(settings);

    int wordwrap = isEnabled(settings, wordwrap_index);
    if (wordwrap) {
        startingCol %= rect.width;
    }
    short x = rect.x, y = rect.y;
    while (1) {
        while (win && pos == endOfSection) {
            pos = getPositionFromEffectiveOffset(win, effectiveOffset);
            int ret = getSectionBounds(win, pos, &startOfSection, &endOfSection, &startOfNextSection);
            if (ret == -1) {
                break;
            }
        }
        if (y == rect.y + rect.height) {
            break;
        }
        if (rect.x <= x && x < rect.x + rect.width && cursor == pos) {
            TRACE("Setting cursor %d %d\n", x, y);
            assert(targetSet);
            if (screen && targetSet->set) {
                screen->header.cursorX = x;
                screen->header.cursorY = y;
            }
            targetSet->x = x;
            targetSet->y = y;
            targetSet->set = 1;
        }
        if (newLine && (y == rect.y || pos != end)) {
            if (dockParameters && screen) {
                drawDocksHelper(dockParameters, settings, screen, DOCK_LEFT,
                        dockParameters->originalRect.x, y, rect.x - dockParameters->originalRect.x, 1);
                drawDocksHelper(dockParameters, settings, screen, DOCK_RIGHT,
                        rect.x + rect.width, y, dockParameters->originalRect.x + dockParameters->originalRect.width - (rect.x + rect.width), 1);
                dockParameters->dockInfo->lineno += 1;
            }
            newLine = 0;
        }
        if (pos == end)
            break;
        if (screen && y >= screen->header.rows)
            break;

        int isValidRangeInScreen = rect.x <= x && x < rect.x + rect.width && screen && x < screen->header.cols && y < screen->header.rows;
        CountCharResult result = {};

        assert(pos);
        assert(settings);
        const char * set = getNumCharsUsed(pos, endOfSection, settings, &result);
        if (isValidRangeInScreen) {
            if(set == pos) {
                setCell(screen, x, y, effectiveOffset, pos, *pos, result.len);
            } else if (result.len && set) {
                assert(set == result.scratchSpace);
                for (int i = 0; i < result.xDelta && x + i < rect.x + rect.width; i++) {
                    setCell(screen, i + x, y, effectiveOffset, NULL, result.scratchSpace[i % result.len], 1);
                }
            }
        }
        pos += result.bytesUsed;
        effectiveOffset += result.bytesUsed;
        x += result.xDelta;

        if (result.yDelta || wordwrap && rect.x + rect.width <= x) {
            newLine = !!result.yDelta;
            y += result.yDelta ? result.yDelta : 1;
            x = rect.x - startingCol;
        }
    }
    return pos;
}

static void drawDocksHelper(const DockParameters * dockParams, const WindowSettings* settings, Screen* screen, DockType type, int x, int y, int width, int height) {
    if (width == 0 || height == 0)
        return;
    Rect rect = {x, y, width, height};
    int maxLen = 1 + width * height;
    char buffer[maxLen];

    FOR_EACH_BUFFER(const Dock*, dock, dockParams->docks) {
        if (!isDockEnabledForType(settings, dock, type))
           continue;
        Buffer labelRanges = {};
        int num = getDockText(dockParams->win, settings, dockParams->dockInfo, dock, buffer, maxLen, &labelRanges);
        int offset = screen->header.dockSpace.size;
        appendToBuffer(&screen->header.dockSpace, buffer, num);
        const char* start = screen->header.dockSpace.readOnlyData + offset;
        const char* end = start + num;
        updateScreenContent(screen, NULL, settings, NULL, rect, NULL, start, end, 0, NULL);
        updateScreenStyle(screen, &rect, NULL, &labelRanges, dockParams->styleSchemes);
        freeBufferData(&labelRanges);
    }
}

static const char* getViewportContainingTarget(Window * win, Rect rect, const char* target, const WindowSettings* settings, int * startingColp) {
    if (rect.width == 0 || rect.height == 0) {
        return NULL;
    }

    int startingCol;
    const char* pos = getStartOfViewport(win, &startingCol);
    // If the target is before the start of the viewport, move the start to target line
    size_t effectiveOffsetTarget = getEffectiveOffsetOfPosition(win, target);
    size_t effectiveOffsetOfViewport = getEffectiveOffsetOfPosition(win, pos);
    if ( effectiveOffsetTarget < effectiveOffsetOfViewport) {
        startingCol = getOffsetIntoLine(win, target);
        pos = getPositionFromEffectiveOffset(win, getEffectiveOffsetOfPosition(win, target) - startingCol);
        startingCol = 0;
    }

    const char* end;
    TargetSet targetSet = {};

    const char* endEffectiveOffset = getEffectiveEndPosition(win);
    int skip = target == NULL;
    int targetCol = getOffsetIntoLine(win, target);
    int wordwrap = isEnabled(settings, wordwrap_index);
    while (1) {
        assert((startingCol) >= 0);
        end = updateScreenContent(NULL, win, settings, NULL, rect, target, pos, endEffectiveOffset, startingCol, &targetSet);
        if (targetSet.set)
            break;
        TRACE("Post updateScreenContent pos %p end %p; target %p; targetSet %d startingCol %d\n", pos, end, target, targetSet, startingCol);
        assert(pos != end);
        int targetAfterEnd = getEffectiveOffsetOfPosition(win, target) >= getEffectiveOffsetOfPosition(win, end) ;

        int dir = 1;
        if (!targetAfterEnd) {
            int diff = startingCol - targetCol;
            if (diff <= -rect.width) {
                // targetCol if off to the right of the screen
                if (*pos == '\n') {
                    // We are at the end of the line and can't adjust the line anymore
                    startingCol++;
                    continue;
                } else {
                    dir = 1;
                }
            } else if (diff > 0) {
                // targetCol if off to the left of the screen
                assert(startingCol);
                int startingColForFirstLine = getOffsetIntoLine(win, pos);
                if (startingCol > startingColForFirstLine) {
                    startingCol--;
                    continue;
                } else {
                    dir = -1;
                }
            } else {
                assert(0);
            }
        }

        pos = getPositionFromEffectiveOffset(win, getEffectiveOffsetOfPosition(win, pos) + dir);
        startingCol = getOffsetIntoLine(win, pos);
    }
    assert(targetSet.set);
    if (startingColp) {
        *startingColp = startingCol;
    }
    return pos;
}
static const char* adjustViewport(Window * win, Rect rect, const char* target, const WindowSettings* settings) {
    int startingCol;
    const char* pos = getViewportContainingTarget(win, rect, target, settings, &startingCol);
    setStartOfViewport(win, pos, startingCol);
    return pos;
}

void drawWindow(Window * win, Rect originalRect, Rect rect, void* _drawInfo) {
    if (originalRect.width == 0 || originalRect.height == 0) {
        return;
    }
    DrawInfo* drawInfo = _drawInfo;

    WindowSettings* settings = getWindowSettings(win);
    int startingCol;
    const char* cursor = getCursorPosition(win);
    int drawCursor = drawInfo->focusedWindow == win;

    if (drawCursor && isAutoAudjustViewportForCursor(win)) {
        adjustViewport(win, rect, cursor, settings);
    }

    const char* pos = getStartOfViewport(win, &startingCol);
    DockInfo dockInfo = {.lineno = getLineNumberOfLine(win, pos)};

    DockParameters _dockParameters = {
        .originalRect = originalRect,
        .dockInfo = &dockInfo,
        .win = win,
        .styleSchemes = drawInfo->styleSchemes,
        .docks = drawInfo->docks,
    };
    DockParameters* dockParameters = drawInfo->docks ? &_dockParameters : NULL;

    TRACE("Computed viewport; now actually drawing text\n");
    if (dockParameters) {
        drawDocksHelper(dockParameters, settings, drawInfo->screen, DOCK_TOP, originalRect.x, originalRect.y, originalRect.width, rect.y);
        drawDocksHelper(dockParameters, settings, drawInfo->screen, DOCK_BOTTOM, originalRect.x, rect.y + rect.height, originalRect.width, originalRect.y + originalRect.height - rect.y - rect.height);
    }

    const char* endEffectiveOffset = getEffectiveEndPosition(win);
    TargetSet targetSet = {.set = drawCursor};
    updateScreenContent(drawInfo->screen, win, settings, dockParameters, rect, cursor, pos, endEffectiveOffset, startingCol, &targetSet);

    updateScreenStyle(drawInfo->screen, &rect, win, NULL, drawInfo->styleSchemes);
}

void adjustViewportForCursor(Window * win, Rect rect) {
    const char* cursor = getCursorPosition(win);
    TRACE("Adjusting viewport for Cursor\n");
    adjustViewport(win, rect, cursor, getWindowSettings(win));
}

static int getAbsLineOfScreen(Screen * screen, Window * win, const WindowSettings* settings, Rect * rect, unsigned long lineNumber, const char* target) {
    const char* start = getPositionFromEffectiveOffset(win, 0);
    const char* endEffectiveOffset = getEffectiveEndPosition(win);
    TargetSet targetSet = {};
    int lineOffset = 0;
    while (1) {
        clearScreen(screen, rect);
        start = updateScreenContent(screen, win, settings, NULL, *rect, target ? target : endEffectiveOffset, start, endEffectiveOffset, 0, &targetSet);
        if (lineNumber != -1 && lineOffset  + rect->height > lineNumber) {
            return lineNumber;
        }
        if (targetSet.set) {
            return lineOffset + targetSet.y - rect->y;
        }
        assert(start != endEffectiveOffset);
        lineOffset += rect->height;
    }
}

static const char* moveViewportVertHelper(Screen * screen, Window * win, Rect * rect, int lineDelta, int adjustStart) {
    if (!rect) {
        return NULL;
    }
    const WindowSettings * settings = getWindowSettings(win);
    const char* pos = getStartOfViewport(win, NULL);

    int row;
    if (0 <= lineDelta && lineDelta < rect->height) {
        row = rect->y + lineDelta;
    } else {
        int currentScreenLineNumber = getAbsLineOfScreen(screen, win, settings, rect, -1, pos);
        int targetLineNumber = MAX(0, currentScreenLineNumber + lineDelta);
        if (targetLineNumber == getAbsLineOfScreen(screen, win, settings, rect, targetLineNumber, NULL)) {
            row = rect->y + (targetLineNumber % rect->height);
        } else {
            row = rect->y + rect->height - 1;
        }
    }
    while (row >= rect->y) {
        const ScreenCell * cell = getCell(screen, rect->x, row);
        const char * target = getData(cell);
        if (target) {
            if (adjustStart) {
                int startingCol = getOffsetIntoLine(win, target);
                setStartOfViewport(win, target, startingCol);
            } else {
                adjustViewport(win, *rect, target, settings);
            }
            if (lineDelta < 0) {
                if (getEffectiveOffsetOfPosition(win, target) != 0 || getEffectiveOffsetOfPosition(win, pos) != 0) {
                    const char * endingLine = NULL;
                    for (int i = rect->height - 1; i && endingLine == NULL; i--) {
                        endingLine = getData(getCell(screen, 0, row + i));
                    }
                    target = endingLine;
                }
            }
            return target;
        }
        row--;
    }
    return NULL;
}
const char* moveViewportVert(Screen* screen, Window * win, Rect rect, int lineDelta) {
    return moveViewportVertHelper(screen, win, &rect, lineDelta, 0);
}

const char* moveViewportStart(Screen* screen, Window * win, Rect rect, int lineDelta) {
    return moveViewportVertHelper(screen, win, &rect, lineDelta, 1);
}

void drawToBuffer(Window*dest, Window*win, const Buffer* docks, Rect dimensions) {
    Screen* screen =  resizeScreen(NULL, dimensions.width, dimensions.height);
    DrawInfo drawInfo = {
        .screen = screen,
        .docks = docks,
    };
    Rect rect = {0, 0, dimensions.width, dimensions.height};
    drawWindow(win, rect, rect, &drawInfo);

    int lastLine = screen->header.rows;
    for (int y = 0; y < screen->header.rows; y++) {
        if (!getData(getCell(screen, 0, y))) {
            lastLine = y;
        }
    }
    for (int y = 0; y < lastLine; y++) {
        for (int x = 0; x < screen->header.cols; x++) {
            const ScreenCell * cell = getCell(screen, x, y);
            const char* data = getData(cell);
            if (!data) {
                break;
            }
            appendToInsertionBuffer(dest, data, cell->width);
        }
        appendToInsertionBuffer(dest, "\n", 1);
    }
    freeScreen(screen);
    commitInsertion(dest);
}

static const RenderBackend * renderImpl;
void setRenderBackend(const RenderBackend * backend) {
    renderImpl = backend;
}
const RenderBackend * getRenderBackend() {return renderImpl;}

void prepareScreen(Screen* screen) {
    if (!getRenderBackend())
        return;
    getRenderBackend()->clear();
    clearScreen(screen, NULL);
    freeBufferData(&screen->header.dockSpace);
}

void drawScreen(const Screen* screen) {
    if (!getRenderBackend())
        return;
    TRACE("Presenting %dx%d\n", screen->header.rows, screen->header.cols);

    getRenderBackend()->set_cursor(screen->header.cursorX, screen->header.cursorY);
    for (int y = 0; y < screen->header.rows; y++) {
        for (int x = 0; x < screen->header.cols; x++) {
            const ScreenCell * cell = getCell(screen, x, y);
            const char* data = getData(cell);
            if (data) {
                getRenderBackend()->set_cell(x, y, data, cell->width, cell->style);
            }
        }
    }

    getRenderBackend()->present();
}
