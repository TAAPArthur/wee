#include "autocommands.h"
#include "container.h"
#include "context.h"
#include "dock.h"
#include "events.h"
#include "logger.h"
#include "rect.h"
#include "registers.h"
#include "render.h"
#include "screen.h"
#include "settings.h"
#include "system.h"
#include "view.h"
#include "window.h"

#include <assert.h>
#include <stddef.h>
#include <string.h>

typedef enum {
    INFO_VIEW,
    COMMAND_VIEW,
    COMMAND_HISTORY_VIEW,
    MESSAGES_VIEW,
    PRINT_VIEW,
    NUM_SPECIAL_VIEWS
} SpecialViewIndex;

static FileType specialViewIndexToFileTypeMapping[NUM_SPECIAL_VIEWS] = {
    [INFO_VIEW] = FT_PLAIN_TEXT,
    [MESSAGES_VIEW] = FT_PLAIN_TEXT,
    [COMMAND_VIEW] = FT_WEE_CMD,
};

static SettingsIndex specialViewIndexToSettingsIndex[NUM_SPECIAL_VIEWS] = {
    [INFO_VIEW] = show_info_bar_index,
    [COMMAND_VIEW] = show_cmd_bar_index,
    [COMMAND_HISTORY_VIEW] = show_cmd_bar_index,
};

typedef struct Context {
    Buffer globalWindowList;
    Window* focusedWindow;
    Window* backupFocusedWindow;
    Window* cmdWindow;
    Window* printWindow;
    Buffer globalViewList;

    Rect commandWindowBounds;
    View* specialViews[NUM_SPECIAL_VIEWS];

    char dirty;
    char _state;
    char shutdownRequested;

    ViewSettings* globalSettings;
    Screen * screen;
    Buffer stateBuffers[NUM_STATE_INDEX];
} Context;

Screen * getScreen(const Context* context) {
    return context->screen;
}

const Buffer* getStateBufferConst(const Context* context, StateIndex index) {
    return context->stateBuffers + index;
}

Buffer* getStateBuffer(Context* context, StateIndex index) {
    return context->stateBuffers + index;
}

void addStateBuffer(Context* context, StateIndex index, const Buffer * buffer) {
    appendElementConst(getStateBuffer(context, index), (void*)buffer);
}
void removeStateBuffer(Context* context, StateIndex index, const Buffer * buffer) {
    removeElement(getStateBuffer(context, index), buffer);
}

void clearStateBuffer(Context* context, StateIndex index) {
    freeBufferData(getStateBuffer(context, index));
}

static Context _globalContext;
Context* globalContext = &_globalContext;

Window* getFocusedWindow(const Context* context) {
    return context->focusedWindow;
}

void forEachView(const Context* context, void (*func)(View*)) {
    for (int i = getLen(&context->globalViewList) - 1; i >=0; i--) {
        func(getElementAt(&context->globalViewList, i));
    }
}

View* getFocusedView(const Context* context) {
    unsigned long viewId = getViewIdOfWindow(getFocusedWindow(context));
    for (int i = getLen(&context->globalViewList) - 1; i >=0; i--) {
        if (viewId  == getViewId(getElementAt(&context->globalViewList, i)))
            return getElementAt(&context->globalViewList, i);
    }
    return NULL;
}

Window* getActiveWindow(const Context* context) {
    if (context->cmdWindow == getFocusedWindow(context)) {
        return context->backupFocusedWindow;
    }
    return getFocusedWindow(context);
}

ViewSettings* getGlobalSettings(const Context* context) {
    return context->globalSettings;
}

const Rect* getBoundsOfViewport(const Context* context, const Window * win) {
    const Buffer* containerInfo = getStateBufferConst(context, CONTAINER_INDEX);
    ContainerId id = findContainerOfWindow(containerInfo, win);
    if (id != INVALID_CONTAINER_ID) {
            return getBoundsOfContainerIgnoringDocks(containerInfo, id);
    }
    return NULL;
}

void setFocusedWindowTransient(Context* context, Window* win) {
    context->focusedWindow = win;
}

void setFocusedWindow(Context* context, Window* win) {
    if (win != context->cmdWindow) {
        const Buffer* containerInfo = getStateBufferConst(context, CONTAINER_INDEX);
        activateContainer(containerInfo, findContainerOfWindow(containerInfo, win));
        context->backupFocusedWindow = win;
    }
    context->focusedWindow = win;
}

void focusCommandWindow(Context* context) {
    setViewForWindow(context->cmdWindow, context->specialViews[COMMAND_VIEW]);
    const Buffer* historyBuffer = getCurrentBuffer(context->specialViews[COMMAND_HISTORY_VIEW]);
    appendToView(context->specialViews[COMMAND_VIEW], historyBuffer->readOnlyData, historyBuffer->size);

    const Buffer* buffer = getCurrentBuffer(context->specialViews[COMMAND_VIEW]);
    setCursorOffset(context->cmdWindow, buffer->size);
    setFocusedWindow(context, context->cmdWindow);

    historyBuffer = getCurrentBuffer(context->specialViews[COMMAND_HISTORY_VIEW]);
    historyBuffer = getCurrentBuffer(context->specialViews[COMMAND_VIEW]);
}

int appendToCommandHistory(Context* context, const char* str, int n) {
    assert(n >= 2);
    assert(str[n - 1] == '\n');
    assert(str[n - 2] != '\n');
    return appendToView(context->specialViews[COMMAND_HISTORY_VIEW], str, n);
}

void restoreFocus(Context* context) {
    const Buffer* historyBuffer = getCurrentBuffer(context->specialViews[COMMAND_HISTORY_VIEW]);
    deleteFromView(context->specialViews[COMMAND_VIEW], 0, getCurrentBuffer(context->specialViews[COMMAND_VIEW])->size);
    assert(getCurrentBuffer(context->specialViews[COMMAND_VIEW])->size == 0);

    setViewForWindow(context->cmdWindow, context->specialViews[INFO_VIEW]);
    if (context->globalWindowList.size)
        setFocusedWindow(context, getActiveWindow(context));
}

void setInfoMessage(Context * context, const char* msg) {
    replaceInView(context->specialViews[INFO_VIEW], msg, 0, strnlen(msg, 255), 255);
    if (getViewIdOfWindow(context->cmdWindow) == getViewId(context->specialViews[INFO_VIEW])) {
        setCursorOffset(context->cmdWindow, 0);
    }
}

void appendMessage(Context * context, const char* msg) {
    appendToView(context->specialViews[MESSAGES_VIEW], msg, strlen(msg));
    appendToView(context->specialViews[MESSAGES_VIEW], "\n", 1);
}

int initContext(Context* context) {
    TRACE("Initializing context\n");

    context->globalSettings = allocGlobalSettings();
    setPointerSettings(context->globalSettings, autocommand_index, getStateBufferConst(context, AUTOCOMMAND_INDEX));

    context->screen = allocScreen(context);

    initContainerState(getStateBuffer(context, CONTAINER_INDEX), getStateBufferConst(context, DOCK_INDEX));

    context->cmdWindow = allocWindow(context->globalSettings);
    context->printWindow = allocWindow(context->globalSettings);

    addContainer(getStateBuffer(context, CONTAINER_INDEX), context->cmdWindow);
    addContainer(getStateBuffer(context, CONTAINER_INDEX), context->printWindow);


    for(int i = 0; i < NUM_SPECIAL_VIEWS; i++) {
        context->specialViews[i] = addView(context);
        setNumericSettings(getSettings(context->specialViews[i]), filetype_index, specialViewIndexToFileTypeMapping[i]);
    }
    // TODO load history
    setViewForWindow(context->cmdWindow, context->specialViews[INFO_VIEW]);
    setViewForWindow(context->printWindow, context->specialViews[PRINT_VIEW]);


    assert(!context->_state);
    context->_state = 1;

    // Setup up some sane defaults

    return 0;
}

static void uninitContext(Context* context) {
    TRACE("Uninitializing context\n");
    assert(context->_state);

    TRACE("Removing all windows\n");

    freeWindow(context->cmdWindow);
    context->cmdWindow = NULL;
    freeWindow(context->printWindow);
    context->printWindow = NULL;


    for (int i = getLen(&context->globalWindowList) - 1; i >=0; i--) {
        removeWindow(context, getElementAt(&context->globalWindowList, i));
    }
    TRACE("Removing all views\n");
    for (int i = getLen(&context->globalViewList) - 1; i >=0; i--) {
        removeView(context, getElementAt(&context->globalViewList, i));
    }

    freeRules(getStateBufferConst(context, RULE_INDEX));
    for (int i = 0; i < NUM_STATE_INDEX; i++) {
        freeBufferData(getStateBuffer(context, i));
    }

    freeSettings(context->globalSettings);

    freeScreen(context->screen);

    context->_state = 0;

}

int shouldRetile(const Context* context) {
    return context->dirty;
}


static SpecialViewIndex getSpecialVisibleSpecialViewIndex(const Context* context, const ViewSettings* settings) {
    for (SpecialViewIndex i = 0; i < NUM_SPECIAL_VIEWS; i++) {
        if (isEnabled(settings, specialViewIndexToSettingsIndex[i])) {
            if (isAltVersion(settings, specialViewIndexToSettingsIndex[i]))
                return i;
            switch(i) {
                case INFO_VIEW:
                    if (getCurrentBuffer(context->specialViews[i])->size)
                        return i;
                    break;
                default:
                    if (getFocusedWindow(context) == context->cmdWindow)
                        return i;
            }
        }
    }
    return NUM_SPECIAL_VIEWS;
}

void drawToWindow(Context* context, Window * win) {
    Buffer buffer = {};
    Rect rect = getRootContainerDimensions(getStateBufferConst(context, CONTAINER_INDEX));
    drawToBuffer(context->printWindow, win, getStateBufferConst(context, DOCK_INDEX), rect);
}

void redraw(Context* context) {
    DrawInfo drawInfo = {
        .screen = getScreen(context),
        .styleSchemes = getStateBufferConst(context, COLOR_SCHEME_INDEX),
        .docks = getStateBufferConst(context, DOCK_INDEX),
        .focusedWindow = getFocusedWindow(context),
    };
    assert(getStateBufferConst(context, DOCK_INDEX));
    prepareScreen(drawInfo.screen);

    const Settings * settings = getGlobalSettings(context);
    if (isEnabled(settings, window_index)) {
        forEachContainerWithWindow(getStateBufferConst(context, CONTAINER_INDEX), drawWindow, &drawInfo);
    } else {
        Rect rect = getRootContainerDimensions(getStateBufferConst(context, CONTAINER_INDEX));
        drawWindow(context->printWindow, rect, rect, &drawInfo);
    }

    SpecialViewIndex index = getSpecialVisibleSpecialViewIndex(context, settings);
    if (index != NUM_SPECIAL_VIEWS) {
        drawInfo.docks = NULL;
        drawWindow(context->cmdWindow, context->commandWindowBounds, context->commandWindowBounds, &drawInfo);
    }
    drawScreen(getScreen(context));
}

void retile(Context* context) {
    Rect rect = getRootContainerDimensions(getStateBufferConst(context, CONTAINER_INDEX));
    context->commandWindowBounds = (Rect){0, 0, rect.width, 1};
    ViewSettings * settings = getGlobalSettings(context);
    SpecialViewIndex index = getSpecialVisibleSpecialViewIndex(context, settings);
    if (index != NUM_SPECIAL_VIEWS) {
        TRACE("Detected Special index %d is in use\n", index);
        DockType dockType = getNumericSettings(settings, specialViewIndexToSettingsIndex[index]);
        updateBounds(dockType, 1, &rect, &context->commandWindowBounds);
    }
    setEffectiveRootContainerDimensions(getStateBufferConst(context, CONTAINER_INDEX), rect);
    tileContainers(getStateBufferConst(context, CONTAINER_INDEX));
}

void setDimensions(Context* context, unsigned short width, unsigned short height) {
    ViewSettings * settings = getGlobalSettings(context);
    width = getNumericSettingsOrDefault(settings, columns_index, width);
    height = getNumericSettingsOrDefault(settings, lines_index, height);
    context->screen = resizeScreen(context->screen, width, height);
    Rect dims = {0, 0, width, height};
    setRootContainerDimensions(getStateBuffer(context, CONTAINER_INDEX), dims);
    runAutoCommands(getStateBufferConst(context, AUTOCOMMAND_INDEX), AUTO_CMD_ON_RESIZE, (AutocommandParams){.contextParams = {.context = context, .resizeParams = {dims}}});
    retile(context);
    context->dirty = 1;
}

void getDimensions(const Context* context, unsigned short * width, unsigned short * height) {
    Rect rect = getRootContainerDimensions(getStateBufferConst(context, CONTAINER_INDEX));
    *width =  rect.width;
    *height =  rect.height;
}

const Buffer* getWindowList(const Context* context) {
    return &context->globalWindowList;
}

Window* addWindow(Context* context) {
    context->dirty = 1;
    Window* newWindow = allocWindow(getFocusedWindow(context) ? getWindowSettings(getFocusedWindow(context)) : getGlobalSettings(context));
    appendElement(&context->globalWindowList, newWindow);

    addContainer(getStateBuffer(context, CONTAINER_INDEX), newWindow);

    if(!getFocusedWindow(context))
        setFocusedWindow(context, newWindow);
    return newWindow;
}

Window* splitWindow(Context* context) {
    Buffer* buffer = getStateBuffer(context, CONTAINER_INDEX);
    Window* win = addWindow(context);
    mergeContainers(buffer, findContainerOfWindow(buffer, getFocusedWindow(context)), findContainerOfWindow(buffer, win));
    return win;
}

void setActiveLayout(Context* context, struct Layout* layout) {
    const Buffer* buffer = getStateBuffer(context, CONTAINER_INDEX);
    ContainerId containerId = findContainerOfWindow(buffer, getFocusedWindow(context));
    setLayoutForContainer(buffer, containerId, layout);
}

void removeWindow(Context* context, Window* win) {
    removeElement(&context->globalWindowList, win);

    Buffer* containerBuffer = getStateBuffer(context, CONTAINER_INDEX);
    ContainerId nextContainer = removeContainer(getStateBuffer(context, CONTAINER_INDEX), findContainerOfWindow(containerBuffer, win));

    if (!context->globalWindowList.size) {
        context->focusedWindow = NULL;
        assert(nextContainer == INVALID_CONTAINER_ID);
    } else if (context->focusedWindow == win || context->backupFocusedWindow == win) {
        Window* newFocuseWindow = nextContainer == INVALID_CONTAINER_ID ? getElementAt(getWindowList(context), 0) : getWindowOfContainer(containerBuffer, nextContainer);
        if (context->focusedWindow == win)
            setFocusedWindow(context, newFocuseWindow);
        else if (context->backupFocusedWindow == win)
            context->backupFocusedWindow = newFocuseWindow;
    }

    freeWindow(win);
}

void removeView(Context* context, View * view) {
    removeElement(&context->globalViewList, view);
    freeView(view);
}

View* addView(Context* context) {
    View* view = allocView(getFocusedWindow(context) ? getWindowSettings(getFocusedWindow(context)): getGlobalSettings(context));
    appendElement(&context->globalViewList, view);
    setRulesBuffer(view, getStateBufferConst(context, RULE_INDEX));
    return view;
}

int isShuttingDown(const Context* context) {
    return !context->globalWindowList.size || context->shutdownRequested;
}

int requestShutdown(Context* context) {
    context->shutdownRequested = 1;
    return 0;
}

int shutdownContext(Context* context) {
    if (!context->_state) {
        assert(isShuttingDown(context));
        return 1;
    }
    uninitContext(context);
    clearAllRegisterData();
    assert(isShuttingDown(context));
    return 0;
}

#define RUN_AUTOCMD(CONTEXT, TYPE) if (runAutoCommands(getStateBufferConst(CONTEXT, AUTOCOMMAND_INDEX), TYPE, (AutocommandParams){.contextParams = {.context = CONTEXT}})) return -1

int suspendContext(Context* context) {
    RUN_AUTOCMD(context, AUTO_CMD_PRE_SUSPEND);
    suspendProcess();
    RUN_AUTOCMD(context, AUTO_CMD_POST_SUSPEND);
    return 0;
}
