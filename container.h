#ifndef CONTAINER_H
#define CONTAINER_H

#include "dock.h"
#include "typedefs.h"

struct Layout ;
void tileContainers(const Buffer* buffer);

typedef short ContainerId;
#define INVALID_CONTAINER_ID (-1)

void initContainerState(Buffer* buffer, const Buffer* docks);

ContainerId addContainer(Buffer* buffer, Window* win);
ContainerId mergeContainers(Buffer* buffer, ContainerId first, ContainerId second);
ContainerId removeContainer(Buffer* buffer, ContainerId id);

ContainerId findContainerOfWindow(const Buffer* buffer, const Window*win);
Window* getWindowOfContainer(const Buffer* buffer, ContainerId id);

void updateBounds(DockType type, int thickness, Rect* bounds, Rect* dockBounds);

void setRootContainerDimensions(const Buffer* buffer, Rect rect);
Rect getRootContainerDimensions(const Buffer* buffer);

void setEffectiveRootContainerDimensions(const Buffer* buffer, Rect rect);

void forEachContainerWithWindow(const Buffer* buffer, void(*callback)(Window* win, Rect bounds, Rect subBounds, void*), void* userData);

void activateContainer(const Buffer* buffer, ContainerId containerId);

void setLayoutForContainer(const Buffer* buffer, ContainerId containerId, struct Layout* layout);

const Rect * getBoundsOfContainerIgnoringDocks(const Buffer* buffer, ContainerId containerId);

#endif

