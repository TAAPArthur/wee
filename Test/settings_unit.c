#include "../buffer.h"
#include "../filetypes.h"
#include "../memory.h"
#include "../settings.h"
#include "scutest.h"

#include <assert.h>

Settings* settings;

static void setup() {
    settings = allocGlobalSettings();
}

static void cleanup() {
    freeSettings(settings);
    assert(checkForLeaks() == 0);
}

SCUTEST_SET_FIXTURE(setup, cleanup);

SCUTEST(test_look_up_unknown_setting) {
    assert(getSettingsIndexFromString(" ", 1) == NUM_SETTINGS);
}

SCUTEST(test_get_settings_name) {
    for (int i = 0; i < NUM_SETTINGS; i++) {
        const char * name = getSettingsName(i);
        assert(getSettingsIndexFromString(name, strlen(name)) == i);
    }
}

SCUTEST(test_global_settings) {
    setEnabled(settings, timeout_index, 0);
    setNumericSettings(settings, timeout_index, 0);

    Settings* settingsArr[] = {settings, allocViewSettings(settings), allocWindowSettings(settings, NULL)};
    for (int i = 0; i < LEN(settingsArr); i++) {
        assert(!isEnabled(settingsArr[i], timeout_index));
        assert(!getNumericSettings(settingsArr[i], timeout_index));
    }
    for (int i = 0; i < LEN(settingsArr); i++) {
        setEnabled(settingsArr[i], timeout_index, 1);
        setNumericSettings(settingsArr[i], timeout_index, i + 1);
        for (int n = 0; n < LEN(settingsArr); n++) {
            assert(isEnabled(settingsArr[n], timeout_index));
            assert(getNumericSettings(settingsArr[n], timeout_index) == i + 1);
        }
        setEnabled(settingsArr[i], timeout_index, 1);
        setNumericSettings(settingsArr[i], timeout_index, 1);
    }
    for (int i = 1; i < LEN(settingsArr); i++) {
        freeSettings(settingsArr[i]);
    }
}

SCUTEST(test_set_alt) {
    for (int i = 0; i < NUM_SETTINGS; i++) {
        setAltVersion(settings, i, 0);
        assert(!isAltVersion(settings, i));
        setAltVersion(settings, i, 1);
        assert(isAltVersion(settings, i));
    }
}

SCUTEST(test_set_settings, .iter = NUM_SETTING_TYPES) {
    for (int i = 0; i < NUM_SETTINGS; i++) {
        if (getSettingsType(i) != _i)
            continue;
        // None of these should crash
        setSettings(settings, i, "");
        setSettings(settings, i, "0");
        setSettings(settings, i, "not_a_number");
        switch(getSettingsType(i)) {
            case SETTING_ENV_STR:
            case SETTING_STR:
                setSettings(settings, i, "some_value");
                assert(getStringSettings(settings, i));
                assert(strcmp(getStringSettings(settings, i), "some_value") == 0);
                break;
            case SETTING_BOOL:
                setSettings(settings, i, "0");
                assert(!isEnabled(settings, i));
                assert(getNumericSettings(settings, i) == 0);
                setSettings(settings, i, "");
                assert(!isEnabled(settings, i));
                setSettings(settings, i, "1");
                assert(getNumericSettings(settings, i) == 1);
                assert(isEnabled(settings, i));
                break;
            case SETTING_ENUM:
                if (i == filetype_index) {
                    setSettings(settings, filetype_index, "c");
                    assert(getNumericSettings(settings, filetype_index) == FT_C);
                    setSettings(settings, filetype_index, "cpp");
                    assert(getNumericSettings(settings, filetype_index) == FT_CPP);
                }
                break;
            case SETTING_POINTER: {
                Buffer b;
                setPointerSettings(settings, i, &b);
                assert(getPointerSettings(settings, i) == &b);
                setPointerSettings(settings, i, NULL);
                assert(!getPointerSettings(settings, i));
                break;
            }
            default:
                for(int n = 0; n < 2; n++) {
                    setEnabled(settings, i, n);
                    setSettings(settings, i, "0");
                    assert(getNumericSettings(settings, i) == 0);
                    setSettings(settings, i, "2");
                    assert(getNumericSettings(settings, i) == 2);
                    setSettings(settings, i, "2abc");
                    assert(getNumericSettings(settings, i) == 2);
                    setSettings(settings, i, "0xA");
                    assert(getNumericSettings(settings, i) == 0xA);

                    setSettings(settings, i, "");
                    assert(getNumericSettings(settings, i) == 0);
                }
                break;
        }
    }
}

SCUTEST(test_settings_defaults) {
    for (int i = 0; i < NUM_SETTINGS; i++) {
        if (isEnabled(settings, i))
            return;
    }
    assert(0);
}

SCUTEST(test_settings_parse_set_value) {
    assert(parseAndSetSetting(settings, "linenu=o") != -1);
    assert(strcmp(getStringSettings(settings, linenumberf_index), "o") != -1);
    assert(parseAndSetSetting(settings, "linenu=2") != -1);
    assert(strcmp(getStringSettings(settings, linenumberf_index), "2") != -1);
}

SCUTEST(test_settings_parse_set_value_ignore_white_space) {
    assert(parseAndSetSetting(settings, "      linenu =2") != -1);
    assert(strcmp(getStringSettings(settings, linenumberf_index), "2") != -1);
}

SCUTEST(test_settings_parse_enabled) {
    setEnabled(settings, linenumberf_index, 0);
    for (int i = 0; i < 2; i++) {
        assert(parseAndSetSetting(settings, "linenu") != -1);
        assert(isEnabled(settings, linenumberf_index));
    }
    for (int i = 0; i < 2; i++) {
        assert(parseAndSetSetting(settings, "nolinenu") != -1);
        assert(!isEnabled(settings, linenumberf_index));
    }
}

SCUTEST(test_settings_parse_toggle, .iter = 2) {
    setEnabled(settings, linenumberf_index, 0);
    const char* toggleStr = _i ? "linenu!" : "invlinenu";
    assert(parseAndSetSetting(settings, toggleStr) != -1);
    assert(isEnabled(settings, linenumberf_index));

    assert(parseAndSetSetting(settings, toggleStr) != -1);
    assert(!isEnabled(settings, linenumberf_index));

    assert(parseAndSetSetting(settings, toggleStr) != -1);
    assert(isEnabled(settings, linenumberf_index));
}

SCUTEST(test_settings_parse_unknown) {
    assert(parseAndSetSetting(settings, "unknown_setting") == -1);
    assert(parseAndSetSetting(settings, "unknown_setting=a") == -1);
}

SCUTEST(test_settings_batch_mode) {
    setupBatchMode(settings);
    assert(!isEnabled(settings, autoindent_index));
    assert(setEnabled(settings, autoindent_index, 1) == -1);
    assert(!isEnabled(settings, autoindent_index));
}
