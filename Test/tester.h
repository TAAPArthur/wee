#ifndef TESTER_H
#define TESTER_H
#include "../buffer.h"
#include "../context.h"
#include "../logger.h"
#include "../rect.h"
#include "../settings.h"
#include "../typedefs.h"
#include "tester_basic.h"
#include "tester_screen.h"
#include <assert.h>

int bindingTriggered();
int resetBindingTriggered();
int success(Context* context, Arg arg);

static inline int fail() {
    assert(0);
    return -1;
}

Rect getBoundsOfContext(const Context* context);
Context* getGlobalContext();

void setupBuffer(Context* context, const char* buffer) ;

void safe_write(int fd, const void* data, int len);
void safe_read(int fd, void* data, int len);

void setupContext();
void setupContextWithDefaultSettings(int keepDefaultSettings);
void cleanupContext();


int getNumberOfWindows(const Context* context);

ViewSettings* getActiveSettings(const Context* context);

void shutdownInput();

void verifyNoLeaks();

void waitForAllOutstandingEvents();
#endif
