#include "../memory.h"
#include "../settings.h"
#include "../system.h"
#include "tester_basic.h"

#include "scutest.h"

#include <assert.h>
#include <signal.h>
#include <time.h>
#include <unistd.h>

static void waitForAllOutstandingEvents() {
    while(!isIdle()) {
        int ret = waitForEvent(10);
        assert(ret != -1);
        if (ret) {
            processEvents();
        }
    }
}

static char tempFile[SCUTEST_TEMP_FILE_NAME_MAX_LEN];
void setupTempFile() {
    int fd = createTempFileForTest(tempFile);
    assert(fd != -1);
    close(fd);
}

SCUTEST_SET_FIXTURE_NO_ARGS(setupTempFile, NULL);

SCUTEST(test_open_file) {
    int fd = openFile(tempFile, OPEN_IO_READ);
    assert(fd != -1);
    close(fd);
}

SCUTEST(test_remove_file) {
    assert(removeFile(tempFile) != -1);
    assert(openFile(tempFile, OPEN_IO_READ) == -1);
    int fd = openFile(tempFile, OPEN_IO_READ | OPEN_IO_CREATE);
    assert(fd != -1);
    close(fd);
}

static void initBufferForWriting(Buffer* buff, void* p, int len) {
    *buff = (Buffer){.readOnlyData = p, .size = len, .maxSize = len};
}

SCUTEST(test_read_write, .iter = 2) {
    int fd = openFile(tempFile, OPEN_IO_WRITE);
    int dataSize = 16;
    void * p = wee_malloc(ALLOCATION_RAW, dataSize);
    memset(p, '0', dataSize);
    int writeTransferLen = _i ? dataSize / 2 : dataSize;

    {
        Buffer buffer;
        initBufferForWriting(&buffer, p, dataSize);
        writeDataToFd(fd, &buffer, 0, writeTransferLen, NULL, NULL, NULL);
        waitForAllOutstandingEvents();
    }
    close(fd);

    {
        fd = openFile(tempFile, OPEN_IO_READ);
        Buffer buffer = {};
        readDataFromFd(fd, &buffer, 0, NULL, NULL, NULL);
        waitForAllOutstandingEvents();
        assert(buffer.size == writeTransferLen);
        assert(bufncmp(&buffer, p, buffer.size) == 0);
        freeBufferData(&buffer);
    }
    wee_free(p);
}

SCUTEST(test_read_write_append) {
    int fd = openFile(tempFile, OPEN_IO_WRITE | OPEN_IO_APPEND);
    assert(fd != -1);
    Buffer buffer;
    initBufferForWriting(&buffer, "A", 1);
    writeDataToFd(fd, &buffer, 0, buffer.size, NULL, NULL, NULL);

    fd = openFile(tempFile, OPEN_IO_WRITE | OPEN_IO_APPEND);
    assert(fd != -1);
    Buffer buffer2;
    initBufferForWriting(&buffer2, "B", 1);
    writeDataToFd(fd, &buffer2, 0, buffer2.size, NULL, NULL, NULL);

    fd = openFile(tempFile, OPEN_IO_WRITE | OPEN_IO_APPEND);
    writeDataToFd(fd, &buffer, 0, buffer.size, NULL, NULL, NULL);

    fd = openFile(tempFile, OPEN_IO_READ);
    Buffer buffer3 = {};
    readDataFromFd(fd, &buffer3, 0, NULL, NULL, NULL);
    assert(bufncmp(&buffer3, "ABA", buffer3.size) == 0);
    freeBufferData(&buffer3);
}

SCUTEST(test_read_write_large_file) {
    int ret = truncate(tempFile, 1 << 20);
    assert(ret != -1);
    int fd = openFile(tempFile, OPEN_IO_READ);
    assert(fd != -1);
    Buffer readBuffer = {};
    readDataFromFd(fd, &readBuffer, 0, NULL, NULL, NULL);
    waitForAllOutstandingEvents();

    fd = openFile(tempFile, OPEN_IO_WRITE);
    writeDataToFd(fd, &readBuffer, 0, readBuffer.size, NULL, NULL, NULL);
    waitForAllOutstandingEvents();
    freeBufferData(&readBuffer);
}

static int completionCount;
static int progressStatus;
static int progressRetValue;
static int trackSuccess(CompletionStatus* status) {
    assert(status->exitStatus == 0);
    completionCount++;
    return 0;
}

static int trackProgress(ProgressStatus* status) {
    TRACE("Tracking progress offset %d len %d\n", status->offset, status->len);
    if (progressStatus)
        assert(progressStatus == status->offset);
    progressStatus = status->offset + status->len;
    return progressRetValue;
}

static int verifyError(CompletionStatus* status) {
    assert(status->exitStatus);
    completionCount++;
    return 0;
}

static void verifyCommandRan(unsigned long len) {
    TRACE("CompletionCount %d Progress %ld; Target Progress %ld\n", completionCount, progressStatus, len);
    assert(completionCount == 1);
    assert(progressStatus == len);
    completionCount = 0;
    progressStatus = 0;
}

SCUTEST_SET_FIXTURE_NO_ARGS(NULL, NULL);

SCUTEST(test_read_from_cmd, .iter = 2) {
    Buffer buffer = {};
    const char* target = "hi\n";
    readFromCmd("echo hi", &buffer, 0, trackProgress, trackSuccess, NULL);
    waitForAllOutstandingEvents();
    verifyCommandRan(3);
    assert(bufcmp(&buffer, target) == 0);
    freeBufferData(&buffer);
}

SCUTEST(test_write_to_cmd) {
    char target[] = "hi\n";
    Buffer buffer = {};
    initBufferForWriting(&buffer, target, strlen(target));
    writeToCmd("grep -q hi", &buffer, 0, buffer.size, trackProgress, trackSuccess, NULL);
    waitForAllOutstandingEvents();
    verifyCommandRan(buffer.size);
}

SCUTEST(test_filter_with_cmd) {
    char target[] = "A\nB\nC\n";
    Buffer buffer;
    initBufferForWriting(&buffer, target, strlen(target));
    Buffer readBuffer = {};
    filterWithCmd("grep B", &buffer, 0, buffer.size, &readBuffer, 0, NULL, trackSuccess, NULL);
    waitForAllOutstandingEvents();
    verifyCommandRan(0);
    assert(bufcmp(&readBuffer, "B\n") == 0);
    freeBufferData(&readBuffer);
}

SCUTEST(test_filter_with_cmd_err) {
    char target[] = "A";
    Buffer buffer;
    initBufferForWriting(&buffer, target, strlen(target));
    Buffer readBuffer = {};
    filterWithCmd("exit 1", &buffer, 0, buffer.size, &readBuffer, 0, NULL, verifyError, NULL);
    waitForAllOutstandingEvents();
    verifyCommandRan(0);
    assert(readBuffer.size == 0);
    freeBufferData(&readBuffer);
}

SCUTEST(test_run_cmd) {
    runCmd("echo", trackSuccess, NULL);
    waitForAllOutstandingEvents();
    verifyCommandRan(0);
}

SCUTEST(test_async_io) {
    Buffer readBuffer = {};
    int ret = readFromCmd("cat /dev/zero", &readBuffer, 0, trackProgress, verifyError, NULL);
    assert(ret == 0);
    while (1) {
        int ret = waitForEvent(-1);
        assert(ret != -1);
        processEvents();
        assert(progressStatus == readBuffer.size);
        if (readBuffer.size > 1 << 13) {
            assert(readBuffer.data[0] == 0);
            assert(memcmp(readBuffer.data, readBuffer.data + 1, readBuffer.size - 1) == 0);
            progressRetValue = -1;

            waitForEvent(-1);
            processEvents();

            while(!(ret = waitForEvent(0)));
            assert(ret > 0);
            processEvents();
            assert(waitForEvent(-1) == -1);
            break;
        }
    }
    freeBufferData(&readBuffer);
}


SCUTEST_SET_FIXTURE_NO_ARGS(NULL, NULL);
SCUTEST(test_wait_for_events_empty) {
    assert(waitForEvent(0) == -1);
}

SCUTEST(test_suspend, .exitCode = SIGALRM) {

    timer_t timerid;
    if (timer_create(CLOCK_MONOTONIC, NULL, &timerid) == -1) {
        exit(-1);
    }
    struct timespec interval = {.tv_sec = 0, .tv_nsec = 1e6} ;
    struct itimerspec timerInfo = {.it_interval = interval, .it_value = interval};
    if (timer_settime(timerid, 0, &timerInfo, NULL) == -1) {
        exit(-1);
    }
    suspendProcess();
    pause();
    exit(1);
}
