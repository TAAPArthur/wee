#include "../context.h"
#include "../events.h"
#include "../functions.h"
#include "../settings.h"
#include "../system.h"
#include "../window.h"
#include "tester.h"

#include "scutest.h"
#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

extern int hitTarget;

int successC(Context* context, Arg arg) {
    hitTarget += arg.c;
    return 0;
}

int successA(Context* context, Arg arg) {
    hitTarget += abs(arg.i);
    return 0;
}

int successM(Context* context, Arg arg) {
    hitTarget *= arg.i;
    return 0;
}

int assertBindingTriggered() {
    assert(bindingTriggered());
    return 0;
}

int testBindings(KeyBinding * bindings, int size, const char* str) {
    Buffer buffer = {.raw = bindings, .size = size};
    addStateBuffer(getGlobalContext(),  BINDING_INDEX, &buffer);
    int ret = processEventString(getGlobalContext(), str, 0);
    removeStateBuffer(getGlobalContext(),  BINDING_INDEX, &buffer);
    return ret;
}

SCUTEST(simple_bindings, .iter = 2) {
    KeyBinding bindings[] = {
        {MODE_NORMAL, 'a', .func = success},
        {MODE_NORMAL, 'a', .func = _i ? fail : NULL},
    };
    testBindings(bindings, sizeof(bindings), "a");
    assert(bindingTriggered());
};

SCUTEST(bindings_with_motion) {
    setupBuffer(getGlobalContext(), "abcd");
    KeyBinding bindings[] = {
        {MODE_NORMAL, 'a', .func = successA, .arg.i=1, .flags  = BF_REQUIRES_MOTION_CMD},
        {MODE_NORMAL, 'b', .func = moveHor, .arg.i=3, .flags  = BF_MOTION_CMD},
    };
    testBindings(bindings, sizeof(bindings), "ab");

    assert(getCursorOffset(getFocusedWindow(getGlobalContext())) == 3);
    assert(bindingTriggered() == 3);
};

SCUTEST(bindings_no_match) {
    KeyBinding bindings[] = {
        {MODE_NORMAL, 'b', .func = fail},
    };
    testBindings(bindings, sizeof(bindings), "a");
    assert(!bindingTriggered());
};

SCUTEST(bindings_no_abort_on_match) {
    KeyBinding bindings[] = {
        {MODE_NORMAL, 'a', .func = success, .flags = BF_NO_ABORT_ON_MATCH},
        {MODE_NORMAL, 'a', .func = success},
    };
    testBindings(bindings, sizeof(bindings), "a");
    assert(bindingTriggered() == 2);
};

SCUTEST(test_bindings_fallthrough) {
    KeyBinding bindings[] = {
        {MODE_NORMAL, 'a', .func = success, .flags = BF_FALLTHROUGH},
        {MODE_NORMAL, 'b', .func = success},
        {MODE_NORMAL, 'c', .func = fail},
    };
    testBindings(bindings, sizeof(bindings), "a");
    assert(bindingTriggered() == 2);
};

SCUTEST(bindings_chain) {
    KeyBinding bindings[] = {
        {MODE_NORMAL, .str = "aa", .func = success},
        {MODE_NORMAL, 'a', .func = fail},
    };
    testBindings(bindings, sizeof(bindings), "aa");
    assert(bindingTriggered());
};

SCUTEST(bindings_chain_fallthrough) {
    KeyBinding bindings[] = {
        {MODE_NORMAL, .str = "a", .func = success, .flags = BF_NO_ABORT_ON_MATCH},
        {MODE_NORMAL, .str = "aa", .func = success, .flags = BF_NO_ABORT_ON_MATCH},
        {MODE_NORMAL, .str = "aaa", .func = success, .flags = BF_NO_ABORT_ON_MATCH},
        {MODE_NORMAL, .str="aa", .func = fail},
    };
    testBindings(bindings, sizeof(bindings), "aaa");
    assert(bindingTriggered() == 3);
};

SCUTEST(bindings_chain_abort) {
    KeyBinding bindings[] = {
        {MODE_NORMAL, 'b', .func = assertBindingTriggered},
        {MODE_NORMAL, .str = "aa", .func = fail},
        {MODE_NORMAL, 'a', .func = success},
    };
    testBindings(bindings, sizeof(bindings), "ab");
    assert(bindingTriggered());
};

SCUTEST(bindings_chain_flush) {
    KeyBinding bindings[] = {
        {MODE_NORMAL, .str = "jjk", .func = fail},
        {MODE_NORMAL, .str = "jj", .func = success},
        {MODE_NORMAL, .str = "j", .func = fail},
    };
    testBindings(bindings, sizeof(bindings), "jjjj");
    assert(bindingTriggered());
};

static int testDigit(int i) {
    return '0' <= i && i <= '9';
}
SCUTEST(bindings_with_filter) {
    KeyBinding bindings[] = {
        {MODE_NORMAL, .testFunc = testDigit, .func = success},
    };
    for(int i = '0'; i <= '9'; i++) {
        char eventString[2] = {i, 0};
        testBindings(bindings, sizeof(bindings), eventString);
    }
    assert(bindingTriggered() == 10);
};

SCUTEST(bindings_with_count) {
    updateCount(getGlobalContext(), (Arg){.i = '9'});
    KeyBinding bindings[] = {
        {MODE_NORMAL, 'a', .func = success, .arg.i=1},
    };
    testBindings(bindings, sizeof(bindings), "a");
    assert(bindingTriggered() == 9);

    testBindings(bindings, sizeof(bindings), "a");
    assert(bindingTriggered() == 10);
};

SCUTEST(bindings_using_event_as_arg) {
    KeyBinding bindings[] = {
        {MODE_NORMAL, 'a', .func = successC, .flags = BF_USE_EVENT_CHAR_AS_ARG},
        {MODE_NORMAL, 'b', .func = successC, .flags = BF_USE_NEXT_EVENT_CHAR_AS_ARG},
        {MODE_NORMAL, 'c', .func = fail},
    };
    testBindings(bindings, sizeof(bindings), "a");
    assert(bindingTriggered() == 'a');
    hitTarget = 0;
    testBindings(bindings, sizeof(bindings), "bc");
    assert(bindingTriggered() == 'c');
};

SCUTEST(bindings_count_replace_arg) {
    KeyBinding bindings[] = {
        {MODE_NORMAL, '4', .func = updateCount, .flags = BF_USE_EVENT_CHAR_AS_ARG},
        {MODE_NORMAL, 'a', .func = successM, .flags = BF_COUNT_REPLACE_ARG},
        {MODE_NORMAL, 'b', .func = successA, .arg.i=2, .flags = BF_COUNT_REPLACE_ARG},
    };
    // ((0 + 2) * 4 + 2) * 4
    testBindings(bindings, sizeof(bindings), "b4ab4a");
    assert(bindingTriggered() == 40);
};

SCUTEST(test_multi_event_commands_with_prefix) {
    const char * commands[] = {
        "4df",
        "d4f",
        "2d2f",
        "d4f",
    };

    KeyBinding bindings[] = {
        {MODE_NORMAL, '2', .func = updateCount, .flags = BF_IGNORE_OTHER_COMMANDS | BF_USE_EVENT_CHAR_AS_ARG},
        {MODE_NORMAL, '4', .func = updateCount, .flags = BF_IGNORE_OTHER_COMMANDS | BF_USE_EVENT_CHAR_AS_ARG},
        {MODE_NORMAL, 'f', .func = jumpToCharInLine,  .flags = BF_USE_NEXT_EVENT_CHAR_AS_ARG | BF_MOTION_INCLUSIVE_CMD},
        {MODE_NORMAL, 'd', .func = successA, .arg.i=1, .flags  = BF_REQUIRES_MOTION_CMD},
    };

    const char* text = "1b.2b..3b...4b....5b.....6b";
    char magic = 'b';
    const char* s = text;
    for (int i = 0; i < 4; i++)
        s = strchr(s, magic) + 1;
    char target = s - text;

    Buffer buffer = {.raw = bindings, .size = sizeof(bindings)};
    addStateBuffer(getGlobalContext(),  BINDING_INDEX, &buffer);
    for(int i = 0; i < LEN(commands); i++) {
        TRACE("Iter %d %d\n", i, target);
        setupBuffer(getGlobalContext(), text);
        setCursorOffset(getFocusedWindow(getGlobalContext()), 0);
        resetBindingTriggered();

        processEventString(getGlobalContext(), commands[i], 0);
        processEventChar(getGlobalContext(), magic, 0);
        assert(bindingTriggered() == target);
    }
    removeStateBuffer(getGlobalContext(),  BINDING_INDEX, &buffer);
}

static int inputFDs[2];

int process_raw_event(Context* context) {
    char buffer[1];
    safe_read(inputFDs[0], buffer, sizeof(buffer));
    Event event = {.key = buffer[0]};
    processEvent(context, &event, 0);
    return 0;
}

void send_keys(const char* c, int len) {
    safe_write(inputFDs[1], c, sizeof(c[0]) * len);
}

void send_string(const char* c) {
    send_keys(c, strlen(c));
}

void send_key(char c) {
    send_keys(&c, 1);
}

SCUTEST(test_event_loop_idle) {
    shutdownInput();
    eventLoop(getGlobalContext());
}

static const char* buffer = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
static void setup() {
    setupContext();
    setupBuffer(getGlobalContext(), buffer);
    assert(pipe(inputFDs) != -1);
    addPollFd(inputFDs[0], EVENT_READ, process_raw_event, NULL, getGlobalContext());
}

static void tearDown() {
    close(inputFDs[0]);
    close(inputFDs[1]);
    while (!isIdle()) {
        if (waitForEvent(-1) > 0)
            processEvents();
    }
    cleanupContext();
}

SCUTEST_SET_FIXTURE(setup, tearDown, .timeout=1);

SCUTEST(no_op) {
    verifyViewData(getFocusedView(getGlobalContext()), buffer);
}

SCUTEST(event_loop) {
    KeyBinding bindings[] = {
        {MODE_NORMAL, .str = "qq", .func = shutdownContext},
    };
    Buffer buffer = {.raw = bindings, .size = sizeof(bindings)};
    addStateBuffer(getGlobalContext(),  BINDING_INDEX, &buffer);
    send_string("qq");
    eventLoop(getGlobalContext());
};

SCUTEST(event_loop_timeout) {
    KeyBinding bindings[] = {
        {MODE_NORMAL, .str = "qqq", .func = fail},
        {MODE_NORMAL, .str = "qq", .func = shutdownContext},
        {MODE_NORMAL, .str = "q", .func = fail},
    };

    ViewSettings * settings = getActiveSettings(getGlobalContext());
    setEnabled(settings, timeout_index, 1);
    setNumericSettings(settings, timeout_index, 100);

    Buffer buffer = {.raw = bindings, .size = sizeof(bindings)};
    addStateBuffer(getGlobalContext(),  BINDING_INDEX, &buffer);
    send_string("qq");
    eventLoop(getGlobalContext());
};
