#include "../context.h"
#include "../defaults.h"
#include "../events.h"
#include "../functions.h"
#include "../render.h"
#include "../screen.h"
#include "../system.h"
#include "../view.h"
#include "../window.h"
#include "tester.h"

#include "scutest.h"
#include <assert.h>
#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include <fcntl.h>
#include <stdlib.h>


static void setup(int i) {
    enableTermboxImpl();
    assert(initContext(getGlobalContext()) == 0);

    openFileInNewWindow(getGlobalContext(), (Arg){.s = "/dev/null"});
    getRenderBackend()->init(getGlobalContext());
}

static void teardown() {
    getRenderBackend()->shutdown(getGlobalContext());
    waitForEvent(-1);
    processEvents();
    cleanupContext();
}

SCUTEST_SET_FIXTURE(setup, teardown, .iter = 1);


SCUTEST(test_events_processing, .flags = SCUTEST_CREATE_TTY) {
    for (int i = 0; i < 256; i++) {
        int ret = write(SCUTEST_getTTYMasterFd(), "\n", 1);
        assert(ret != -1);
    }
    waitForEvent(0);
    processEvents();

    assert(getNumericSettings(getSettings(getFocusedView(getGlobalContext())), event_counter_index) == 256);
}
