#define SCUTEST_IMPLEMENTATION

#include "../container.h"
#include "../memory.h"
#include "../screen.h"
#include "../settings.h"
#include "../system.h"
#include "../view.h"
#include "../window.h"
#include "scutest.h"

#include "tester_basic.h"
#include "tester_screen.h"
#include <assert.h>

static const char* terminal_window[20 * 20];
static char terminal_num_code_points[20 * 20];
static RenderTuple terminal_colors[20 * 20];
static char cursor[2];

static Rect storedBounds;

void storeBounds(Rect rect) {
    storedBounds = rect;
}
static Rect getStoredBounds() {
    return storedBounds;
}

static int fake_present();
static int fake_set_cell(int x, int y, const char *ch, size_t n, RenderTuple tuple) {
    Rect rect = getStoredBounds();
    assert(n);
    assert(ch);
    INFO("Setting cell (%d, %d) to '%c' (%d) width %d; %d %d 0x%X\n", x, y, *ch, *ch, n, tuple.fg, tuple.bg, tuple.style);
    assert( 0 <= x && x < rect.width);
    assert( 0 <= y && y < rect.height);
    assert(y * rect.width + x < LEN(terminal_window));
    assert(!terminal_window[y * rect.width + x]);
    for(int i = x - 1; i >= 0 && !terminal_window[y * rect.width + i]; i--) {
        terminal_window[y * rect.width + i] = " ";
        terminal_num_code_points[y * rect.width + i] = 1;
    }
    terminal_window[y * rect.width + x] = ch;
    terminal_num_code_points[y * rect.width + x] = n;
    terminal_colors[y * rect.width + x] = tuple;
    return 0;
}

static int fake_clear() {
    memset(terminal_window, 0x0, sizeof(terminal_window));
    memset(terminal_num_code_points, 0x0, sizeof(terminal_num_code_points));
    memset(terminal_colors, 0x0, sizeof(terminal_colors));
    memset(cursor, 0xFF, sizeof(cursor));
    return 0;
}

static int fake_present() {
    static int frame;
    Rect rect = getStoredBounds();
    INFO("Frame #%d %d x %d\n", frame++, rect.height, rect.width);
    assert(rect.height * rect.width < LEN(terminal_window));
    for(int y = 0; y < rect.height; y++) {
        INFO("|");
        for(int x = 0; x < rect.width; x++) {
            assert(terminal_num_code_points[y * rect.width + x] == 0 || terminal_window[y * rect.width + x]);
            for (int i = 0; i < terminal_num_code_points[y * rect.width + x]; i++) {
                INFO("%c", terminal_window[y * rect.width + x][i]);
            }
            if (terminal_num_code_points[y * rect.width + x] == 0) {
                INFO(" ");
            }
        }
        INFO("|\n");
    }
    for(int y = 0; y < rect.height; y++)
        INFO("-");
    INFO("-\n");
    return 0;
}

static int fake_set_cursor(int x, int y) {
    INFO("Setting cursor position to (%d, %d)\n", x, y);
    assert(x != -1 && y != -1);
    assert(cursor[0] == -1 && cursor[1] == -1);
    cursor[0] = x;
    cursor[1] = y;
    return 0;
}

static RenderBackend dummyRenderBackend = {
    .init = noop,
    .shutdown = noop,
    .set_cell = fake_set_cell,
    .clear = fake_clear,
    .set_cursor = fake_set_cursor,
    .present = fake_present,
};


void verifyCursorPosition(int x, int y) {
    assert(cursor[0] == x);
    assert(cursor[1] == y);
}

void verifyStyle(int lineNo, int col, StyleTuple tuple) {
    Rect rect = getStoredBounds();
    assert(memcmp(&terminal_colors[lineNo * rect.width + col], &tuple, sizeof(tuple)) == 0);
}


void verifyLineN(int lineNo, const char* str, int strLen) {
    Rect rect = getStoredBounds();
    assert(rect.width);
    if(!str[0]) {
        assert(!terminal_window[lineNo * rect.width]);
        return;
    }
    int index = lineNo * rect.width;
    INFO("%d: '%.*s' (%d)\n", lineNo, strLen, str, strLen);
    int sindex = 0;
    for (int i = 0; i < rect.width && sindex < strLen; i++) {
        const char * c = terminal_window[index + i];
        for (int n = 0; n < terminal_num_code_points[index + i]; n++) {
            INFO("Comparing Col %d byte %d; '%c' (%d) '%c' (%d); width %d\n", i, n, c[n], c[n], str[sindex], str[sindex], terminal_num_code_points[index]);
            assert(str[sindex] == c[n]);
            sindex++;
        }
    }
    assert(strLen == sindex);
}

void verifyLine(int lineNo, const char* str) {
    verifyLineN(lineNo, str, strlen(str));
}

void insertStringIntoView(View* view, const char* str) {
    insertInView(view, str, 0, strlen(str));
}

void verifyViewData(const View* view, const char* target) {
    const Buffer* buffer = getCurrentBuffer(view);
    assert(bufcmp(buffer, target) == 0);
}

void verifyFileMatchesViewData(const View* view, const char* fileName) {
    int fd = openFile(fileName, OPEN_IO_READ);
    assert(fd != -1);
    Buffer buffer = {};
    readDataFromFd(fd, &buffer, 0, NULL, NULL, NULL);
    assert(bufncmp(getCurrentBuffer(view), buffer.data, buffer.size) == 0);
    freeBufferData(&buffer);
    close(fd);
}

static void cleanup() {
    assert(checkForLeaks() == 0);
}
SCUTEST_SET_DEFAULT_FIXTURE(NULL, cleanup, .timeout=1, .flags = (SCUTEST_CREATE_TMP_DIR | SCUTEST_CHECK_FD_LEAK | SCUTEST_CHECK_CHILD_LEAK));

int main(int argc, char *argv[]) {
    atexit(cleanup);
    setRenderBackend(&dummyRenderBackend);
    return runUnitTests();
}
