#ifndef TESTER_BASIC_H
#define TESTER_BASIC_H

#include "../logger.h"
#include "../typedefs.h"

static inline int bufncmp(const Buffer* buffer, const char* target, int len) {
    INFO("Actual vs expected: '%.*s' (%d) '%.*s' (%d)\n", buffer->size, buffer->data, buffer->size, len, target, len);
    if (len != buffer->size)
        return -1;
    return memcmp(buffer->data, target, buffer->size);
}

static inline int bufcmp(const Buffer* buffer, const char* target) {
    return bufncmp(buffer, target, strlen(target));
}

#endif
