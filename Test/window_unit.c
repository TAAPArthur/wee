#include "../memory.h"
#include "../settings.h"
#include "../view.h"
#include "../window.h"
#include "tester_basic.h"

#include "scutest.h"
#include <assert.h>

static AutoCommandArray arr = {};
static View* view;
static Window* win;

static void setup() {
    win = allocWindow(NULL);
    assert(win);
    view = allocView(NULL);
    setPointerSettings(getSettings(view), autocommand_index, &arr);
    assert(view);
    setViewForWindow(win, view);
}

static void cleanup() {
    freeBufferData(&arr);
    freeWindow(win);
    freeView(view);
    assert(checkForLeaks() == 0);
}

SCUTEST_SET_FIXTURE(setup, cleanup);

static inline void appendText(View* view, const char* str) {
    appendToView(view, str, strlen(str));
}

SCUTEST(test_link_view_window) {
    assert(getViewIdOfWindow(win));
}

SCUTEST(test_relink_view_window) {
    View* view2 = allocView(NULL);
    setViewForWindow(win, view2);
    assert(getViewIdOfWindow(win) == getViewId(view2));
    setViewForWindow(win, view);
    assert(getViewIdOfWindow(win) == getViewId(view));
    freeView(view2);
    setViewForWindow(win, view);
    assert(getViewIdOfWindow(win) == getViewId(view));
}

SCUTEST(test_link_subview_window) {
    const char* str = "04";
    const char* target = "01234";
    appendText(view, str);
    setCursorOffset(win, 0);
    setSubViewForWindow(win, 0, 2);
    assert(getCursorOffset(win) == 1);
    insertAtCurrentPosition(win, "123", 3);

    assert(bufcmp(getCurrentBuffer(view), target) == 0);
    setCursorOffset(win, 0);
    assert(getCursorOffset(win) == 1);
    for (int i = 1; i <strlen(str); i++) {
        setCursorOffset(win, i);
        assert(getCursorOffset(win) == i);
    }
}

SCUTEST(test_link_subview_window_single_char) {
    const char* str = "0234";
    const char* target = "01234";
    appendText(view, str);
    setSubViewForWindow(win, 1, 1);
    assert(getCursorOffset(win) == 1);

    for (int i = 0; i < strlen(str); i++) {
        setCursorOffset(win, i);
        assert(getCursorOffset(win) == 1);
    }
    insertAtCurrentPosition(win, "1", 1);

    assert(bufcmp(getCurrentBuffer(view), target) == 0);
    assert(getCursorOffset(win) == 2);
    setCursorOffset(win, 1);
    assert(getCursorOffset(win) != 1);
}

SCUTEST(test_append_delete_replace_subview) {
    insertInView(view, "|||", 0, 3);
    setSubViewForWindow(win, 0, 1);
    Window* win2 = allocWindow(NULL);
    setViewForWindow(win2, view);
    setSubViewForWindow(win2, 1, 2);
    insertInView(view, "a", 1, 1);
    insertInView(view, "b", 3, 1);
    assert(bufcmp(getCurrentBuffer(view), "|a|b|") == 0);
    assert(getCursorPosition(win)[-1] == 'a');
    assert(getCursorPosition(win2)[-1] == 'b');

    setCursorOffset(win, 0);
    assert(*getCursorPosition(win) == 'a');
    setCursorOffset(win2, 0);
    assert(*getCursorPosition(win2) == 'b');

    removeAtCurrentPosition(win, getCurrentBuffer(view)->size);
    assert(bufcmp(getCurrentBuffer(view), "||b|") == 0);

    assert(*getCursorPosition(win2) == 'b');
    replaceAtCurrentPosition(win2, "c", 1, getCurrentBuffer(view)->size);
    assert(bufcmp(getCurrentBuffer(view), "||c|") == 0);

    setCursorOffset(win2, 0);
    replaceAtCurrentPosition(win2, "d", 1, 1);
    assert(bufcmp(getCurrentBuffer(view), "||d|") == 0);

    setCursorOffset(win2, 0);
    replaceAtCurrentPosition(win2, "ef", 2, 1);
    assert(bufcmp(getCurrentBuffer(view), "||ef|") == 0);

    freeWindow(win2);
}

SCUTEST(test_cursor_position_empty) {
    assert(getCursorOffset(win) == 0);
    setCursorOffset(win, 1);
    assert(getCursorOffset(win) == 0);
    setCursorPosition(win, NULL);
    assert(getCursorOffset(win) == 0);
}

SCUTEST(test_get_set_cursor_position) {
    const char* str = "abcd";
    appendText(view, str);
    for (int i = 0; i < strlen(str); i++) {
        setCursorOffset(win, i);
        assert(getCursorOffset(win) == i);
        assert(*getCursorPosition(win) == str[i]);
        setCursorPosition(win, getCursorPosition(win));
        assert(getCursorOffset(win) == i);
        assert(*getCursorPosition(win) == str[i]);
    }
}

SCUTEST(test_cursor_position_with_pending_insertion) {
    const char* str = "abcd";
    appendText(view, str);
    assert(getCursorOffset(win) == strlen(str));
    assert(getCursorPosition(win)[-1] == str[strlen(str) - 1]);

    appendToInsertionBuffer(win, "e", 1);
    assert(getCursorOffset(win) == strlen(str));
    commitInsertion(win);
    assert(getCursorOffset(win) == strlen(str) + 1);
    assert(getCursorPosition(win)[-1] == 'e');
}


SCUTEST(test_cursor_position_out_of_bounds) {
    const char* str = "abcd";
    appendText(view, str);
    setCursorOffset(win, 0);
    assert(getCursorOffset(win) == 0);
    assert(getCursorPosition(win)[0] == str[0]);
    setCursorOffset(win, strlen(str));
    assert(getCursorOffset(win) == strlen(str));

    setCursorOffset(win, strlen(str) + 1);
    assert(getCursorOffset(win) == strlen(str));
    setCursorOffset(win, - 1);
    assert(getCursorOffset(win) == strlen(str));
}

SCUTEST(test_update_offsets_in_view) {
    const char* text = "1234";
    appendText(view, text);
    Window* win[strlen(text)];
    for(int i = 0; i < LEN(win); i++) {
        win[i] = allocWindow(NULL);
        setViewForWindow(win[i], view);
        setCursorOffset(win[i], i);
        assert(getCursorOffset(win[i]) == i);
    }

    appendToInsertionBuffer(win[0], "0", 1);
    commitInsertion(win[0]);
    for(int i = 0; i < LEN(win); i++) {
        assert(getCursorOffset(win[i]) == i + 1);
        assert(*getCursorPosition(win[i]) == '1' + i);
    }

    appendToInsertionBuffer(win[2], "3", 1);
    commitInsertion(win[2]);

    for(int i = 0; i < LEN(win); i++) {
        assert(getCursorOffset(win[i]) == i + 1 + (i >= 2));
        assert(*getCursorPosition(win[i]) == '1' + i);
    }

    for(int i = 0; i < LEN(win); i++) {
        freeWindow(win[i]);
    }
}

SCUTEST(test_copy_text) {
    insertAtCurrentPosition(win, "abc", 3);
    appendEffectiveRangeToInsertionBuffer(win, 0, 2);
    assert(bufcmp(getInsertionBuffer(win), "ab") == 0);
    commitInsertion(win);
    assert(bufcmp(getCurrentBuffer(view), "abcab") == 0);

    setCursorOffset(win, 0);
    appendToInsertionBuffer(win, "AB", 2);
    appendEffectiveRangeToInsertionBuffer(win, 1, 2);
    assert(bufcmp(getInsertionBuffer(win), "ABB") == 0);
    commitInsertion(win);
    assert(bufcmp(getCurrentBuffer(view), "ABBabcab") == 0);
}
