#include "../autocommands.h"
#include "../context.h"
#include "../defaults.h"
#include "../events.h"
#include "../functions.h"
#include "../options.h"
#include "../system.h"
#include "../view.h"
#include "../window.h"
#include "tester.h"

#include "scutest.h"
#include <assert.h>
#include <stdarg.h>
#include <string.h>
#include <unistd.h>

// utility functions

static Window* win;
static void setup() {
    setupContextWithDefaultSettings(1);
    addDefaults(getGlobalContext());

    setDimensions(getGlobalContext(), 40, 8);
    win = getFocusedWindow(getGlobalContext());
    changeWorkingDirectory(getTempDirForTest());
}

SCUTEST_SET_FIXTURE(setup, cleanupContext, .flags = (SCUTEST_CREATE_TMP_DIR));

SCUTEST(test_autoindent) {
    setEnabled(getWindowSettings(win), autoindent_index, 1);
    const char* text = "A\nB\n\tB.1\nB.2\n B.2.a\nB.2.b";
    const char* expectedText = "A\nB\n\tB.1\n\tB.2\n\t B.2.a\n\t B.2.b";
    for (int i = 0; i < strlen(text); i++) {
        appendToInsertionBuffer(win, text + i, 1);
    }
    assert(bufcmp(getInsertionBuffer(win), expectedText) == 0);
    commitInsertion(win);
    verifyViewData(getFocusedView(getGlobalContext()), expectedText);
}

SCUTEST(test_autoindent_extend_all_white_space) {
    setEnabled(getWindowSettings(win), autoindent_index, 1);
    insertAtCurrentPosition(win, "\t", 1);
    const char* text = "\n\n\t";
    const char* expectedText = "\t\n\t\n\t\t";
    for (int i = 0; i < strlen(text); i++) {
        appendToInsertionBuffer(win, text + i, 1);
    }
    assert(bufcmp(getInsertionBuffer(win), expectedText + 1) == 0);
    commitInsertion(win);
    verifyViewData(getFocusedView(getGlobalContext()), expectedText);
}

SCUTEST(test_snap_to_visible) {
    setEnabled(getWindowSettings(win), snap_to_visible_index, 1);
    setCursorPosition(win, 0);
    assert(getCursorOffset(win) == 0);
    const char* text = "Hello\nWorld\n";
    for (int i = 0; i < strlen(text); i++) {
        appendToInsertionBuffer(win, text + i, 1);
        assert(getCursorOffset(win) == 0);
        assert(getEffectiveOffsetOfPosition(win, getCursorPosition(win)) == i + 1);
    }
    commitInsertion(win);
    assert(getCursorOffset(win) == strlen(text));
    int newLineIndex = strchr(text, '\n') - text;
    setCursorOffset(win, newLineIndex);
    assert(getCursorOffset(win) == newLineIndex - 1);
    setCursorOffset(win, strlen(text));
    assert(getCursorOffset(win) == strlen(text) - 2);
}

SCUTEST(test_snap_to_visible_consecutive_newlines) {
    setEnabled(getWindowSettings(win), snap_to_visible_index, 1);
    const char* text = "A\n\nB";
    insertAtCurrentPosition(win, text, strlen(text));
    for (int i = 0; i < strlen(text); i++) {
        setCursorOffset(win, i);
        if (i == 1) {
            assert(getCursorOffset(win) == i - 1);
        } else {
            assert(getCursorOffset(win) == i);
        }
    }
}

static int test_auto_write_helper() {
    return requestShutdown(getGlobalContext());
}

SCUTEST(test_autowrite) {
    shutdownInput();
    static AutocommandCallback callbacks[] = {
        {AUTO_CMD_SAVE, .contextFunc = test_auto_write_helper},
    };
    STATIC_BUFFER(callbackBuffer, callbacks);
    setName(getFocusedView(getGlobalContext()), "test");
    addStateBuffer(getGlobalContext(), AUTOCOMMAND_INDEX, &callbackBuffer);
    setEnabled(getWindowSettings(win), autowrite_index, 1);
    setNumericSettings(getWindowSettings(win), timeout_index, 0);
    insertAtCurrentPosition(win, "A", 1);
    eventLoop(getGlobalContext());
}

SCUTEST(test_expand_tab) {
    setNumericSettings(getGlobalSettings(getGlobalContext()), mode_index, MODE_INSERT);
    setEnabled(getWindowSettings(win), expandtab_index, 1);
    setNumericSettings(getWindowSettings(win), expandtab_index, 2);
    appendTab(getGlobalContext());
    verifyViewData(getFocusedView(getGlobalContext()), "");
    commitInsertion(win);
    verifyViewData(getFocusedView(getGlobalContext()), "  ");
    assert(getCursorOffset(win) == getNumericSettings(getWindowSettings(win), expandtab_index));
}
