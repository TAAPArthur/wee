#include "../addresses.h"
#include "../utility.h"
#include "scutest.h"
#include "tester.h"

#include <assert.h>
#include <string.h>
#include <unistd.h>

static const char text[] = ""
"0\n"     // [0,  1]
"12\n"    // [2,  4]
"345\n"   // [5,  8]
"6789\n"; // [9, 13]
static void setup() {
    setupContext();
    setupBuffer(getGlobalContext(), text);
}

SCUTEST_SET_FIXTURE_NO_ARGS(setup, cleanupContext);

static struct AddrTestInfo {
    const char* addr;
    long addrLen;
    long offset1;
    long offset2;
    int linemode;
} addrInfo[] = {
    // [0, 1]
    {".", 1, 0, -1, 0},
    {".", 1, 0, -1, 1},
    // [2, 3]
    {"$", 1, LEN(text) - 2, -1, 0},
    {"$", 1, 9, -1, 1},
    // [4, 9]
    {"0", 1, 0, -1, 0},
    {"0", 1, 0, -1, 1},
    {"1", 1, 1, -1, 0},
    {"1", 1, 2, -1, 1},
    {"4", 1, 4, -1, 0},
    {"4", 1, 9, -1, 1},
    // [10, 13]
    {"+1", 2, 1, -1, 0},
    {"+1", 2, 2, -1, 1},
    {"-1 2", 4, 1, -1, 0},
    {"-1 2", 4, 2, -1, 1},
    // [14, 17]
    {"1,2", 3, 1, 2},
    {"1;2", 3, 1, 2},
    {"1;+2", 4, 1, 3},
    {"1; +2", 5, 1, 3},
    // [18, 20]
    {"1 1 1, 2 2", 10, 3, 4},
    {"1 1 1; 2 2", 10, 3, 4},
    {"1 1 1; +2 2", 11, 3, 7},
    // [21, 22]
    {"%", 1, 0, LEN(text) - 2, 0},
    {"%", 1, 0, LEN(text) - 2, 1},
    // [23, 25]
    {"", 0, -1, -1},
    {"  ", 2, -1, -1},
    {"abc", 0, -1, -1},
    // [26, 29]
    {".;.", 3, 0, 1, 1},
    {"$;$", 3, 9, LEN(text) - 2, 1},
    {",$", 2, 0, LEN(text) - 2, 1},
    {".,", 2, 0, 1, 1},
};

SCUTEST(test_address_parsing, .iter = LEN(addrInfo)) {
    struct AddrTestInfo * info = &addrInfo[_i];
    Window* win = getFocusedWindow(getGlobalContext());
    const char* addr1 = NULL, * addr2 = NULL;
    setEnabled(getWindowSettings(win), linemode_index, info->linemode);
    const char* rem = parseAddresses(win, info->addr, &addr1, &addr2);
    assert(rem);
    assert(info->addr <= rem && rem <= info->addr + strlen(info->addr));
    assert(rem - info->addr == info->addrLen);
    if (!addr1) {
        assert(info->offset1 == -1);
    } else {
        assert(info->offset1 == getEffectiveOffsetOfPosition(win, addr1));
    }
    if (!addr2) {
        assert(info->offset2 == -1);
    } else {
        assert(info->offset2 == getEffectiveOffsetOfPosition(win, addr2));
    }
}
