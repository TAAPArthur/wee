#include "../buffer.h"
#include "../context.h"
#include "../memory.h"
#include "tester_basic.h"

#include "scutest.h"

#include <assert.h>

static Buffer buffer;
static void cleanup() {
    freeBufferData(&buffer);
    assert(checkForLeaks() == 0);
}

SCUTEST_SET_FIXTURE_NO_ARGS(NULL, cleanup)

SCUTEST(test_append_to_buffer) {
    const char * str = "ABCD";
    insertInBuffer(&buffer, str, 0, strlen(str));
    assert(bufcmp(&buffer, str) == 0);
}

SCUTEST(test_append_to_buffer_null) {
    insertInBuffer(&buffer, NULL, 0, 10);
    assert(buffer.size == 10);
}

SCUTEST(test_append_to_buffer_multi) {
    const char * str = "ABCD";
    for(int i = strlen(str) - 1; i >=0; i--)
        insertInBuffer(&buffer, str + i, 0, 1);
    assert(bufcmp(&buffer, str) == 0);
}

SCUTEST(test_append_to_buffer_middle) {
    insertInBuffer(&buffer, "B", 0, 1);
    insertInBuffer(&buffer, "C", 0, 1);
    insertInBuffer(&buffer, "A", 1, 1);
    assert(bufcmp(&buffer, "CAB") == 0);
}

SCUTEST(test_delete_from_buffer) {
    const char * str = "ABCDE";
    insertInBuffer(&buffer, str, 0, strlen(str));
    assert(bufcmp(&buffer, str) == 0);
    assert(1 == removeFromBuffer(&buffer, 0, 1));
    assert(bufcmp(&buffer, str + 1) == 0);

    assert(1 == removeFromBuffer(&buffer, 1, 1));
    assert(bufcmp(&buffer, "BDE") == 0);

    assert(2 == removeFromBuffer(&buffer, 0, 2));
    assert(bufcmp(&buffer, str + 4) == 0);

    assert(1 == removeFromBuffer(&buffer, 0, 1));
    assert(bufcmp(&buffer, "") == 0);
}

const char* replacements[] = { "abcd", "1", "ABCDEFG", };

SCUTEST(test_replace_in_buffer,.iter = LEN(replacements)) {
    const char * str =  "ABCD";
    const char* str2 = replacements[_i];
    insertInBuffer(&buffer, str, 0, strlen(str));
    assert(bufcmp(&buffer, str) == 0);
    replaceInBuffer(&buffer, str2, 0, strlen(str2), strlen(str));
    assert(bufcmp(&buffer, str2) == 0);
}

SCUTEST(test_grow_buffer) {
    growBuffer(&buffer, 1 << 20);
    assert(buffer.maxSize >= 1 << 20);
    memset(buffer.data, 0x0, 1 << 20);
}

SCUTEST(test_default_max_size) {
    buffer.maxSize = 4096;
    insertInBuffer(&buffer, NULL, 0, buffer.maxSize);
    memset(buffer.data, 0x0, buffer.size);
}

SCUTEST(test_array_list_insert) {
    char s[] = "123";
    appendElement(&buffer, s + 1);
    appendElement(&buffer, s + 2);
    insertElementAt(&buffer, s, 0);
    for(int i = 0; s[i]; i++) {
        assert(getElementAt(&buffer, i) == s + i);
    }
}

SCUTEST(test_for_each) {
    const char * str = "ABCDEFG";
    insertInBuffer(&buffer, str, 0, strlen(str));
    int i = 0;
    FOR_EACH(char*, c, &buffer, 0) {
        assert(*c == str[i++]);
    }
    assert(i == strlen(str));

    FOR_EACH_REV(char*, c, &buffer, 0) {
        assert(*c == str[--i]);
    }
    assert(i == 0);
}

SCUTEST(test_for_each_delete) {
    const char * str = "ABCD";
    insertInBuffer(&buffer, str, 0, strlen(str));
    FOR_EACH_REV(char*, c, &buffer, 0) {
        assert(*c);
        removeFromBuffer(&buffer, 0, 1);
    }
    assert(buffer.size == 0);
}
