#include "../settings.h"
#include "../system.h"
#include "../view.h"
#include "tester_basic.h"

#include "scutest.h"
#include <assert.h>
#include <string.h>
#include <unistd.h>

static View* fakeAddView() {
    View* view = allocView(NULL);
    return view;
}


SCUTEST(test_alloc_free_view) {
    View* view = fakeAddView();
    assert(view);
    freeView(view);
}

SCUTEST(test_view_sanity) {
    View* view = fakeAddView();
    assert(getCurrentBuffer(view));
    assert(getSettings(view));
    for(int i = 0; i < NUM_VIEW_STATE; i++) {
        assert(getViewStateBuffer(view, i));
        assert(getViewStateBuffer(view, i)->size == 0);
    }
    freeView(view);
}

SCUTEST(test_view_name) {
    View* view = fakeAddView();
    assert(!getName(view));
    const char* name = "name";
    setName(view, name);
    assert(getName(view));
    // The returned name should be a copy
    assert(name != getName(view));
    assert(strcmp(name, getName(view)) == 0);
    freeView(view);
}


#define FOO "foo"
#define BAR "bar"
#define BAZ "baz"

SCUTEST(test_append_delete_replace_view) {
    View* view = fakeAddView();
    assert(insertInView(view, FOO, 0, strlen(FOO)) == 0);
    assert(bufcmp(getCurrentBuffer(view), FOO) == 0);
    assert(insertInView(view, BAR, 0, strlen(BAR)) == 0);
    assert(bufcmp(getCurrentBuffer(view), BAR FOO) == 0);

    assert(insertInView(view, BAZ, strlen(BAR) + strlen(FOO), strlen(BAZ)) != -1);
    assert(bufcmp(getCurrentBuffer(view), BAR FOO BAZ) == 0);

    deleteFromView(view, strlen(BAR), strlen(FOO));
    assert(bufcmp(getCurrentBuffer(view), BAR BAZ) == 0);

    assert(replaceInView(view, BAZ, 0, strlen(BAZ), strlen(BAR)) != -1);
    assert(bufcmp(getCurrentBuffer(view), BAZ BAZ) == 0);
    freeView(view);
}

static void waitForAllOutstandingEvents() {
    while(!isIdle()) {
        int ret = waitForEvent(10);
        assert(ret != -1);
        if (ret) {
            processEvents();
        }
    }
}

SCUTEST(test_load_cmd_into_view) {
    View* view = fakeAddView();
    appendCmdIntoView(view, "echo hi");
    waitForAllOutstandingEvents();
    appendCmdIntoView(view, "echo !");
    waitForAllOutstandingEvents();
    appendCmdIntoView(view, "echo bye");
    waitForAllOutstandingEvents();
    assert(bufcmp(getCurrentBuffer(view), "hi\n!\nbye\n") == 0);
    freeView(view);
}

SCUTEST_SET_FIXTURE(NULL, NULL, .flags = (SCUTEST_CREATE_TMP_DIR));

SCUTEST(test_save_load) {
    View* view = fakeAddView();
    setEnabled(getSettings(view), autocreate_index, 1);
    changeWorkingDirectory(getTempDirForTest());
    assert(insertInView(view, FOO, 0, strlen(FOO)) == 0);
    assert(!getName(view));
    assert(saveView(view, "foo.txt") == 0);
    assert(!getName(view)); // name should not have changed

    appendFileIntoView(view, "foo.txt");
    assert(bufcmp(getCurrentBuffer(view), FOO FOO) == 0);
    assert(!getName(view)); // name should not have changed
    assert(saveView(view, "foo_foo.txt") == 0);

    assert(replaceInView(view, BAR, 0, strlen(BAR), strlen(FOO) * 2) != -1);
    appendFileIntoView(view, "foo_foo.txt");
    assert(bufcmp(getCurrentBuffer(view), BAR FOO FOO) == 0);

    setName(view, "bar_foo_foo.txt");
    assert(saveView(view, NULL) == 0);
    // TODO more verification
    freeView(view);
}
