#include "../context.h"
#include "../functions.h"
#include "../undo.h"
#include "../view.h"
#include "../window.h"

#include "scutest.h"

#include <assert.h>
#include <string.h>

static View* view;
static AutoCommandArray autoCommands;
static void setup() {
    view = allocView(NULL);
    setPointerSettings(getSettings(view), autocommand_index, &autoCommands);
    appendElementConst(&autoCommands, getDefaultFileTypeAutoCommands());
}

static void cleanup() {
    freeView(view);
    freeBufferData(&autoCommands);
}

SCUTEST_SET_FIXTURE(setup, cleanup);

SCUTEST(test_auto_detect_ft) {
    setName(view, "test.c");
    assert(getNumericSettings(getSettings(view), filetype_index) == FT_C);
    setName(view, "test.cpp");
    assert(getNumericSettings(getSettings(view), filetype_index) == FT_CPP);
}
