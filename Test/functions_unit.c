#include "../context.h"
#include "../defaults.h"
#include "../functions.h"
#include "../utility.h"
#include "../view.h"
#include "../window.h"
#include "tester.h"

#include "scutest.h"

#include <assert.h>
#include <string.h>

// Movement functions
static void setup() {
    setupContext();
    setEnabled(getActiveSettings(getGlobalContext()), window_index, 1);
}

SCUTEST_SET_FIXTURE(setup, cleanupContext);

SCUTEST(test_page_up_down) {
    setDimensions(getGlobalContext(), 2, 2);
    const char* text = "a\nb\nc\nd\ne";
    setupBuffer(getGlobalContext(), text);

    Window * win = getFocusedWindow(getGlobalContext());
    for (int i = 0; i < 2; i++) {
        movePage(getGlobalContext(), (Arg){.i=UP});
        redraw(getGlobalContext());
        assert(getCurrentLineNumber(win) == 0);
        verifyLine(0, "a");
        verifyLine(1, "b");

    }

    movePage(getGlobalContext(), (Arg){.i=DOWN});
    redraw(getGlobalContext());
    verifyLine(0, "c");
    verifyLine(1, "d");
    assert(getCurrentLineNumber(win) == 2);

    for (int i = 0; i < 2; i++) {
        movePage(getGlobalContext(), (Arg){.i=DOWN});
        redraw(getGlobalContext());
        assert(getCurrentLineNumber(win) == 4);
        verifyLine(0, "e");
        verifyLine(1, "");
    }

    movePage(getGlobalContext(), (Arg){.i=UP});
    redraw(getGlobalContext());
    assert(getCurrentLineNumber(win) == 3);
    verifyLine(0, "c");
    verifyLine(1, "d");

    movePage(getGlobalContext(), (Arg){.i=UP});
    redraw(getGlobalContext());
    assert(getCurrentLineNumber(win) == 1);
    verifyLine(0, "a");
    verifyLine(1, "b");
}

SCUTEST(move_cursor_hor_eol) {
    char * str = "1\n2";
    setupBuffer(getGlobalContext(), str);
    assert(getCursorOffset(getFocusedWindow(getGlobalContext())) == 0);
    for (int i = 0; i < 2; i++) {
        moveHor(getGlobalContext(), (Arg) {.i = 1});
        assert(getCursorOffset(getFocusedWindow(getGlobalContext())) == 1);
    }
    for (int i = 0; i < 2; i++) {
        moveHor(getGlobalContext(), (Arg) {.i = -1});
        assert(getCursorOffset(getFocusedWindow(getGlobalContext())) == 0);
    }
    setCursorOffset(getFocusedWindow(getGlobalContext()), 2);
    for (int i = 0; i < 4; i++) {
        moveHor(getGlobalContext(), (Arg) {.i = i < 2 ? -1 : 1});
        assert(getCursorOffset(getFocusedWindow(getGlobalContext())) == 2);
    }
    insertAtCurrentPosition(getFocusedWindow(getGlobalContext()), "\n", 1);
    setCursorOffset(getFocusedWindow(getGlobalContext()), 2);
    for (int i = 0; i < 2; i++) {
        moveHor(getGlobalContext(), (Arg) {.i = 1});
        assert(getCursorOffset(getFocusedWindow(getGlobalContext())) == 3);
    }
    setCursorOffset(getFocusedWindow(getGlobalContext()), strlen(str));
    for (int i = 0; i < 2; i++) {
        moveHor(getGlobalContext(), (Arg) {.i = 1});
        assert(getCursorOffset(getFocusedWindow(getGlobalContext())) == strlen(str));
    }
}

SCUTEST(move_cursor_ver_hor_empty, .iter=2) {
    int num = 3;
    for(int i = 0; i < num * 2; i++) {
        assert(getCursorOffset(getFocusedWindow(getGlobalContext())) == 0);
        moveHor(getGlobalContext(), (Arg){.i = i < num ? 1 : -1});
        moveVert(getGlobalContext(), (Arg){.i = i < num ? 1 : -1});
    }
}

SCUTEST(move_cursor_ver_hor, .iter=2) {
    char * str = _i ? "123" : "1\n2\n3";
    int num = 3;
    int (*func)() = _i ? moveHor : moveVert;

    setupBuffer(getGlobalContext(), str);
    for(int i = 0; i < num; i++) {
        assert(*getCursorPosition(getFocusedWindow(getGlobalContext())) == '1' + i);
        func(getGlobalContext(), (Arg){.i = 1});
    }

    assert(getCursorOffset(getFocusedWindow(getGlobalContext())) == strlen(str) - 1);

    for(int i = num - 1; i >= 0 ; i--) {
        assert(*getCursorPosition(getFocusedWindow(getGlobalContext())) == '1' + i);
        func(getGlobalContext(), (Arg){.i = -1});
    }
    assert(*getCursorPosition(getFocusedWindow(getGlobalContext())) == '1');
}

SCUTEST(move_cursor_hor_and_vert) {
    setupBuffer(getGlobalContext(),
            "12\n"
            "abcd\n"
            "ABCDEF");
    assert(*getCursorPosition(getFocusedWindow(getGlobalContext())) == '1');

    moveVert(getGlobalContext(), (Arg){.i = 1});
    assert(*getCursorPosition(getFocusedWindow(getGlobalContext())) == 'a');

    for (int i = 0; i < 3; i++) {
        moveHor(getGlobalContext(), (Arg){.i = 1});
    }
    assert(*getCursorPosition(getFocusedWindow(getGlobalContext())) == 'd');

    moveVert(getGlobalContext(), (Arg){.i = -1});
    assert(*getCursorPosition(getFocusedWindow(getGlobalContext())) == '\n');
    assert(getCursorOffset(getFocusedWindow(getGlobalContext())) == 2);

    moveVert(getGlobalContext(), (Arg){.i = 1});
    assert(*getCursorPosition(getFocusedWindow(getGlobalContext())) == 'd');

    moveVert(getGlobalContext(), (Arg){.i = 1});
    assert(*getCursorPosition(getFocusedWindow(getGlobalContext())) == 'D');

    for (int i = 0; i < 3; i++) {
        moveVert(getGlobalContext(), (Arg){.i = -1});
    }
    assert(*getCursorPosition(getFocusedWindow(getGlobalContext())) == '\n');
}

SCUTEST(move_cursor_vert_remeber_col) {
    const char* text = "1\n23\n456\n";
    setupBuffer(getGlobalContext(), text);
    setCursorOffset(getFocusedWindow(getGlobalContext()), strlen(text) - 1);
    assert(getCursorPosition(getFocusedWindow(getGlobalContext()))[-1] == '6');

    moveVert(getGlobalContext(), (Arg){.i = -1});
    assert(*getCursorPosition(getFocusedWindow(getGlobalContext())) == '\n');
    assert(getCursorOffset(getFocusedWindow(getGlobalContext())) == 4);

    moveVert(getGlobalContext(), (Arg){.i = -1});
    assert(*getCursorPosition(getFocusedWindow(getGlobalContext())) == '\n');
    assert(getCursorOffset(getFocusedWindow(getGlobalContext())) == 1);

    moveVert(getGlobalContext(), (Arg){.i = 1});
    assert(*getCursorPosition(getFocusedWindow(getGlobalContext())) == '\n');
    assert(getCursorOffset(getFocusedWindow(getGlobalContext())) == 4);

    moveVert(getGlobalContext(), (Arg){.i = 1});
    redraw(getGlobalContext());
    assert(getCursorPosition(getFocusedWindow(getGlobalContext()))[-1] == '6');
    assert(getCursorOffset(getFocusedWindow(getGlobalContext())) == strlen(text) - 1);
}

static void resize(int rows, int columns) {
    setDimensions(getGlobalContext(), columns, rows);
    retile(getGlobalContext());
}

SCUTEST(move_viewport_vert) {
    char * text = "1\n2\n3";
    setupBuffer(getGlobalContext(), text);
    resize(1, 2);
    redraw(getGlobalContext());
    verifyLine(0, "1");
    moveVert(getGlobalContext(), (Arg){.i = 1});
    redraw(getGlobalContext());
    verifyLine(0, "2");

    for(int i = 0; i < 2; i++) {
        moveVert(getGlobalContext(), (Arg){.i = 1});
        redraw(getGlobalContext());
        verifyLine(0, "3");
    }
    moveVert(getGlobalContext(), (Arg){.i = -1});
    redraw(getGlobalContext());
    verifyLine(0, "2");
    for(int i = 0; i < 2; i++) {
        moveVert(getGlobalContext(), (Arg){.i = -1});
        redraw(getGlobalContext());
        verifyLine(0, "1");
    }
}

SCUTEST(move_viewport_hor) {
    char * text = "123";
    setupBuffer(getGlobalContext(), text);
    resize(1, 1);
    for(int i = 0; i < strlen(text); i++) {
        redraw(getGlobalContext());
        verifyLineN(0, text + i, 1);
        moveHor(getGlobalContext(), (Arg){.i = 1});
    }
    setCursorOffset(getFocusedWindow(getGlobalContext()), strlen(text));
    redraw(getGlobalContext());
    verifyLine(0, "");
    moveHor(getGlobalContext(), (Arg){.i = -1});
    for(int i = strlen(text) - 1; i >=0; i--){
        redraw(getGlobalContext());
        verifyLineN(0, text + i, 1);
        moveHor(getGlobalContext(), (Arg){.i = -1});
    }
}

SCUTEST(test_jump_col, .iter = 2) {
    const char * text = _i ? "01" : "01\n";
    Window * win = getFocusedWindow(getGlobalContext());
    setupBuffer(getGlobalContext(), text);
    for(int i = 0; i < strlen(text); i++) {
        jumpToCol(getGlobalContext(), (Arg){.i = i});
        assert(getCursorOffset(win) == i);
        // jumpToCol(getGlobalContext(), (Arg){.i = -i});
        // assert(getCursorOffset(win) == strlen(text) - i - 1);
    }
}

SCUTEST(test_jump_to) {
    const char * text = "012345678901";
    setupBuffer(getGlobalContext(), text);
    Window * win = getFocusedWindow(getGlobalContext());
    assert(getCursorOffset(win) == 0);

    jumpToCharInLine(getGlobalContext(), (Arg){.c = '1'});
    assert(getCursorOffset(win) == 1);

    for(int i = 0; i < 2; i++) {
        jumpToCharInLine(getGlobalContext(), (Arg){.c = '1'});
        assert(getCursorOffset(win) == 11);
    }
    for(int i = 0; i < 2; i++) {
        jumpToCharInLineBackwards(getGlobalContext(), (Arg){.c = '1'});
        assert(getCursorOffset(win) == 1);
    }

}

SCUTEST(test_jump_before_after) {
    const char * text = "01230123";
    setupBuffer(getGlobalContext(), text);
    Window * win = getFocusedWindow(getGlobalContext());

    for(int i = 0; i < 2; i++) {
        jumpToBeforeCharInLine(getGlobalContext(), (Arg){.c = '2'});
        assert(getCursorOffset(win) == 1);
    }
    for(int i = 0; i < 2; i++) {
        jumpToBeforeCharInLine(getGlobalContext(), (Arg){.c = '0'});
        assert(getCursorOffset(win) == 3);
    }

    for(int i = 0; i < 2; i++) {
        jumpToAfterCharInLineBackwards(getGlobalContext(), (Arg){.c = '0'});
        TRACE("HERE %d %d\n", i, getCursorOffset(win));
        assert(getCursorOffset(win) == 1);
    }
}

SCUTEST(test_jumpToLastNonNewLineCharInLine) {
    const char * text = "1\n2b\n3cd";
    setupBuffer(getGlobalContext(), text);
    Window * win = getFocusedWindow(getGlobalContext());
    for(int i = 0; i < 2; i++) {
        jumpToLastNonNewLineCharInLine(getGlobalContext(), (Arg){.i = 0});
        assert(getCursorOffset(win) == 0);
    }
}

SCUTEST(test_jump_word, .iter = 2) {
    const char * texts[] = {
        "word1 w0rd2  w_rd3 4word 5wwww !@#$%",
        "w$rd1 w0!d2  w~rd3 '=<<< (_.*) !@#$%",
    };
    int bigword = _i;
    const char * text = texts[bigword];
    int wordStarts[] = {0, 6 , 13, 19, 25, 31};
    int wordEnds[] =   {4, 10, 17, 23, 29, 35};
    setupBuffer(getGlobalContext(), text);

    Window * win = getFocusedWindow(getGlobalContext());
    for (int i = 0; i < LEN(wordStarts); i++){
        assert(getCursorOffset(win) == wordStarts[i]);
        Arg arg = {.index = bigword ? bigword_regex_index : word_regex_index, .dir = 1, .end = 0};
        jumpToBoundary(getGlobalContext(), arg);
    }
    assert(getCursorOffset(win) == strlen(text) - 1);
    jumpToCol(getGlobalContext(), (Arg){.i = 0});

    for (int i = 0; i < LEN(wordStarts); i++){
        Arg arg = {.index = bigword ? bigword_regex_index : word_regex_index, .dir = 1, .end = 1};
        jumpToBoundary(getGlobalContext(), arg);
        assert(getCursorOffset(win) == wordEnds[i]);
    }
}

// Edit functions
SCUTEST(test_append_delete_char) {
    char text[] = "123";
    setupBuffer(getGlobalContext(), text);

    resize(1, 10);
    assert(*getCursorPosition(getFocusedWindow(getGlobalContext())) == '1');
    appendChar(getGlobalContext(), (Arg){.c = 'a'});
    setMode(getGlobalContext(), (Arg){.m = MODE_NORMAL});
    redraw(getGlobalContext());
    verifyLine(0, "a123");
    assert(*getCursorPosition(getFocusedWindow(getGlobalContext())) == '1');

    deleteChar(getGlobalContext(), (Arg)-1);
    setMode(getGlobalContext(), (Arg){.m = MODE_NORMAL});
    redraw(getGlobalContext());
    verifyLine(0, "123");
}

SCUTEST(test_delete_empty, .iter=2) {
    deleteChar(getGlobalContext(), (Arg){.i = _i ? 1 : -1});
}

SCUTEST(test_replace_char) {
    char text[] = "123";
    setupBuffer(getGlobalContext(), text);
    resize(1, 10);
    assert(*getCursorPosition(getFocusedWindow(getGlobalContext())) == '1');
    redraw(getGlobalContext());
    verifyLine(0, text);
    replaceChar(getGlobalContext(), (Arg){.c = 'a'});
    redraw(getGlobalContext());
    verifyLine(0, "a23");
    assert(*getCursorPosition(getFocusedWindow(getGlobalContext())) == '2');
}

SCUTEST(test_delete_line) {
    const char *text = "1\n22\n333\n4444";
    setupBuffer(getGlobalContext(), text);
    deleteCurrentLine(getGlobalContext(), (Arg){.i = 1});
    verifyViewData(getFocusedView(getGlobalContext()), strchr(text, '2'));
    for (int i = 0; i <  2; i++) {
        deleteCurrentLine(getGlobalContext(), (Arg){.i = 1});
    }
    verifyViewData(getFocusedView(getGlobalContext()), strchr(text, '4'));
    deleteCurrentLine(getGlobalContext(), (Arg){.i = 1});
    verifyViewData(getFocusedView(getGlobalContext()), "");
}

SCUTEST(test_append_delete_newline) {
    char text[] = "123";
    setupBuffer(getGlobalContext(), text);
    resize(2, 10);
    assert(*getCursorPosition(getFocusedWindow(getGlobalContext())) == '1');
    moveHor(getGlobalContext(), (Arg){.i = 1});
    assert(*getCursorPosition(getFocusedWindow(getGlobalContext())) == '2');
    assert(0 == getCurrentLineNumber(getFocusedWindow(getGlobalContext())));
    appendChar(getGlobalContext(), (Arg){.c = '\n'});
    setMode(getGlobalContext(), (Arg){.m = MODE_NORMAL});
    assert(1 == getCurrentLineNumber(getFocusedWindow(getGlobalContext())));

    redraw(getGlobalContext());
    verifyLine(0, "1");
    verifyLine(1, "23");
    assert(*getCursorPosition(getFocusedWindow(getGlobalContext())) == '2');

    deleteChar(getGlobalContext(), (Arg)-1);
    setMode(getGlobalContext(), (Arg){.m = MODE_NORMAL});
    redraw(getGlobalContext());
    verifyLine(0, "123");
}

SCUTEST(test_append_delete_multi_window) {
    char text[] = "1\n2a\n3bc";
    setupBuffer(getGlobalContext(), text);
    Window* win1 = getFocusedWindow(getGlobalContext());
    View * view = getFocusedView(getGlobalContext());
    Window* win2 = addWindow(getGlobalContext());
    setViewForWindow(win2, view);

    assert(*getCursorPosition(win1) == '1');
    assert(*getCursorPosition(win2) == '1');
    setCursorOffset(win2, 2);
    assert(*getCursorPosition(win2) == '2');

    deleteChar(getGlobalContext(), (Arg){.i = 2});
    assert(*getCursorPosition(win1) == '2');
    assert(*getCursorPosition(win2) == '2');

    setCursorPosition(win2, getCursorPosition(win2) + 3);
    assert(*getCursorPosition(win2) == '3');

    appendChar(getGlobalContext(), (Arg){.c = '_'});
    setMode(getGlobalContext(), (Arg)MODE_NORMAL);
    assert(*getCursorPosition(win1) == '2');
    assert(*getCursorPosition(win2) == '3');
    deleteChar(getGlobalContext(), (Arg){.i = -2});
    assert(*getCursorPosition(win1) == '2');
    assert(*getCursorPosition(win2) == '3');

    deleteChar(getGlobalContext(), (Arg){.i = 1 + strlen(text)});
    assert(!getCursorPosition(win1));
    assert(!getCursorPosition(win2));
}

SCUTEST(test_delete_multi_window_intersection, .iter = 2) {
    char text[] = "1\n2a\n";
    setupBuffer(getGlobalContext(), text);
    Window* win1 = getFocusedWindow(getGlobalContext());
    View * view = getFocusedView(getGlobalContext());
    Window* win2 = addWindow(getGlobalContext());
    setViewForWindow(win2, view);
    int offset = _i;
    assert(getCursorOffset(win1) == 0);
    setCursorPosition(win1, getCursorPosition(win1) + offset);
    setCursorPosition(win2, getCursorPosition(win2) + offset + 2);
    deleteChar(getGlobalContext(), (Arg){.i = 4});
    assert(getCursorOffset(win1) == offset);
    assert(getCursorOffset(win2) == offset);
}

SCUTEST(test_set_mode, .iter = 2) {
    char* text = "123";
    setupBuffer(getGlobalContext(), text);
    resize(1, 10);
    setMode(getGlobalContext(), (Arg)MODE_INSERT);
    appendChar(getGlobalContext(), (Arg){.c = 'a'});
    if(_i) {
        redraw(getGlobalContext());
        verifyLine(0, "a123");
    }
    setMode(getGlobalContext(), (Arg)MODE_NORMAL);
    redraw(getGlobalContext());
    verifyLine(0, "a123");
}

// File functions

SCUTEST(test_open_file_in_new_window) {
    resize(1, 10);
    const char* fileName = "file_doesn't_exit";
    openFileInNewWindow(getGlobalContext(), (Arg){.s = fileName});
    verifyLine(0, "");
}

// Register functions

SCUTEST(test_yank_paste) {
    setupBuffer(getGlobalContext(), "01234");
    yankRange(getGlobalContext(), (Arg){.i = 2});
    pasteText(getGlobalContext(), (Arg){.i = 0});
    assert(getCursorOffset(getFocusedWindow(getGlobalContext())) == 2);
    verifyViewData(getFocusedView(getGlobalContext()), "0101234");
    yankCurrentLine(getGlobalContext(), (Arg){.i = 0});
    pasteText(getGlobalContext(), (Arg){.i = 0});
    setCursorOffset(getFocusedWindow(getGlobalContext()), 2);
    verifyViewData(getFocusedView(getGlobalContext()), "01010123401234");
}
