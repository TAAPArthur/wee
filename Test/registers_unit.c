#include "../buffer.h"
#include "../registers.h"
#include "tester.h"

#include "scutest.h"

#include <assert.h>
#include <string.h>

static const char * text = "0123456789";

static void setup() {
}

static void cleanup() {
    clearAllRegisterData();
}

SCUTEST_SET_FIXTURE(setup, cleanup);

static void yankTest(Register reg) {
    TRACE("yankTest register '%c'\n", reg);
    int ret = yankText(reg, text, strlen(text));
    assert(ret != -1);
    assert(bufcmp(getRegisterBufferConst(reg), text) == 0);
    assert(bufcmp(getRegisterBufferConst(REGISTER_DEFAULT), text) == 0);
    ret = yankText(reg, text + 1, 1);
    assert(ret != -1);
    assert(bufncmp(getRegisterBufferConst(reg), text + 1, 1) == 0);
    assert(bufncmp(getRegisterBufferConst(REGISTER_DEFAULT), text + 1, 1) == 0);
}

static void deleteTest(Register reg) {
    TRACE("deleteTest register '%c'\n", reg);
    int ret = storeDeletedText(reg, text, strlen(text));
    assert(ret != -1);
    assert(bufcmp(getRegisterBufferConst(reg), text) == 0);
    assert(bufcmp(getRegisterBufferConst(REGISTER_DEFAULT), text) == 0);
    ret = storeDeletedText(reg, text + 1, 1);
    assert(ret != -1);
    assert(bufncmp(getRegisterBufferConst(reg), text + 1, 1) == 0);
    assert(bufncmp(getRegisterBufferConst(REGISTER_DEFAULT), text + 1, 1) == 0);
}

SCUTEST(test_register0) {
    yankTest(REGISTER_DEFAULT);
}

SCUTEST(test_register1) {
    deleteTest(REGISTER_DEFAULT);
}

SCUTEST(test_registerN) {
    for (int i = 0; i < strlen(text); i++) {
        int ret = storeDeletedText(REGISTER_DEFAULT, text + i, 1);
        assert(ret != -1);
    }
    const char * str = text + strlen(text) - 1;
    for (int reg = REGISTER_1; reg <= REGISTER_NUM_LAST; reg++) {
        assert(bufncmp(getRegisterBufferConst(reg), str--, 1) == 0);
    }
}

SCUTEST(test_namedRegisters) {
    for (int reg = REGISTER_A; reg <= REGISTER_NAMED_LAST; reg++) {
        yankTest(reg);
        deleteTest(reg);
    }
}

SCUTEST(test_namedRegistersAppend) {
    for (int reg = REGISTER_A_APPEND; reg <= REGISTER_NAMED_APPEND_LAST; reg++) {
        for (int i = 0; i < strlen(text); i++) {
            int ret = yankText(reg, text + i, 1);
            assert(ret != -1);
        }
        assert(bufcmp(getRegisterBufferConst(reg), text) == 0);
    }
}
