#include "../autocommands.h"
#include "../context.h"
#include "../view.h"
#include "../window.h"
#include "tester.h"

#include "scutest.h"

#include <assert.h>
#include <string.h>

int ranCount = 0;
int callback() {
    ranCount++;
    return 0;
}

SCUTEST(test_register_autocommand, .iter  = NUM_AUTO_COMMAND_TYPES) {
    AutoCommandArray arr = {};
    AutoCommandType type = _i;
    AutocommandParams params = {};
    if (_i <= LAST_VIEW_AUTO_CMD) {
        params.viewParams.view = (void*)1;
    } else {
        params.contextParams.context = (void*)1;
    }


    AutocommandCallback callbacks[] = {
        {type, callback},
        {!type, fail}
    };
    Buffer buffer = {.raw = callbacks, .size = sizeof(callbacks)};


    assert(runAutoCommands(&arr, type, params) == 0);
    assert(ranCount == 0);

    appendElement(&arr, &buffer);

    assert(runAutoCommands(&arr, type, params) == 0);
    assert(ranCount == 1);
    freeBufferData(&arr);
}
