#include "../memory.h"
#include "../settings.h"
#include "../undo.h"
#include "../view.h"
#include "tester_basic.h"

#include "scutest.h"

#include <assert.h>
#include <string.h>


static void verifyData(View * view, const char* target) {
    const Buffer* buffer = getCurrentBuffer(view);
    assert(bufcmp(buffer, target) == 0);
}

int appendCharToActiveView(View * view, int pos, const char c) {
    return insertInView(view, &c, pos, 1);
}

static AutoCommandArray arr = {};
View* view;
static void setup() {
    view = allocView(NULL);
    setPointerSettings(getSettings(view), autocommand_index, &arr);

    appendElementConst(&arr, getUndoAutocommands());
}

static void cleanup() {
    freeView(view);
    freeBufferData(&arr);
    assert(checkForLeaks() == 0);
}

SCUTEST_SET_FIXTURE(setup, cleanup);

SCUTEST(test_undo_no_op) {
    undoLast(view);
}

SCUTEST(test_redo_no_op) {
    redoLast(view);
    appendCharToActiveView(view, 0, 'A');
    redoLast(view);
}

SCUTEST(test_delete_from_view_empty) {
    deleteFromView(view, 0, 1);
}

SCUTEST(test_replace_in_view_empty) {
    replaceInView(view, 0, 0, 0, 1);
}

SCUTEST(test_undo_redo_append_delete, .iter = 2) {
    const char* text = "ab1c";
    int len = 0;
    for(int i = 0; i < strlen(text); i++) {
        int ret = appendCharToActiveView(view, i, text[i]);
        len++;
        incrementSettings(getSettings(view), event_counter_index);
    }
    verifyData(view, text);
    const Buffer* buffer = getCurrentBuffer(view);

    for(int i = strlen(text) - 1; i >= 0; i--) {
        assert(undoLast(view) != -1);
        assert(0 == bufncmp(buffer, text, i));
    }
    for (int n = 0; n < _i + 1; n++) {
        verifyData(view, "");

        for(int i = 1; i <= strlen(text); i++) {
            assert((n == 0 ? redoLast : undoLast)(view) != -1);
            assert(0 == bufncmp(buffer, text, i));
            assert(0 == memcmp(buffer->data, text, buffer->size));
        }
        verifyData(view, text);

        for(int i = strlen(text) - 1; i >= 0; i--) {
            deleteFromView(view, i, 1);
            incrementSettings(getSettings(view), event_counter_index);
        }
    }
}

SCUTEST(test_undo_redo_many) {
    int num = 256;
    char target[num + 1];
    target[num] = 0;
    for(int i = 0; i < num; i++) {
        target[i] = '0' + i % 10;
        assert(bufncmp(getCurrentBuffer(view), target, i) == 0);
        appendCharToActiveView(view, i, target[i]);
    }
    incrementSettings(getSettings(view), event_counter_index);
    verifyData(view, target);
    undoLast(view);
    verifyData(view, "");
    redoLast(view);
    verifyData(view, target);
}
