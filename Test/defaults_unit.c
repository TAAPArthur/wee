#include "../autocommands.h"
#include "../context.h"
#include "../defaults.h"
#include "../events.h"
#include "../functions.h"
#include "../system.h"
#include "../view.h"
#include "../window.h"
#include "tester.h"

#include "scutest.h"
#include <assert.h>
#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

// utility functions

static void setup() {
    setupContextWithDefaultSettings(1);
    addDefaults(getGlobalContext());

    setDimensions(getGlobalContext(), 40, 8);

    for (int i = 0; i < NUM_STATE_INDEX; i++) {
            assert(getStateBufferConst(getGlobalContext(), i)->size);
    }
    changeWorkingDirectory(getTempDirForTest());
}

SCUTEST_SET_FIXTURE(setup, cleanupContext, .flags = (SCUTEST_CREATE_TMP_DIR));

SCUTEST(test_reachable_bindings) {
    int num = 0;
    FOR_EACH_BUFFER(const KeyBinding*, binding, getStateBufferConst(getGlobalContext(), BINDING_INDEX)) {
        num++;
    }
    KeyBinding arr[num];
    num = 0;
    FOR_EACH_BUFFER(const KeyBinding*, binding, getStateBufferConst(getGlobalContext(), BINDING_INDEX)) {
        arr[num] = *binding;
        arr[num].func = noop;
        num++;
    }
    Buffer arrBuffer = {.raw = arr, .size = sizeof(arr)};
    clearStateBuffer(getGlobalContext(), BINDING_INDEX);
    addStateBuffer(getGlobalContext(),  BINDING_INDEX, &arrBuffer);

    KeyBinding motion_arr[] = {
        {MODE_ANY, '0', .func = noop,  .flags = BF_MOTION_CMD},
        {MODE_ANY, ANY_CHAR, .func = fail},
    };
    Buffer motionBuffer = {.raw = motion_arr, .size = sizeof(motion_arr)};

    for(int i = 0; i < num; i++) {
        TRACE("Testing reachability of binding %d of %d; key '%c' (%d) '%s'; func %p\n", i, num, arr[i].key, arr[i].key, arr[i].str, arr[i].testFunc);
        int passed = 0;

        setNumericSettings(getWindowSettings(getFocusedWindow(getGlobalContext())), filetype_index, arr[i].fileType);
        arr[i].func = success;
        for(int m = 0; m < 32 && !passed; m++) {
            Mode mode = 1<<m;
            if ( !(mode & arr[i].mode))
                continue;
            TRACE("Testing with mode %d\n", m);
            setMode(getGlobalContext(), (Arg){.m = mode});
            if (arr[i].testFunc) {
                for(int c =0; c < 255; c++) {
                    if (arr[i].testFunc(c)) {
                        processEventChar(getGlobalContext(), c, 1);
                        break;
                    }
                }
            }
            else if(arr[i].str) {
                processEventString(getGlobalContext(), arr[i].str, 0);
            } else {
                processEventChar(getGlobalContext(), arr[i].key, 1);
            }

            if (arr[i].flags & BF_REQUIRES_MOTION_CMD) {
                addStateBuffer(getGlobalContext(),  BINDING_INDEX, &motionBuffer);
                processEventChar(getGlobalContext(), motion_arr[0].key, 1);
                removeStateBuffer(getGlobalContext(),  BINDING_INDEX, &motionBuffer);
            }
            if (arr[i].flags & BF_USE_NEXT_EVENT_CHAR_AS_ARG) {
                processEventChar(getGlobalContext(), '0', 1);
            }
            if (bindingTriggered()) {
                passed = 1;
            }
        }
        arr[i].func = noop;
        assert(passed);
        resetBindingTriggered();
    }
}

SCUTEST(test_input_simple) {
    const char input[] = {'i', 'a', ESC};
    const char* target = "a";
    processEventString(getGlobalContext(), input, 0);
    verifyViewData(getFocusedView(getGlobalContext()), target);
}

SCUTEST(test_input_newline) {
    const char input[] = {'i', '1', CTRL('m'), '2', ESC};
    const char* target = "1\n2";
    processEventString(getGlobalContext(), input, 0);
    verifyViewData(getFocusedView(getGlobalContext()), target);
}

SCUTEST(test_input_no_newline_delete, .iter = 2) {
    setEnabled(getWindowSettings(getFocusedWindow(getGlobalContext())), snap_to_visible_index, 0);
    const char input[] = {'i', 'a', 'b', 'c', '\n', 'd', 'e', 'f', ESC, 'k', 0};
    const char* target = "abc\ndef";
    processEventString(getGlobalContext(), input, 0);
    assert(*getCursorPosition(getFocusedWindow(getGlobalContext())) == '\n');
    int deleteKey[] = {'x', DELETE};
    processEventChar(getGlobalContext(), deleteKey[_i], 0);
    verifyViewData(getFocusedView(getGlobalContext()), target);
}

SCUTEST(test_input) {
    const char input[] = {'i', 'a', 'b', 'b', 'c', CTRL('m'), 'e', 'f', 'g', ESC, 'k', 'h', 'X'};
    const char* target = "abc\nefg";
    processEventString(getGlobalContext(), input, 0);
    verifyViewData(getFocusedView(getGlobalContext()), target);
}

SCUTEST(test_replace_input) {
    setupBuffer(getGlobalContext(), "123456");
    const char* input = "Rabcd";
    const char* target = "abcd56";
    processEventString(getGlobalContext(), input, 0);
    verifyViewData(getFocusedView(getGlobalContext()), target);
    assert(*getCursorPosition(getFocusedWindow(getGlobalContext())) == '5');
}

SCUTEST(test_undo_redo) {
    const char input[] = {'i', 'a', 'b', 'c', ESC, 'i', 'd', 'e', ESC};
    processEventString(getGlobalContext(), input, 0);
    verifyViewData(getFocusedView(getGlobalContext()), "abcde");
    processEventChar(getGlobalContext(), 'u', 0);
    verifyViewData(getFocusedView(getGlobalContext()), "abc");
    processEventChar(getGlobalContext(), CTRL('r'), 0);
    verifyViewData(getFocusedView(getGlobalContext()), "abcde");
}

SCUTEST(test_multi_event_commands) {
    setupBuffer(getGlobalContext(), "abcdabcdabcd");
    processEventString(getGlobalContext(), "2dfb", 0);
    verifyViewData(getFocusedView(getGlobalContext()), "cdabcd");
}

SCUTEST(test_commands) {
    const char input[] = ":setall linenumber=hi\n";
    processEventString(getGlobalContext(), input, 0);
    WindowSettings * settings = getWindowSettings(getFocusedWindow(getGlobalContext()));
    assert(strcmp(getStringSettings(settings, linenumberf_index), "hi") == 0);
}

SCUTEST(test_command_history) {
    const char input[] = ":setall linenu=hi\n:setall linenu=hi2\n:gg\n";
    processEventString(getGlobalContext(), input, 0);
    WindowSettings * settings = getWindowSettings(getFocusedWindow(getGlobalContext()));
    assert(strcmp(getStringSettings(settings, linenumberf_index), "hi") == 0);
    // Modify a previously run command
    const char input2[] = ":gg$xXibye\n";
    processEventString(getGlobalContext(), input2, 0);
    assert(strcmp(getStringSettings(settings, linenumberf_index), "bye") == 0);
    // Verify that the original command is unchanged in the history
    const char input3[] = ":gg\n";
    processEventString(getGlobalContext(), input3, 0);
    assert(strcmp(getStringSettings(settings, linenumberf_index), "hi") == 0);
}

SCUTEST(test_quit) {
    const char input[] = ":q\n";
    processEventString(getGlobalContext(), input, 0);
    assert(isShuttingDown(getGlobalContext()));
}

SCUTEST(test_delete_paste) {
    const char* target = "012";
    processEventChar(getGlobalContext(), 'i', 0);
    processEventString(getGlobalContext(), target, 0);
    processEventChar(getGlobalContext(), '', 0);
    verifyViewData(getFocusedView(getGlobalContext()), target);
    processEventString(getGlobalContext(), "dd", 0);
    verifyViewData(getFocusedView(getGlobalContext()), "");
    processEventString(getGlobalContext(), "p", 0);
    verifyViewData(getFocusedView(getGlobalContext()), target);
}

static void processKeyEvents(Context* context, int n, ...) {
    va_list args;
    va_start(args, n);
    for(int i = 0; i < n; i++) {
        const char* s = va_arg(args, const char*);
        processEventString(context, s, 0);
    }
    va_end(args);
}

SCUTEST(test_open_file_and_draw) {
    const char* text = "#include <stdio.h>\nint main (int argc, char *argv[]) {\n\tprintf(\"Hello world\\n\");\n\treturn 0;\n}";
    const char* fileName = "test.c";
    openFileInCurrentWindow(getGlobalContext(), (Arg){.s = fileName});
    assert(getNumericSettings(getActiveSettings(getGlobalContext()), filetype_index) == FT_C);
    processKeyEvents(getGlobalContext(), 2, "i", text);
    setMode(getGlobalContext(), (Arg)MODE_NORMAL);
    verifyViewData(getFocusedView(getGlobalContext()), text);

    // Checking for correct tab handling is hard, so just don't here
    setEnabled(getActiveSettings(getGlobalContext()), tabstop_index, 0);
    setEnabled(getActiveSettings(getGlobalContext()), display_cntrl_index, 0);
    redraw(getGlobalContext());
    const char* line = text;
    int i = 0;
    do {
        const char* endOfLineMarker = strchr(line, '\n');
        if (endOfLineMarker) {
            verifyLineN(i, line, endOfLineMarker - line);
        } else {
            verifyLine(i, line);
        }
        i++;
        line = endOfLineMarker;
    } while(line++);
}

SCUTEST(test_auto_create_on_write) {
    const char* fileName = "test.c";
    openFileInCurrentWindow(getGlobalContext(), (Arg){.s = fileName});
    assert(openFile(fileName, OPEN_IO_READ) == -1);
    save(getGlobalContext(), (Arg){});
    int fd = openFile(fileName, OPEN_IO_READ);
    assert(fd != -1);
    close(fd);
}

SCUTEST(test_workflow) {
    const char* text = "abcd";
    const char* fileName = "test.txt";
    processKeyEvents(getGlobalContext(), 2, "i", text);
    setMode(getGlobalContext(), (Arg)MODE_NORMAL);
    verifyViewData(getFocusedView(getGlobalContext()), text);

    processKeyEvents(getGlobalContext(), 3, ":w ", fileName, "\n");
    assert(getNumberOfWindows(getGlobalContext()) == 1);
    assert(!getName(getFocusedView(getGlobalContext())));
    verifyViewData(getFocusedView(getGlobalContext()), text);
    verifyFileMatchesViewData(getFocusedView(getGlobalContext()), fileName);

    processKeyEvents(getGlobalContext(), 3, ":e ", fileName, "\n");
    assert(getNumberOfWindows(getGlobalContext()) == 1);
    assert(strcmp(getName(getFocusedView(getGlobalContext())), fileName) == 0);

    assert(getNumericSettings(getActiveSettings(getGlobalContext()), filetype_index) == FT_PLAIN_TEXT);

    verifyViewData(getFocusedView(getGlobalContext()), text);
    const char* text2 = "efgh";
    processKeyEvents(getGlobalContext(), 2, "$a\n", text2);
    setMode(getGlobalContext(), (Arg)MODE_NORMAL);
    redraw(getGlobalContext());
    verifyLine(0, text);
    verifyLine(1, text2);

    processKeyEvents(getGlobalContext(), 3, ":e ", fileName, "\n");
    verifyViewData(getFocusedView(getGlobalContext()), text);
}

SCUTEST(test_small_window, .iter=2) {
    if (_i)
        setupBuffer(getGlobalContext(), "1234\n1234");
    int num = 4;
    for(int width = 0; width < num; width++) {
        for(int height = 0; height < num; height++) {
            setDimensions(getGlobalContext(), width, height);
            retile(getGlobalContext());
            redraw(getGlobalContext());
        }
    }
}

SCUTEST(test_workflow_cmd_abort) {
    setDimensions(getGlobalContext(), 2, 2);
    setEnabled(getActiveSettings(getGlobalContext()), show_info_bar_index, 1);
    processEventChar(getGlobalContext(), ':', 0);
    for(int i = 0; i < 3; i++)
        processEventChar(getGlobalContext(), CTRL('H'), 0);
    processEventString(getGlobalContext(), "echo hi\n", 0);

    retile(getGlobalContext());
    redraw(getGlobalContext());
    verifyLine(1, "hi");
}

SCUTEST(test_workflow_quit_all) {
    for(int i = 0; i < 10; i ++) {
        openFileInNewWindow(getGlobalContext(), (Arg){});
    }

    processEventString(getGlobalContext(), ":qall\n", 0);
    assert(isShuttingDown(getGlobalContext()));
}

SCUTEST(test_workflow_switch_files) {
    changeWorkingDirectory(getTempDirForTest());
    const char* fileNames[] = {"test.c", "test2.c"};
    const char* text = "int i = 0;";
    setupBuffer(getGlobalContext(), text);
    for (int i = 0; i < LEN(fileNames); i++) {
        processKeyEvents(getGlobalContext(), 3, ":w ", fileNames[i], "\n");
    }
    const char * cmds[] = {":e ", ":tabe "};
    for(int n = 0; n < LEN(cmds); n++) {
        for (int i = 0; i < LEN(fileNames); i++) {
            processKeyEvents(getGlobalContext(), 3, cmds[n], fileNames[i], "\n");
            redraw(getGlobalContext());
            verifyLine(0, text);
            assert(getNumericSettings(getActiveSettings(getGlobalContext()), filetype_index) == FT_C);
        }
    }
}

SCUTEST(test_workflow_movement) {
    setupBuffer(getGlobalContext(), "1\n23\n456\n7890");
    char ends[] = {'1', '3','6', '0'};

    processEventString(getGlobalContext(), "$", 0);
    for(int i =0 ; i< LEN(ends); i++) {
        assert(*getCursorPosition(getFocusedWindow(getGlobalContext())) == ends[i]);
        processEventString(getGlobalContext(), "j$", 0);
    }
    for(int i = LEN(ends) - 1 ; i >= 0; i--) {
        assert(*getCursorPosition(getFocusedWindow(getGlobalContext())) == ends[i]);
        processEventString(getGlobalContext(), "k", 0);
    }
    for(int i =0 ; i < LEN(ends); i++) {
        assert(*getCursorPosition(getFocusedWindow(getGlobalContext())) == ends[i]);
        processEventString(getGlobalContext(), "j", 0);
    }
}

SCUTEST(test_workflow_open_multiple_quit) {
    processKeyEvents(getGlobalContext(), 3, ":tabe\n", ":q\n", ":q\n");
}
SCUTEST(test_read_write_large_file) {
    processEventString(getGlobalContext(), "i", 0);
    int size = 1 << 16;
    for (int i = 0; i < size - 2; i++) {
        processEventChar(getGlobalContext(), 'a', 0);
    }
    processEventChar(getGlobalContext(), 'b', 0);
    processEventChar(getGlobalContext(), 'c', 0);
    processEventChar(getGlobalContext(), ESC, 0);
    assert(getCursorOffset(getFocusedWindow(getGlobalContext())) == size);

    processEventString(getGlobalContext(), ":w test.txt\n", 0);
    processEventString(getGlobalContext(), ":e test.txt\n", 0);

    while(!isIdle()) {
        int ret = waitForEvent(10);
        assert(ret != -1);
        if (ret) {
            processEvents();
        }
    }
    // Jump to end of buffer
    processEventString(getGlobalContext(), "G$", 0);
    assert(*getCursorPosition(getFocusedWindow(getGlobalContext())) == 'c');
    assert(getCursorOffset(getFocusedWindow(getGlobalContext())) == size - 1);
}
