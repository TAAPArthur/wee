#include "../autocommands.h"
#include "../context.h"
#include "../defaults.h"
#include "../functions.h"
#include "../settings.h"
#include "../setup.h"
#include "../system.h"
#include "tester.h"

#include "scutest.h"
#include <assert.h>
#include <stdarg.h>
#include <stdlib.h>
#include <unistd.h>

SCUTEST_SET_FIXTURE(shutdownInput, verifyNoLeaks);

static void simpleOnStartup(Context* context) {}


static int runSimpleHelper(const char* arg ) {
    const char * argv[] = {"wee", arg, "/dev/null"};
    return parseArgsAndRuninitWee(3, argv, getGlobalContext(), simpleOnStartup);
}

SCUTEST(test_dash_h) {
    assert(runSimpleHelper("-h") == 0);
}

SCUTEST(test_unknown_arg) {
    assert(runSimpleHelper("-"));
}

SCUTEST(test_batch_mode, .flags = SCUTEST_CREATE_TTY) {
    assert(runSimpleHelper("-s") == 0);
}

SCUTEST(test_assume_batch_mode_when_no_tty, .rflags = SCUTEST_CREATE_TTY) {
    assert(runSimpleHelper("-e") == 0);
}

struct {
    char arg;
    const char* value;
    SettingsIndex index;
    char enabled;
} args[] = {
    {'R', NULL, readonly_index, 1},
    {'w', "10", window_index, 10},
};

SCUTEST(test_basic_args, .iter = LEN(args)) {
    char option[3] = {'-'};
    int i = _i;
    option[1] = args[i].arg;
    const char * argv[] = {"wee", option, "/dev/null", NULL, NULL};
    int len = 3;
    if (args[i].value) {
        argv[3] = argv[2];
        argv[2] = args[i].value;
        len++;
    }
    assert(parseArgsAndRuninitWee(len, argv, getGlobalContext(), simpleOnStartup) == 0);
}

SCUTEST_SET_FIXTURE(NULL, verifyNoLeaks);

static int shutdownInputWrapper() {
    shutdownInput();
    return 0;
}
static AutocommandCallback closeInputOnIdle[] = {
    {AUTO_CMD_IDLE, .contextFunc = shutdownInputWrapper},
};
STATIC_BUFFER(callbackBuffer, closeInputOnIdle);

static void testOnStartup(Context* context) {
    setNumericSettings(getGlobalSettings(context), timeout_index, 0);
    addStateBuffer(context, AUTOCOMMAND_INDEX, &callbackBuffer);
}

SCUTEST(test_no_args) {
    const char * argv[] = {"wee", NULL};
    assert(parseArgsAndRuninitWee(1, argv, getGlobalContext(), testOnStartup) == 0);
}
