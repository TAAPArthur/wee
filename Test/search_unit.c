#include "../buffer.h"
#include "../enum_conversion.h"
#include "../search.h"

#include "scutest.h"

#include <assert.h>

static void setup(int i) {
    if (i == NUM_REGEX_IMPL) {
        return;
    }
    if (setRegexBackend(i)) {
        SCUTEST_skipTest("Regex backend %s is not supported", toRegexImplStr(i));
    }
}

SCUTEST_SET_FIXTURE(setup, NULL, .iter = NUM_REGEX_IMPL + 1);


SCUTEST(test_regex_alloc_free) {
    void * regex = allocRegex("a", 0);
    assert(regex);
    freeRegex(regex);
}

static const char* text = "abaacaaa";
static void veriyMatchResults(int n, RegexMatch match[n]) {
    for (int i = 1; i < n; i++) {
        assert(match[i].start <= match[i].end);
        for (int k = i + 1; k < n; k++) {
            assert(match[i].end <= match[k].start);
        }
    }
}

SCUTEST(test_regex_match) {
    void * regex = allocRegex("a", 0);
    RegexMatch match;
    int n = regexMatch(regex, text, 0, strlen(text), 1, &match, 0);
    assert(n == 1);
    veriyMatchResults(n, &match);
    assert(match.start == 0);
    assert(match.end == 1);
    freeRegex(regex);
}

SCUTEST(test_regex_capture_group) {
    void * regex = allocRegex("(a).(a+)", 0);
    RegexMatch match[4];
    int n = regexMatch(regex, text, 0, strlen(text), LEN(match), match, 0);
    assert(n == 3);
    veriyMatchResults(n, match);
    assert(match[0].start == 0);
    assert(match[0].end == 4);
    assert(match[1].start == 0);
    assert(match[1].end == 1);
    assert(match[2].start == 2);
    assert(match[2].end == 4);
    freeRegex(regex);
}

SCUTEST(test_regex_limit) {
    void * regex = allocRegex("aaa", 0);
    RegexMatch match[4];
    assert(1 == regexMatch(regex, text, 0, strlen(text), LEN(match), match, 0));

    assert(0 == regexMatch(regex, text, 0, 3, LEN(match), match, 0));

    assert(1 == regexMatch(regex, text, strstr(text, "aaa") - text, strlen(text), LEN(match), match, 0));

    freeRegex(regex);
}

SCUTEST(test_regex_search_across_newlines) {
    void * regex = allocRegex("^.*$", 0);
    RegexMatch match[4];
    const char * text = "Hello\nWorld\n";
    int n = regexMatch(regex, text, 0, strlen(text), LEN(match), match, 0);
    assert(n == 1);
    veriyMatchResults(n, match);
    assert(match[0].start == 0);
    assert(match[0].end == strlen(text));
    freeRegex(regex);
}

SCUTEST(test_regex_embedded_null_byte) {
    if (getRegexBackend() == REGEX_IMPL_POSIX || getRegexBackend() == REGEX_IMPL_BSD) {
        SCUTEST_skipTest("Regex backend %s does not support embedded NULL bytes", toRegexImplStr(getRegexBackend()));
        return;
    }
    void * regex = allocRegex("^.*$", 0);
    RegexMatch match[4];
    const char text[] = {'H', 0, 'W', 0};
    int n = regexMatch(regex, text, 0, LEN(text), LEN(match), match, 0);
    assert(n == 1);
    veriyMatchResults(n, match);
    assert(match[0].start == 0);
    assert(match[0].end == LEN(text));
    freeRegex(regex);
}

SCUTEST(test_regex_find_all) {
    char text[16];
    memset(text, 'A', sizeof(text));
    void * regex = allocRegex("A", 0);

    RegexMatch matches[LEN(text)];
    int n = regexFindAll(regex, text, 0, strlen(text), LEN(matches), matches, 0);
    for (int i = 0; i < n; i++) {
        TRACE("HERE [%d %d)\n", matches[i].start, matches[i].end);
    }
    TRACE("HERE %d %d\n", n, LEN(text));
    assert(n == LEN(text));
    veriyMatchResults(n, matches);
    text[0] = 'B';
    n = regexFindAll(regex, text, 0, strlen(text), LEN(matches), matches, 0);
    assert(n == LEN(text) - 1);
    veriyMatchResults(n, matches);

    freeRegex(regex);
}

SCUTEST(test_regex_find_all_with_capture_group) {
    char text[16];
    memset(text, 'A', sizeof(text));
    void * regex = allocRegex("A(AA)", 0);

    RegexMatch matches[LEN(text)];
    int n = regexFindAll(regex, text, 0, strlen(text), LEN(matches), matches, 0);
    veriyMatchResults(n, matches);
    assert(n == LEN(text) / 3);

    freeRegex(regex);
}

SCUTEST(test_regex_find_match_containing_position) {
    char text[8];
    memset(text, 'A', sizeof(text));
    void * regex = allocRegex("A(AA)", 0);

    for (int snapToEnd = 0; snapToEnd <= 1; snapToEnd++) {
        int ret = regexFindMatchContainingPosition(regex, text, 0, strlen(text), 0, 1, snapToEnd, 0);
        if (snapToEnd) {
            assert(ret == 2);
        } else {
            assert(ret == 1);
        }
        for (int expectedRet = ret + 3; expectedRet < 2 * 3; expectedRet += 3) {
            ret = regexFindMatchContainingPosition(regex, text, 0, strlen(text), ret, 1, snapToEnd, 0);
            assert(ret == expectedRet);
        }

        for (int expectedRet = ret - 3; expectedRet >= 0; expectedRet -= 3) {
            ret = regexFindMatchContainingPosition(regex, text, 0, strlen(text), ret, -1, snapToEnd, 0);
            assert(ret == expectedRet);
        }
        ret = regexFindMatchContainingPosition(regex, text, 0, strlen(text), ret, -1, snapToEnd, 0);
        assert(ret == -1);
    }
    freeRegex(regex);
}

SCUTEST(test_regex_find_match_containing_position_large) {
    int size = 1 << 12;
    char * text = wee_malloc(ALLOCATION_RAW, size );
    memset(text, 'A', size);
    text[size / 2] = 'B';
    void * regex = allocRegex("ABA", 0);
    for (int snapToEnd = 0; snapToEnd <= 1; snapToEnd++) {
        int ret = regexFindMatchContainingPosition(regex, text, 0, strlen(text), 0, 1, snapToEnd, 0);
        assert(ret == size / 2 + snapToEnd * 2 - 1);
    }
    freeRegex(regex);
    wee_free(text);
}

SCUTEST(test_regex_find_match_containing_position_per_line) {
    const char * text = "Hello\nWorld\n!";
    void * regex = allocRegex("^.*$", SEARCH_FLAG_NEWLINE);
    for (int snapToEnd = 0; snapToEnd <= 1; snapToEnd++) {
        int ret = regexFindMatchContainingPosition(regex, text, 0, strlen(text), 0, 1, snapToEnd, 0);
        int expectedOffset = strchr(text, '\n') - text + !snapToEnd * 2 -1;
        assert(ret == expectedOffset);
    }
    freeRegex(regex);
}
