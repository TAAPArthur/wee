#include "../memory.h"
#include "../settings.h"
#include "../utility.h"
#include "../view.h"
#include "../window.h"

#include "scutest.h"
#include <assert.h>

static View* view;
static Window* win;
static Settings* settings;

static void setup() {
    settings = allocGlobalSettings();
    win = allocWindow(settings);
    assert(win);
    view = allocView(settings);
    assert(view);
    setViewForWindow(win, view);
}

static void cleanup() {
    freeWindow(win);
    freeView(view);
    freeSettings(settings);
    assert(checkForLeaks() == 0);
}

SCUTEST_SET_FIXTURE(setup, cleanup);

static inline void appendText(View* view, const char* str) {
    appendToView(view, str, strlen(str));
}

SCUTEST(test_get_effective_offset_empty) {
    assert(getPositionFromEffectiveOffset(win, 0) == NULL);
    assert(getEffectiveOffsetOfPosition(win, NULL) == 0);
    appendToInsertionBuffer(win, "a", 1);
    assert(*getPositionFromEffectiveOffset(win, 0) == 'a');
    assert(getEffectiveOffsetOfPosition(win, getPositionFromEffectiveOffset(win, 0)) == 0);
    const char* endOfSection = NULL;
    const char* startOfSection = NULL;
    const char* startOfNextSection = NULL;
    int ret = getSectionBounds(win, getPositionFromEffectiveOffset(win, 0), &startOfSection, &endOfSection, &startOfNextSection);
    assert(getEffectiveOffsetOfPosition(win, getPositionFromEffectiveOffset(win, 1)) == 1);
    assert(startOfNextSection == NULL);
}

SCUTEST(test_get_effective_offset) {
    insertAtCurrentPosition(win, "A", 1);
    assert(*getPositionFromEffectiveOffset(win, 0) == 'A');
    assert(getEffectiveOffsetOfPosition(win, getPositionFromEffectiveOffset(win, 0)) == 0);

    appendToInsertionBuffer(win, "B", 1);
    assert(*getPositionFromEffectiveOffset(win, 0) == 'A');
    assert(*getPositionFromEffectiveOffset(win, 1) == 'B');
    assert(getEffectiveOffsetOfPosition(win, getPositionFromEffectiveOffset(win, 0)) == 0);
    assert(getEffectiveOffsetOfPosition(win, getPositionFromEffectiveOffset(win, 1)) == 1);
}

SCUTEST(test_get_number_of_lines) {
    assert(getLastLineNumber(win) == _i + 0);
    insertAtCurrentPosition(win, "\n", 1);
    assert(getLastLineNumber(win) == _i + 1);
    insertAtCurrentPosition(win, "\n", 1);
    assert(getLastLineNumber(win) == _i + 2);
}

SCUTEST(test_line_number_of_line) {
    assert(getLineNumberOfLine(win, NULL) == 0);
    appendToInsertionBuffer(win, "\n", 1);
    assert(getLineNumberOfLine(win, getPositionFromEffectiveOffset(win, 0)) == 0);
    assert(getLineNumberOfLine(win, getPositionFromEffectiveOffset(win, 1)) == 1);
    commitInsertion(win);
    assert(getLineNumberOfLine(win, getPositionFromEffectiveOffset(win, 0)) == 0);
    assert(getLineNumberOfLine(win, getPositionFromEffectiveOffset(win, 1)) == 1);
}

SCUTEST(test_get_cursor_line) {
    unsigned long len;
    assert(getCursorLine(win, &len) == NULL);
    assert(len == 0);

    const char* str = "A\nB\n";
    insertAtCurrentPosition(win, str, strlen(str));
    int i = 0;
    while (1) {
        setCursorOffset(win, i);
        assert(*getCursorLine(win, &len) == *str);
        assert(len == 2);
        assert(getCurrentLineNumber(win) == 0);
        if (str[i++] == '\n')
            break;
    }
    for (; i < strlen(str); i++) {
        setCursorOffset(win, i);
        assert(*getCursorLine(win, &len) == strchr(str, '\n')[1]);
        assert(len == 2);
        assert(getCurrentLineNumber(win) == 1);
    }
    setCursorOffset(win, i);
    getCursorLine(win, &len);
    assert(len == 0);
}

SCUTEST(test_get_cursor_line_insertion_whitespace) {
    insertAtCurrentPosition(win, "\t", 1);
    appendToInsertionBuffer(win, "\n", 1);
    unsigned long len;
    assert(getEffectiveOffsetOfPosition(win, getCursorLine(win, &len)) == 2);
    assert(len == 0);
    assert(*getLine(win, getPositionFromEffectiveOffset(win, 0), &len) == '\t');
    assert(len == 2);
}

SCUTEST(test_get_cursor_line_insertion) {
    unsigned long len;
    for (int i = 0; i < 5; i++) {
        TRACE("ITER %d\n", i);
        assert(getCurrentLineNumber(win) == i);
        assert(getEffectiveOffsetOfPosition(win, getCursorLine(win, &len)) == i);
        appendToInsertionBuffer(win, "\n", 1);
        assert(getEffectiveOffsetOfPosition(win, getCursorLine(win, &len)) == i + 1);
        assert(len == 0);
    }
}

SCUTEST(test_get_cursor_line_insertion_from_start) {
    const char* str = "03";
    insertAtCurrentPosition(win, str, strlen(str));
    setCursorOffset(win, 1);
    appendToInsertionBuffer(win, "1", 1);
    unsigned long len;
    assert(*getLine(win, getPositionFromEffectiveOffset(win, 0), &len) == '0');
    assert(len == 3);
    appendToInsertionBuffer(win, "2", 1);
    assert(*getLine(win, getPositionFromEffectiveOffset(win, 0), &len) == '0');
    assert(len == 4);
}

SCUTEST(test_get_cursor_line_single) {
    const char* str = "0123";
    insertAtCurrentPosition(win, str, strlen(str));
    unsigned long len;
    for (int i = 0; i < strlen(str); i++) {
        TRACE("ITER %d\n", i);
        setCursorOffset(win, i);
        assert(*getCursorLine(win, &len) == str[0]);
        assert(len == strlen(str));
    }
}

SCUTEST(move_cursor_hor_and_vert) {
    const char* str = "1\na\nA\n";
    insertAtCurrentPosition(win, str, strlen(str));
    unsigned long len;
    for (int i = 0; i < strlen(str); i++) {
        setCursorOffset(win, i);
        assert(*getCursorLine(win, &len) == str[i / 2 * 2]);
        assert(len == 2);
    }
}

SCUTEST(test_get_offset_into_line) {
    const char* str = "12\n34\n567";
    insertAtCurrentPosition(win, str, strlen(str));
    setCursorOffset(win, 0);
    const char * pos = getCursorPosition(win);

    for (int i = 0; i < strlen(str); i++) {
        assert(getOffsetIntoLine(win, pos + i) == i % 3);
    }
}

SCUTEST(test_get_col) {
    // TODO uncomment when we have the colnumber_offset_index settings
    // setEnabled(getWindowSettings(win), colnumber_offset_index, 1);
    // setNumericSettings(getWindowSettings(win), colnumber_offset_index, _i);

    assert(getOffsetIntoLine(win, getCursorPosition(win)) == _i + 0);
    insertAtCurrentPosition(win, "A", 1);
    assert(getOffsetIntoLine(win, getCursorPosition(win)) == _i + 1);
    insertAtCurrentPosition(win, "B", 1);
    assert(getOffsetIntoLine(win, getCursorPosition(win)) == _i + 2);
    insertAtCurrentPosition(win, "\nC", 2);
    assert(getOffsetIntoLine(win, getCursorPosition(win)) == _i + 1);
}


SCUTEST(test_get_next_line) {
    for  (int i = -1; i <= 1; i++) {
        assert(getNextLineAfterCursor(win, i) == NULL);
    }
    const char* str = "1a\n2b\n3c";
    insertAtCurrentPosition(win, str, strlen(str));
    setCursorOffset(win, 5);
    assert(*getNextLineAfterCursor(win, -1) == '1');
    assert(*getNextLineAfterCursor(win, 0) == '2');
    assert(*getNextLineAfterCursor(win, 1) == '3');
}

SCUTEST(test_jump_to_line_empty) {
    for(int i = 0; i < 3; i++) {
        jumpToLine(win, i);
        assert(getCurrentLineNumber(win) == 0);
    }
}

SCUTEST(test_jump_to_line) {
    insertAtCurrentPosition(win, "1\n2", 3);
    assert(getLastLineNumber(win) == 1);

    jumpToLine(win, -1);
    assert(getCurrentLineNumber(win) == getLastLineNumber(win));
    jumpToLine(win, -2);
    assert(getCurrentLineNumber(win) == getLastLineNumber(win) -1);

    jumpToLine(win, 0);
    assert(getCurrentLineNumber(win) == 0);

    jumpToLine(win, 1);
    assert(getCurrentLineNumber(win) == 1);

    jumpToLine(win, 2);
    assert(getCurrentLineNumber(win) == getLastLineNumber(win));
}

SCUTEST(test_jump_to_start_of_current_line) {
    insertAtCurrentPosition(win, "ab", 2);
    setCursorOffset(win, 1);
    jumpToLine(win, 0);
    assert(getCursorOffset(win) == 0);
    insertAtCurrentPosition(win, "\n", 1);
    setCursorOffset(win, 2);
    jumpToLine(win, 1);
    assert(getCursorOffset(win) == 1);
}

SCUTEST(test_jump_to_char_in_line) {
    for  (int i = -1; i <= 1; i++) {
        assert(jumpToCharInCurrentLineOfWindow(win, i, 'a') == -1);
    }
    insertAtCurrentPosition(win, "ABCABC", 6);
    setCursorOffset(win, 0);
    for (char c = 'A'; c <= 'C'; c++) {
        assert(*getNextCharInCurrentLineOfWindow(win, 1, c) == c);
        assert(getEffectiveOffsetOfPosition(win, getNextCharInCurrentLineOfWindow(win, 1, c)) == (c == 'A' ? 3 :c -'A'));
        assert(!getNextCharInCurrentLineOfWindow(win, -1, 'c'));
    }
    assert(!getNextCharInCurrentLineOfWindow(win, 1, 'D'));

    setCursorOffset(win, 1);
    assert(getEffectiveOffsetOfPosition(win, getNextCharInCurrentLineOfWindow(win, 1, 'A')) == 3);
    assert(getEffectiveOffsetOfPosition(win, getNextCharInCurrentLineOfWindow(win, -1, 'A')) == 0);
    setCursorOffset(win, 3);
    assert(getEffectiveOffsetOfPosition(win, getNextCharInCurrentLineOfWindow(win, 1, 'C')) == 5);
    assert(getEffectiveOffsetOfPosition(win, getNextCharInCurrentLineOfWindow(win, -1, 'C')) == 2);
}


static void validateCountChar(const char* str, int len) {
    CountCharResult result = {};
    const char * set = getNumCharsUsed(str, str + len, getWindowSettings(win), &result);
    assert(result.xDelta || result.yDelta);
    assert(result.bytesUsed);
    assert(result.len || set == NULL);
    if (set == str) {
        assert(result.len <= len);
    } else if (set == result.scratchSpace) {
        assert(result.len < LEN(result.scratchSpace));
    } else {
        assert(set == NULL);
    }
}

static SettingsIndex getNumCharsUsedRelatedSettings[] = {tabstop_index, display_cntrl_index, unicode_index, list_index, bytes_per_cell_index};
SCUTEST(test_count_used_chars, .iter = LEN(getNumCharsUsedRelatedSettings)) {
    disableAllSettings(getWindowSettings(win));
    for (int n = 0; n < 4; n++) {
        setEnabled(settings, getNumCharsUsedRelatedSettings[_i], n % 2);
        setAltVersion(settings, getNumCharsUsedRelatedSettings[_i], n % 2);
        for (int i = 0; i < 256; i++) {
            char c = i;
            validateCountChar(&c, 1);
        }
        const char * unicode_str = "♞";
        validateCountChar(unicode_str, strlen(unicode_str));
    }
}
