#include "../enum_conversion.h"
#include "../format.h"
#include "../view.h"
#include "../window.h"

#include "scutest.h"
#include <assert.h>
#include <stdio.h>

static View* view;
static Window* win;

static void setup() {
    win = allocWindow(NULL);
    assert(win);
    view = allocView(NULL);
    assert(view);
    setViewForWindow(win, view);
}

static void cleanup() {
    freeWindow(win);
    freeView(view);
}

SCUTEST_SET_FIXTURE(setup, cleanup);

SCUTEST(test_token_parsing) {
    char buffer[64];
    char token[64];
    for(int i = 0; i < NUM_TOKENS; i++) {
        assert(strlen(toTokensStr(i)) < LEN(token));
        snprintf(token, LEN(token), "${%s}", toTokensStr(i));
        wee_format(win, buffer, LEN(buffer),  token);
    }
}

SCUTEST(test_token_parsing_escape) {
    char buffer[64];
    const char* str = "\\${NAME:016s}";
    int n = wee_format(win, buffer, LEN(buffer), str);
    assert(strlen(str + 1) == n);
    assert(strncmp(buffer, str + 1, n) == 0);
}

SCUTEST(test_token_parsing_ext) {
    char buffer[64];
    int n = wee_format(win, buffer, LEN(buffer), "${NAME:016s}${SIZE:016d}");
    assert(n == 32);
}

SCUTEST(test_token_parsing_invalid) {
    char buffer[64];
    assert(!wee_format(win, buffer, LEN(buffer), "${UNKNOWN:016s}${UNKNOWN:016d}"));
    assert(!wee_format(win, buffer, LEN(buffer), "${SIZE:016s}"));
    assert(!wee_format(win, buffer, LEN(buffer), "${SIZE"));
    assert(!wee_format(win, buffer, LEN(buffer), "${NAME:00000000000000000000000000000000016s}"));
}

SCUTEST(test_token_parsing_right_align) {
    char buffer[4] = {};
    int n = wee_format(win, buffer, LEN(buffer), "L|R");
    assert(n == LEN(buffer));
    assert(strncmp(buffer, "L  R", n) == 0);
}
