#include "../colors.h"
#include "../dock.h"
#include "../logger.h"
#include "../settings.h"
#include "../view.h"
#include "../window.h"

#include "scutest.h"
#include <assert.h>
#include <stdio.h>
#include <string.h>

static Buffer dockBuffer;
static WindowSettings* settings;
static View* view;
static Window* win;

static void setup() {
    appendElementConst(&dockBuffer, getDefaultDocks());
    win = allocWindow(NULL);
    settings = getWindowSettings(win);
    view = allocView(NULL);
    setViewForWindow(win, view);
}

static void cleanup() {
    freeBufferData(&dockBuffer);
    freeWindow(win);
    freeView(view);
}

SCUTEST_SET_FIXTURE(setup, cleanup);

int lineNumberDockFunc(const Window* win, const ViewSettings * settings, const DockInfo* info, char* str, int maxLen, Buffer* labelRanges);
int statusBarDockFunc(const Window* win, const ViewSettings * settings, const DockInfo* info, char* str, int maxLen, Buffer* labelRanges);

SCUTEST(test_line_numbers) {
    DockInfo info = {};
    Buffer labelRanges = {};

    const char* format = "%3d ";
    setSettings(settings, linenumberf_index, format);
    setEnabled(settings, linenumberf_index, 1);

    for (int i = 0; i < 10; i++) {
        info.lineno = i;
        char actual[255] = {};
        char expected[255] = {};
        int nExpected = sprintf(expected, format, i);
        int nActual = lineNumberDockFunc(win, settings, &info, actual, LEN(actual), &labelRanges);
        assert(nExpected == nActual);
        assert(actual[nActual] == 0);
        assert(strcmp(actual, expected) == 0);
        assert(labelRanges.size);
        assert(labelRanges.size == sizeof(LabelRange));
        LabelRange* rule = labelRanges.raw;
        assert(rule->start == 0);
        assert(rule->end >= nActual);
        freeBufferData(&labelRanges);
    }
}

SCUTEST(test_line_numbers_size) {
    const char* format = "%3d";
    setSettings(settings, linenumberf_index, format);
    setEnabled(settings, linenumberf_index, 1);
    int expectedSize = lineNumberDockFunc(win, settings, NULL, NULL, 0, NULL);
    assert(expectedSize == 3);
}

SCUTEST(test_status_bar) {
    const char* str = "HI";
    setSettings(settings, statusbarstr_index, str);
    setEnabled(settings, statusbarstr_index, 1);

    DockInfo info = {};
    Buffer labelRanges = {};
    char actual[16] = {};
    int n = statusBarDockFunc(win, settings, &info, actual, LEN(actual), &labelRanges);
    assert(n == strlen(str));
    assert(strcmp(actual, str) == 0);

    const char* name = "win_name";
    setName(view, name);
    setSettings(settings, statusbarstr_index, "${NAME}");
    n = statusBarDockFunc(win, settings, &info, actual, LEN(actual), &labelRanges);
    assert(n == strlen(name));
    assert(strcmp(actual, name) == 0);
    freeBufferData(&labelRanges);
}

SCUTEST(test_status_bar_right_align) {
    const char* str = "L|R";
    setSettings(settings, statusbarstr_index, str);
    setEnabled(settings, statusbarstr_index, 1);

    DockInfo info = {};
    Buffer labelRanges = {};
    char actual[9] = {};
    int n = statusBarDockFunc(win, settings, &info, actual, LEN(actual), &labelRanges);
    assert(n == LEN(actual) - 1);
    assert(strcmp(actual, "L      R") == 0);
    freeBufferData(&labelRanges);
}

SCUTEST(test_dock_sanity_check) {
    const DockInfo info = {};
    FOR_EACH_BUFFER(const Dock*, dock, &dockBuffer) {
        setEnabled(settings, dock->settingIndex, 1);
        for (int alt = 0; alt <=1; alt++) {
            setAltVersion(settings, dock->settingIndex, alt);
            DockType effectiveType = getEffectiveDockType(settings, dock);
            assert(effectiveType != NUM_DOCK_TYPES);
            for (DockType type = 0; type < NUM_DOCK_TYPES; type++) {
                assert(isDockEnabledForType(settings, dock, type) == (type == effectiveType));
            }
            int num = getDockText(win, settings, &info, dock, NULL, 0, NULL);
            assert(num);
        }
        setEnabled(settings, dock->settingIndex, 0);
        assert(NUM_DOCK_TYPES == getEffectiveDockType(settings, dock));
    }
}
