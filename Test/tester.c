#include "../autocommands.h"
#include "../container.h"
#include "../context.h"
#include "../events.h"
#include "../functions.h"
#include "../memory.h"
#include "../settings.h"
#include "../system.h"
#include "../view.h"
#include "../window.h"
#include "scutest.h"
#include "tester.h"
#include "tester_screen.h"

#include <assert.h>
#include <unistd.h>


Context* getGlobalContext() {
    extern Context* globalContext;
    return globalContext;
}


static void initWindow(Context* context) {
    View* view = addView(context);
    Window* win = addWindow(context);
    setViewForWindow(win, view);
}

static int storeBoundsOfGlobalContext(AutoCommandType type, const ContextAutoParams * params) {
    storeBounds(params->resizeParams.rect);
    return 0;
}

static AutocommandCallback testInfraAutocommands[] = {
    {AUTO_CMD_ON_RESIZE, {.contextFunc = storeBoundsOfGlobalContext}},
};

STATIC_BUFFER(testInfraAutocommandsBuffer, testInfraAutocommands);

void setupContextWithDefaultSettings(int keepDefaultSettings) {
    initContext(getGlobalContext());
    if (!keepDefaultSettings) {
        disableAllSettings(getActiveSettings(getGlobalContext()));
    }

    addStateBuffer(getGlobalContext(),  AUTOCOMMAND_INDEX, &testInfraAutocommandsBuffer);

    setDimensions(getGlobalContext(), 10, 10);
    initWindow(getGlobalContext());
    assert(!isShuttingDown(getGlobalContext()));
    assert(getNumberOfWindows(getGlobalContext()) == 1);
    retile(getGlobalContext());
}

void setupContext() {
    setupContextWithDefaultSettings(0);
}

void setupBuffer(Context* context, const char* buffer) {
    View* view = addView(context);
    insertInView(view, buffer, 0, strlen(buffer));
    setViewForWindow(getFocusedWindow(context), view);
    setCursorOffset(getFocusedWindow(context), 0);
    retile(context);
}

void verifyNoLeaks() {
    assert(checkForLeaks() == 0);
}

void cleanupContext() {
    shutdownContext(getGlobalContext());
    assert(checkForLeaks() == 0);
}

SCUTEST_SET_DEFAULT_FIXTURE(setupContext, cleanupContext, .timeout=1, .flags = (SCUTEST_CREATE_TMP_DIR | SCUTEST_CHECK_FD_LEAK | SCUTEST_CHECK_CHILD_LEAK));


int readDataFromFD(int fd, void* data, int len);
int writeDataToFD(int fd, const void* data, int len);
void safe_write(int fd, const void* data, int len) {
    assert(len == writeDataToFD(fd, data, len));
}
void safe_read(int fd, void* data, int len) {
    assert(len == readDataFromFD(fd, data, len));
}

int getNumberOfWindows(const Context* context) {
    int i = 0;
    Window* win;
    FOR_EACH_WINDOW(win, context) {
        i++;
    }
    return i;
}

int hitTarget = 0;
int success(Context* context, Arg arg) {
    hitTarget++;
    return 0;
}

int bindingTriggered() {
    return hitTarget;
}
int resetBindingTriggered() {
    hitTarget = 0;
    return 0;
}

ViewSettings* getActiveSettings(const Context* context) {
    return getFocusedWindow(context) ? getWindowSettings(getFocusedWindow(context)) :  getGlobalSettings(context);
}

void shutdownInput() {
    int fds[2];
    if (pipe(fds) == -1 || dup2(fds[0], STDIN_FILENO) == -1 || close(fds[0]) == -1 || close(fds[1]) == -1)
        exit(-1);
}

void waitForAllOutstandingEvents() {
    while(!isIdle()) {
        int ret = waitForEvent(10);
        assert(ret != -1);
        if (ret) {
            processEvents();
        }
    }
}
