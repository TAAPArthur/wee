#include "../buffer.h"
#include "../memory.h"
#include "../wee_string.h"
#include "scutest.h"

#include <assert.h>
#include <string.h>

static void cleanup() {
    assert(checkForLeaks() == 0);
}

SCUTEST_SET_FIXTURE(NULL, cleanup);

SCUTEST(test_count_chars) {
    const char * multiLineStr = "1\n22\n333\n4444";
    assert(3 == countInstancesOfChar(multiLineStr, multiLineStr + strlen(multiLineStr), '\n'));
    for(int i = 0; i < 5; i++) {
        assert(i == countInstancesOfChar(multiLineStr, multiLineStr + strlen(multiLineStr), '0' + i));
    }
}

SCUTEST(test_get_count_instances_of_char) {
    const char* str = "\n\n";
    assert(countInstancesOfChar(str, str, '\n') == 0);
    assert(countInstancesOfChar(str, str + 1, '\n') == 1);
    assert(countInstancesOfChar(str, str + 2, '\n') == 2);
}

SCUTEST(test_parse_as_number) {
    struct {
        const char* str;
        long num;
        char numWidth;
    } info [] = {
        {"0", 0, 1},
        {"1", 1, 1},
        {"+", 1, 1},
        {"-", -1, 1},
        {"", 0, 0},
        {"0xA", 10, 3},
    };
    for (int i = 0; i < LEN(info); i++) {
        TRACE("ITER %d\n", i);
        long num;
        const char* s = parseAsNumber(info[i].str, &num);
        assert(s - info[i].str == info[i].numWidth);
        assert(num == info[i].num);
    }
}
