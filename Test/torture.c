#include "../context.h"
#include "../defaults.h"
#include "../events.h"
#include "../functions.h"
#include "../system.h"
#include "../view.h"
#include "../window.h"
#include "tester.h"

#include "scutest.h"
#include <assert.h>
#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

// utility functions

static void setup() {
    setupContextWithDefaultSettings(1);
    addDefaults(getGlobalContext());

    setDimensions(getGlobalContext(), 40, 8);
    changeWorkingDirectory(getTempDirForTest());
}
static void teardown() {
    retile(getGlobalContext());
    redraw(getGlobalContext());
    cleanupContext();
}

SCUTEST_SET_FIXTURE(setup, teardown, .flags = (SCUTEST_CREATE_TMP_DIR));

SCUTEST(test_no_size, .iter = 2) {
    if (_i) {
        setEnabled(getActiveSettings(getGlobalContext()), linenumberf_index, 1);
        setDimensions(getGlobalContext(), 1, 1);
    } else {
        setDimensions(getGlobalContext(), 0, 0);
    }
    retile(getGlobalContext());
    redraw(getGlobalContext());

    Window* win = getFocusedWindow(getGlobalContext());

    appendToInsertionBuffer(win, "A", 1);
    retile(getGlobalContext());
    redraw(getGlobalContext());

    commitInsertion(win);
}

SCUTEST(test_new_line) {
    setDimensions(getGlobalContext(), 2, 2);
    Window* win = getFocusedWindow(getGlobalContext());
    for (int i = 0; i < 2; i++) {
        appendToInsertionBuffer(win, "A\n", 2);
        retile(getGlobalContext());
        redraw(getGlobalContext());
    }
    commitInsertion(win);
}
