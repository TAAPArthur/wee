#include "../container.h"
#include "../context.h"
#include "../dock.h"
#include "../events.h"
#include "../functions.h"
#include "../logger.h"
#include "../rect.h"
#include "../view.h"
#include "../window.h"

#include "scutest.h"

#include "../tile.h"
#include <assert.h>
#include <string.h>

static Buffer dockBuffer;
static Buffer containerBuffer;
static Buffer windowList;
static Buffer viewList;
static Window* focusedWindow;


static Window* fakeAddWindow() {
    View* view = allocView(NULL);
    Window* win = allocWindow(NULL);
    setViewForWindow(win, view);

    appendElement(&windowList, win);
    appendElement(&viewList, view);
    addContainer(&containerBuffer, win);
    focusedWindow = win;
    return win;
}

static Window* fakeSplitWindow() {
    Window* prevFocusedWin = focusedWindow;
    Window* win = fakeAddWindow();
    mergeContainers(&containerBuffer, findContainerOfWindow(&containerBuffer, prevFocusedWin), findContainerOfWindow(&containerBuffer, win));

    ContainerId containerId = findContainerOfWindow(&containerBuffer, win);
    setLayoutForContainer(&containerBuffer, containerId, &TWO_ROW);
    return win;
}

static void setup() {
    appendElementConst(&dockBuffer, getDefaultDocks());
    initContainerState(&containerBuffer, &dockBuffer);

    setRootContainerDimensions(&containerBuffer, (Rect){0, 0, 16, 16});
}

static void cleanup() {
    for (int i = 0; i < getLen(&windowList); i++) {
        freeWindow(getElementAt(&windowList, i));
    }
    freeBufferData(&windowList);
    for (int i = 0; i < getLen(&viewList); i++) {
        freeView(getElementAt(&viewList, i));
    }
    freeBufferData(&viewList);
    freeBufferData(&containerBuffer);
    freeBufferData(&dockBuffer);
}

SCUTEST_SET_FIXTURE(setup, cleanup);

static int _count = 0;
static void countFunc(){
    _count++;
}

static int countVisibleContainers() {
    tileContainers(&containerBuffer);
    _count = 0;
    forEachContainerWithWindow(&containerBuffer, countFunc, NULL);
    return _count;
}

SCUTEST(test_container_dimensions) {
    Rect target = {0 ,0, 4, 9};

    setRootContainerDimensions(&containerBuffer, (Rect){0, 0, target.width, target.height});
    Rect bounds = getRootContainerDimensions(&containerBuffer);
    assert(memcmp(&bounds, &target, sizeof(Rect)) == 0);
    Rect target2 = {0 ,0, 2, 7};
    setRootContainerDimensions(&containerBuffer, (Rect){0, 0, target.width, target.height});
    bounds = getRootContainerDimensions(&containerBuffer);
    assert(memcmp(&bounds, &target, sizeof(Rect)) == 0);
}

SCUTEST(test_container_reuse) {
    Window* win = fakeAddWindow();
    Window* win2 = fakeAddWindow();
    Window* win3 = fakeAddWindow();

    removeContainer(&containerBuffer, findContainerOfWindow(&containerBuffer, win2));
    Window* win4 = fakeAddWindow();
}

SCUTEST(test_multi_containers) {
    ContainerId c1 = findContainerOfWindow(&containerBuffer, fakeAddWindow());
    ContainerId c2 = findContainerOfWindow(&containerBuffer, fakeAddWindow());
    ContainerId c3 = findContainerOfWindow(&containerBuffer, fakeAddWindow());
    assert(c1 != c2);
    assert(c1 != c3);
    assert(c2 != c3);
}

SCUTEST(test_split_containers) {
    Window* win1 = fakeAddWindow();
    assert(countVisibleContainers() == 1);
    Window* win2 = fakeSplitWindow();
    assert(countVisibleContainers() == 2);
    removeContainer(&containerBuffer, findContainerOfWindow(&containerBuffer, win2));
    assert(countVisibleContainers() == 1);
}

typedef struct AreaCount{
    int totalArea;
    int count;
} AreaCount;

static void getTotalArea(Window* win, Rect rect, Rect subRect, void*p){
    assert(win);
    assert(p);
    AreaCount* ac = p;
    ac->totalArea += rect.width * rect.height;
    ac->count++;
    TRACE("Fake draw window %p %d\n", win, ac->count);
}

static void verifyTotalTiledArea() {
    Rect bounds = getRootContainerDimensions(&containerBuffer);

    AreaCount ac = {};
    tileContainers(&containerBuffer);
    forEachContainerWithWindow(&containerBuffer, getTotalArea, &ac);
    assert(ac.count);
    assert(ac.totalArea);
    assert(bounds.width * bounds.height == ac.totalArea);
}

SCUTEST(test_tile, .iter = 2) {
    int w = 1, h = 8;
    fakeAddWindow();
    for(int i = 0; i < 3; i++, w++) {

        setRootContainerDimensions(&containerBuffer, (Rect){0, 0, w, h});
        verifyTotalTiledArea();

        if (_i) {
            fakeSplitWindow();
        } else  {
            fakeAddWindow();
        }
    }
}

SCUTEST(test_multiple_visible_windows, .iter = 2) {
    int w = 1, h = 8;
    setRootContainerDimensions(&containerBuffer, (Rect){0, 0, w, h});
    ContainerId containerId = findContainerOfWindow(&containerBuffer, fakeAddWindow());
    struct Layout* layouts[] = {&FULL, &GRID};
        fakeSplitWindow();
        setLayoutForContainer(&containerBuffer, containerId, layouts[_i]);

        AreaCount ac = {};
        tileContainers(&containerBuffer);
        forEachContainerWithWindow(&containerBuffer, getTotalArea, &ac);
        assert(ac.totalArea == w * h);
        assert(ac.count == 1 + _i);
}

SCUTEST(test_excessive_containers) {
    for(int i = 0; i < 100; i ++) {
        fakeAddWindow();
    }
    verifyTotalTiledArea();
}
