#include "../colors.h"
#include "../logger.h"

#include "scutest.h"
#include <assert.h>

SCUTEST(test_8bit_color_conversion) {
    char counts[16] = {};
    for (int b = 0; b <= 256; b += 127) {
        for (int g = 0; g <= 256; g += 127) {
            for (int r = 0; r <= 256; r += 127) {
                int color = r | (g << 8) | (b << 16);
                int colorIdx = get8BitColor(color);
                assert(colorIdx < 16);
                assert(colorIdx >= 0);
                counts[colorIdx ]++;
            }
        }
    }
}
