#include "../autocommands.h"
#include "../commands.h"
#include "../context.h"
#include "../defaults.h"
#include "../dock.h"
#include "../functions.h"
#include "../memory.h"
#include "../memory.h"
#include "../system.h"
#include "../utility.h"
#include "../view.h"
#include "../window.h"
#include "tester.h"

#include "scutest.h"

#include <assert.h>
#include <string.h>
#include <unistd.h>

static int counter;
static int count() {
    counter++;
    return 0;
}

static int alt_count() {
    counter--;
    return 0;
}

static Command defaultTestCommands[] = {
    {"test", count, ALL_FLAG},
    {"test", alt_count, ALT_FLAG},
};
STATIC_BUFFER(defaultTestCommandsBuffer, defaultTestCommands);

static void setup() {
    setupContext();
    addStateBuffer(getGlobalContext(), COMMAND_INDEX, &defaultTestCommandsBuffer);
}

SCUTEST_SET_FIXTURE_NO_ARGS(setup, cleanupContext);

SCUTEST(test_run_command) {
    assert(runCommand(getGlobalContext(), "test") != -1);
    assert(counter == 1);
}

SCUTEST(test_run_command_empty) {
    assert(runCommand(getGlobalContext(), "") == -1);
    assert(counter == 0);
}

SCUTEST(test_run_command_bad) {
    assert(runCommand(getGlobalContext(), "_unknown_cmd") == -1);
}

SCUTEST(test_run_command_alt, .iter = 2) {
    assert(runCommand(getGlobalContext(), _i ? "test!" : "test! hi") != -1);
    assert(counter == -1);
}

SCUTEST(test_run_command_all, .iter = 2) {
    while (getNumberOfWindows(getGlobalContext()) < 4) {
        addWindow(getGlobalContext());
    }
    assert(getNumberOfWindows(getGlobalContext()) == 4);
    assert(runCommand(getGlobalContext(), _i ? "testall" : "testall hi") != -1);
    assert(counter == 4);
}


static void verifyExModeAppendToInsertionBufferAndCommit(Window* win, const char* str) {
    assert(isCompitableMode(getGlobalSettings(getGlobalContext()), MODE_EX));
    appendToInsertionBuffer(win, str, strlen(str));
    commitInsertion(win);
}


// Default commands
//
#define TEST_FILE "test.txt"
#define TEST_FILE_COPY "test_copy.txt"
static void defaultSetup() {
    setupContext();
    addStateBuffer(getGlobalContext(), COMMAND_INDEX, getDefaultCommands());
    changeWorkingDirectory(getTempDirForTest());

    int fd = openFile(TEST_FILE, OPEN_IO_CREATE | OPEN_IO_WRITE);
    assert(fd != -1);
    long dataSize = 4;
    void * p = wee_malloc(ALLOCATION_RAW, dataSize);
    memset(p, '0', dataSize);
    safe_write(fd, p, dataSize);
    wee_free(p);
    close(fd);
}

SCUTEST_SET_FIXTURE(defaultSetup, cleanupContext, .flags = (SCUTEST_CREATE_TMP_DIR));

SCUTEST(test_read_from_file_cmd, .iter = 2) {
    const char * cmds[] = {"read " TEST_FILE, "read! cat " TEST_FILE};
    const char* cmd = cmds[_i];
    Buffer buffer = {};
    const Buffer* currentBuffer = getCurrentBuffer(getFocusedView(getGlobalContext()));
    assert(runCommand(getGlobalContext(), cmd) != -1);
    waitForAllOutstandingEvents();
    setCursorOffset(getFocusedWindow(getGlobalContext()), 0);
    verifyFileMatchesViewData(getFocusedView(getGlobalContext()), TEST_FILE);

    insertInBuffer(&buffer, currentBuffer->data, 0, currentBuffer->size);
    insertInBuffer(&buffer, "a", 0, 1);
    insertInBuffer(&buffer, currentBuffer->data, 0, currentBuffer->size);
    assert(buffer.size == currentBuffer->size * 2 + 1);

    appendChar(getGlobalContext(), (Arg){.c = 'a'});

    commitInsertion(getFocusedWindow(getGlobalContext()));
    setCursorOffset(getFocusedWindow(getGlobalContext()), 0);

    assert(runCommand(getGlobalContext(), cmd) != -1);
    waitForAllOutstandingEvents();

    assert(bufncmp(getCurrentBuffer(getFocusedView(getGlobalContext())), buffer.data, buffer.size) == 0);
    freeBufferData(&buffer);
}

SCUTEST(test_empty_open_command) {
    assert(runCommand(getGlobalContext(), "edit " TEST_FILE) != -1);
    verifyFileMatchesViewData(getFocusedView(getGlobalContext()), TEST_FILE);
    const char * cmds[] = {"edit",  "split", "vsplit", "tabedit"};
    for (int i = 0; i < LEN(cmds); i++) {
        assert(runCommand(getGlobalContext(), cmds[i]) != -1);
        Window* win;
        FOR_EACH_WINDOW(win, getGlobalContext()) {
            setFocusedWindow(getGlobalContext(), win);
            verifyFileMatchesViewData(getFocusedView(getGlobalContext()), TEST_FILE);
        }
    }
}

SCUTEST(test_no_autocommands) {
    static AutocommandCallback failOnBufferChangeAutoCommands[] = {
        {AUTO_CMD_BUFFER_POST_CHANGE_CHAR, fail},
        {},
    };
    STATIC_BUFFER(failOnBufferChangeAutoCommandsBuffer, failOnBufferChangeAutoCommands);

    addStateBuffer(getGlobalContext(),  AUTOCOMMAND_INDEX, &failOnBufferChangeAutoCommandsBuffer);
    assert(runCommand(getGlobalContext(), "noau read " TEST_FILE) != -1);
    verifyFileMatchesViewData(getFocusedView(getGlobalContext()), TEST_FILE);
}

SCUTEST(test_messages) {
    addStateBuffer(getGlobalContext(),  DOCK_INDEX, getDefaultDocks());
    const char * cmds[] = {"echo hi",  "echom hi"};

    setEnabled(getActiveSettings(getGlobalContext()), show_info_bar_index, 1);
    setNumericSettings(getActiveSettings(getGlobalContext()), show_info_bar_index, DOCK_BOTTOM);
    setDimensions(getGlobalContext(), 2, 2);
    for (int i = 0; i < LEN(cmds); i++) {
        assert(runCommand(getGlobalContext(), cmds[i]) != -1);
        retile(getGlobalContext());
        redraw(getGlobalContext());
        verifyLine(1, "hi");

        assert(runCommand(getGlobalContext(), "echo ") != -1);
        retile(getGlobalContext());
        redraw(getGlobalContext());
        verifyLine(1, "");
    }
}

SCUTEST(test_extra_white_space) {
    const char * cmds[] = {"echo hi", "echo  hi", "   echo  hi"};
    setEnabled(getActiveSettings(getGlobalContext()), show_info_bar_index, 1);
    setNumericSettings(getActiveSettings(getGlobalContext()), show_info_bar_index, DOCK_BOTTOM);
    setDimensions(getGlobalContext(), 2, 2);
    for (int i = 0; i < LEN(cmds); i++) {
        assert(runCommand(getGlobalContext(), cmds[i]) != -1);
        redraw(getGlobalContext());
        verifyLine(1, "hi");
        assert(runCommand(getGlobalContext(), "echo ") != -1);
    }
}

SCUTEST(test_set) {
    char* str = wee_strdup("setall linenumber=hi");
    assert(runCommand(getGlobalContext(), str) != -1);
    ViewSettings * settings = getWindowSettings(getFocusedWindow(getGlobalContext()));
    assert(strcmp(getStringSettings(settings, linenumberf_index), "hi") == 0);
    wee_free(str);
    assert(strcmp(getStringSettings(settings, linenumberf_index), "hi") == 0);
}


static void setupDefaultCommands() {
    setupContext();
    addStateBuffer(getGlobalContext(), COMMAND_INDEX, getDefaultCommands());
}

SCUTEST_SET_FIXTURE_NO_ARGS(setupDefaultCommands, cleanupContext);

// Ex commands
SCUTEST(test_noop) {
    Window* win = getFocusedWindow(getGlobalContext());
    setupBuffer(getGlobalContext(), "a\nb\nc");
    assert(getCurrentLineNumber(win) == 0);
    assert(runCommand(getGlobalContext(), "") != -1);
    assert(getCurrentLineNumber(win) == 1);
    assert(runCommand(getGlobalContext(), "") != -1);
    // TODO HERE
    assert(getCurrentLineNumber(win) == 2);
}

SCUTEST(test_append_insert) {
    Window* win = getFocusedWindow(getGlobalContext());
    View* view = getFocusedView(getGlobalContext());
    setEnabled(getWindowSettings(win), linemode_index, 0);
    assert(runCommand(getGlobalContext(), "a") != -1);
    verifyExModeAppendToInsertionBufferAndCommit(win, "abc");
    assert(runCommand(getGlobalContext(), "a") != -1);
    verifyExModeAppendToInsertionBufferAndCommit(win, "d");
    assert(runCommand(getGlobalContext(), "a") != -1);
    verifyExModeAppendToInsertionBufferAndCommit(win, "f");
    verifyViewData(view, "abcdf");
    assert(runCommand(getGlobalContext(), "-1i") != -1);
    verifyExModeAppendToInsertionBufferAndCommit(win, "e");
    verifyViewData(view, "abcdef");
    assert(runCommand(getGlobalContext(), "0i") != -1);
    verifyExModeAppendToInsertionBufferAndCommit(win, "0");
    verifyViewData(view, "0abcdef");
}

SCUTEST(test_append_insert_linemode) {
    Window* win = getFocusedWindow(getGlobalContext());
    View* view = getFocusedView(getGlobalContext());
    setEnabled(getWindowSettings(win), linemode_index, 1);
    assert(runCommand(getGlobalContext(), "a") != -1);
    verifyExModeAppendToInsertionBufferAndCommit(win, "abc\n");
    assert(runCommand(getGlobalContext(), "a") != -1);
    verifyExModeAppendToInsertionBufferAndCommit(win, "d\n");
    assert(runCommand(getGlobalContext(), "a") != -1);
    verifyExModeAppendToInsertionBufferAndCommit(win, "f\n");
    verifyViewData(view, "abc\nd\nf\n");
    assert(runCommand(getGlobalContext(), "-1i") != -1);
    verifyExModeAppendToInsertionBufferAndCommit(win, "e\n");
    verifyViewData(view, "abc\nd\ne\nf\n");
    assert(runCommand(getGlobalContext(), "0i") != -1);
    verifyExModeAppendToInsertionBufferAndCommit(win, "0\n");
    verifyViewData(view, "0\nabc\nd\ne\nf\n");
}


SCUTEST(test_abort_command) {
    focusCmdWindow(getGlobalContext(), (Arg)0);
    submitCommand(getGlobalContext(), (Arg)0);
}
