#define SCUTEST_IMPLEMENTATION

#include "scutest.h"
#include "../memory.h"
#include <assert.h>
static void cleanup() {
    assert(checkForLeaks() == 0);
}
SCUTEST_SET_DEFAULT_FIXTURE(NULL, cleanup, .timeout=1, .flags = (SCUTEST_CREATE_TMP_DIR | SCUTEST_CHECK_FD_LEAK | SCUTEST_CHECK_CHILD_LEAK));

int main(int argc, char *argv[]) {
    atexit(cleanup);
    setLogLevel(LOG_LEVEL_ALL);
    return runUnitTests();
}
