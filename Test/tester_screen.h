#ifndef TESTER_SCREEN_H
#define TESTER_SCREEN_H
#include "../colors.h"
#include "../rect.h"
#include "../typedefs.h"

void verifyCursorPosition(int x, int y);
void verifyStyle(int lineNo, int col, StyleTuple tuple);

void insertStringIntoView(View* view, const char* str);

static inline int noop(){return 0;};

void storeBounds(Rect rect);

void verifyViewData(const View* view, const char* target);
void verifyFileMatchesViewData(const View* view, const char* fileName);
void verifyLine(int lineNo, const char* str);
void verifyLineN(int lineNo, const char* str, int strLen);
#endif
