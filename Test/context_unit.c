#include "../container.h"
#include "../context.h"
#include "../dock.h"
#include "../functions.h"
#include "../system.h"
#include "../tile.h"
#include "../view.h"
#include "../window.h"
#include "tester.h"

#include "scutest.h"
#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

static void setup() {
    setupContext();
    addStateBuffer(getGlobalContext(), DOCK_INDEX, getDefaultDocks());
    setEnabled(getActiveSettings(getGlobalContext()), window_index, 1);
}

SCUTEST_SET_FIXTURE(setup, cleanupContext);

SCUTEST(test_show_command_window) {
    const char* text = "123";
    setupBuffer(getGlobalContext(), text);
    setDimensions(getGlobalContext(), 3, 2);

    Window* win = getFocusedWindow(getGlobalContext());
    redraw(getGlobalContext());
    verifyLine(0, text);
    verifyLine(1, "");
    setEnabled(getActiveSettings(getGlobalContext()), show_cmd_bar_index, 1);
    setAltVersion(getActiveSettings(getGlobalContext()), show_cmd_bar_index, 0);
    setNumericSettings(getActiveSettings(getGlobalContext()), show_cmd_bar_index, DOCK_BOTTOM);

    retile(getGlobalContext());
    redraw(getGlobalContext());
    verifyLine(0, text);
    verifyLine(1, "");

    focusCmdWindow(getGlobalContext(), (Arg)0);
    appendChar(getGlobalContext(), (Arg){.c='a'});
    retile(getGlobalContext());
    redraw(getGlobalContext());
    verifyLine(0, text);
    verifyLine(1, "a");

    setFocusedWindow(getGlobalContext(), win);
    redraw(getGlobalContext());
    verifyLine(1, "");
}

SCUTEST(test_show_command_window_always) {
    const char* text = "1\n2";
    setupBuffer(getGlobalContext(), text);
    setDimensions(getGlobalContext(), 2, 2);

    setEnabled(getActiveSettings(getGlobalContext()), show_cmd_bar_index, 1);
    setAltVersion(getActiveSettings(getGlobalContext()), show_cmd_bar_index, 0);
    setNumericSettings(getActiveSettings(getGlobalContext()), show_cmd_bar_index, DOCK_BOTTOM);

    retile(getGlobalContext());
    redraw(getGlobalContext());
    verifyLine(0, "1");
    verifyLine(1, "2");
    verifyCursorPosition(0, 0);

    setAltVersion(getActiveSettings(getGlobalContext()), show_cmd_bar_index, 1);
    retile(getGlobalContext());
    redraw(getGlobalContext());
    verifyLine(0, "1");
    verifyLine(1, "");
    verifyCursorPosition(0, 0);
}

SCUTEST(test_show_command_window_with_line_nu) {
    setDimensions(getGlobalContext(), 3, 2);

    Window* win = getFocusedWindow(getGlobalContext());
    setEnabled(getActiveSettings(getGlobalContext()), show_cmd_bar_index, 1);
    setNumericSettings(getActiveSettings(getGlobalContext()), show_cmd_bar_index, DOCK_BOTTOM);
    const char* format = "%1d";
    setSettings(getWindowSettings((getFocusedWindow(getGlobalContext()))), linenumberf_index, format);
    setEnabled(getWindowSettings((getFocusedWindow(getGlobalContext()))), linenumberf_index, 1);

    focusCmdWindow(getGlobalContext(), (Arg)0);
    appendChar(getGlobalContext(), (Arg){.c='a'});
    retile(getGlobalContext());
    redraw(getGlobalContext());
    verifyLine(1, "a");
}

SCUTEST(test_show_info_window) {
    const char* text = "123";
    setupBuffer(getGlobalContext(), text);
    setDimensions(getGlobalContext(), 3, 3);
    setEnabled(getActiveSettings(getGlobalContext()), show_info_bar_index, 1);
    setNumericSettings(getActiveSettings(getGlobalContext()), show_info_bar_index, DOCK_BOTTOM);

    const char* infoMessage = "Hi";
    const char* infoMessage2 = "Bye";
    setInfoMessage(getGlobalContext(), infoMessage);
    retile(getGlobalContext());
    redraw(getGlobalContext());
    verifyLine(0, text);
    verifyLine(2, infoMessage);
    setInfoMessage(getGlobalContext(), infoMessage2);
    retile(getGlobalContext());
    redraw(getGlobalContext());
    verifyLine(0, text);
    verifyLine(2, infoMessage2);
}


SCUTEST(test_dock_dup) {
    char* text = "ab";
    char* text2 = "cd";
    setupBuffer(getGlobalContext(), text2);
    setDimensions(getGlobalContext(), strlen(text) + strlen(text2) + 2, 3);

    setActiveLayout(getGlobalContext(), &TWO_COL);
    retile(getGlobalContext());
    redraw(getGlobalContext());
    verifyLine(0, text2);

    const char* format = "%d";
    Settings* settings = getWindowSettings(getFocusedWindow(getGlobalContext()));
    setSettings(settings, linenumberf_index, format);
    setEnabled(settings, linenumberf_index, 1);

    View* view = addView(getGlobalContext());
    Window* win2 = splitWindow(getGlobalContext());
    setViewForWindow(win2, view);
    insertInView(view, text, 0, strlen(text));

    setCursorOffset(win2, 0);
    for(int i = 0; i < 2; i++) {
        char buffer[16];
        snprintf(buffer, sizeof(buffer), "0%s0%s", text, text2);
        retile(getGlobalContext());
        redraw(getGlobalContext());
        verifyLine(0, buffer);
        verifyLine(3, "");
        verifyLine(2, i ? "a" : "");

        setEnabled(getActiveSettings(getGlobalContext()), show_cmd_bar_index, 1);
        setNumericSettings(getActiveSettings(getGlobalContext()), show_cmd_bar_index, DOCK_BOTTOM);

        focusCommandWindow(getGlobalContext());
        appendChar(getGlobalContext(), (Arg){.c='a'});
    }
}

SCUTEST(test_draw_to_window) {
    Settings* settings = getWindowSettings(getFocusedWindow(getGlobalContext()));
    char* text = "ab";
    setupBuffer(getGlobalContext(), text);
    redraw(getGlobalContext());
    verifyLine(0, text);

    setEnabled(settings, window_index, 0);
    retile(getGlobalContext());
    redraw(getGlobalContext());
    verifyLine(0, "");

    drawToWindow(getGlobalContext(), getFocusedWindow(getGlobalContext()));
    redraw(getGlobalContext());
    verifyLine(0, text);
}
