#include "../memory.h"
#include "scutest.h"
#include <assert.h>
#include <string.h>

SCUTEST_SET_FIXTURE(NULL, NULL);
SCUTEST(test_malloc_free, .iter = 2) {
    void * p = wee_malloc(ALLOCATION_RAW, 1);
    assert(p);
    if (_i) {
        p = wee_realloc(ALLOCATION_RAW, p, 10);
        assert(p);
    }
    wee_free(p);
}

SCUTEST(test_realloc_null) {
    void * p = wee_realloc(ALLOCATION_RAW, NULL, 1);
    assert(p);
    wee_free(p);
}

SCUTEST(test_realloc_change_size) {
    void * p = wee_realloc(ALLOCATION_RAW, NULL, 1);
    assert(p = wee_realloc(ALLOCATION_RAW, p, 2));
    assert(p = wee_realloc(ALLOCATION_RAW, p, 1));
    wee_free(p);
}

SCUTEST(test_strdup) {
    const char* str = "abcde";
    char * p = wee_strdup(str);
    assert(p);
    *p = '0';
    assert(*p != *str);
    wee_free(p);
}

SCUTEST(test_memory_leak_detection) {
    for(int i = 0; i < NUM_ALLOCATION_TYPES; i++) {
        assert(checkForLeaks() == 0);
        void * p = wee_malloc(i, 1);
        assert(checkForLeaks());
        wee_free(p);
        assert(checkForLeaks() == 0);
    }
}
