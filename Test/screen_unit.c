#include "../dock.h"
#include "../logger.h"
#include "../screen.h"
#include "../settings.h"
#include "../tile.h"
#include "../view.h"
#include "../window.h"

#include "tester_screen.h"

#include "scutest.h"
#include <assert.h>
#include <stdio.h>
#include <string.h>


static Buffer ruleBuffer;
static Buffer dockBuffer;
static Buffer containerBuffer;
static Buffer styleBuffer;
static Screen* screen;
static View* view;
static Window* win;
static DrawInfo drawInfo = {
    .styleSchemes = &styleBuffer,
    .docks = &dockBuffer,
};
ViewSettings * settings;

static void simpleResize(int width, int height) {
    setRootContainerDimensions(&containerBuffer, (Rect){0, 0, width, height});
    drawInfo.screen = screen = resizeScreen(screen, width, height);
    tileContainers(&containerBuffer);
    storeBounds(getRootContainerDimensions(&containerBuffer));
}

static void simpleDraw() {
    prepareScreen(screen);
    forEachContainerWithWindow(&containerBuffer, drawWindow, &drawInfo);
    drawScreen(screen);
}

static void setup() {
    appendElementConst(&dockBuffer, getDefaultDocks());
    initContainerState(&containerBuffer, &dockBuffer);
    win = allocWindow(NULL);
    view = allocView(NULL);
    drawInfo.focusedWindow = win;
    setViewForWindow(win, view);
    settings = getWindowSettings(win);
    disableAllSettings(settings);
    screen = allocScreen();
    addContainer(&containerBuffer, win);

    setRulesBuffer(view, &ruleBuffer);

    simpleResize(16, 16);
}

static void cleanup() {
    freeWindow(win);
    freeView(view);
    freeScreen(screen);

    freeRules(&ruleBuffer);
    freeBufferData(&ruleBuffer);
    freeBufferData(&styleBuffer);
    freeBufferData(&containerBuffer);
    freeBufferData(&dockBuffer);
}

SCUTEST_SET_FIXTURE(setup, cleanup);

SCUTEST(test_cursor_position_empty) {
    simpleDraw();
    verifyCursorPosition(0, 0);
}

SCUTEST(test_single_cell, .iter = 2) {
    setEnabled(settings, wordwrap_index, _i % 2);
    simpleResize(1, 1);

    for (int i = 0; i < 4; i++) {
        appendToInsertionBuffer(win, "A", 1);
        simpleDraw();
        verifyLine(0, "");
    }
    commitInsertion(win);
    simpleDraw();
    verifyLine(0, "");
}

SCUTEST(test_give_docks_correct_size, .iter = 2) {
    const char* str = "BL|BR";
    setSettings(settings, statusbarstr_index, str);
    setEnabled(settings, statusbarstr_index, 1);
    setAltVersion(settings, statusbarstr_index, _i);
    simpleResize(6, 2);
    simpleDraw();
    verifyLine(!_i, "BL  BR");

}

SCUTEST(test_new_line) {
    simpleResize(4, 4);
    appendToInsertionBuffer(win, "A\n", 2);
    simpleDraw();
    verifyLine(0, "A");
    verifyLine(1, "");
    appendToInsertionBuffer(win, "A\n", 2);
    simpleDraw();
    verifyLine(0, "A");
    verifyLine(1, "A");

    commitInsertion(win);
    simpleDraw();
    verifyLine(0, "A");
    verifyLine(1, "A");
}

SCUTEST(test_draw_tab) {
    WindowSettings* settings = getWindowSettings(win);
    setEnabled(settings, tabstop_index, 1);
    setEnabled(settings, display_cntrl_index, 0);
    setNumericSettings(settings, tabstop_index, 4);
    simpleResize(8, 2);

    const char* target = "A\t   B";

    appendToInsertionBuffer(win, "A\tB", 3);
    simpleDraw();
    verifyLine(0, target);
    commitInsertion(win);
    simpleDraw();
    verifyLine(0, target);
}

SCUTEST(test_starting_col) {
    setEnabled(settings, wordwrap_index, 0);
    const char * text =
        "abcdefgh\n"
        "ij\n"
        "klmn\n"
        "o";

    insertStringIntoView(view, text);
    simpleResize(2, 2);

    setCursorOffset(win, strchr(text, 'h') - text);
    simpleDraw();
    verifyLine(0, "gh");
    verifyLine(1, "");

    setCursorOffset(win, strchr(text, 'j') - text);
    simpleDraw();
    verifyLine(0, "bc");
    verifyLine(1, "j");

    setCursorOffset(win, strchr(text, 'i') - text);
    simpleDraw();
    verifyLine(0, "ab");
    verifyLine(1, "ij");

    setCursorOffset(win, strchr(text, 'n') - text);
    simpleDraw();
    verifyLine(0, "");
    verifyLine(1, "mn");

    setCursorOffset(win, strchr(text, 'o') - text);
    simpleDraw();
    verifyLine(0, "kl");
    verifyLine(1, "o");

    setCursorOffset(win, strchr(text, 'i') - text);
    simpleDraw();
    verifyLine(0, "ij");
    verifyLine(1, "kl");

}

SCUTEST(test_long_line, .iter = 2) {
    setEnabled(settings, wordwrap_index, _i);
    const char * text = "abcd";
    insertStringIntoView(view, text);
    simpleResize(1, 1);
    for (int i = 0; i < strlen(text); i++) {
        setCursorOffset(win, i);
        simpleDraw();
        verifyLineN(0, text + i, 1);
        verifyCursorPosition(0, 0);
    }
}

SCUTEST(test_really_long_line, .iter = 2) {
    setEnabled(settings, wordwrap_index, _i);
    char text[255];
    memset(text, 'A', sizeof(text));
    text[sizeof(text) - 1] = 0;
    insertStringIntoView(view, text);
    simpleResize(1, 1);
    for (int i = 0; i < strlen(text); i++) {
        setCursorOffset(win, i);
        simpleDraw();
        verifyLineN(0, text + i, 1);
        verifyCursorPosition(0, 0);
    }
}

SCUTEST(test_long_line_insertion, .iter = 4) {
    setEnabled(settings, wordwrap_index, _i % 2);
    simpleResize(2, 1);
    const char * texts[2] = {"abcd", "efg"};

    for(int n = 0; n <= _i / 2 ; n++) {
        const char* text = texts[n];
        char expectedView[2] = {0};
        for (int i = 0; i < strlen(text); i++) {
            appendToInsertionBuffer(win, &text[i], 1);
            expectedView[0] = text[i];
            simpleDraw();
            verifyLine(0, expectedView);
            verifyCursorPosition(1, 0);
        }
        commitInsertion(win);
        simpleDraw();
        verifyLine(0, expectedView);
        verifyCursorPosition(1, 0);
    }
}

SCUTEST(test_word_wrap_multi_line_with_dock) {
    setEnabled(settings, wordwrap_index, 1);
    const char * text = "abcd\nefgh";
    insertStringIntoView(view, text);
    setCursorOffset(win, 0);
    simpleResize(1, strlen(text));
    const char* s = text;
    simpleDraw();
    for(int i = 0; *s; i++) {
        verifyLineN(i, *s == '\n' ? "" : s, 1);
        s++;
    }
    int width = 4;

    const char* format = "%d";
    setSettings(settings, linenumberf_index, format);
    setEnabled(settings, linenumberf_index, 1);

    simpleResize(width, strlen(text));
    for(int i = 0; i < width + 1; i++) {
        setCursorOffset(win, i);
        simpleDraw();
        verifyLine(0, "0abc");
        verifyLine(1, " d");
        verifyLine(2, "1efg");
        verifyLine(3, " h");
    }
}

SCUTEST(test_no_word_wrap_multi_line_with_dock) {
    setEnabled(settings, wordwrap_index, 0);
    const char * text = "abcd\nefgh";
    int width = 4;
    insertStringIntoView(view, text);

    const char* format = "%d";
    setSettings(settings, linenumberf_index, format);
    setEnabled(settings, linenumberf_index, 1);
    simpleResize(width, strlen(text));
    for(int i = 0; i < width - 1; i++) {
        setCursorOffset(win, i);
        simpleDraw();
        verifyLine(0, "0abc");
        verifyLine(1, "1efg");
    }
    setCursorOffset(win, width - 1);
    simpleDraw();
    verifyLine(0, "0bcd");
    verifyLine(1, "1fgh");
}

SCUTEST(test_line_numbers_single_line) {
    const char* format = "%d";
    setSettings(settings, linenumberf_index, format);
    setEnabled(settings, linenumberf_index, 1);

    tileContainers(&containerBuffer);
    simpleDraw();
    // test empty buffer
    verifyLine(0, "0");
    insertStringIntoView(view, "\n");
    simpleDraw();
    verifyLine(0, "0");
    verifyLine(1, "");
}

SCUTEST(test_no_word_viewport_with_long_lines) {
    setEnabled(settings, wordwrap_index, 0);
    const char * text = "12\nabcdefgh\n34";
    // 12      |
    // abcdefgh|
    // 34      |
    int width = 4;
    simpleResize(width, 3);
    insertStringIntoView(view, text);
    setCursorOffset(win, 0);
    simpleDraw();
    verifyLine(0, "12");
    verifyLine(1, "abcd");
    verifyLine(2, "34");

    setCursorOffset(win, strchr(text, 'e') - text);
    simpleDraw();
    verifyLine(0, "2");
    verifyLine(1, "bcde");
    verifyLine(2, "4");

    setCursorOffset(win, strchr(text, 'f') - text);
    simpleDraw();
    verifyLine(0, "");
    verifyLine(1, "cdef");
    verifyLine(2, "");

    setCursorOffset(win, strchr(text, 'g') - text);
    simpleDraw();
    verifyLine(0, "");
    verifyLine(1, "defg");
    verifyLine(2, "");

    setCursorOffset(win, strchr(text, 'b') - text);
    simpleDraw();
    verifyLine(0, "2");
    verifyLine(1, "bcde");
    verifyLine(2, "4");
}

SCUTEST(test_draw_with_style) {
    static Rule testRules[] = {
        {LABEL_STRING,  .pattern = "\".*\""},
        {LABEL_KEYWORD, .pattern = "(char \\* c )"},
    };
    static StyleScheme testStyleScheme[] = {
        {LABEL_KEYWORD, {COLOR_GREEN, COLOR_RED}},
        {LABEL_STRING,  {COLOR_BLUE, .style = STYLE_BOLD}},
        {},
    };
    STATIC_BUFFER(testRulesBuffer, testRules);
    STATIC_BUFFER(testStylesBuffer, testStyleScheme);

    appendElement(&ruleBuffer, (void*) &testRulesBuffer);
    appendElement(&styleBuffer,(void*)  &testStylesBuffer);
    const char* text = "char * c = \"hi\"";

    simpleResize(strlen(text), 1);
    insertStringIntoView(view, text);
    setCursorOffset(win, 0);

    updateLabelsOfView(view);

    simpleDraw();

    for(int i = 0; i < 4; i++)
        verifyStyle(0, i, testStyleScheme[0].tuple);

    int offset = strchr(text, '"') - text;
    for(int i = 0; i < 4; i++)
        verifyStyle(0, offset + i, testStyleScheme[1].tuple);
}

SCUTEST(test_move_viewport) {
   const char * text =
        "abcdefgh\n"
        "ij\n"
        "klmn\n"
        "o";

    setEnabled(settings, wordwrap_index, 0);

    insertStringIntoView(view, text);
    simpleResize(2, 2);

    setAutoAudjustViewportForCursor(win, 0);

    Rect rect = getRootContainerDimensions(&containerBuffer);

    simpleDraw();
    verifyLine(0, "ab");
    verifyLine(1, "ij");

    moveViewportStart(screen, win, rect, 1);
    simpleDraw();
    verifyLine(0, "ij");
    verifyLine(1, "kl");

    moveViewportStart(screen, win, rect, 1);
    simpleDraw();
    verifyLine(0, "kl");
    verifyLine(1, "o");
    for (int i = 0; i < 3; i++) {
        moveViewportStart(screen, win, rect, 1);
        simpleDraw();
        verifyLine(0, "o");
        verifyLine(1, "");
    }
    moveViewportStart(screen, win, rect, -1);
    simpleDraw();
    verifyLine(0, "kl");
    verifyLine(1, "o");

    moveViewportStart(screen, win, rect, -10);
    simpleDraw();
    verifyLine(0, "ab");
    verifyLine(1, "ij");

    moveViewportStart(screen, win, rect, 10);
    simpleDraw();
    verifyLine(0, "o");
    verifyLine(1, "");
}

SCUTEST(test_draw_change_view) {
    insertStringIntoView(view, "A");
    View* view2 = allocView(NULL);
    setViewForWindow(win, view2);
    insertStringIntoView(view2, "B");
    simpleDraw();
    verifyLine(0, "B");

    setViewForWindow(win, view);
    simpleDraw();
    verifyLine(0, "A");
    freeView(view2);
}

SCUTEST(test_draw_subview) {
    insertStringIntoView(view, "ABCD");
    simpleDraw();
    verifyLine(0, "ABCD");

    setSubViewForWindow(win, 0, 3);
    simpleDraw();
    verifyLine(0, "BC");
}

SCUTEST(test_draw_multi_window) {
    Window* win2 = allocWindow(NULL);
    setViewForWindow(win2, view);
    addContainer(&containerBuffer, win2);

    ContainerId containerId = findContainerOfWindow(&containerBuffer, win);
    setLayoutForContainer(&containerBuffer, containerId, &TWO_ROW);
    mergeContainers(&containerBuffer, findContainerOfWindow(&containerBuffer, win), findContainerOfWindow(&containerBuffer, win2));

    simpleResize(2, 2);
    simpleDraw();
    verifyCursorPosition(0, 1);
    drawInfo.focusedWindow = win2;
    simpleDraw();
    verifyCursorPosition(0, 0);
    freeWindow(win2);
}

SCUTEST(test_list) {
    setEnabled(settings, list_index, 1);
    setEnabled(settings, display_cntrl_index, 1);
    const char text[] = "0\nA\tB\n\t\tC";
    insertInView(view, text, 0, sizeof(text));
    simpleDraw();
    verifyLine(0, "0$");
    verifyLine(1, "A^IB$");
    verifyLine(2, "^I^IC");
}

SCUTEST(test_bytes_per_cell) {
    char text[] = {'A', 'B', 'C', 'D'};
    setEnabled(settings, bytes_per_cell_index, 1);
    setNumericSettings(settings, bytes_per_cell_index, 2);

    insertInView(view, text, 0, sizeof(text));

    simpleDraw();
    verifyLine(0, "AC");
}

#ifdef HAVE_GRAPHEME

SCUTEST(test_draw_unicode_single) {
    const char * unicode_str = "♞";
    const char * str = "\nK";
    setEnabled(settings, unicode_index, 1);
    simpleResize(1,2);
    insertStringIntoView(view, str);
    insertStringIntoView(view, unicode_str);

    setCursorOffset(win, 0);
    simpleDraw();
    verifyLine(0, unicode_str);
    verifyLine(1, str + 1);
}

SCUTEST(test_draw_unicode) {
    const char unicode_str[] = "♚♛♜♝♞";
    const char * mixed_string = "X♕X";
    setEnabled(settings, unicode_index, 1);
    simpleResize(10,2);

    insertInView(view, unicode_str, 0, LEN(unicode_str) - 1);
    setCursorOffset(win, 0);

    simpleDraw();
    verifyLineN(0, unicode_str, LEN(unicode_str) - 1);

    insertStringIntoView(view, "\n");
    insertStringIntoView(view, mixed_string);
    simpleDraw();
    verifyLine(0, mixed_string);
    verifyLine(1, unicode_str);
}

SCUTEST(test_draw_unicode_multi_word) {
    setEnabled(settings, unicode_index, 1);
    const char unicode_str[] = "♟︎";
    assert(LEN(unicode_str) - 1 > sizeof(int));
    insertInView(view, unicode_str, 0, LEN(unicode_str) - 1);
    simpleDraw();
    verifyLineN(0, unicode_str, LEN(unicode_str) - 1);
}
#endif
