#include "../labels.h"
#include "../memory.h"
#include "../settings.h"
#include "../view.h"

#include "scutest.h"

#include <assert.h>
#include <string.h>

static View* view;
static AutoCommandArray autoCommands;
static Buffer rulesBuffer = {};
static void setup() {
    view = allocView(NULL);
    setPointerSettings(getSettings(view), autocommand_index, &autoCommands);
    appendElementConst(&autoCommands, getDefaultFileTypeAutoCommands());
    setRulesBuffer(view, &rulesBuffer);
}

static void cleanup() {
    freeView(view);
    freeBufferData(&autoCommands);
    freeRules(&rulesBuffer);
    freeBufferData(&rulesBuffer);
}

SCUTEST_SET_FIXTURE(setup, cleanup);

SCUTEST(test_find_label) {
    Buffer rangesBuffer = {};
    LabelRange sampleLabelRange = {LABEL_TEXT, 1, 3};
    addLabel(&rangesBuffer, &sampleLabelRange);

    assert(findLabel(&rangesBuffer, 0) == LABEL_UNKNOWN);
    assert(findLabel(&rangesBuffer, 1) == sampleLabelRange.label);
    assert(findLabel(&rangesBuffer, 2) == sampleLabelRange.label);
    assert(findLabel(&rangesBuffer, 3) == LABEL_UNKNOWN);
    freeBufferData(&rangesBuffer);
}

SCUTEST(test_label_code_simple) {
    static Rule testRules[] = {
        {LABEL_MACRO,   {FT_C},                 .pattern = "p1"},
        {LABEL_COMMENT, {FT_ANY, C_FAMILY},     .pattern = "p2"},
        {LABEL_LITERAL, {FT_ANY, .dialect = 2}, .pattern = "p3"},
    };
    STATIC_BUFFER(testRulesBuffer, testRules);
    appendElement(&rulesBuffer, (void*) &testRulesBuffer);
    setNumericSettings(getSettings(view), filetype_index, FT_C);
    setNumericSettings(getSettings(view), filetype_dialect_index, 2);

    for(int i = 0; i < LEN(testRules); i++) {
        Buffer destLabelRanges = {};
        insertInView(view, testRules[i].pattern, 0, strlen(testRules[i].pattern));
        labelCodeOfView(&destLabelRanges, &rulesBuffer, view);

        assert(destLabelRanges.size == sizeof(LabelRange));
        LabelRange* rule = destLabelRanges.raw;
        assert(rule->label == testRules[i].label);
        freeBufferData(&destLabelRanges);
        deleteFromView(view, 0, strlen(testRules[i].pattern));
    }
}

SCUTEST(test_label_code_multi) {
    static Rule testRules[] = {
        {LABEL_LITERAL, {FT_ANY}, .pattern = "."},
    };
    STATIC_BUFFER(testRulesBuffer, testRules);
    appendElement(&rulesBuffer, (void*) &testRulesBuffer);
    const char* str = "0123456789";
    insertInView(view, str, 0, strlen(str));
    Buffer destLabelRanges = {};
    labelCodeOfView(&destLabelRanges, &rulesBuffer, view);
    LabelRange lastRange = {};
    FOR_EACH(LabelRange*, rule, &destLabelRanges, 0) {
        assert(lastRange.end == rule->start);
        assert(rule->label == LABEL_LITERAL);
        lastRange = *rule;
    }
    assert(lastRange.end = strlen(str));
    freeBufferData(&destLabelRanges);
}

SCUTEST(test_label_lang_change) {
    static Rule testRules[] = {
        {FT_C,          {FT_ANY},      .pattern = "```c\n([^`]*)```", .matchIndexes = 0x1},
        {LABEL_COMMENT, {FT_C},        .pattern = "//\\s*[^\n]*"},
        {LABEL_TEXT,    {FT_MARKDOWN}, .pattern = ".*"},
    };
    STATIC_BUFFER(testRulesBuffer, testRules);
    const char* text = "P```c\nint i;\n// C code\n;```S";
    appendElement(&rulesBuffer, (void*) &testRulesBuffer);

    setNumericSettings(getSettings(view), filetype_index, FT_MARKDOWN);

    Buffer destLabelRanges = {};
    insertInView(view, text, 0, strlen(text));
    labelCodeOfView(&destLabelRanges, &rulesBuffer, view);

    const char * p = text;
    const char* boundaryStart = strstr(p, "`");
    const char* boundaryEnd = strstr(p, "\n") + 1;
    const char* commentStart = strchr(p, '/');
    const char* commentEnd = strchr(commentStart + 1, '\n');
    const char* cEnd= strchr(commentEnd + 1, ';') + 1;
    const char* boundaryEnd2 = strstr(cEnd, "S");
    int i = 0;

    while (p + i < boundaryStart) {
        assert(findLabel(&destLabelRanges, i++) == LABEL_TEXT);
    }

    while (p + i < boundaryEnd)
        assert(findLabel(&destLabelRanges, i++) == LABEL_BOUNDARY);

    while(p + i < commentStart) {
        assert(findLabel(&destLabelRanges, i++) == FT_C);
    }

    while(p + i < commentEnd) {
        assert(findLabel(&destLabelRanges, i++) == LABEL_COMMENT);
    }
    while(p + i < cEnd)
        assert(findLabel(&destLabelRanges, i++) == FT_C);

    while (p + i < boundaryEnd2) {
        assert(findLabel(&destLabelRanges, i++) == LABEL_BOUNDARY);
    }
    while(p[i]) {
        assert(findLabel(&destLabelRanges, i++) == LABEL_TEXT);
    }
    freeBufferData(&destLabelRanges);
}

static struct {
    const char* str;
    LabelRange range[4];
    FileType fileType;
    int dialect;
} samples[] = {
    {"\"Hello World\"",                  {{LABEL_STRING, .start=1, .end=-1}}},
    {"const char * s = \"Hello World\"", {{LABEL_STRING, .start=18, .end=-1}}},
    {"\"Hello\" \"World\"",              {{LABEL_STRING, .start=1, .end=5}, {LABEL_STRING, .start=9, .end =-1}}},
    {"\"Hello\" \"World\"",              {{LABEL_STRING, .start=1, .end=5}, {LABEL_STRING, .start=9, .end =-1}}},
    {"\"H\\\nello\"",                    {{LABEL_STRING, .start=1, .end=6}}},
    {"\"\\\"H\\\nello\\\"\"",            {{LABEL_STRING, .start=1, .end=-1}}},

    {"#include <file.h>",                {{LABEL_MACRO, .start=0, .end=8}, {LABEL_INCLUDE, .start=10, .end=-1}}, FT_C},
    {"#include \"file.h\"",              {{LABEL_MACRO, .start=0, .end=8}, {LABEL_INCLUDE, .start=10, .end=-1}}, FT_C},
    {"#define",  {{LABEL_MACRO}}, FT_C},

    {"//Comment",                        {{LABEL_COMMENT, .start=2}}           , FT_C},
    {"/*Multi\nline\nComment*/",         {{LABEL_COMMENT, .start=2, .end = -2}}, FT_C, .dialect = 99},
    {"/**Doc Comment**/",                {{LABEL_COMMENT, .start=2, .end = -2}}, FT_CPP},

    {"int main (int argc, char *argv[]) {", {{LABEL_KEYWORD, .start=0, .end = 3}, {LABEL_KEYWORD, .start=10, .end = 13}, {LABEL_KEYWORD, .start=20, .end = 24}}, FT_C},
    {"0b01",  {{LABEL_LITERAL}}, FT_C},
    {"0xFF",  {{LABEL_LITERAL}}, FT_C},
    {"0777",  {{LABEL_LITERAL}}, FT_C},

    {"if",       {{LABEL_KEYWORD}}, FT_C},
};

SCUTEST(test_label_code) {
    appendElementConst(&rulesBuffer, getDefaultRules());
    ViewSettings* settings = getSettings(view);
    for (int i = 0; i < LEN(samples); i++) {
        const char* str = samples[i].str;
        TRACE("Sample %d: %s\n", i, str);
        Buffer buffer = {.readOnlyData = str, .size = strlen(str)};
        setNumericSettings(settings, filetype_index, samples[i].fileType);
        setNumericSettings(settings, filetype_dialect_index, samples[i].dialect);

        Buffer destLabelRanges = {};
        labelCode(&destLabelRanges, &rulesBuffer, &buffer, settings);
        assert(destLabelRanges.size);

        for (int n = 0; n < LEN(samples[i].range); n++) {
            LabelRange targetLabelRange = samples[i].range[n];
            if (targetLabelRange.label == 0)
                continue;
            if(targetLabelRange.end <= 0) {
                targetLabelRange.end += strlen(str);
            }

            assert(targetLabelRange.end >targetLabelRange.start);
            for(int offset = targetLabelRange.start; offset < targetLabelRange.end; offset++) {
                Label actual = findLabel(&destLabelRanges, offset);
                TRACE("Pos %d: actual %d vs expected %d; char %c\n", offset, actual, targetLabelRange.label, str[offset]);
                assert(targetLabelRange.label == findLabel(&destLabelRanges, offset));
            }
        }
        freeBufferData(&destLabelRanges);
    }
}
