#include "context.h"
#include "enum_conversion.h"
#include "format.h"
#include "utility.h"
#include "wee_string.h"
#include "window.h"

#include <assert.h>
#include <stdio.h>
#include <string.h>

static int copyTokenToBufferHelper(const char* _formatSpecifier, const char* end, const char* value, char* buffer, int n) {
    if (!value)
        value="";
    if (_formatSpecifier >= end) {
        int len = strnlen(value, n);
        memcpy(buffer, value, len);
        return len;
    }
    char formatSpecifier[16] = {'%'};
    int formatLen = MIN(end - _formatSpecifier, LEN(formatSpecifier) - 2);
    memcpy(formatSpecifier + 1, _formatSpecifier, formatLen);
    if (formatSpecifier[formatLen] != 's')
        return 0;
    return snprintf(buffer, n, formatSpecifier, value);
}

static int copyTokenToBufferIntHelper(const char* _formatSpecifier, const char* end, int value, char* buffer, int n) {
    if (_formatSpecifier >= end) {
        _formatSpecifier = "d";
    }
    char formatSpecifier[16] = {'%'};
    int formatLen = MIN(end - _formatSpecifier, LEN(formatSpecifier) - 2);
    memcpy(formatSpecifier + 1, _formatSpecifier, formatLen);
    if (formatSpecifier[formatLen] == 's')
        return 0;
    return snprintf(buffer, n, formatSpecifier, value);
}
#define TEST(S, V) return copyTokenToBufferHelper(token + strlen(S) + 1, token + token_len, V, buffer, n)
#define TEST_INT(S, V) return copyTokenToBufferIntHelper(token + strlen(S) + 1, token + token_len, V, buffer, n)
static int copyTokenToBuffer(const Window* win, const char* token, int token_len, char* buffer, int n) {
    const char* needle = findChar(token, token + token_len, ':', 1);
    int tokenPrefixLen = needle ? needle - token : token_len;
    for(int i = 0; i < NUM_TOKENS; i++) {
        if (strncmp(toTokensStr(i), token, tokenPrefixLen))
            continue;
        switch(i) {
            case TOKEN_NAME:
            case TOKEN_FOCUSED:
                TEST(toTokensStr(i), getWindowName(win));
            case TOKEN_SIZE:
                TEST_INT(toTokensStr(i), getWindowDataLen(win));
            case TOKEN_LINENO:
                TEST_INT(toTokensStr(i), getCurrentLineNumber(win));
            case TOKEN_PERCENT_LINES:
                TEST_INT(toTokensStr(i), getLastLineNumber(win) ? 100 * getCurrentLineNumber(win) / getLastLineNumber(win) : 0);
            case TOKEN_COLNO:
                TEST_INT(toTokensStr(i), getCursorCol(win));
            case TOKEN_LABEL:
                TEST(toTokensStr(i), toLabelStr(getLabelAtPosition(win, getCursorPosition(win))));
        }
    }
    return 0;
}
int wee_format(const Window* win, char* buffer, int n, const char* fmt) {
    const int size = n;
    int i = 0;
    while (*fmt && n) {
        if (fmt[0] == '$' && fmt[1] == '{') {
            char* end = strchr(fmt, '}');
            if (!end)
                break;
            fmt += 2;
            int token_len = end - fmt;
            int len = copyTokenToBuffer(win, fmt, token_len, buffer, n);
            if (len == -1) {
                break;
            }
            fmt += token_len + 1;
            n -= len;
            buffer += len;
        } else if (fmt[0] == '|') {
            fmt++;
            int ret = wee_format(win, buffer, n, fmt);
            memmove(buffer + n - ret, buffer, ret);
            memset(buffer, ' ', n - ret);
            n = 0;
            break;
        } else {
            if (*fmt == '\\') {
                *fmt++;
            }
            if (buffer) {
                buffer[0] = *fmt;
                buffer++;
            }
            n--;
            fmt++;
        }
    }
    return size - n;
}
