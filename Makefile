.POSIX:
.PRAGMA: target_name
include config.mk
USER_CFLAGS  = -Wall -Werror

DEBUG_LDFLAGS_tcc = -bt10
LDFLAGS_0 = -static
LDFLAGS_1 = $(DEBUG_LDFLAGS_$(CC))

DEBUG_CPPFLAGS_tcc = -bt10
CPPFLAGS_1 = -DDEBUG -DDEFAULT_IO_BUFFER_SIZE=1 $(DEBUG_CPPFLAGS_$(CC))

COVERAGE_FLAGS_1 = -ftest-coverage
CFLAGS_1 = -g


CFLAGS   = -std=c99 $(USER_CFLAGS) $(CFLAGS_$(DEBUG)) $(COVERAGE_FLAGS_$(COVERAGE))
LDFLAGS  = $(USER_LDFLAGS) $(CONFIG_LDFLAGS) $(LDFLAGS_$(DEBUG))
CPPFLAGS = -g $(CONFIG_CPPFLAGS) $(CPPFLAGS_$(DEBUG))

ENUM_HEADERS = autocommands.h colors.h dock.h events.h filetypes.h format.h labels.h memory.h search.h

LAYER0 = enum_conversion.c
LAYER1 = logger.c memory.c buffer.c wee_string.c # BASIC_SRCS
LAYER2 = system.c # OS_SRCS
LAYER3 = settings.c autocommands.c undo.c view.c # CORE_SRCS
LAYER4 = colors.c search.c filetypes.c labels.c window.c utility.c registers.c # EDITOR_SRCS
LAYER5 = format.c dock.c container.c screen.c # UI_SRCS
LAYER6 = addresses.c context.c functions.c events.c render.c # UX_SRCS
LAYER7 = options.c commands.c setup.c defaults.c # EXT_SRCS

OTHER_SRCS = wee.c config.c
SRCS   = $(LAYER0) $(LAYER1) $(LAYER2) $(LAYER3) $(LAYER4) $(LAYER5) $(LAYER6) $(LAYER7) $(OTHER_SRCS)
OBJS   = $(SRCS:.c=.o)

SPECIAL_HEADERS = typedefs.h settings_list.h enum_conversion.h
TEST_PREREQUISITES = Test/test.mk enum_conversion.h

.PHONY: test coverage clean

# Override the default suffice rule to always include CPPFLAGS and the output file
.c.o:
	$(CC) $(CPPFLAGS) $(CFLAGS) -c -o $@ $<

wee: $(OBJS)
	$(CC) $(CFLAGS) $(LDFLAGS) -o $@ $(SRCS)

enum_conversion.c: enum_conversion.h $(ENUM_HEADERS) gen_str_to_enum_header.sh
	./gen_str_to_enum_header.sh $(ENUM_HEADERS) > $@ || { err=$$?; rm ./$@ && exit $$err; }

enum_conversion.h: $(ENUM_HEADERS) gen_str_to_enum_header.sh
	./gen_str_to_enum_header.sh $(ENUM_HEADERS) | sed -n -e "/enum_conversion/d" -e "/include/p" -e "s/ { return.*}/;/p" > $@


config.c:
	cp sample_config.c $@


clean:
	rm -f *.o wee render/*.o Test/*.o unit_test* enum_conversion.h *.tcov Test/*.tcov

## Below this are just test specific rules/recipes

ALL_LAYER1 = $(LAYER0:.c=.o) $(LAYER1:.c=.o)
ALL_LAYER2 = $(ALL_LAYER1) $(LAYER2:.c=.o)
ALL_LAYER3 = $(ALL_LAYER2) $(LAYER3:.c=.o)
ALL_LAYER4 = $(ALL_LAYER3) $(LAYER4:.c=.o)
ALL_LAYER5 = $(ALL_LAYER4) $(LAYER5:.c=.o)
ALL_LAYER6 = $(ALL_LAYER5) $(LAYER6:.c=.o)
ALL_LAYER7 = $(ALL_LAYER6) $(LAYER7:.c=.o)

$(LAYER1:.c=.o): $(ALL_LAYER1:.o=.h) $(SPECIAL_HEADERS)
$(LAYER2:.c=.o): $(ALL_LAYER2:.o=.h) $(SPECIAL_HEADERS)
$(LAYER3:.c=.o): $(ALL_LAYER3:.o=.h) $(SPECIAL_HEADERS)
$(LAYER4:.c=.o): $(ALL_LAYER4:.o=.h) $(SPECIAL_HEADERS)
$(LAYER5:.c=.o): $(ALL_LAYER5:.o=.h) $(SPECIAL_HEADERS)
$(LAYER6:.c=.o): $(ALL_LAYER6:.o=.h) $(SPECIAL_HEADERS)
$(LAYER7:.c=.o): $(ALL_LAYER7:.o=.h) $(SPECIAL_HEADERS)


Test/test.mk: Makefile
	sed -En '/^LAYER/ {s/LAYER/TEST_&/ ; s|(\w*)\.c|Test/\1_unit.o|gp};'  Makefile > $@
	sed -n '/^$$(LAYER[0-9]\+:.c=.o)/ s/LAYER/TEST_&/p'  Makefile >> $@

include Test/test.mk

unit_test1: Test/tester_basic.o $(ALL_LAYER1) $(TEST_LAYER1)
	$(CC) $(CFLAGS) -o $@ Test/tester_basic.o $(ALL_LAYER1) $(TEST_LAYER1) $(LDFLAGS)
	./$@ || { err=$$?; rm ./$@ && exit $$err; }

unit_test2: Test/tester_basic.o $(ALL_LAYER2) $(TEST_LAYER2)
	$(CC) $(CFLAGS) -o $@ Test/tester_basic.o $(ALL_LAYER2) $(TEST_LAYER2) $(LDFLAGS)
	./$@ || { err=$$?; rm ./$@ && exit $$err; }

unit_test3: Test/tester_basic.o $(ALL_LAYER3) $(TEST_LAYER3)
	$(CC) $(CFLAGS) -o $@ Test/tester_basic.o $(ALL_LAYER3) $(TEST_LAYER3) $(LDFLAGS)
	./$@ || { err=$$?; rm ./$@ && exit $$err; }

unit_test4: Test/tester_basic.o $(ALL_LAYER4) $(TEST_LAYER4)
	$(CC) $(CFLAGS) -o $@ Test/tester_basic.o $(ALL_LAYER4) $(TEST_LAYER4) $(LDFLAGS)
	./$@ || { err=$$?; rm ./$@ && exit $$err; }

unit_test5: Test/tester_screen.o $(ALL_LAYER5) $(TEST_LAYER5)
	$(CC) $(CFLAGS) -o $@ Test/tester_screen.o $(ALL_LAYER5) $(TEST_LAYER5) $(LDFLAGS)
	./$@ || { err=$$?; rm ./$@ && exit $$err; }

unit_test6: Test/tester.o Test/tester_screen.o $(ALL_LAYER6) $(TEST_LAYER6)
	$(CC) $(CFLAGS) -o $@ Test/tester_screen.o Test/tester.o $(ALL_LAYER6) $(TEST_LAYER6) $(LDFLAGS)
	./$@ || { err=$$?; rm ./$@ && exit $$err; }

unit_test7: Test/tester.o Test/tester_screen.o $(ALL_LAYER7) $(TEST_LAYER7) Test/torture.o
	$(CC) $(CFLAGS) -o $@ Test/tester_screen.o Test/tester.o $(ALL_LAYER7) $(TEST_LAYER7) Test/torture.o $(LDFLAGS)
	./$@ || { err=$$?; rm ./$@ && exit $$err; }

extest: wee
	ExTests/tester.sh ExTests


_test: Test/test.mk unit_test1 unit_test2 unit_test3 unit_test4 unit_test5 unit_test6 unit_test7 extest

test: Test/test.mk
	$(MAKE) COVERAGE=1 DEBUG=1 _test
