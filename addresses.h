#ifndef ADDRESSES_H
#define ADDRESSES_H

#include "typedefs.h"
const char* parseAddresses(const Window* win, const char* cmdStr, const char** addr1, const char** addr2);
#endif
