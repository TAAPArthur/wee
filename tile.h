#ifndef TILE_H
#define TILE_H

#include <math.h>
#define TILE_MAX(A,B) (A>=B?A:B)
#define TILE_MIN(A,B) (A<=B?A:B)

#ifndef getDataElem
#define getDataElem(X, i) (X[i])
#endif
/**
 * Transformations that can be applied to layouts.
 * Everything is relative to the midpoint of the view port of the monitor
 */
typedef enum {
    /// Apply no transformation
    NONE = 0,
    /// Rotate by 180 deg
    ROT_180,
    /// Reflect across x axis
    REFLECT_HOR,
    /// Reflect across the y axis
    REFLECT_VERT,
    TRANSFORM_LEN
} LayoutTransform;

/// Represents a field(s) in Layout Args
typedef enum {
    LAYOUT_LIMIT = 0,
    /// represents all padding fields
    LAYOUT_PADDING,
    LAYOUT_LEFT_PADDING,
    LAYOUT_TOP_PADDING,
    LAYOUT_RIGHT_PADDING,
    LAYOUT_BOTTOM_PADDING,
    LAYOUT_NO_BORDER,
    LAYOUT_NO_ADJUST_FOR_BORDERS,
    LAYOUT_DIM,
    LAYOUT_TRANSFORM,
    LAYOUT_ARG,
} LayoutArgIndex ;
/**
 * Options used to customize layouts
 */
typedef struct {
    /// at most limit windows will be tiled
    unsigned short limit;
    /// @{ padding between the tiled size and the actual size of the window
    short leftPadding;
    short topPadding;
    short rightPadding;
    short bottomPadding;
    /// #}
    /// if set windows won't have a border
    char noBorder;
    /// if we set the size of the window with/without borders
    char noAdjustForBorders;
    /// if set layouts based on X/WIDTH will use Y/HEIGHT instead
    char dim;
    /// the transformation (about the center of the monitor) to apply to all window positions
    LayoutTransform transform;
    /// generic argument
    float arg;
    /// generic argument
    float argStep;
} LayoutArgs ;

struct LayoutState;

///holds meta data to to determine what tiling function to call and when/how to call it
struct Layout {
    /**
     * The name of the layout.
     */
    const char* name;
    /**
     * The function to call to tile the windows
     */
    void (*func)(const struct LayoutState*);
    /**
     * Arguments to pass into layout functions that change how windows are tiled.
     */
    LayoutArgs args;
    /**
     * Used to restore args after they have been modified
     */
    LayoutArgs refArgs;
};

/**
 * Saves the current args for layout so they can be restored later
 *
 */
static inline void layoutSaveArgs(struct Layout* l) { l->refArgs = l->args; }
/**
 * Restores saved args for the active layout
 */
static inline void layoutRestoreArgs(struct Layout* l) { l->args = l->refArgs; }

// Default layouts
extern struct Layout FULL, GRID, TWO_COL, THREE_COL, TWO_ROW, TWO_PANE, TWO_PLANE_H, MASTER, TWO_MASTER, TWO_MASTER_FLIPPED,
       TWO_MASTER_H;

/**
 * Increases the corresponding field for the layout by step
 *
 * @param index a LayoutArgIndex
 * @param step
 * @param l the layout to change
 */
void increaseLayoutArg(int index, int step, struct Layout* l);

/**
 * Windows will be the size of the monitor view port
 * @param state
 */
void full(const struct LayoutState* state);

/**
 * If the number of windows to tile is 1, this method behaves like full();
 * Windows will be arranged in state->args->arg[0] columns. If arg[0] is 0 then there will log2(num windows to tile) columns
 * All columns will have the same number of windows (+/-1) and the later columns will have less
 * @param state
 */
void column(const struct LayoutState* state);
/**
 * If the number of windows to tile is 1, this method behaves like full();
 * The head of the windowStack will take up TILE_MAX(state->args->arg[0]% of the screen, 1) and the remaining windows will be in a single column
 * @param state
 */
void masterPane(const struct LayoutState* state);

static struct Layout LAYOUT_FAMILIES[] = {
    {.name="full_family", .func=full},
    {.name="column_family", .func=column},
    {.name="master_pane_family", .func=masterPane},
};

// Expose some internal methods/structs if unit testing
#if defined(TILE_IMPL) || defined(UNIT_TESTING)

#ifndef MAX_TILE_DIMS
#define MAX_TILE_DIMS 5
#elif MAX_TILE_DIMS < 5
#error MAX_TILE_DIMS is too small
#endif

#ifndef TILE_TYPE
#define TILE_TYPE short
#endif

#ifndef TILE_USER_DATA_TYPE
#define TILE_USER_DATA_TYPE const void*
#endif

typedef TILE_TYPE  tile_type_t;
void layoutTransformConfig(const struct LayoutState* state, tile_type_t* config);

enum {TILE_INDEX_X = 0, TILE_INDEX_Y = 1, TILE_INDEX_WIDTH, TILE_INDEX_HEIGHT, TILE_INDEX_BORDER};


typedef void (*TileFunc)(const struct LayoutState*, void*, tile_type_t*) ;


typedef struct {
    TileFunc preTileWindow;
    TileFunc tileWindow;
    int (*tileFilterFunc)();
    int defaultBorderWidth;
    int maxWindowsToTile;
    char hideInvisibleWindows;

} LayoutSettings;

/**
 * Contains info provided to layout functions (and helper methods) that detail how the windows should be tiled
 */
typedef struct LayoutState {
    /// Customized to the layout family
    const LayoutArgs* args;
    tile_type_t* bounds;
    /// number of windows that should be tiled
    const int numWindows;
    void* data;
    int len;
    TILE_USER_DATA_TYPE userData;
    const LayoutSettings* settings;
} LayoutState;

#endif


#ifdef TILE_IMPL

struct Layout FULL = {.name = "Full", .func = full, .args = {.noBorder = 1} },
       GRID = {.name = "Grid", .func = column},
       TWO_COL = {.name = "2 Col", .func = column, .args = {.dim = 0, .arg = 2} },
       THREE_COL = {.name = "2 Col", .func = column, .args = {.dim = 0, .arg = 3} },
       TWO_ROW = {.name = "2 Row", .func = column, .args = {.dim = 1, .arg = 2} },
       TWO_PANE = {.name = "2 Pane", .func = column, .args = {.limit = 2, .dim = 0, .arg = 2} },
       TWO_PANE_H = {.name = "2 HPane", .func = column, .args = {.limit = 2, .dim = 1, .arg = 2 } },
       MASTER = {.name = "Master", .func = masterPane, .args = {.arg = .7, .argStep = .1} },
       MASTER_H = {.name = "Master", .func = masterPane, .args = {.dim = 1, .arg = .7, .argStep = .1} },
       TWO_MASTER = {.name = "2 Master", .func = masterPane, .args = {.limit = 2, .arg = .7, .argStep = .1} },
       TWO_MASTER_FLIPPED = {.name = "2 Master Flipped", .func = masterPane, .args = {.limit = 2, .transform = ROT_180, .arg = .7, .argStep = .1} },
       TWO_MASTER_H = {.name = "2 HMaster", .func = masterPane, .args = {.limit = 2, .dim = 1, .arg = .7, .argStep = .1} };
static struct Layout* defaultLayouts[] = {&FULL, &GRID, &TWO_ROW, &TWO_COL, &THREE_COL, &TWO_PANE, &TWO_PANE_H, &MASTER, &TWO_MASTER, &TWO_MASTER_FLIPPED,  &TWO_MASTER_H};

void increaseLayoutArg(int index, int step, struct Layout* l) {
    if (l) {
        switch(index) {
            case LAYOUT_LIMIT:
                l->args.limit += step;
                break;
            case LAYOUT_PADDING:
                l->args.leftPadding += step;
                l->args.topPadding += step;
                l->args.rightPadding += step;
                l->args.bottomPadding += step;
                break;
            case LAYOUT_LEFT_PADDING:
                l->args.leftPadding += step;
                break;
            case LAYOUT_TOP_PADDING:
                l->args.topPadding += step;
                break;
            case LAYOUT_RIGHT_PADDING:
                l->args.rightPadding += step;
                break;
            case LAYOUT_BOTTOM_PADDING:
                l->args.bottomPadding += step;
                break;
            case LAYOUT_NO_BORDER:
                l->args.noBorder = !l->args.noBorder;
                break;
            case LAYOUT_NO_ADJUST_FOR_BORDERS:
                l->args.noAdjustForBorders = !l->args.noAdjustForBorders;
                break;
            case LAYOUT_DIM:
                l->args.dim = !l->args.dim;
                break;
            case LAYOUT_TRANSFORM:
                l->args.transform = ((l->args.transform + step % TRANSFORM_LEN + TRANSFORM_LEN) % TRANSFORM_LEN) ;
                break;
            case LAYOUT_ARG:
                l->args.arg += l->args.argStep * step;
                break;
        }
    }
}

/**
 * Provides a transformation of config
 *
 * @param state
 * @param config the config that will be modified
 */
void layoutTransformConfig(const LayoutState* state, tile_type_t* config) {
    if (state->args) {
        int endX = state->bounds[TILE_INDEX_X] * 2 + state->bounds[TILE_INDEX_WIDTH];
        int endY = state->bounds[TILE_INDEX_Y] * 2 + state->bounds[TILE_INDEX_HEIGHT];
        switch(state->args->transform) {
            case NONE:
                break;
            case REFLECT_HOR:
                config[TILE_INDEX_X] = endX - (config[TILE_INDEX_X] + config[TILE_INDEX_WIDTH]);
                break;
            case REFLECT_VERT:
                config[TILE_INDEX_Y] = endY - (config[TILE_INDEX_Y] + config[TILE_INDEX_HEIGHT]);
                break;
            case ROT_180:
                config[TILE_INDEX_X] = endX - (config[TILE_INDEX_X] + config[TILE_INDEX_WIDTH]);
                config[TILE_INDEX_Y] = endY - (config[TILE_INDEX_Y] + config[TILE_INDEX_HEIGHT]);
                break;
        }
    }
}

static void layoutAdjustBorders(const LayoutState* state, tile_type_t* config) {
    if (state->args) {
        config[TILE_INDEX_BORDER] = state->args->noBorder ? 0 : state->settings->defaultBorderWidth;
        if (!state->args->noAdjustForBorders) {
            config[TILE_INDEX_WIDTH] -= config[TILE_INDEX_BORDER] * 2;
            config[TILE_INDEX_HEIGHT] -= config[TILE_INDEX_BORDER] * 2;
        }
    }
}

static void layoutApplyPadding(const LayoutState* state, tile_type_t* config) {
    if (state->args)
        for (int i = 0; i <= TILE_INDEX_HEIGHT; i++) {
            if (i < TILE_INDEX_WIDTH)
                config[i] += (&state->args->leftPadding)[i];
            else
                config[i] -= (&state->args->rightPadding)[i % 2] + (&state->args->leftPadding)[i % 2];
        }
}


int layoutApply(const struct Layout* layout, const tile_type_t* bounds, void* data, int len, void*userData, const LayoutSettings* settings) {
    if(!layout) {
        return 0;
    }
    int maxWindowToTile = settings->maxWindowsToTile;
    if (!maxWindowToTile) {
        for(int i = 0; i < len; i++) {
            const void* element = getDataElem(data, i);
            if (!settings->tileFilterFunc || settings->tileFilterFunc(userData, element))
                maxWindowToTile++;
        }
    }
    if (layout->args.limit)
        maxWindowToTile = TILE_MIN(maxWindowToTile, layout->args.limit);

    if (maxWindowToTile && layout->func) {
        tile_type_t values[MAX_TILE_DIMS];
        for(int i = 0; i < MAX_TILE_DIMS; i++)
            values[i] = bounds[i];
        LayoutState state = {.args = &layout->args, .bounds = values, .numWindows = maxWindowToTile, .data=data, .len=len, .userData=userData, .settings = settings};
        layout->func(&state);
    }
    return maxWindowToTile;
}

static unsigned int _layoutHelperSplit(const struct LayoutState* state, int offset, const tile_type_t* baseValues, int dim, int num, int last) {
    tile_type_t values[TILE_INDEX_HEIGHT + 1] = {};
    for(int i = 0; i <= TILE_INDEX_HEIGHT; i++)
        values[i] = baseValues[i];
    int sizePerWindow = values[dim] / num;
    int rem = values[dim] % num;
    unsigned i = offset;
    int count = 0;
    while (i < state->len) {

        void* element = getDataElem(state->data, i++);
        if (state->settings->tileFilterFunc && !state->settings->tileFilterFunc(state->userData, element))continue;
        values[dim] = sizePerWindow + (rem-- > 0 ? 1 : 0);

        {
            tile_type_t config[MAX_TILE_DIMS] = {0};
            for (int i = 0; i <= TILE_INDEX_HEIGHT; i++)
                config[i] = values[i];

            layoutTransformConfig(state, config);
            if (state->settings->preTileWindow) {
                state->settings->preTileWindow(state, element, config);
            }
            layoutAdjustBorders(state, config);
            layoutApplyPadding(state, config);

            state->settings->tileWindow(state, element, config);
        }
        count++;

        if (count >= num) {
            if(last) {
                if (state->settings->hideInvisibleWindows)
                    break;
                    sizePerWindow = 0;
                continue;
            }
            break;
        }
        values[dim - 2] += values[dim]; //convert width/height index to x/y
    }
    return i;
}

static int getLayoutDimIndex(int dim) {
    return dim == 0 ? TILE_INDEX_WIDTH : TILE_INDEX_HEIGHT;
}

void full(const LayoutState* state) {
    _layoutHelperSplit(state, 0, state->bounds, getLayoutDimIndex(0), 1, 1);
}

void column(const LayoutState* state) {
    int size = state->numWindows;
    if (size == 1) {
        full(state);
        return;
    }
    int numCol = TILE_MAX(size, state->args->arg == 0 ? log2(size + 1) : state->args->arg);
    int rem = numCol - size % numCol;
    // split of width(0) or height(1)
    int splitDim = getLayoutDimIndex(state->args->dim);
    tile_type_t * values = state->bounds;
    values[splitDim] /= numCol;
    int offset = 0;
    for (int i = 0; i < numCol; i++) {
        offset = _layoutHelperSplit(state, offset, values,
                getLayoutDimIndex(!state->args->dim), size / numCol + (rem-- > 0 ? 0 : 1), i == numCol - 1);
        values[splitDim - 2] += values[splitDim];
    }
}

void masterPane(const LayoutState* state) {
    int size = state->numWindows;
    if (size == 1) {
        full(state);
        return;
    }
    tile_type_t * values = state->bounds;
    int dimIndex = getLayoutDimIndex(state->args->dim);
    int dim = values[dimIndex];
    values[dimIndex] = TILE_MAX(dim * state->args->arg, 1);
    int offset = _layoutHelperSplit(state, 0, values, getLayoutDimIndex(state->args->dim), 1, 0);
    values[dimIndex - 2] += values[dimIndex];
    values[dimIndex] = dim - values[dimIndex];
    _layoutHelperSplit(state, offset, values, getLayoutDimIndex(!state->args->dim), size - 1, 1);
}
#endif // TILE_IMPL

#endif
