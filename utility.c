#include "buffer.h"
#include "logger.h"
#include "memory.h"
#include "settings.h"
#include "utility.h"
#include "view.h"
#include "wee_string.h"
#include "window.h"
#include <assert.h>
#include <ctype.h>
#include <stdio.h>

#ifdef HAVE_GRAPHEME
#include <grapheme.h>
#endif

int getLineNumberOfLine(const Window* win, const char*s) {
    int lineno = 0;
    const char* start, * end, * startOfSection = getPositionFromEffectiveOffset(win, 0);
    while (getSectionBounds(win, startOfSection, &start, &end, &startOfSection) != -1) {
        if (start <= s && s < end) {
            lineno += countInstancesOfChar(start, s, '\n');
            break;
        } else {
            lineno += countInstancesOfChar(start, end, '\n');
        }
    }
    return lineno;
}

int getLastLineNumber(const Window* window) {
    return getLineNumberOfLine(window, getEffectiveEndPosition(window));
}

int getCurrentLineNumber(const Window* win) {
    return getLineNumberOfLine(win, getCursorPosition(win));
}

static const char* getEffectiveEndOfLine(const Window* win, const char* pos, unsigned long * lenp) {
    const char* endOfLine = pos;
    int lenToEnd = 0;
    const char* start, * end, * startOfNextSection = pos;
    for (int i = 0; getSectionBounds(win, startOfNextSection, &start, &end, &startOfNextSection) != -1; i++) {
        const char * s = i == 0 ? pos : start;
        const char* p = findChar(s, end, '\n', 1);
        if (p) {
            lenToEnd += p - s + 1;
            endOfLine = p;
            break;
        }
        lenToEnd += end - s;
    }
    *lenp += lenToEnd;
    return endOfLine;
}

static const char* getEffectiveStartOfLine(const Window* win, const char* pos, unsigned long * colp) {
    int col = 0;
    *colp = 0;
    const char* startOfTargetLine = getPositionFromEffectiveOffset(win, 0);
    const char* endOfBuffer = getEffectiveEndPosition(win);
    if (pos == endOfBuffer ) {
        unsigned long offset = getWindowDataLen(win);
        if (offset) {
            const char* prev = getPositionFromEffectiveOffset(win, offset - 1);
            if (prev && *prev == '\n') {
                return endOfBuffer;
            }
        }
    } else if (pos && *pos == '\n') {
        unsigned long offset = getEffectiveOffsetOfPosition(win, pos);
        if (offset) {
            const char* prev = getPositionFromEffectiveOffset(win, offset - 1);
            if (*prev == '\n') {
                *colp = 1;
                return pos;
            }
            col++;
            pos = prev;
        }
    }
    const char* start, * end, * startOfSection = pos;
    while (pos) {
        int ret = getSectionBounds(win, startOfSection, &start, &end, &startOfSection);
        if  (start != end) {
            int inRange = start <= pos && pos < end;
            const char * e = inRange ? pos: end;
            const char* s = findChar(e - !inRange, start - 1, '\n', -1);
            const char* p = s ? s + 1 : NULL;
            if (p == NULL) {
                col += e - start;
            } else {
                col += e - p;
                startOfTargetLine = p;
                break;
            }
        }
        unsigned long startingOffset = getEffectiveOffsetOfPosition(win, start);
        if (!startingOffset)
            break;
        startOfSection = getPositionFromEffectiveOffset(win, startingOffset - 1);
    }
    if (colp) {
        *colp = col;
    }
    return startOfTargetLine;
}

int getOffsetIntoLine(const Window* win, const char* pos) {
    unsigned long col;
    getEffectiveStartOfLine(win, pos, &col);
    return col;
}

const char* getLine(const Window* win, const char* pos, unsigned long* len) {
    const char * start = getEffectiveStartOfLine(win, pos, len);
    const char* end = getEffectiveEndOfLine(win, pos, len);
    assert(!start || getEffectiveOffsetOfPosition(win, start) == 0 || *getPositionFromEffectiveOffset(win, getEffectiveOffsetOfPosition(win, start) - 1) == '\n');
    return start;
}

const char* getCursorLine(const Window* win, unsigned long* len) {
    return getLine(win, getCursorPosition(win), len);
}

unsigned long clampToLine(const Window* win, const char* pos, unsigned long offset) {
    unsigned long len;
    const char* startOfLine = getLine(win, getCursorPosition(win), &len);
    unsigned long startOffset = getEffectiveOffsetOfPosition(win, startOfLine);
    unsigned long endOffset = startOffset + len;
    return CLAMP(startOffset, offset, startOffset + len - 1);
}

unsigned long clampToCursorLine(const Window* win, unsigned long offset) {
    return clampToLine(win, getCursorPosition(win), offset);
}

unsigned long getCursorCol(const Window* win) {
    const char * cursor = getCursorPosition(win);
    unsigned long len;
    return getEffectiveOffsetOfPosition(win, cursor) - getEffectiveOffsetOfPosition(win, getCursorLine(win, &len));
}

static const char* getNextLineAfterPos(const Window* win, const char* pos, int dir) {
    unsigned long len;
    const char* startOfLine = getLine(win, pos, &len);
    if (dir == 0) {
        return startOfLine;
    } else if (dir > 0) {
        unsigned long offset = getEffectiveOffsetOfPosition(win, startOfLine) + len;
        if (offset == getWindowDataLen(win)) {
            return NULL;
        }
        return getPositionFromEffectiveOffset(win, offset);
    } else {
        unsigned long offset = getEffectiveOffsetOfPosition(win, startOfLine);
        if (offset == 0)
            return NULL;
        return getLine(win, getPositionFromEffectiveOffset(win, offset - 1), &len);
    }
}

const char* getNextLineAfterCursor(const Window*win, int dir) {
    return getNextLineAfterPos(win, getCursorPosition(win), dir);
}
const char* getNthLine(const Window* win, unsigned int targetLineNumber) {
    int offset = 0;
    int iter = targetLineNumber - offset;
    if (0 <= targetLineNumber && targetLineNumber <= offset) {
        iter = 0;
    } else if (iter < 0) {
        iter += getLastLineNumber(win) + 1;
    }
    const char* line = getPositionFromEffectiveOffset(win, 0);
    for (int i = 0; i < iter; i++) {
        const char* p = getNextLineAfterPos(win, line, 1);
        if (!p)
            break;
        line = p;
    }
    return line;
}

int jumpToLine(Window* win, unsigned int targetLineNumber) {
    setCursorPosition(win, getNthLine(win, targetLineNumber));
    return 0;
}

const char* getNextCharInCurrentLineOfWindow(Window* win, int dir, char c) {
    unsigned long len;
    const char* startOfLine = getCursorLine(win, &len);
    if (len == 0)
        return NULL;
    const char* endOfLine = startOfLine + len;
    const char* cursor = getCursorPosition(win);
    if (cursor && *cursor == c && startOfLine <= cursor + dir && cursor + dir <= endOfLine)
        cursor += dir;
    return findChar(cursor, dir < 0 ? startOfLine - 1 : endOfLine, c, dir);
}

int jumpToCharInCurrentLineOfWindow(Window* win, int dir, char c) {
    const char* pos = getNextCharInCurrentLineOfWindow(win, dir, c);
    if (pos) {
        setCursorPosition(win, pos);
        return 0;
    }
    return -1;
}

const char* getNumCharsUsed(const char * p, const char* end, const ViewSettings* settings, CountCharResult * result) {
    result->bytesUsed = 1;
    result->len = 1;
    result->xDelta = 1;
    if (isEnabled(settings, bytes_per_cell_index)) {
        result->bytesUsed = getNumericSettings(settings, bytes_per_cell_index);
        return p;
    } else if (*p == '\n' || iscntrl(*p)) {
        int isListSet = isEnabled(settings, list_index);
        if (*p == '\t' && isEnabled(settings, tabstop_index) && !isListSet) {
            result->xDelta = getNumericSettings(settings, tabstop_index);
            return p;
        } else if (*p == '\n') {
            result->yDelta = 1;
            result->scratchSpace[0] = '$'; // only matters if the 'list' option is set
            return isEnabled(settings, list_index) ? result->scratchSpace : NULL;
        } else if (isEnabled(settings, display_cntrl_index)) {
            if (isAltVersion(settings, display_cntrl_index)) {
                result->len = 4;
                result->scratchSpace[0] = '<';
                snprintf(result->scratchSpace + 1, 3, "%X", *p);
                result->scratchSpace[3] = '>';
            } else {
                result->len = 2;
                result->scratchSpace[0] = '^';
                result->scratchSpace[1] = '@' + *p;
            }
            result->xDelta = result->len;
            return result->scratchSpace;
        }
    } else if (isEnabled(settings, unicode_index)){
#ifdef HAVE_GRAPHEME
        int ret = grapheme_next_character_break_utf8(p, end - p);
        if (ret > 1) {
            result->bytesUsed = ret;
            result->len = ret;
            return p;
        }
#endif
    }
    return p;
}
