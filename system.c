#include "buffer.h"
#include "logger.h"
#include "memory.h"
#include "settings.h"
#include "system.h"

#include <assert.h>
#include <errno.h>
#include <fcntl.h>
#include <poll.h>
#include <signal.h>
#include <stdio.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <unistd.h>


#ifndef DEFAULT_IO_BUFFER_SIZE
#define DEFAULT_IO_BUFFER_SIZE 4096
#endif

typedef struct {
    void* userData;
    int (*onPoll)();
    int (*onCompletion)();
} PollFDInfo;

static Buffer pollFdBuffer;
static Buffer pollFdInfoBuffer;

static struct pollfd * getPollFds() {
    return pollFdBuffer.raw;
}

static PollFDInfo* getPollFdInfo() {
    return pollFdInfoBuffer.raw;
}

static int getNumFds() {
    return pollFdBuffer.size / sizeof(struct pollfd);
}

int isIdle() {
    return !getNumFds();
}
static int inline getPollFlag(IOEventType mode) {return mode == EVENT_READ ? POLLIN : POLLOUT;}

int findPollFd(int fd, IOEventType mode) {
    int i = 0;
    FOR_EACH(struct pollfd*, pollfdInfo, &pollFdBuffer, 0) {
        if (pollfdInfo->fd == fd && pollfdInfo->events == getPollFlag(mode)) {
            return i;
        }
        i++;
    }
    return -1;
}

int addPollFd(int fd, IOEventType mode, int (*callback)(), int (*onCompletion)(), void* userData) {
    struct pollfd pollFd = {.fd = fd, .events = mode == EVENT_READ ? POLLIN : POLLOUT};
    PollFDInfo pollfdInfo = (PollFDInfo){.userData = userData, .onPoll = callback, .onCompletion = onCompletion};

    if (appendToBuffer(&pollFdBuffer, &pollFd, sizeof(pollFd)) == -1) {
        return -1;
    }
    if (appendToBuffer(&pollFdInfoBuffer, &pollfdInfo, sizeof(pollfdInfo)) == -1) {
        removeFromBuffer(&pollFdBuffer, pollFdBuffer.size - sizeof(pollFd), sizeof(pollFd));
        return -1;
    }
    TRACE("Added pollfd %d; Num %d\n", fd, getNumFds());
    return 0;
}

static void togglePollFd(int i) {
    struct pollfd * pollfds = getPollFds();
    pollfds[i].fd = ~pollfds[i].fd;
}

int removePollFd(int i) {
    if (i < 0 || i >= getNumFds())
        return -1;
    struct pollfd * pollfds = getPollFds();
    removeFromBuffer(&pollFdBuffer, i * sizeof(struct pollfd), sizeof(struct pollfd));
    removeFromBuffer(&pollFdInfoBuffer, i * sizeof(PollFDInfo), sizeof(PollFDInfo));
    TRACE("Removed pollfd; Num %d\n", getNumFds());
    return 0;
}

static int isHarmlessError() {
    return errno == EAGAIN || errno == EWOULDBLOCK || errno == EINTR;
}

static int waitOnChild(pid_t child, short* exitStatus) {
    int waitStatus;
    int ret = waitpid(child, &waitStatus, WNOHANG);
    if (ret > 0) {
        *exitStatus = WIFEXITED(waitStatus) ? WEXITSTATUS(waitStatus) : 0x80 | WTERMSIG(waitStatus);
    }
    return ret;
}

static inline int weeFlagsToOpenFlags(int flags) {
    int openFlags = 0;
    if (flags & OPEN_IO_CREATE) {
        openFlags |= O_CREAT;
    }
    if( flags & OPEN_IO_NON_BLOCKING ) {
        openFlags |= O_NONBLOCK;
    }
    if( flags & OPEN_IO_APPEND) {
        openFlags |= O_APPEND;
    }
    if( flags & OPEN_IO_TRUNCATE) {
        openFlags |= O_TRUNC;
    }
    if( flags & OPEN_IO_CLOSE_ON_EXEC ) {
        openFlags |= O_CLOEXEC;
    }

    if( (flags & OPEN_IO_READ_WRITE) == OPEN_IO_READ_WRITE) {
        openFlags |= O_RDWR;
    } else {
        if (flags & OPEN_IO_READ) {
            openFlags |= O_RDONLY;
        } else
            openFlags |= O_WRONLY;
    }
    return openFlags;
}

int openFile(const char* name, int openFlags) {
    assert(name);
    assert(openFlags);
    int fd = open(name, weeFlagsToOpenFlags(openFlags), 0666);
    if(fd == -1) {
        SYS_TRACE("Failed to open file %s with flags 0x%X\n", name, openFlags);
        if (errno == ENOENT) {
            return -1;
        }
        return -1;
    }
    TRACE("Opening '%s' with openFlags 0x%X as fd %d\n", name, openFlags, fd);
    return fd;
}

int removeFile(const char* name) {
    return unlink(name);
}

int readDataFromFD(int fd, void* data, int len) {
    TRACE("Attempting to read %d bytes from fd %d\n", len, fd);
    int offset = 0;
    int ret;
    while ((ret = read(fd, data + offset, len - offset)) && offset < len) {
        if(ret == -1) {
            if (isHarmlessError()) {
                if (offset)
                    break;
                else
                    continue;
            }
            SYS_TRACE("read error from fd %d\n", fd);
            return -1;
        } else {
            offset += ret;
        }
    }
    TRACE("Read %d bytes from fd %d\n", offset, fd);
    return offset;
}

int writeDataToFD(int fd, const void* data, int len) {
    TRACE("Attempting to write %d bytes from fd %d\n", len, fd);
    int offset = 0;
    int ret;
    while(offset < len && (ret = write(fd, data + offset, len - offset))) {
        if(ret == -1) {
            if (isHarmlessError()) {
                if (offset)
                    break;
                else
                    continue;
            }
            SYS_TRACE("write error %d", fd);
            return -1;
        } else {
            offset += ret;
        }
    }
    TRACE("Wrote %d bytes from fd %d\n", offset, fd);
    return offset;
}

#define IO_FLAG_ASYNC           (1<<0)
#define IO_FLAG_NO_ERROR        (1<<2)
#define IO_FLAG_EXIT_STATUS_SET (1<<3)
#define IO_FLAG_FD_CLOSED       (1<<4)

typedef struct {
    void* userData;
    OnProgressFunc onProgress;
    OnCompletionFunc onCompletion;
    Buffer* buffer;
    size_t offset;
    size_t transfered;
    size_t maxTransferBytes;
    pid_t child;
    short exitStatus;
    unsigned char ioFlags;
} IOState;

static int _resumeIO(IOState* state, int fd, IOEventType type) {
    int ret;
    const int transferred = state->transfered;
    int complete = 0;
    while (!complete && (state->transfered - transferred) < MAX_SYNC_IO_TRANSFER_BYTES) {
        if (type == EVENT_READ) {
                if (state->offset == state->buffer->maxSize) {
                    growBuffer(state->buffer, state->offset*2);
                }
                ret = readDataFromFD(fd, state->buffer->data + state->offset, MIN(state->maxTransferBytes, state->buffer->maxSize - state->offset));
                if (ret != -1) {
                    resizeBuffer(state->buffer, MAX(state->buffer->size, state->offset + ret));
                }
        } else {
                ret = writeDataToFD(fd, state->buffer->readOnlyData + state->offset, MIN(state->maxTransferBytes, state->buffer->size - state->offset));
        }

        if (ret == -1) {
            if (!isHarmlessError()) {
                SYS_TRACE("io failed for fd %d; type %d\n", fd, type);
                complete = 1;
            }
        } else  {
            state->offset += ret;
            state->transfered += ret;
            state->maxTransferBytes -= ret;
            if (state->onProgress) {
                ProgressStatus status = {state->userData, state->offset - ret, ret};
                if (state->onProgress(&status) == -1) {
                    state->ioFlags |= IO_FLAG_NO_ERROR;
                    complete = 1;
                    continue;
                }
                if (state->buffer->size == 0) {
                    state->offset = 0;
                }
            }
            if (ret == 0 || state->maxTransferBytes == 0 || (type != EVENT_READ && state->offset == state->buffer->size)) {
                state->ioFlags |= IO_FLAG_NO_ERROR;
                complete = 1;
            }
        }
    }
    if (complete) {
        state->ioFlags |= IO_FLAG_FD_CLOSED;
        close(fd);
        return -1;
    }
    return 0;
}

static int resumeRead(IOState* state, int fd) {
    return _resumeIO(state, fd, EVENT_READ);
}

static int resumeWrite(IOState* state, int fd) {
    return _resumeIO(state, fd, EVENT_WRITE);
}

static int storeExitStatus(IOState* state) {
    assert(state);
    if (state->ioFlags & IO_FLAG_EXIT_STATUS_SET)
        return 0;
    int ret = waitOnChild(state->child, &state->exitStatus);
    if (ret == 0) {
        return -1;
    }
    state->ioFlags |= IO_FLAG_EXIT_STATUS_SET;
    return 0;
}

static int onIOCompletion(IOState* state, int fd) {
    if (fd >= 0 && !(state->ioFlags & IO_FLAG_FD_CLOSED)) {
        state->ioFlags |= IO_FLAG_FD_CLOSED;
        close(fd);
    }
    if (state->child != -1) {
        if (storeExitStatus(state) == -1) {
            TRACE("IO Completed but child hasn't terminated\n");
            return -1;
        }
    }
    int exitStatus = state->exitStatus;
    TRACE("IO Completed flags: 0x%x exitStatus %d\n", state->ioFlags, state->exitStatus);
    if(state->onCompletion) {
        CompletionStatus status = {state->userData, state->ioFlags & IO_FLAG_NO_ERROR ? exitStatus: -1};
        state->onCompletion(&status);
    }
    if (state->ioFlags & IO_FLAG_ASYNC) {
        wee_free(state);
    }
    return exitStatus;
}

static int doIO(int fd, Buffer* buffer, IOEventType iotype, int offset, int len, pid_t child, OnProgressFunc onProgress, OnCompletionFunc onCompletion, void* userData) {
    IOState state = {.buffer = buffer, .offset = offset, .maxTransferBytes = len, .child = child, .onProgress = onProgress, .onCompletion=onCompletion, .userData=userData};
    int (*ioFunc)(IOState* state, int fd) = iotype == EVENT_READ ? resumeRead : resumeWrite;
    int complete = 0;
    if (fd != -1) {
        if (!buffer->data && iotype == EVENT_READ) {
            if (growBuffer(buffer, buffer->maxSize ? buffer->maxSize : DEFAULT_IO_BUFFER_SIZE) == -1)
                return -1;
        }
        struct pollfd localPollfd = {fd, .events  = iotype == EVENT_READ ? POLLIN : POLLOUT};
        if (poll(&localPollfd, 1, 0) == 1) {
            complete = ioFunc(&state, fd) == -1;
        }
    } else {
        assert(iotype == EVENT_CHILD);
        complete = 1;
        state.ioFlags |= IO_FLAG_NO_ERROR;
    }
    if (complete) {
        int ret;
        if ((ret = onIOCompletion(&state, fd)) != -1)
            return ret;
        fd = -1;
    }
    TRACE("%s IO on fd %d, child %d didn't finish; going async\n", iotype == EVENT_READ ? "Read" : "Write", fd, child);
    IOState* _state = wee_malloc(ALLOCATION_MISC, sizeof(IOState));
    *_state = state;
    _state->ioFlags |= IO_FLAG_ASYNC;
    addPollFd(fd, iotype, ioFunc, onIOCompletion, _state);
    return 0;
}

int writeDataToFd(int fd, const Buffer* buffer, int offset, int len, OnProgressFunc onProgress, OnCompletionFunc onCompletion, void* userData) {
    assert(fd != -1);
    return doIO(fd, (Buffer*)buffer, EVENT_WRITE, offset, len, -1, onProgress, onCompletion, userData);
}

int readDataFromFd(int fd, Buffer* buffer, int offset, OnProgressFunc onProgress, OnCompletionFunc onCompletion, void* userData) {
    assert(fd != -1);
    return doIO(fd, buffer, EVENT_READ, offset, -1, -1, onProgress, onCompletion, userData);
}

int readDataFromStdin(Buffer* buffer, OnProgressFunc onProgress, OnCompletionFunc onCompletion, void* userData) {
    return readDataFromFd(STDIN_FILENO, buffer, 0, onProgress, onCompletion, userData);
}

int pipeFromFdToFd(int fromParentFd, int toParentFd, const char* cmd, int* fdsToCloseInChild, int len) {
    pid_t pid = fork();
    if (!pid) {
        for(int i = 0; i < len; i++)
            if (fdsToCloseInChild[i] != -1)
                close(fdsToCloseInChild[i]);
        if (fromParentFd != -1) {
            if(dup2(fromParentFd, STDIN_FILENO) == -1) {
                SYS_TRACE("dup2 STDIN_FILENO\n");
                exit(-1);
            }
            close(fromParentFd);
        }
        if (toParentFd != -1) {
            if(dup2(toParentFd, STDOUT_FILENO) == -1) {
                SYS_TRACE("dup2 STDOUT_FILENO\n");
                exit(-1);
            }
            close(toParentFd);
        }
        execl("/bin/sh", "/bin/sh", "-c", cmd, NULL);
        exit(-1);
    }
    if (fromParentFd != -1)
        close(fromParentFd);
    if (toParentFd != -1)
        close(toParentFd);
    return pid;
}

int filterWithCmd(const char* cmd, Buffer* bufferSend, unsigned long offsetSend, unsigned long lenSend, Buffer* bufferReceive, unsigned long offsetReceive, OnProgressFunc onProgress, OnCompletionFunc onCompletion, void* userData) {
    int fdToChild[2] = {-1, -1};
    int fdFromChild[2] = {-1, -1};
    if (bufferSend && pipe(fdToChild) == -1)
        return -1;
    if (bufferReceive && pipe(fdFromChild) == -1) {
        if (fdToChild[0] != -1) {
            close(fdToChild[0]);
            close(fdToChild[1]);
        }
        return -1;
    }
    int fdsToCloseInChild[] = {fdToChild[1], fdFromChild[0]};
    pid_t child = pipeFromFdToFd(fdToChild[0], fdFromChild[1], cmd, fdsToCloseInChild, 2);
    int ret;
    if (bufferSend) {
        ret = doIO(fdToChild[1], bufferSend, EVENT_WRITE, offsetSend, lenSend, bufferReceive ? -1 : child, onProgress, bufferReceive ? NULL : onCompletion, userData);
        if (ret == -1) {
            if (fdFromChild[0] != -1)
                close(fdFromChild[0]);
            return -1;
        }
    }
    if (bufferReceive) {
        ret = doIO(fdFromChild[0], bufferReceive, EVENT_READ, offsetReceive, -1, child, onProgress, onCompletion, userData);
    }
    if (!bufferReceive && !bufferSend) {
        assert(child != -1);
        ret = doIO(-1, NULL, EVENT_CHILD, 0, 0, child, onProgress, onCompletion, userData);
    }
    return ret;
}

int runCmd(const char* cmd, OnCompletionFunc onCompletion, void* userData) {
    return filterWithCmd(cmd, NULL, 0, 0, NULL, 0, NULL, onCompletion, userData);
}

int writeToCmd(const char* cmd, Buffer* buffer, unsigned long offset, unsigned long len, OnProgressFunc onProgress, OnCompletionFunc onCompletion, void* userData) {
    return filterWithCmd(cmd, buffer, offset, len, NULL, 0, onProgress, onCompletion, userData);
}

int readFromCmd(const char* cmd, Buffer* buffer, unsigned long offset, OnProgressFunc onProgress, OnCompletionFunc onCompletion, void* userData) {
    return filterWithCmd(cmd, NULL, 0, 0, buffer, offset, onProgress, onCompletion, userData);
}

int changeWorkingDirectory(const char* path) {
    return chdir(path);
}

int suspendProcess() {
    return kill(getpid(), SIGTSTP);
}

int isTTY() {
    return isatty(STDIN_FILENO);
}

int waitForEvent(int timeout) {
    struct pollfd * pollfds = getPollFds();
    const PollFDInfo* pollfdInfo = getPollFdInfo();
    // If there are no events sources and we are being asked to wait forever return -1
    if (isIdle()) {
        return -1;
    }

    while (1) {
        int ret = 0;
        for (int i = getNumFds() - 1; i >= 0; i--) {
            if (pollfds[i].fd < 0 && pollfdInfo[i].onCompletion == onIOCompletion) {
                IOState* state = pollfdInfo[i].userData;
                // Note there is a race here between checking if the child is a zombie
                // and calling poll below. If we are unlucky, a child could become a zombie
                // after this check but before the call to poll. In this case, we'd block
                // until the next IO event/signal.
                //
                // Solving this is complicated and
                // (1) we don't expect child to live long after they close their buffer
                // (2) the caller won't immediately block forever after an event
                if (state->child != -1 && storeExitStatus(state) == 0) {
                    ret++;
                }
            }
        }

        if (ret) {
            TRACE("Some child process completed; returning without calling poll\n");
            return ret;
        }

        ret = poll(pollfds, getNumFds(), timeout);
        if(ret == -1) {
            if (isHarmlessError())
                continue;
            SYS_TRACE("Polling\n");
            return -1;
        }
        return ret;
    }
}

int processEvents() {
    int numEvents = 0;

    struct pollfd * pollfds = getPollFds();
    PollFDInfo* pollfdInfo = getPollFdInfo();

    for (int i = getNumFds() - 1; i >= 0; i--) {
        int complete = 0;
        if (pollfds[i].revents) {
            if(pollfds[i].revents & (POLLIN|POLLOUT)) {
                complete = pollfdInfo[i].onPoll(pollfdInfo[i].userData, pollfds[i].fd, pollfds[i].revents);
                // The underlying memory may have changed so refresh it
                pollfds = getPollFds();
                pollfdInfo = getPollFdInfo();
            } else if(pollfds[i].revents & ~(POLLIN|POLLOUT)) {
                TRACE("Error occurred on index %d 0x%X\n", i, pollfds[i].revents);
                complete = 1;
            }
            pollfds[i].revents = 0;
            numEvents++;
        }
        // TODO consider allowing an FD to be paused
        if (pollfds[i].fd < 0 || complete) {
            if(!pollfdInfo[i].onCompletion || pollfdInfo[i].onCompletion(pollfdInfo[i].userData, pollfds[i].fd) != -1) {
                removePollFd(i);
                numEvents += !complete;
            } else if (pollfds[i].fd >= 0){
                TRACE("IO completed but still waiting on onCompletion to return something != -1\n");
                togglePollFd(i);
            }

            // The underlying memory may have changed so refresh it
            pollfds = getPollFds();
            pollfdInfo = getPollFdInfo();
        }
    }
    return numEvents;
}
