#ifndef ENUM_CONVERSION_HELPER_H
#define ENUM_CONVERSION_HELPER_H

#include <strings.h>
#include <stddef.h>
typedef struct EnumInfo {
    unsigned long num;
    const char* name;
} EnumInfo;

static int getEnumFromString(const EnumInfo* info, int len, const char* s) {
    for(int i = 0; i < len; i++) {
        if (strcasecmp(info[i].name, s) == 0)
            return info[i].num;
    }
    return -1;
}

static const char* getStrFromEnum(const EnumInfo* info, int len, long value) {
    for(int i = 0; i < len; i++) {
        if (info[i].num == value)
            return info[i].name;
    }
    return NULL;
}

#endif
