#ifndef SETTINGS_DEFAULTS_H
#define SETTINGS_DEFAULTS_H 1

#define DEFAULT_BIGWORD_BOUNDARY_REGEX "[^[:blank:]]+|\n+"
#define DEFAULT_WORD_BOUNDARY_REGEX "[[:alnum:]_]+|[^[:alnum:]_[:blank:]]+|\n+"
// Skip handling of 2 char pair (see ex)
#define DEFAULT_SECTION_BOUNDARY_REGEX "^\f|^{|^\\.[A-z][A-z]|^\\.[A-z]|^\\.[A-z]$"
// Inherits from section boundary
#define DEFAULT_PARAGRAPH_BOUNDARY_REGEX DEFAULT_SECTION_BOUNDARY_REGEX "|\n+"
// Inherits from paragraph boundary;
#define DEFAULT_SENTENCE_BOUNDARY_REGEX "(" DEFAULT_PARAGRAPH_BOUNDARY_REGEX ")|" DEFAULT_PARAGRAPH_BOUNDARY_REGEX "([^[:blank:]])" "|[.?!][])'\"]*  ([^[:blank:]])|^[[:blank:]]*([^[:blank:]])"

#endif
// The following is of format
// SettingType
// name - name # _index will be how the setting is accessed
// bits - bit
//      0: Default enabled
//      1: Default alt implementation
//      2: Enabled in vi by default
//      3: Enabled in vim by default
// value
//      .i - default int value
//      .s - default str value
//
//

#ifndef SETTINGS_LIST
#define GLOBAL_SETTING(TYPE, NAME, ...) INIT_SETTING(TYPE, NAME, SETTINGS_GLOBAL, __VA_ARGS__)
#ifndef WINDOW_SETTING
#define WINDOW_SETTING(TYPE, NAME, ...) INIT_SETTING(TYPE, NAME, SETTINGS_WINDOW, __VA_ARGS__)
#endif
#ifndef VIEW_SETTING
#define VIEW_SETTING(TYPE, NAME, ...)   INIT_SETTING(TYPE, NAME, SETTINGS_VIEW, __VA_ARGS__)
#endif
#define SETTINGS_LIST \
    GLOBAL_SETTING(SETTING_BOOL,   __null__,            , 0b0000) /* Blackhole setting */\
    VIEW_SETTING(SETTING_BOOL,     appendonly,          , 0b0000) /* File will be appended to when writing */\
    GLOBAL_SETTING(SETTING_POINTER,autocommand,         , 0b1101) /* Enables autocommands */ \
    VIEW_SETTING(SETTING_BOOL,     autocreate,          , 0b0001) /* Auto create file when writing. Alt version will additionally do so when reading*/\
    VIEW_SETTING(SETTING_BOOL,     autocreatedir,       , 0b0000) /* Auto create parent dir(s) when writing. Alt version will additionally do so when reading*/ \
    VIEW_SETTING(SETTING_BOOL,     autoindent,        ai, 0b0000) \
    VIEW_SETTING(SETTING_BOOL,     autowrite,         aw, 0b0000) /* If set, then the view will automatically be saved after changes*/ \
    VIEW_SETTING(SETTING_STR,      bigword_regex,       , 0b0001, .s = DEFAULT_BIGWORD_BOUNDARY_REGEX) \
    VIEW_SETTING(SETTING_LONG,     bytes_per_cell,      , 0b0000, .i = 1) \
    VIEW_SETTING(SETTING_BOOL,     closeonexec,         , 0b0001) /* File descriptions will not be inherited by children */\
    GLOBAL_SETTING(SETTING_ENV_INT,columns,             , 0b1101) /* Assume the number of columns is the value specified instead of the system detected value */\
    VIEW_SETTING(SETTING_ENUM,     default_bg,          , 0b0001, .i = 0) \
    VIEW_SETTING(SETTING_ENUM,     default_fg,          , 0b0001, .i = 0xFFFFFF) \
    VIEW_SETTING(SETTING_ENUM,     default_style,       , 0b0001) \
    WINDOW_SETTING(SETTING_BOOL,   display_cntrl,       , 0b0001) /*Controls how cntrl chars are displayed. If enabled, they will be displayed like ^V. The Alt implementation will use hex like <xx>. This corresponds to default in vim and display=uhex respectively */\
    GLOBAL_SETTING(SETTING_LONG,   event_counter,       , 0b0001) /* Tracks the number of events*/ \
    GLOBAL_SETTING(SETTING_ENV_STR,exinit,              , 0b1101) /* File containing commands to run on startup */\
    VIEW_SETTING(SETTING_CHAR,     expandtab,           , 0b0000, .i = 8) \
    VIEW_SETTING(SETTING_ENUM,     filetype,            , 0b0001)\
    VIEW_SETTING(SETTING_CHAR,     filetype_dialect,    , 0b0001) \
    VIEW_SETTING(SETTING_BOOL,     interactive,         , 0b1101) /* If set, the assume we are in an interactive session */ \
    WINDOW_SETTING(SETTING_BOOL,   linemode,            , 0b0001) /* Command address will snap to start of a line. If unset, they will be byte ranges*/\
    WINDOW_SETTING(SETTING_STR,    linenumberf,         , 0b0000, .s = "%3d ") /* Format for line numbers*/\
    GLOBAL_SETTING(SETTING_ENV_INT,lines,               , 0b1101) /* Assume the number of rows is the value specified instead of the system detected value */\
    WINDOW_SETTING(SETTING_BOOL,   list,                , 0b0000) /* If enabled, '\n' is rendered as '$' and '\t' is render like other control chars*/\
    GLOBAL_SETTING(SETTING_ENUM,   mode,                , 0b1101, .i = MODE_NORMAL)\
    VIEW_SETTING(SETTING_BOOL,     modified,            , 0b0000) /* If set, then the view has been modified since it was last saved*/ \
    VIEW_SETTING(SETTING_BOOL,     nonblocking,         , 0b0001) /* IO will be made non blocking */\
    VIEW_SETTING(SETTING_BOOL,     opentrunc,           , 0b0000) /* File will be truncated when opened for writing. Alt version will additionally do so for reading */\
    VIEW_SETTING(SETTING_STR,      paragraph_regex,     , 0b0011, .s = DEFAULT_PARAGRAPH_BOUNDARY_REGEX) \
    GLOBAL_SETTING(SETTING_CHAR,   print_flags,         , 0b1101) /* Set of flags to control printing in ex mode */\
    VIEW_SETTING(SETTING_BOOL,     readonly,            , 0b0000) \
    VIEW_SETTING(SETTING_STR,      section_regex,       , 0b0011, .s = DEFAULT_SECTION_BOUNDARY_REGEX) \
    VIEW_SETTING(SETTING_STR,      sentence_regex,      , 0b0011, .s = DEFAULT_SENTENCE_BOUNDARY_REGEX) \
    GLOBAL_SETTING(SETTING_ENV_STR,shell,               , 0b1101) /* SHELL env; If disabled, null or unset, the use of the shell will be disabled */\
    GLOBAL_SETTING(SETTING_ENUM,   show_cmd_bar,        , 0b0011, .i = DOCK_BOTTOM) /* Show the cmd window if focused; Alt version always displays it*/ \
    GLOBAL_SETTING(SETTING_ENUM,   show_info_bar,       , 0b0011, .i = DOCK_BOTTOM) /* Show the info/cmd window if focused; Alt version always displays it*/ \
    WINDOW_SETTING(SETTING_BOOL,   snap_to_visible,     , 0b1101) /* Snaps cursor position to a visible char*/\
    WINDOW_SETTING(SETTING_STR,    statusbarstr,        , 0b0000, .s = "${NAME} | ${LINENO},${COLNO} ${PERCENT_LINES}%") \
    VIEW_SETTING(SETTING_CHAR,     tabstop,             , 0b0001, .i = 8) \
    GLOBAL_SETTING(SETTING_ENV_STR,term,                , 0b1101) /* TERM env; If disabled, null or unset, an unspecified terminal will be assumed */\
    GLOBAL_SETTING(SETTING_INT,    timeout,             , 0b0001, .i = 1000) /* After going this amount of time in ms, all pending events will be flushed. If disabled, then we'd wait until a match could be found*/\
    WINDOW_SETTING(SETTING_BOOL,   unicode,             , 0b0000) /* Enables unicode support. Only has an effect if compiled with a unicode library like libgrapheme */\
    GLOBAL_SETTING(SETTING_INT,    window,              , 0b1101, .i=-1) /* Controls height of the bounds to draw windows*/\
    VIEW_SETTING(SETTING_STR,      word_regex,          , 0b0001, .s = DEFAULT_WORD_BOUNDARY_REGEX) \
    WINDOW_SETTING(SETTING_BOOL,   wordwrap,            , 0b0001) \

#define ALIAS_LIST \
    ALIAS_SETTING(number,              nu, linenumberf) \
    ALIAS_SETTING(shiftwidth,          sw, expandtab) \

#endif
