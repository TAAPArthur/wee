#ifndef BUFFER_H
#define BUFFER_H


#define MIN(A,B) ((A) < (B) ? (A) : (B))
#define MAX(A,B) ((A) > (B) ? (A) : (B))
#define CLAMP(A,V, B) MIN(MAX(A,V), B)
#define LEN(A) (sizeof(A)/sizeof(A[0]))

/**
 * Stores arbitrary data
 */
typedef struct Buffer {
    // The backing data
    union {
        char* data;
        const char* readOnlyData;
        void** p;
        void* raw;
    };
    // The used len of data
    unsigned long size;
    // The size of the allocated region
    unsigned long maxSize;
} Buffer;

static inline const char* getEnd(const Buffer* buffer) {
    return buffer->readOnlyData + buffer->size;
}

static inline int isInBuffer(const Buffer* buffer, const char* value) {
    return buffer->readOnlyData <= value && value < buffer->readOnlyData + buffer->size;
}

static inline int isInBufferInclusive(const Buffer* buffer, const char* value) {
    return buffer->readOnlyData <= value && value <= buffer->readOnlyData + buffer->size;
}

/**
 * Appends data to the buffer starting at offset.
 * buffer is resized if needed
 * Return -1 if the memory couldn't be allocated
 */
int insertInBuffer(Buffer* buffer, const void* data, unsigned long offset, unsigned long len);

/**
 * Inserts data at the end of the buffer
 * Return -1 if the memory couldn't be allocated
 */
int appendToBuffer(Buffer* buffer, const void* data, unsigned long len);

/**
 * Deletes bytes [offset, MIN(buffer->size, offset + len)) from buffer
 * Returns the number of chars actually removed from the buffer
 */
int removeFromBuffer(Buffer* buffer, unsigned long offset, unsigned long len);

/**
 * Combination of removeFromBuffer and insertInBuffer
 * Returns the number of characters deleted or -1 on error
 */
int replaceInBuffer(Buffer* buffer, const char* data, unsigned long offset, unsigned long len, unsigned long deletionLen);

/*
 * Free data held by buffer
 */
void freeBufferData(Buffer* buffer);

/*
 * Grow the buffer can hold len more elements
 * Returns -1 if the grow failed
 */
int growBuffer(Buffer* buffer, unsigned long len);


int resizeBuffer(Buffer* buffer, unsigned long len);


typedef Buffer ArrayList;

int insertElementAt(ArrayList* buffer, void* data, unsigned long pos);
int appendElement(ArrayList* buffer, void* data);
int appendElementConst(ArrayList* buffer, const void* data);
void removeElement(ArrayList* buffer, const void* data);
int indexOf(const ArrayList* buffer, const void* data);
void* getElementAt(const ArrayList* buffer, unsigned long pos);
long getLen(const ArrayList* buffer);

#define FOR_EACH_REV(Type, elem, buffer, offset) for (Type elem = (buffer)->raw + (buffer)->size - sizeof(*elem), * _base = (buffer)->raw; (buffer)->size && (void*)elem >= (buffer)->raw + offset; elem = (void*)elem -(void*) _base + (buffer)->raw - sizeof(*elem), _base = (buffer)->raw )
#define FOR_EACH(Type, elem, buffer, offset) for (Type elem = (buffer)->raw + offset; (void*)elem < (buffer)->raw + (buffer)->size; elem++)
#define FOR_EACH_BUFFER(Type, elem, buffer) FOR_EACH_REV(Buffer**, __temp ## __LINE__, buffer, 0) FOR_EACH(Type, elem, *__temp ## __LINE__, 0)

#define STATIC_BUFFER(NAME, DATA) static Buffer NAME = {.raw = DATA, .size = sizeof(DATA)}

#endif
