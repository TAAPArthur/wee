#include "dock.h"
#include "format.h"
#include "labels.h"
#include "settings.h"
#include "utility.h"
#include "window.h"

#include <assert.h>
#include <stdio.h>

static DockType getAltType(DockType type) {
    switch(type) {
        case DOCK_TOP:
            return DOCK_BOTTOM;
        case DOCK_BOTTOM:
            return DOCK_TOP;
        case DOCK_LEFT:
            return DOCK_RIGHT;
        default:
        case DOCK_RIGHT:
            return DOCK_LEFT;
    }
}

int lineNumberDockFunc(const Window* win, const ViewSettings * settings, const DockInfo* info, char* str, int maxLen, Buffer* labelRanges) {
    const char* fmt = getStringSettings(settings, linenumberf_index);
    if (!str) {
        return snprintf(str, 0, fmt, getLastLineNumber(win));
    }
    LabelRange r = {LABEL_LINENO, 0, maxLen};
    addLabel(labelRanges, &r);
    return snprintf(str, maxLen, fmt, info->lineno);
}

int statusBarDockFunc(const Window* win, const ViewSettings * settings, const DockInfo* info, char* str, int maxLen, Buffer* labelRanges) {
    const char* fmt = getStringSettings(settings, statusbarstr_index);
    if (!str) {
        return 1;
    }
    LabelRange r = {LABEL_STATUS, 0, maxLen};
    addLabel(labelRanges, &r);
    return wee_format(win, str, maxLen - 1, fmt);
}

int getDockText(const Window* win, const ViewSettings* settings, const DockInfo* info, const Dock* dock, char* buffer, int bufferLen, Buffer* labelRanges) {
    assert(isEnabled(settings, dock->settingIndex));
    int num = dock->func(win, settings, info, buffer, bufferLen, labelRanges);
    return num;
}

static Dock defaultDocks[] = {
    { DOCK_LEFT, linenumberf_index, lineNumberDockFunc},
    { DOCK_BOTTOM, statusbarstr_index, statusBarDockFunc},
};

STATIC_BUFFER(defaultDocksBuffer, defaultDocks);

const Buffer* getDefaultDocks() {
    return &defaultDocksBuffer;
}

static DockType getEffectiveTypeOfDock(const ViewSettings* settings, SettingsIndex settingIndex, DockType type) {
    if (isEnabled(settings, settingIndex)) {
        return isAltVersion(settings, settingIndex) ? getAltType(type) : type;
    }
    return NUM_DOCK_TYPES;
}

DockType getEffectiveDockType(const ViewSettings* settings, const Dock* dock) {
    return getEffectiveTypeOfDock(settings, dock->settingIndex, dock->dockType);
}

int isDockEnabledForType(const ViewSettings* settings, const Dock* dock, DockType type) {
    return getEffectiveTypeOfDock(settings, dock->settingIndex, dock->dockType) == type;
}
