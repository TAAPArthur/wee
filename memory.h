#ifndef MEMORY_H
#define MEMORY_H
#include "logger.h"
#include <stdlib.h>
#include <string.h>

/**
 * This header provides malloc/free/realloc wrappers to do some hand rolled memory tracking in debug builds
 */

/**
 * Denotes the type of object we are allocated
 */
typedef enum AllocationTypes {
    ALLOCATION_RAW,
    ALLOCATION_STRING,
    ALLOCATION_VIEW,
    ALLOCATION_VIEW_SETTINGS,
    ALLOCATION_WINDOW,
    ALLOCATION_SCREEN,
    ALLOCATION_EXT,
    ALLOCATION_MISC,
    ALLOCATION_THIRD_PARTY,
    NUM_ALLOCATION_TYPES,
} AllocationTypes;


#ifndef NDEBUG
#define ALLOC_LABEL CONCAT(__FILE__, :__LINE__)
void* recordAllocation(AllocationTypes type, void* old, void* new, size_t size, const char* label);
#define wee_malloc(index, size) recordAllocation(index, NULL, malloc(size), size, ALLOC_LABEL)
#define wee_free(ptr) do { recordAllocation(NUM_ALLOCATION_TYPES, ptr, NULL, 0, ALLOC_LABEL); free(ptr); } while(0)
#define wee_realloc(index, ptr, size) recordAllocation(index, ptr, realloc(ptr, size), size, ALLOC_LABEL)
#define wee_strdup(str) memdup(ALLOCATION_STRING, str, strlen(str) + 1, ALLOC_LABEL)
int checkForLeaks();
#else
#define wee_malloc(index, size) malloc(size)
#define wee_free(index, ptr) free(ptr)
#define wee_realloc(index, size) realloc(ptr, size)
#define wee_strdup(str) strdup(str)
#endif

static inline void* memdup(AllocationTypes index, const void* src, int size, const char* allocLabel) {
    void* p =recordAllocation(index, NULL, malloc(size), size, allocLabel);
    return p ? memcpy(p, src, size) : NULL;
}

/**
 * Wrapper to give 3rd party code
 */
static inline void  wee_third_party_free(void*p) {wee_free(p);}
static inline void* wee_third_party_malloc(size_t size){return wee_malloc(ALLOCATION_THIRD_PARTY, size);}
static inline void* wee_third_party_realloc(void*p, size_t size) {return wee_realloc(ALLOCATION_THIRD_PARTY, p, size);};

#endif
