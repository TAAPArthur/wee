#ifndef LABELS_H
#define LABELS_H

#include "filetypes.h"
#include "typedefs.h"

typedef enum Label {
    LABEL_UNKNOWN = 0,
    LABEL_BOUNDARY = NUM_FT,
    LABEL_INSERTION_BUFFER,
    LABEL_TEXT,
    LABEL_COMMENT,
    LABEL_LITERAL,
    LABEL_MACRO,
    LABEL_STRING,
    LABEL_KEYWORD,
    LABEL_LINENO,
    LABEL_STATUS,
    LABEL_INCLUDE,
    // Markdown labels
    LABEL_HEADING,
    LABEL_ITALIC,
    LABEL_BLOCKQUOTE,
    LABEL_ORDERED_LIST,
    LABEL_UNORDERED_LIST,
    LABEL_CODE,
    LABEL_HORIZONTAL_RULE,
    LABEL_LINK,
    LABEL_IMAGE,
    NUM_LABELS,
} Label;

typedef struct {
    Label label;
    struct {
        FileType ft;
        unsigned int group;
        int dialect;
    };
    const char* pattern;
    int matchIndexes;
    char cflags;
    void* _regex_metadata;
} Rule;

typedef struct {
    Label label;
    long start;
    long end;
} LabelRange;

void addLabel(Buffer* buffer, const LabelRange* range);


int updateLabelsOfView(View* view);

void labelCode(Buffer* rules, const Buffer* labelers, const Buffer* buffer, const ViewSettings* settings);
Label findLabel(const Buffer* ranges, int offset);
int findLabels(const Buffer* labelBuffer, int offset, int n, LabelRange ranges[n]);

void labelCodeOfView(Buffer* ranges, const Buffer* rules, View* view);

const Buffer* getDefaultRules();
void addLabelAutoCommands(AutoCommandArray* autoCommands);

const Buffer* getLabelAutocommands();

void freeRules(const Buffer* rules);

#endif
