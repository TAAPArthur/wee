#ifndef RECT_H
#define RECT_H

typedef struct Rect {
    short x; short y; unsigned short width; unsigned short height;
} Rect;

#endif
