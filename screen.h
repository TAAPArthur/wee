#ifndef SCREEN_H
#define SCREEN_H


#include "container.h"
#include "typedefs.h"
#include "colors.h"
typedef struct Screen Screen;
typedef struct RenderFunctions RenderFunctions;

typedef struct DrawInfo {
    Screen* screen;
    const Buffer* styleSchemes;
    const Buffer* docks;
    Window* focusedWindow;
} DrawInfo;

void clearScreen(Screen * screen, const Rect * rect);
Screen* allocScreen();
Screen* resizeScreen(Screen* screen, unsigned short width, unsigned short height);
void freeScreen(Screen* screen);

const char* moveViewportVert(Screen* screen, Window * win, Rect rect, int lineDelta);
const char* moveViewportStart(Screen* screen, Window * win, Rect rect, int lineDelta);
void adjustViewportForCursor(Window * win, Rect rect);

void prepareScreen(Screen* screen);
void drawWindow(Window * win, Rect originalRect, Rect rect, void* context);
void drawScreen(const Screen *);

void drawToBuffer(Window* dest, Window*win, const Buffer* docks, Rect dimensions);


typedef struct RenderBackend {
    int (*init)(Context* context);
    int (*shutdown)();
    int (*set_cell)(int x, int y, const char *ch, unsigned long n, RenderTuple tuple);
    int (*clear)();
    int (*set_cursor)(int x, int y);
    int (*present)();
} RenderBackend;

void setRenderBackend(const RenderBackend * backend);
const RenderBackend * getRenderBackend();

#endif
