#include "commands.h"
#include "container.h"
#include "context.h"
#include "functions.h"
#include "logger.h"
#include "rect.h"
#include "registers.h"
#include "screen.h"
#include "search.h"
#include "settings.h"
#include "system.h"
#include "tile.h"
#include "undo.h"
#include "utility.h"
#include "view.h"
#include "window.h"
#include <assert.h>
#include <stdlib.h>
#include <string.h>

int setMode(Context* context, Arg arg) {
    setNumericSettings(getGlobalSettings(context), mode_index, arg.m);
    if (arg.m == MODE_NORMAL) {
        Window* win = getFocusedWindow(context);
        int delta = commitInsertion(win);
    }
    return 0;
}

int enterNormalModeIf(Context* context, Arg arg) {
    unsigned long len;
    Window* win = getFocusedWindow(context);

    unsigned long offset = getEffectiveOffsetOfPosition(win, getCursorPosition(win));
    if (offset == 0)
        return -1;

    const char* prev = getPositionFromEffectiveOffset(win, offset - 1);
    const char* s = getLine(win, prev, &len);
    int slen = strlen(arg.s);
    if (slen == len && strncmp(s, arg.s, len) == 0) {
        removeAtCurrentPosition(win, -len);
        return setMode(context, (Arg){.m = MODE_NORMAL});
    }
    return 0;
}

// Window creation functions

static int openFileInWindow(Context* context, Window* win, const char* s) {
    if (win == NULL)
        return -1;
    View* view = addView(context);
    if (view == NULL)
        return -1;
    if (!s) {
        s = getWindowName(getFocusedWindow(context));
    }
    if (s) {
        setName(view, s);
        appendFileIntoView(view, s);
    }
    setViewForWindow(win, view);
    setFocusedWindow(context, win);
    return 0;
}

int openFileInCurrentWindow(Context* context, Arg arg) {
    return openFileInWindow(context, getFocusedWindow(context), arg.s);
}

int openFileInNewWindow(Context* context, Arg arg) {
    return openFileInWindow(context, addWindow(context), arg.s);
}

static int openFileInSplitWindowHelper(Context* context, Arg arg, struct Layout* layout) {
    Window* win = splitWindow(context);
    if (openFileInWindow(context, win, arg.s) == -1)
        return -1;
    setActiveLayout(context, layout);
    return 0;
}

int openFileInSplitWindow(Context* context, Arg arg) {
    return openFileInSplitWindowHelper(context, arg, &TWO_ROW);
}

int openFileInVSplitWindow(Context* context, Arg arg) {
    return openFileInSplitWindowHelper(context, arg, &TWO_COL);
}

int loadIntoWindow(Context* context, Arg arg) {
    return readIntoCurrentPosition(getFocusedWindow(context), 0, arg.s);
}

int loadCommandIntoWindow(Context* context, Arg arg) {
    return readIntoCurrentPosition(getFocusedWindow(context), 1, arg.s);
}

int save(Context* context, Arg arg) {
    saveWindow(getFocusedWindow(context), arg.s);
    return 0;
}

int closeWindow(Context* context, Arg arg) {
    removeWindow(context, getActiveWindow(context));
    return 0;
}

int saveAndCloseWindow(Context* context, Arg arg) {
    save(context, arg);
    closeWindow(context, arg);
    return 0;
}

// movement functions
int gotoLine(Context* context, Arg arg) {
    return jumpToLine(getFocusedWindow(context), arg.i);
}

int movePage(Context* context, Arg arg) {
    Window* win = getFocusedWindow(context);

    const Rect * rect = getBoundsOfViewport(context, win);
    const char* startOfViewport = moveViewportStart(getScreen(context), win, *rect, arg.i * rect->height);
    if (!startOfViewport) {
        return -1;
    }
    setCursorPosition(win, startOfViewport);
    return 0;
}

int moveVert(Context* context, Arg arg) {
    Window* win = getFocusedWindow(context);

    unsigned long lenOfNextLine;
    const char* cursor = getCursorPosition(win);
    const char* startOfCurrentLine = getNextLineAfterCursor(win, 0);
    const char* startOfNextLine = getNextLineAfterCursor(win, arg.i);
    if (!startOfNextLine)
        return -1;
    getLine(win, startOfNextLine, &lenOfNextLine);
    long savedCol = getSavedCursorCol(win);
    long cursorCol = cursor - startOfCurrentLine;
    if (savedCol == -1 || savedCol < cursorCol) {
        savedCol = cursorCol;
        setSavedCursorCol(win, savedCol);
    }
    int effectiveCol = MIN(savedCol, lenOfNextLine - 1);
    setCursorPosition(win, startOfNextLine + effectiveCol);
    return 0;
}

int moveHor(Context* context, Arg arg) {
    Window* win = getFocusedWindow(context);
    const Rect * rect = getBoundsOfViewport(context, win);
    unsigned long offset = getEffectiveOffsetOfPosition(win, getCursorPosition(win));
    if  (arg.i < 0 && offset + arg.i > offset || arg.i > 0 && offset + arg.i < offset)
        return -1;
    unsigned long adjustedOffset = clampToCursorLine(win, offset + arg.i);
    setEffectiveCursorOffset(win, adjustedOffset);
    setSavedCursorCol(win, getCursorCol(win));
    return 0;
}

int moveCursor(Context* context, Arg arg) {
    Window* win = getFocusedWindow(context);
    updateCursorPosition(win, arg.i);
    return 0;
}

int jumpToCol(Context* context, Arg arg) {
    Window* win = getFocusedWindow(context);
    unsigned long lineLen;
    const char* startOfLine = getCursorLine(win, &lineLen);
    if (!startOfLine)
        return -1;
    long lastCol = lineLen - 1;
    int target = arg.i;
    if (arg.i < 0) {
        target = lastCol - arg.i + 1;
    }

    long effectiveCol = MIN(MAX(0, target), lastCol);
    setCursorPosition(win, startOfLine + effectiveCol);
    setSavedCursorCol(win, target);
    return 0;
}

int jumpToEndOfLine(Context* context, Arg arg) {
    moveVert(context, (Arg){.i =  arg.i - 1 });
    return jumpToCol(context, (Arg){.i = -1});
}

int jumpToLastNonNewLineCharInLine(Context* context, Arg arg) {
    int ret = jumpToEndOfLine(context, arg);
    moveHor(context, (Arg){.i = -1});
    return ret;
}

int jumpToCharInLine(Context* context, Arg arg) {
    return jumpToCharInCurrentLineOfWindow(getFocusedWindow(context), 1, arg.c);
}

int jumpToCharInLineBackwards(Context* context, Arg arg) {
    return jumpToCharInCurrentLineOfWindow(getFocusedWindow(context), -1, arg.c);
}

int jumpToBeforeCharInLine(Context* context, Arg arg) {
    if (jumpToCharInLine(context, arg) == 0) {
        moveHor(context, (Arg) {.i = -1});
        return 0;
    }
    return -1;
}

int jumpToAfterCharInLineBackwards(Context* context, Arg arg) {
    if (jumpToCharInLineBackwards(context, arg) == 0) {
        moveHor(context, (Arg) {.i = 1});
        return 0;
    }
    return -1;
}

int jumpToBoundary(Context* context, Arg arg) {
    Window* win = getFocusedWindow(context);
    SettingsIndex index = arg.index;
    const WindowSettings * settings = getWindowSettings(win);
    const Buffer* buffer = getSearchBuffer(win);
    int dir = arg.dir;
    int end = arg.end;
    const char* pattern = getStringSettings(settings, index);
    int cflags = isAltVersion(settings, index) ? SEARCH_FLAG_NEWLINE : 0;
    void * regex = allocRegex(pattern, cflags);
    int cursor = getCursorOffset(win);

    // TODO cache this instead of recomputing it every call
    int ret = regexFindMatchContainingPosition(regex, buffer->readOnlyData, 0, buffer->size, cursor, dir, end, cflags);
    if (ret != -1) {
        setCursorOffset(win, ret);
    } else if (dir == -1) {
        setCursorOffset(win, 0);
        return 0;
    } else if (dir == 1) {
        setCursorOffset(win, buffer->size - 1);
    }
    freeRegex(regex);
    return 0;
    return 0;
}

int appendChar(Context * context, Arg arg) {
    appendToInsertionBuffer(getFocusedWindow(context), &arg.c, 1);
    return 0;
}

int deleteChar(Context * context, Arg arg) {
    Window* win = getFocusedWindow(context);
    int delta = removeAtCurrentPosition(win, arg.i);
    return 0;
}

int deleteCharWithinLine(Context * context, Arg arg) {
    Window* win = getFocusedWindow(context);
    unsigned long len;
    const char * line = getCursorLine(getFocusedWindow(context), &len);
    unsigned long cursorPosition = getEffectiveOffsetOfPosition(win, getCursorPosition(win));
    unsigned long startOfLine = getEffectiveOffsetOfPosition(win, line);
    unsigned long endOfLine = startOfLine + len;

    if (cursorPosition + arg.i >= endOfLine || startOfLine > cursorPosition + arg.i)
        return -1;
    return deleteChar(context, arg);
}

int replaceChar(Context * context, Arg arg) {
    Window* win = getFocusedWindow(context);
    int ret = replaceAtCurrentPosition(win, &arg.c, 1, 1);
    if (ret == -1)
        return -1;

    setCursorOffset(win, getCursorOffset(win) + 1);
    return 0;
}

int deleteCurrentLine(Context * context, Arg arg) {
    Window* win = getFocusedWindow(context);
    unsigned long len;
    const char * line = getCursorLine(getFocusedWindow(context), &len);
    setCursorPosition(win, line);
    removeAtCurrentPosition(getFocusedWindow(context), len);
    return 0;
}

int focusCmdWindow(Context* context, Arg arg) {
    setMode(context, (Arg)MODE_INSERT);
    focusCommandWindow(context);
    return 0;
}

int unfocusCmdWindow(Context* context, Arg arg) {
    restoreFocus(context);
    return 0;
}

int typeText(Context* context, Arg arg) {
    processEventString(context, arg.s, 0);
    return 0;
}

int typeChar(Context* context, Arg arg) {
    char s[] = {arg.c, 0};
    return typeText(context, (Arg){.s = s});
}

int echoInfo(Context* context, Arg arg) {
    setInfoMessage(context, arg.s);
    return 0;
}

int echoMessage(Context* context, Arg arg) {
    setInfoMessage(context, arg.s);
    appendMessage(context, arg.s);
    return 0;
}

int undo(Context* context) {
    undoLast(getFocusedView((context)));
    return 0;
}

int redo(Context* context) {
    redoLast(getFocusedView((context)));
    return 0;
}

int yankCurrentLine(Context * context, Arg arg) {
    Window* win = getFocusedWindow(context);
    unsigned long len;
    const char * line = getCursorLine(getFocusedWindow(context), &len);
    return yankText(REGISTER_DEFAULT, line, len);
}

int yankRange(Context * context, Arg arg) {
    Window* win = getFocusedWindow(context);
    const char* cursor = getCursorPosition(win);
    if (arg.i < 0) {
        cursor -= arg.i;
        arg.i *= -1;
    }
    return yankText(REGISTER_DEFAULT, cursor, arg.i);
}

int pasteText(Context * context, Arg arg) {
    Window* win = getFocusedWindow(context);
    const Buffer* buffer = getRegisterBufferConst(REGISTER_DEFAULT);
    return insertAtCurrentPosition(win, buffer->readOnlyData, buffer->size);
}
