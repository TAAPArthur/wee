#ifndef CONTEXT_H
#define CONTEXT_H

#include "colors.h"
#include "events.h"
#include "typedefs.h"

struct Layout;
typedef enum {
    BINDING_INDEX,
    COMMAND_INDEX,
    DOCK_INDEX,
    AUTOCOMMAND_INDEX,
    RULE_INDEX,
    COLOR_SCHEME_INDEX,
    CONTAINER_INDEX,
    NUM_STATE_INDEX
} StateIndex;

struct RenderFunctions;
typedef struct ContextSettings {
    int readfd;
    int writefd;
} ContextSettings;

typedef struct Screen Screen;
Screen * getScreen(const Context* context);

ViewSettings* getGlobalSettings(const Context* context);

int initContext(Context* context);

/**
 * Returns the window currently receiving input focus
 */
Window* getFocusedWindow(const Context* context);
void setFocusedWindow(Context* context, Window* win);
void setFocusedWindowTransient(Context* context, Window* win);


View* getFocusedView(const Context* context);

void focusCommandWindow(Context* context);
void setInfoMessage(Context * context, const char* msg);

void appendMessage(Context * context, const char* msg);

int appendToCommandHistory(Context* context, const char* str, int n);

/**
 * Returns the window current/last live window to have received input focus not
 * counting special windows like the command window
 */
Window* getActiveWindow(const Context* context);

void setDimensions(Context* context, unsigned short width, unsigned short height);
void getDimensions(const Context* context, unsigned short * width, unsigned short * height);



void restoreFocus(Context* context);

void removeWindow(Context* context, Window* win);
Window* addWindow(Context* context);

void updateOffsets(Context* context, Window* ref, int delta);

int isWindowVisible(const Context* context, const Window* win);
/*
 * Returns the new created, empty view or NULL
 */
View* addView(Context* context);
/**
 * Removes the nth view of the given context
 */
void removeView(Context* context, View* view);

int shutdownContext(Context* context);
int isShuttingDown(const Context* context);
int requestShutdown(Context* context);

void drawToWindow(Context* context, Window * win);

const Buffer* getWindowList(const Context* context);

#define FOR_EACH_WINDOW(win, context) for (int _i ## __LINE__= getLen(getWindowList(context)) - 1; _i ## __LINE__ >= 0 && (win = getElementAt(getWindowList(context), _i ## __LINE__)); _i ## __LINE__--)

/*
int isStateBufferEnabled(const Context* context, StateIndex index);
Buffer* getStateBuffer(Context* context, StateIndex index);
*/
const Buffer* getStateBufferConst(const Context* context, StateIndex index);


void addStateBuffer(Context* context, StateIndex index, const Buffer * buffer);
void removeStateBuffer(Context* context, StateIndex index, const Buffer * buffer);

void clearStateBuffer(Context* context, StateIndex index);


int suspendContext(Context* context);

void forEachView(const Context* context, void (*func)(View*));


Window* splitWindow(Context* context);
void setActiveLayout(Context* context, struct Layout* layout);
void retile(Context* context);
void redraw(Context* context);

const Rect* getBoundsOfViewport(const Context* context, const Window * win);

#endif
