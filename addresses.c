#include "logger.h"
#include "settings.h"
#include "utility.h"
#include "view.h"
#include "wee_string.h"
#include "window.h"
#include <assert.h>

static const char* parseAddressOffset(const Window* win, const char* cmdStr, long* delta) {
    while(*cmdStr) {
        cmdStr = skipBlanks(cmdStr);
        long num;
        const char * s = parseAsNumber(cmdStr, &num);
        *delta += num;
        if (s == cmdStr)
            break;
        cmdStr = s;
    }
    return cmdStr;
}

static const char* parseAddress(const Window* win, const char* cmdStr, int snapToEnd, int assumeCurrentLine, const char* refPoint, const char** addr) {
    cmdStr = skipBlanks(cmdStr);
    WindowSettings * settings = getWindowSettings(win);
    int linemode = isEnabled(settings, linemode_index);
    long offset;
    long lineNumber = -1;
    char lineNumberSet = 0;
    long num;
    if (*cmdStr == '.') {
        offset = getEffectiveOffsetOfPosition(win, refPoint);
        cmdStr++;
    } else if (*cmdStr == '$') {
        offset = getWindowDataLen(win) - 1;
        cmdStr++;
    } else if ('0' <= *cmdStr && *cmdStr <= '9') {
        cmdStr = parseAsNumber(cmdStr, &offset);
        if (linemode) {
            lineNumber = offset;
            offset = getEffectiveOffsetOfPosition(win, getNthLine(win, offset));
            lineNumberSet = 1;
        }
    } else if (*cmdStr == '-' || *cmdStr == '+') {
        cmdStr = parseAsNumber(cmdStr, &offset);
        if (linemode) {
            lineNumberSet = 1;
            lineNumber = getLineNumberOfLine(win, refPoint) + offset;
            offset = getEffectiveOffsetOfPosition(win, getNthLine(win, lineNumber));
        } else {
            offset += getEffectiveOffsetOfPosition(win, refPoint);
        }
    } else if (*cmdStr == '\'') {
        //TODO
    } else if (*cmdStr == '/') {
        //TODO
    } else if (*cmdStr == '?') {
        //TODO
    } else {
        TRACE("Address not found\n");
        if (assumeCurrentLine)
            offset = getEffectiveOffsetOfPosition(win, refPoint);
        else
            return cmdStr;
    }
    long delta = 0;
    cmdStr = parseAddressOffset(win, cmdStr, &delta);
    const char* pos = getPositionFromEffectiveOffset(win, offset);
    if (linemode) {
        pos = getPositionFromEffectiveOffset(win, offset);
        if (delta) {
            if (!lineNumberSet)
                lineNumber = getLineNumberOfLine(win, pos);
            pos = getNthLine(win, lineNumber + delta);
        }
        unsigned long len;
        pos = getLine(win, pos, &len);
        if (snapToEnd)
           pos += MAX(0, len - 1);
    } else if (delta) {
        offset += delta;
        pos = getPositionFromEffectiveOffset(win, offset);
    }
    *addr = pos;
    return cmdStr;
}

const char* parseAddresses(const Window* win, const char* cmdStr, const char** addr1, const char** addr2) {
    const char* cursor = getCursorPosition(win);
    cmdStr = skipBlanks(cmdStr);
    if (*cmdStr == '%') {
        parseAddress(win, "0", 0, 0, cursor, addr1);
        parseAddress(win, "$", 1, 0, cursor, addr2);
        cmdStr++;
    } else {
        cmdStr = parseAddress(win, cmdStr, 0, 0, cursor, addr1);
        cmdStr = skipBlanks(cmdStr);
        if (*cmdStr == ',' || *cmdStr == ';') {
            if (!*addr1) {
                parseAddress(win, ".", 0, 1, cursor, addr1);
            }
            if (*cmdStr == ';') {
                cursor = *addr1;
            }
            cmdStr = parseAddress(win, cmdStr + 1, 1, 1, cursor, addr2);
        }
    }
    return cmdStr;
}
