#include "autocommands.h"
#include "container.h"
#include "context.h"
#include "events.h"
#include "filetypes.h"
#include "logger.h"
#include "memory.h"
#include "settings.h"
#include "system.h"
#include "view.h"
#include "window.h"

#include <assert.h>
#include <string.h>

#define EXACT_MATCH  1
#define PREFIX_MATCH 2

typedef struct EventState{
    int count;
    int pendingCount;
    const KeyBinding* bindingStack[2];
    char bindingStackLen;
    unsigned char cmd_chain_count;
    char cmd_chain[255];
    long eventCounter;
    Context* context;
} EventState;

EventState* getEventState(void) {
    static EventState state;
    return &state;
}

int isCompitableMode(const Settings* settings, Mode target) {
    return target == MODE_ANY || getNumericSettings(settings, mode_index) & target;
}

static int doesBindingMatch(const Settings* settings, const Event * event, const EventState* state, FileType ft, const KeyBinding* binding, int ignorePartial) {
    if(!isCompitableMode(settings, binding->mode))
        return 0;
    if(binding->fileType != FT_ANY && binding->fileType != ft)
        return 0;
    if(binding->key == ANY_CHAR)
        return EXACT_MATCH;

    if (binding->testFunc) {
        return (state->cmd_chain_count || binding->testFunc(event->key) == 0) ? 0 : EXACT_MATCH;
    }
    if(binding->str && strncmp(binding->str, state->cmd_chain, state->cmd_chain_count) == 0 && binding->str[state->cmd_chain_count] == event->key) {
        if (strlen(binding->str) == state->cmd_chain_count + 1) {
            return EXACT_MATCH;
        } else {
            if(ignorePartial)
                return 0;
            return PREFIX_MATCH;
        }
    }
    return state->cmd_chain_count == 0 && binding->key == event->key;
}

int processEventChar(Context* context, int key, int ignorePartial) {
    Event e = {key};
    return processEvent(context, &e, ignorePartial);
}

int processEventStringN(Context* context, const char* str, int ignorePartial, int len) {
    int count = 0;
    for(int i = 0; i < len; i++) {
        count += processEventChar(context, str[i], ignorePartial);
    }
    return count;
}

int processEventString(Context* context, const char* str, int ignorePartial) {
    return processEventStringN(context, str, ignorePartial, strlen(str));
}

static int flushPendingEvents(Context* context) {
    EventState * state = getEventState();
    TRACE("Flushing %d events\n", state->cmd_chain_count);
    if (state->cmd_chain_count) {
        int key = state->cmd_chain[--state->cmd_chain_count];
        Event event = {key};
        return processEvent(context, &event, 1);
    }
    return 0;
}

int clearChainCount(const Context* context) {
    EventState * state = getEventState();
    if (state->cmd_chain_count) {
        TRACE("Clearing chain count\n");
        memset(state->cmd_chain, 0, state->cmd_chain_count);
        state->cmd_chain_count = 0;
        return 0;
    }
    return -1;
}

int updateCount(Context* context, Arg arg) {
    EventState * state = getEventState();
    state->count *= 10;
    state->count += arg.i - '0';
    TRACE("Count as been updated to %d\n", state->count);
    return state->count;
}

static int getAndClearCount(EventState* state) {
    int count = state->count;
    if (state->count) {
        TRACE("Resetting count from %d\n", state->count);
        state->count = 0;
    }
    if (state->pendingCount) {
        if (!count)
            count = 1;
        TRACE("Resetting pending count from %d\n", state->pendingCount);
        count *= state->pendingCount;
        state->pendingCount = 0;
    }
    return count;
}

static int callBoundFunction(Context* context, const KeyBinding* binding, Arg arg, int count, int oldFlags) {
    assert(binding->func);

    if (binding->flags & BF_COUNT_REPLACE_ARG && count) {
        arg.i = count;
        count = 1;
    }

    int err;
    int i = 0;
    do {
        err = binding->func(context, arg);
    } while (err == 0 && ++i < count);

    if (err == 0 && (oldFlags & BF_MOTION_INCLUSIVE_CMD) == BF_MOTION_INCLUSIVE_CMD) {
        err = binding->func(context, binding->arg);
    }
    return err;
}

static int runBoundFunction(Context* context, const Event* event, EventState * state, const KeyBinding* binding, int* abort) {
    Arg arg = binding->arg;
    if (binding->flags & BF_USE_EVENT_CHAR_AS_ARG)
        arg.i = event->key;
    if (binding->flags & BF_IGNORE_OTHER_COMMANDS) {
        return callBoundFunction(context, binding, arg, 0, 0);
    }
    if (binding->flags & BF_MULTI_EVENT) {
        if (state->bindingStackLen == LEN(state->bindingStack))
            return -1;
        TRACE("Storing multi event binding %p 0x%X; %d\n", binding, binding->flags, state->count);
        if (state->count) {
            if (!state->pendingCount)
                state->pendingCount = 1;
            state->pendingCount *= state->count;
        }
        state->bindingStack[state->bindingStackLen++] = binding;
        state->count = 0;
        if (abort) {
            *abort = 1;
        }
        return 0;
    }

    int oldFlags = 0;
    while (state->bindingStackLen) {
        const KeyBinding* latestPendingBinding = state->bindingStack[state->bindingStackLen - 1];
        assert(latestPendingBinding->flags & BF_MULTI_EVENT);
        if (latestPendingBinding->flags & BF_USE_NEXT_EVENT_CHAR_AS_ARG) {
            arg.i = event->key;
        } else if ((latestPendingBinding->flags & BF_REQUIRES_MOTION_CMD) && (binding->flags & BF_MOTION_CMD)) {
            TRACE("Running motion command from %p\n", binding);
            Window* win = getFocusedWindow(context);
            int offset = getCursorOffset(win);
            callBoundFunction(context, binding, arg, getAndClearCount(state), oldFlags);

            arg.i = latestPendingBinding->arg.i * (offset - getCursorOffset(win));
            TRACE("Delta from motion command %d; preparing to run binding %p\n", arg.i, latestPendingBinding);
        } else {
            TRACE("Tried to run a multi part command command with an invalid function; clearing stack\n");
            state->bindingStackLen = 0;
            return -1;
        }
        state->bindingStackLen--;
        oldFlags = binding->flags;
        binding = latestPendingBinding;
        continue;
    }
    int err = callBoundFunction(context, binding, arg, getAndClearCount(state), oldFlags);
    if (err)
        VERBOSE("Bound function returned %d\n", err);
    return err;
}

int processEventHelper(Context* context, const Event * event, EventState* state, int ignorePartial) {
    int result = 0;

    Settings* settings = getWindowSettings(getFocusedWindow(context));
    incrementSettings(settings, event_counter_index);
    FileType ft = getNumericSettings(getWindowSettings(getFocusedWindow(context)), filetype_index);

    int fallthrough = 0;

    FOR_EACH_BUFFER(const KeyBinding*, binding, getStateBufferConst(context, BINDING_INDEX)) {
        int ret = fallthrough ? EXACT_MATCH : doesBindingMatch(settings, event, state, ft, binding, ignorePartial);
        fallthrough = 0;
        result |= ret;
        if (ret) {
            ALL("Found match; Binding %p; type %d; flags 0x%X\n", binding, ret, binding->flags);
        }
        int err = 0;
        if (ret == EXACT_MATCH) {
            if (binding->key == ANY_CHAR)
                ALL("Received func %p bound to ANY (0x%X)\n", binding->func, event->key);
            else if (!binding->str)
                ALL("Received func %p bound to char (0x%X)\n", binding->func, binding->key);
            else
                ALL("Received func %p bound to '%s'\n", binding->func,  binding->str);

            int abort = isShuttingDown(context);
            err = runBoundFunction(context, event, state, binding, &abort);
            if (isShuttingDown(context)) {
                return ~result;
            }
            if (binding->flags & (BF_FALLTHROUGH &~BF_NO_ABORT_ON_MATCH))
                fallthrough = 1;
        }
        if (ret) {
            if (ret == PREFIX_MATCH || (err && !(binding->flags & BF_NO_ABORT_ON_ERR)) || (!err && !(binding->flags & BF_NO_ABORT_ON_SUCCESS))) {
                return ~result;
            }
        }
    }
    return result;
}

int processEvent(Context* context, const Event * event, int ignorePartial) {
    EventState * state = getEventState();

    int result = processEventHelper(context, event, state, ignorePartial);

    if ((result == 0)) {
        if (state->bindingStackLen) {
            KeyBinding dummy = {};
            runBoundFunction(context, event, state, &dummy, NULL);
            assert(state->bindingStackLen == 0);
        }
    }

    int count = result == EXACT_MATCH || ~result == EXACT_MATCH;
    if (isShuttingDown(context))
        return count;
    if (result != PREFIX_MATCH && ~result != PREFIX_MATCH) {
        if (result == 0 || ~result == 0) {
            if(state->cmd_chain_count) {
                // Sequence didn't match any binding. Re-process all pending events literally
                flushPendingEvents(context);
                // Replay the active event now that all pending events have been cleared.
                count += processEvent(context, event, ignorePartial);
            }
        }
        clearChainCount(context);
    } else if(!ignorePartial) {
        TRACE("Storing event to see if it can match an string binding\n");
        // Store the event to see if we'll get an exact match later
        state->cmd_chain[state->cmd_chain_count] = event->key;
        state->cmd_chain_count++;
    }
    return count;
}

static Buffer inputBuffer;
static int onInputRead(ProgressStatus* status) {
    processEventStringN(status->userData, inputBuffer.readOnlyData, 0, inputBuffer.size);
    freeBufferData(&inputBuffer);
    return 0;
}

int eventLoop(Context* context) {
    if (isIdle()) {
        readDataFromStdin(&inputBuffer, onInputRead, NULL, context);
    }
    while (!isShuttingDown(context)) {
        retile(context);
        redraw(context);
        const Settings * settings = getGlobalSettings(context);
        int timeout = isEnabled(settings, timeout_index) ? getNumericSettings(settings, timeout_index) : -1;
        int timeouts[2] = {timeout, -1};
        for (int i = 0; i < LEN(timeouts); i++) {
            if (timeouts[i] == -1) {
                runAutoCommands(getStateBufferConst(context, AUTOCOMMAND_INDEX), AUTO_CMD_IDLE, (AutocommandParams){.contextParams = {.context = context}});
                if (isShuttingDown(context))
                    break;
            }
            int ret = waitForEvent(timeouts[i]);
            if (ret == -1) {
                goto eventLoopEnd;
            } else if (ret == 0) {
                if (flushPendingEvents(context)) {
                    break;
                }
                continue;
            }
            processEvents(context);
            break;
        }
    }
eventLoopEnd:
    freeBufferData(&inputBuffer);
    return 0;
}
