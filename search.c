#include "buffer.h"
#include "logger.h"
#include "memory.h"
#include "search.h"

#ifdef HAVE_TRE
#if TRE_CONFIG_SYSTEM_ABI == 1
#define TRE_SYSTEM_REGEX_H_PATH <regex.h>
#define TRE_USE_SYSTEM_REGEX_H
#else
#define NO_SYSTEM_REGEX
#endif // TRE_CONFIG_SYSTEM_ABI
#include <tre/tre.h>

#if TRE_CONFIG_SYSTEM_ABI == 1
#undef regcomp
#undef regexec
#undef regerror
#undef regfree
#endif
#endif

#include <assert.h>
#ifndef NO_SYSTEM_REGEX
#include <regex.h>
#endif

static inline int regnexec(const regex_t *preg, const char *string, size_t len, size_t nmatch, regmatch_t pmatch[], int eflags) {
    pmatch[0].rm_so = 0;
    pmatch[0].rm_eo = len;
    return regexec(preg, string, nmatch, pmatch, 0);
}

struct regex_backend {
    int (*regcomp)(restrict regex_t *preg, restrict const char *regex, int cflags);
    int (*regnexec)(const regex_t *preg, const char *string, size_t len, size_t nmatch, regmatch_t pmatch[], int eflags);
    void (*regfree)(regex_t *preg);
    size_t (*regerror)(int errcode, const regex_t *restrict preg, char errbuf[], size_t errbuf_size);
    int cflags;
    int eflags;
} regex_impls[NUM_REGEX_IMPL] = {
#ifndef NO_SYSTEM_REGEX
    [REGEX_IMPL_POSIX] = {regcomp, regnexec, regfree, regerror, REG_EXTENDED, 0},
#ifdef REG_STARTEND
    [REGEX_IMPL_BSD] = {regcomp, regnexec, regfree, regerror, REG_EXTENDED, REG_STARTEND},
#endif
#endif
#ifdef HAVE_TRE
    [REGEX_IMPL_TRE] = {tre_regcomp, tre_regnexec, tre_regfree, tre_regerror, REG_EXTENDED},
#endif
};

static struct regex_backend * regex_impl;

int isRegexBackendEnabled(RegexImpl i) {
    return 0 <= i && i < NUM_REGEX_IMPL && !!regex_impls[i].regnexec;
}
static void inline initRegexBackend() {
    for (int i = 0; !regex_impl; i++) {
        setRegexBackend(i);
    }
}

RegexImpl getRegexBackend() {
    initRegexBackend();
    assert(regex_impls <= regex_impl && regex_impl < regex_impls + LEN(regex_impls));
    return regex_impl - regex_impls;
}

int setRegexBackend(RegexImpl i) {
    if (isRegexBackendEnabled(i)) {
        regex_impl = regex_impls + i;
        return 0;
    }
    return -1;
}

static inline int compileRegex(regex_t * regex, const char * pattern, int cflags) {
    int ret;
    if ((ret = regex_impl->regcomp(regex, pattern, cflags))) {
        char buffer[255];
        regex_impl->regerror(ret, regex, buffer, sizeof(buffer));
        WARN("Error compiling regex %s", buffer);
    }
    return ret;
}

static char weeToRegexCflagsMap[] = {
    [SEARCH_FLAG_NEWLINE] = REG_NEWLINE,
    [SEARCH_FLAG_IGNORE_CASE] = REG_ICASE,
    [SEARCH_FLAG_BASIX_REGEX] = ~REG_EXTENDED,
};

void * allocRegex(const char * pattern, int flags) {
    initRegexBackend();

    regex_t * regex = wee_malloc(ALLOCATION_EXT, sizeof(regex_t));
    int cflags = regex_impl->cflags;

    for (int i = 1; i < LEN(weeToRegexCflagsMap); i *= 2) {
        int mask = weeToRegexCflagsMap[i];
        if (i & flags) {
            TRACE("I %d Mask %02hhX\n", i, mask);
            if (mask > 0) {
                cflags |= mask;
            } else if (mask < 0) {
                cflags &= ~mask;
            }
        }
    }
    int ret = compileRegex(regex, pattern, cflags);
    if (ret) {
        wee_free(regex);
        return NULL;
    }
    return regex;
}

void freeRegex(void * regex) {
    regex_impl->regfree(regex);
    wee_free(regex);
}

int regexMatch(void * regex, const char* data, int start, int end, int nmatch, RegexMatch matches[nmatch], int eflags) {
    assert(start <= end);
    regmatch_t pmatch[nmatch];
    int ret = regex_impl->regnexec(regex, data + start, end - start, nmatch, pmatch, eflags);
    if (ret == REG_NOMATCH) {
        return 0;
    }

    int n = 0;
    for (int i = 0; i < nmatch; i++) {
        if (pmatch[i].rm_so == -1) {
            break;
        }
        matches[n].start = pmatch[i].rm_so + start;
        matches[n].end = pmatch[i].rm_eo + start;
        if (regex_impl == regex_impls + REGEX_IMPL_POSIX) {
            if (matches[n].end > end) {
                WARN("POSIX regex impl found a match past [%d, %d) match index %d range [%d, %d); Truncating\n", start, end, i, matches[i].start, matches[i].end);
                break;
            }
        }
        n++;
    }
    return n;
}

int regexFindAll(void * regex, const char* data, int start, int end, int nmatch, RegexMatch matches[nmatch], int eflags) {
    RegexMatch matchesForSingleEntry[3];
    int i;
    int offset = start;
    for (i = 0; i < nmatch && offset < end; ) {
        int n = regexMatch(regex, data, offset, end, LEN(matchesForSingleEntry), matchesForSingleEntry, eflags);
        if (!n)
            break;
        if (matchesForSingleEntry[n - 1].start == matchesForSingleEntry[n - 1].end) {
            offset = matchesForSingleEntry[n - 1].start + 1;
            continue;
        }
        eflags |= REG_NOTBOL;
        matches[i++] = matchesForSingleEntry[n - 1];
        offset = matchesForSingleEntry[0].end;
    }
    return i;
}

int regexFindMatchContainingPosition(void * regex, const char* data, int start, int end, int target, int dir, int snapToEnd, int eflags) {
    int nmatch = 8;
    RegexMatch matches[nmatch];
    while (start < end) {
        int ret = regexFindAll(regex, data, start, end, nmatch, matches, eflags);
        int i;
        int foundTarget = 0;
            for (i = 0; i < ret && target != -1; i++) {
                TRACE("HERE %d %d [%d, %d) '%c' (%d)\n", i, dir, matches[i].start, matches[i].end, data[matches[i].start], data[matches[i].start]);
            }
        if (dir >= 0) {
            for (i = 0; i < ret && target != -1; i++) {
                if (target < matches[i].end) {
                    foundTarget = 1;
                    break;
                }
            }
        } else {
            for (i = ret - 1; i >= 0 && target != -1; i--) {
                if (target >= matches[i].start) {
                    foundTarget = 1;
                    break;
                }
            }
        }
        if (ret == 0) {
            break;
        } else if (!foundTarget) {
            eflags |= REG_NOTBOL;
            start = matches[i - 1].end;
            continue;
        } else {
            int value = (&matches[i].start)[snapToEnd] - snapToEnd;
            TRACE("%d [%d, %d)\n", target, matches[i].start, matches[i].end);
            if (dir == 1) {
                if (value == target) {
                    if (i == ret - 1) {
                        start = matches[i].end;
                        target = -1;
                        continue;
                    }
                    i++;
                }
            } else if (dir == -1) {
                if (value == target) {
                    if (i) {
                        i--;
                    } else {
                        end = matches[i].start;
                        target = -1;
                        continue;
                    }
                }
            }
            TRACE("%d [%d, %d)\n", (&matches[i].start)[snapToEnd] - snapToEnd, matches[i].start, matches[i].end);
            return (&matches[i].start)[snapToEnd] - snapToEnd;
        }
    }
    return -1;
}
