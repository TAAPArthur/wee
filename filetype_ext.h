#ifndef FILETYPE_EXT_H
#define FILETYPE_EXT_H

#include "filetypes.h"

static const char* fileTypeToExtMapping[] = {
    [FT_C] = "c",
    [FT_CPP] = "cpp",
    [FT_PLAIN_TEXT] = "txt",
    [FT_MARKDOWN] = "md",
};

#endif

