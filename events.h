#ifndef EVENTS_H
#define EVENTS_H

#include "filetypes.h"
#include "settings.h"
#include "typedefs.h"

#define UP    -1
#define DOWN   1
#define LEFT  -1
#define RIGHT  1

#define CTRL(c) (c & 0x1F)
#define FN(c) (0xFFFF - c + 1)

#define INSERT           (0XFFFF - 12)
#define DELETE           (0XFFFF - 13)
#define HOME             (0XFFFF - 14)
#define END              (0XFFFF - 15)
#define PGUP             (0XFFFF - 16)
#define PGDN             (0XFFFF - 17)
#define ARROW_UP         (0XFFFF - 18)
#define ARROW_DOWN       (0XFFFF - 19)
#define ARROW_LEFT       (0XFFFF - 20)
#define ARROW_RIGHT      (0XFFFF - 21)
#define BACK_TAB         (0XFFFF - 22)
#define ESC              CTRL('[')

#define ANY_CHAR -1


typedef enum Mode {
    MODE_NORMAL  = 1 << 0,
    MODE_INSERT  = 1 << 1,
    MODE_REPLACE = 1 << 2,
    MODE_EX      = 1 << 3,
    MODE_ANY     = -1,
} Mode;

typedef union Arg{
    Mode m;
    const char* s;
    int i;
    char c;
    struct {
        int dir;
        SettingsIndex index;
        char end;
    };
} Arg;

// Override default behavior and continue to find the next binding that can be triggered
// if the binding returned non-zero
#define BF_NO_ABORT_ON_ERR            (1<<0)
// Override default behavior and continue to find the next binding that can be triggered
// if the binding returned zero
#define BF_NO_ABORT_ON_SUCCESS        (1<<1)
#define BF_NO_ABORT_ON_MATCH          (BF_NO_ABORT_ON_SUCCESS | BF_NO_ABORT_ON_ERR)
// This command won't cause count to be cleared or interact with any pending bindings
#define BF_IGNORE_OTHER_COMMANDS      (1<<2)
// Caused Arg::c to be replaced with the key of the current event
#define BF_USE_EVENT_CHAR_AS_ARG      (1<<3)
// Caused Arg::c to be replaced with the key of the next event
#define BF_USE_NEXT_EVENT_CHAR_AS_ARG (1<<4)
// Caused Arg::i to be replaced with the delta in cursor position from the subsequent motion command
#define BF_REQUIRES_MOTION_CMD        (1<<5)
// Causes the cursor motion that will affect the argument to a possibly preceding motion command
// If there is no such pending binding, then this flag has no affect
#define BF_MOTION_CMD                 (1<<6)
#define BF_MOTION_INCLUSIVE_CMD       ((1<<7) | BF_MOTION_CMD)

// If this command matches, automatically match the next in the list.
// Note this implies BF_NO_ABORT_ON_MATCH
#define BF_FALLTHROUGH                ((1<<8) | BF_NO_ABORT_ON_MATCH)

#define BF_COUNT_REPLACE_ARG          (1<<9)

#define BF_MULTI_EVENT             (BF_USE_NEXT_EVENT_CHAR_AS_ARG | BF_REQUIRES_MOTION_CMD)

typedef struct KeyBinding {
    Mode mode;
    int key;
    const char* str;
    int (*testFunc)(int c);
    int (*func)();
    Arg arg;
    FileType fileType;
    unsigned short flags;
} KeyBinding;

typedef struct {
    int key;
} Event;

typedef struct Context Context;

int isCompitableMode(const Settings* settings, Mode target);

int eventLoop(Context* context);

int processEventChar(Context* context, int key, int ignorePartial);
int processEventString(Context* context, const char* str, int ignorePartial);
int processEvent(Context* context, const Event * event, int ignorePartial);


int clearChainCount(const Context* context);
int updateCount(Context* context, Arg arg);

#endif
