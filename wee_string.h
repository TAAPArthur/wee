#ifndef UTIL_H
#define UTIL_H

/**
 * Returns the number of occurrences of target in the range [start, end).
 */
int countInstancesOfChar(const char* start, const char*end, char target);

/**
 * Returns the first occurrence of needle starting from pos and searching in the direction of dir.
 * If dir < 0, we search backwards
 */
const char* findChar(const char* pos, const char* end, char needle, int dir);

static inline const char* skipBlanks(const char* str) {
    while(*str == ' ') str++;
    return str;
}

const char* parseAsNumber(const char* start, long* num);

#endif
