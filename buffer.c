#include "assert.h"
#include "buffer.h"
#include "memory.h"

int growBuffer(Buffer* buffer, unsigned long len) {
    int maxSize = MAX(1, buffer->maxSize);
    while (buffer->size + len > maxSize) {
        maxSize *= 2;
    }
    if (buffer->size == 0 || maxSize != buffer->maxSize) {
        assert(maxSize);
        void *p = wee_realloc(ALLOCATION_RAW, buffer->data, maxSize);
        if (!p)
            return -1;
        buffer->data = p;
        buffer->maxSize = maxSize;
    }
    return 0;
}

int resizeBuffer(Buffer* buffer, unsigned long size) {
    assert(size <= buffer->maxSize);
    buffer->size = size;
    return 0;
}

/*
 * Potentially reclaims unused space on the buffer
 */
static void shrinkBuffer(Buffer* buffer) {
    int maxSize = buffer->maxSize;
    int size = buffer->size;
    while (size < maxSize / 2) {
        maxSize /= 2;
    }
    if (maxSize != buffer->maxSize) {
        if (buffer->size) {
            void* p = wee_realloc(ALLOCATION_RAW, buffer->data, maxSize);
            if (p) {
                buffer->data = p;
                buffer->maxSize = maxSize;
            }
        } else {
            freeBufferData(buffer);
        }
    }
}

int insertInBuffer(Buffer* buffer, const void* data, unsigned long offset, unsigned long len) {
    assert(offset <= buffer->size);
    int copyLen = buffer->size - offset;
    if (growBuffer(buffer, len) == -1)
        return -1;
    if(offset != buffer->size)
        memmove(buffer->data + offset + len, buffer->data + offset, copyLen);
    if (data) {
        memcpy(buffer->data + offset, data, len);
    } else {
        memset(buffer->data + offset, 0, len);
    }
    buffer->size += len;
    return 0;
}

int appendToBuffer(Buffer* buffer, const void* data, unsigned long len) {
    return insertInBuffer(buffer, data, buffer->size, len);
}

int removeFromBuffer(Buffer* buffer, unsigned long offset, unsigned long len) {
    if (len == 0)
       return 0;
    assert(offset + len <= buffer->size);

    memcpy(buffer->data + offset, buffer->data + offset + len, buffer->size - offset - len);
    buffer->size -= len;
    shrinkBuffer(buffer);
    return len;
}

int replaceInBuffer(Buffer* buffer, const char* data, unsigned long offset, unsigned long insertionLen, unsigned long deletionLen) {
    if (insertionLen >= deletionLen) {
        memcpy(buffer->data + offset, data, deletionLen);
        return insertInBuffer(buffer, data + deletionLen, offset + deletionLen, insertionLen - deletionLen);
    }
    memcpy(buffer->data + offset, data, insertionLen);
    removeFromBuffer(buffer, offset + insertionLen, deletionLen - insertionLen);
    return 0;
}

void freeBufferData(Buffer* buffer) {
    if(buffer->data)
        wee_free(buffer->data);
    buffer->data = NULL;
    buffer->size = 0;
    buffer->maxSize = 0;
}

int insertElementAt(ArrayList* buffer, void* data, unsigned long pos) {
    return insertInBuffer(buffer, (void*)&data, pos * sizeof(data), sizeof(data));
}

int appendElement(ArrayList* buffer, void* data) {
    return insertElementAt(buffer, data, buffer->size / sizeof(data));
}

int appendElementConst(ArrayList* buffer, const void* data) {
    return insertElementAt(buffer, (void*) data, buffer->size / sizeof(data));
}

long getLen(const ArrayList* buffer) {
    return buffer->size / sizeof(void*);
}

void* getElementAt(const ArrayList* buffer, unsigned long pos) {
    assert(pos < getLen(buffer));
    return buffer->p[pos];
}

int indexOf(const ArrayList* buffer, const void* data) {
    for (void** p = buffer->p; p < buffer->p + buffer->size; p++) {
        if (*p == data)
            return p - buffer->p;
    }
    return -1;
}

void removeElement(ArrayList* buffer, const void* data) {
    int index = indexOf(buffer, data);
    assert(index != -1);
    if(index != -1) {
        removeFromBuffer(buffer, index * sizeof(data), sizeof(data));
    }
}
