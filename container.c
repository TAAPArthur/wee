#include "buffer.h"
#include "container.h"
#include "dock.h"
#include "logger.h"
#include "rect.h"
#include "view.h"
#include "window.h"

#include <assert.h>
#include <stddef.h>

struct Layout* layout;
typedef struct Container {
    ContainerId index;
    ContainerId parentIndex;

    ContainerId nextSiblingIndex;
    ContainerId prevSiblingIndex;
    ContainerId headChildIndex;
    short childCount;
    char visible;
    Window* win;

    Rect bounds;
    Rect subBounds;
    const struct Layout* layout;
} Container;

#define getDataElem(X, i) ((Container*)X + i)
#define TILE_IMPL
#include "tile.h"

static int isDead(const Container* c){return c->nextSiblingIndex == INVALID_CONTAINER_ID;}
static void markDead(Container* c){c->nextSiblingIndex = INVALID_CONTAINER_ID;}

typedef struct ContainerHeader {
    Rect globalBounds;
    Rect effectiveBounds;
    int activeContainer;
    int lastActiveContainer;
    const Buffer* docks;
} ContainerHeader;

static ContainerHeader* getContainerHeader(const Buffer* buffer) {
    return buffer->raw;
}

static ContainerHeader* getHeaderOfContainer(Container* c) {
    assert(c->index != INVALID_CONTAINER_ID);
    return (ContainerHeader*)(c - c->index) - 1;
}

static Container* getFirstContainer(const Buffer* buffer) {
    return buffer->raw + sizeof(ContainerHeader);
}

static Container* getContainerById(Container* c, ContainerId id) {
    assert(c->index != INVALID_CONTAINER_ID);
    assert(id != INVALID_CONTAINER_ID);
    assert((c - c->index + id)->index == id);
    return c - c->index + id;
}

static Container* getNextSibling(Container* container) {
    return getContainerById(container, container->nextSiblingIndex);
}

static Container* getPrevSibling(Container* container) {
    return getContainerById(container, container->prevSiblingIndex);
}

static Container* getEffectiveRootContainer(const Buffer* buffer) {
    return getFirstContainer(buffer) + getContainerHeader(buffer)->activeContainer;
}

static int getNumberOfContainers(const Buffer* buffer) {
    return (buffer->size - sizeof(ContainerHeader))/sizeof(Container);
}

static void linkSiblings(Container*prev, Container* next) {
    prev->nextSiblingIndex = next->index;
    next->prevSiblingIndex = prev->index;
}

static void removeChild(Container* child) {
    if (child->parentIndex == INVALID_CONTAINER_ID) {
        return;
    }
    Container* parent = getContainerById(child, child->parentIndex);
    TRACE("Removing child %d from parent %d", child->index, child->parentIndex);
    child->parentIndex = INVALID_CONTAINER_ID;
    if (--parent->childCount == 0) {
        assert(!parent->win);
        removeChild(parent);
        markDead(parent);
        parent->parentIndex = INVALID_CONTAINER_ID;
        parent->headChildIndex = INVALID_CONTAINER_ID;
        return;
    }
    Container* next = getNextSibling(child);
    Container* prev = getPrevSibling(child);
    linkSiblings(prev, next);
    if (parent->headChildIndex == child->index) {
        parent->headChildIndex = next->index;
    }
}

static void addChild(Container*parent, Container* child) {
    assert(child->parentIndex == INVALID_CONTAINER_ID);
    TRACE("Adding child %d to parent %d\n", child->index, parent->index);
    parent->childCount++;
    child->parentIndex = parent->index;
    if (parent->headChildIndex) {
        Container* head = getContainerById(child, parent->headChildIndex);
        Container* last = getPrevSibling(head);
        linkSiblings(last, child);
        linkSiblings(child, head);
    } else {
        parent->headChildIndex = child->index;
    }
}

static void reparentContainer(Container*newParent, Container* child) {
    TRACE("Reparenting child %d to parent %d\n", child->index, newParent->index);
    removeChild(child);
    addChild(newParent, child);
}

void initContainerState(Buffer* buffer, const Buffer* docks) {
    assert(buffer->size == 0);
    appendToBuffer(buffer, NULL, sizeof(ContainerHeader));
    ContainerHeader * header = buffer->raw;
    header->docks = docks;
}

ContainerId addContainer(Buffer* buffer, Window* win) {
    assert(buffer->size);

    assert(!win || findContainerOfWindow(buffer, win) == INVALID_CONTAINER_ID);
    Container* c = NULL;
    FOR_EACH(Container*, container, buffer, sizeof(ContainerHeader)) {
        if (isDead(container)) {
            c = container;
            break;
        }
    }
    if (!c) {
        int end = buffer->size;
        appendToBuffer(buffer, NULL, sizeof(Container));
        c = buffer->raw + end;
        c->index = c - getFirstContainer(buffer);
    }
    ContainerHeader * header = buffer->raw;
    if (header->lastActiveContainer == c->index) {
        c->layout = &GRID;
    } else {
        c->layout = getContainerById(c, header->lastActiveContainer)->layout;
    }

    c->win = win;
    c->parentIndex = INVALID_CONTAINER_ID;
    linkSiblings(c, c);
    assert(!isDead(c));
    assert(getContainerById(c, c->index) == c);
    assert(getContainerById(c, c->index)->index == c->index);
    return c->index;
}

ContainerId mergeContainers(Buffer* buffer, ContainerId first, ContainerId second) {
    TRACE("Merging %d %d\n", first, second);
    ContainerId newChildId = addContainer(buffer, NULL);
    Container* newParent = getContainerById(getFirstContainer(buffer), first);

    Container * newChild = getContainerById(newParent, newChildId);
    newChild->win = newParent->win;
    newParent->win = NULL;

    addChild(newParent, newChild );

    reparentContainer(newParent, getContainerById(newParent, second));
    assert(newParent->childCount == 2);
    return first;
}

Container* findNextContainerWithWindowAmongSibilings(Container* sibling);
Container* findNextContainerWithWindowAmongChildren(Container* parent) {
    Container* child = parent->headChildIndex == INVALID_CONTAINER_ID ? NULL : getContainerById(parent, parent->headChildIndex);
    return child ? findNextContainerWithWindowAmongSibilings(child) : NULL;
}

Container* findNextContainerWithWindowAmongSibilings(Container* sibling) {
    ContainerId id = sibling->index;
    do {
        if (sibling->win)
            return sibling;
        Container* c = findNextContainerWithWindowAmongChildren(sibling);
        if (c)
            return c;
        sibling = getNextSibling(sibling);
    } while (sibling->index != id);
    return NULL;
}

ContainerId removeContainer(Buffer* buffer, ContainerId id) {
    if (id == INVALID_CONTAINER_ID)
        return INVALID_CONTAINER_ID;
    assert(id != INVALID_CONTAINER_ID);
    Container* c = getContainerById(getFirstContainer(buffer), id);
    ContainerId parentId = c->parentIndex;
    removeChild(c);
    ContainerId nextId = INVALID_CONTAINER_ID;
    if (parentId != INVALID_CONTAINER_ID) {
        Container* parent = getContainerById(getFirstContainer(buffer), parentId);
        Container * next = findNextContainerWithWindowAmongChildren(parent);
        if (next)
            nextId = next->index;
    }
    markDead(c);
    c->win = NULL;
    if (c + sizeof(Container) == buffer->raw + buffer->size) {
        while (c > (Container*)(buffer->raw + sizeof(Container)) && isDead(c)) {
            c--;
        }
        int offset = ((void*)c) - buffer->raw;
        removeFromBuffer(buffer, offset, buffer->size - offset);
    }
    return nextId;
}

static void tileContainer(const struct LayoutState* state, void* container, tile_type_t* bounds) {
    ((Container*)container)->bounds = *(Rect*)bounds;
    ((Container*)container)->visible = 1;
    TRACE("Making Container %d visible\n", ((Container*)container)->index);
}

static int tileContainerFilter(const Container* parent, const Container* other) {
    return other->parentIndex == parent->index;
}

void setEffectiveRootContainerDimensions(const Buffer* buffer, Rect rect) {
    getContainerHeader(buffer)->effectiveBounds = rect;
}

void setRootContainerDimensions(const Buffer* buffer, Rect rect) {
    getContainerHeader(buffer)->globalBounds = rect;
    setEffectiveRootContainerDimensions(buffer, rect);
}

Rect getRootContainerDimensions(const Buffer* buffer) {
    return getContainerHeader(buffer)->globalBounds;
}

Rect getEffectiveRootContainerDimensions(const Buffer* buffer) {
    return getContainerHeader(buffer)->effectiveBounds;
}

void updateBounds(DockType type, int thickness, Rect* bounds, Rect* dockBounds) {
    if ((type == DOCK_BOTTOM || type == DOCK_TOP) && bounds->height < thickness)
        return;
    if ((type == DOCK_RIGHT || type == DOCK_LEFT) && bounds->width < thickness)
        return;
    if (dockBounds) {
        *dockBounds = *bounds;
        switch(type) {
            case DOCK_BOTTOM:
                dockBounds->y = bounds->y + bounds->height - 1;
            case DOCK_TOP:
                dockBounds->height = thickness;
                break;
            case DOCK_RIGHT:
                dockBounds->x = bounds->x + bounds->width - thickness;
            case DOCK_LEFT:
                dockBounds->width = thickness;
                break;
        }
    }
    switch(type) {
        case DOCK_TOP:
            bounds->y += thickness;
        case DOCK_BOTTOM:
            bounds->height -= thickness;
            break;
        case DOCK_LEFT:
            bounds->x += thickness;
        case DOCK_RIGHT:
            bounds->width -= thickness;
            break;
    }
}

void tileContainerRecursive(const Buffer* buffer, Container* subRoot);
void adjustRect(const Buffer* buffer, Container* subRoot) {
    subRoot->subBounds = subRoot->bounds;
    const ViewSettings* viewSettings = subRoot->win ? getWindowSettings(subRoot->win) : NULL;
    FOR_EACH_BUFFER(const Dock*, dock, getHeaderOfContainer(subRoot)->docks) {
        const ViewSettings* settings = viewSettings;
        DockType dockType = settings ? getEffectiveDockType(settings, dock) : NUM_DOCK_TYPES;
        if (dockType == NUM_DOCK_TYPES) {
            continue;
        }
        int num = dockType == DOCK_LEFT || dockType == DOCK_RIGHT ? getDockText(subRoot->win, settings, NULL, dock, NULL, 0, NULL) : 1;
        updateBounds(dockType, num, &subRoot->subBounds, NULL);
    }
}

void tileContainerRecursive(const Buffer* buffer, Container* subRoot) {
    adjustRect(buffer, subRoot);
    if (!subRoot->childCount)
        return;
    LayoutSettings settings = {.tileWindow = tileContainer, .hideInvisibleWindows = 1, .tileFilterFunc = tileContainerFilter, .maxWindowsToTile = subRoot->childCount};

    layoutApply(subRoot->layout, &subRoot->subBounds.x, getFirstContainer(buffer), getNumberOfContainers(buffer), subRoot, &settings);

    int kids = 0;
    FOR_EACH(Container*, container, buffer, sizeof(ContainerHeader)) {
        if (tileContainerFilter(subRoot, container)) {
            tileContainerRecursive(buffer, container);
            if (++kids == subRoot->childCount)
                break;
        }
    }
}

void tileContainers(const Buffer* buffer) {
    Container* c = getEffectiveRootContainer(buffer);
    TRACE("Tiling with root of %d\n", c->index);
    c->bounds = getEffectiveRootContainerDimensions(buffer);
    FOR_EACH(Container*, container, buffer, sizeof(ContainerHeader)) {
        container->visible = 0;
    }
    c->visible = 1;
    tileContainerRecursive(buffer, c);
}

void setLayoutForContainer(const Buffer* buffer, ContainerId containerId, struct Layout* layout) {
    if (containerId == INVALID_CONTAINER_ID)
        return;
    Container* container = getContainerById(getFirstContainer(buffer), containerId);
    if (container->win && container->parentIndex != INVALID_CONTAINER_ID) {
        container = getContainerById(container, container->parentIndex);
    }
    container->layout = layout;
}

void forEachContainerWithWindow(const Buffer* buffer, void(*callback)(Window*, Rect, Rect, void*), void* userData) {
    FOR_EACH(Container*, container, buffer, sizeof(ContainerHeader)) {
        if (container->win && container->visible) {
            callback(container->win, container->bounds, container->subBounds, userData);
        }
    }
}

ContainerId findContainerOfWindow(const Buffer* buffer, const Window* win) {
    FOR_EACH(Container*, container, buffer, sizeof(ContainerHeader)) {
        if (container->win == win)
            return container->index;
    }
    return INVALID_CONTAINER_ID;
}

Window* getWindowOfContainer(const Buffer* buffer, ContainerId containerId) {
    Container* container = getContainerById(getFirstContainer(buffer), containerId);
    return container->win;
}


const Rect * getBoundsOfContainerIgnoringDocks(const Buffer* buffer, ContainerId containerId) {
    if (containerId == INVALID_CONTAINER_ID)
        return NULL;
    Container * container = getContainerById(getFirstContainer(buffer), containerId);
    return &container->subBounds;
}

void activateContainer(const Buffer* buffer, ContainerId containerId) {
    if (containerId == INVALID_CONTAINER_ID)
        return;
    Container* target = getContainerById(getFirstContainer(buffer), containerId);
    ContainerHeader* header = getContainerHeader(buffer);
    header->lastActiveContainer = containerId;
    while(target->parentIndex != INVALID_CONTAINER_ID) {
        if (target->index == header->activeContainer)
            return;
        target = getContainerById(target, target->parentIndex);
    }
    TRACE("Active container is now %d\n",target->index);
    header->activeContainer = target->index;
}
