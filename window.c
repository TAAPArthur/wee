#include "autocommands.h"
#include "buffer.h"
#include "logger.h"
#include "memory.h"
#include "settings.h"
#include "system.h"
#include "view.h"
#include "window.h"
#include <assert.h>

/**
 * Controls weather a given view will be rendered and where it will be rendered
 * along with string viewport and cursor position.
 * A window holds one view, but a View could be held by multiple Windows
 */
typedef struct Window {
    View* view;
    ViewSettings* viewSettings;
    WindowSettings* windowSettings;
    // Stores data that is pending insertion
    Buffer insertionBuffer;
    unsigned long cursor;
    unsigned long start;
    unsigned long end;

    long cursorCol;

    unsigned int viewportOffset;
    unsigned int viewportCol;
    unsigned int disableAutoAdjustForCursor;
    long updateCounter;
} Window;

long getViewIdOfWindow(const Window* win) {
    return getViewId(win->view);
}

static const View* getView(const Window* win) {
    return win->view;
}

static const char* getStartOfBuffer(const Window* window) {
    return getCurrentBuffer(window->view)->readOnlyData;
}

long getSavedCursorCol(Window* win) {
    if (getUpdateCounter(getView(win)) != win->updateCounter) {
        win->updateCounter = getUpdateCounter(getView(win));
        win->cursorCol = -1;
    }
    return win->cursorCol;
}

void setSavedCursorCol(Window* win, long col) {
    win->updateCounter = getUpdateCounter(getView(win));
    win->cursorCol = col;
}

Window* allocWindow(const WindowSettings* windowSettings) {
    Window* win = wee_malloc(ALLOCATION_WINDOW, sizeof(Window));
    memset(win, 0x0, sizeof(Window));
    win->windowSettings = allocWindowSettings(windowSettings, &win->viewSettings);
    win->start = -1;
    return win;
}

WindowSettings* getWindowSettings(const Window* win) {
    return win->windowSettings;
}

void setViewForWindow(Window* window, View* view) {
    if (window->view) {
        releaseView(window->view, 3, &window->cursor);
    }
    if (view && reserveView(view, 3, &window->cursor) != -1) {
        window->view = view;
        window->viewSettings = getSettings(view);
        window->end = getCurrentBuffer(getView(window))->size;
        window->start = -1;
        window->cursor = 0;
    }
}

int setSubViewForWindow(Window* win, long start, long end) {
    win->start = MAX(-1, start);
    win->end = MIN(getCurrentBuffer(getView(win))->size, end);
    setCursorOffset(win, getCursorOffset(win));
    return 0;
}

void freeWindow(Window* win) {
    setViewForWindow(win, NULL);
    freeBufferData(&win->insertionBuffer);

    freeSettings(win->windowSettings);

    wee_free(win);
}

unsigned long getCursorOffset(const Window* win) {
    return win->cursor;
}

long clampToViewBounds(const Window* win, long offset) {
    return MIN(MAX(offset, win->start + 1), win->end);
}

void setCursorOffset(Window* win, unsigned long offset) {
    win->cursor = clampToViewBounds(win, offset);
    runAutoCommands(getPointerSettings(getWindowSettings(win), autocommand_index), AUTO_CMD_POST_SET_CURSOR, (AutocommandParams){.windowParams = {.win = win}});
}

const char* getCursorPosition(const Window* win) {
    return getStartOfBuffer(win) + getCursorOffset(win);
}

void setCursorPosition(Window* win, const char * pos) {
    assert(isInBufferInclusive(getCurrentBuffer(getView(win)), pos));
    return setCursorOffset(win, pos - getStartOfBuffer(win));
}

void setEffectiveCursorOffset(Window* win, unsigned long offset) {
    const char* pos = getPositionFromEffectiveOffset(win, offset);
    if (isInBufferInclusive(getCurrentBuffer(getView(win)), pos)) {
        setCursorPosition(win, pos);
    }
}

void updateCursorPosition(Window* win, int delta) {
    setCursorOffset(win, getCursorOffset(win) + delta);
}

int isCursorAtStart(const Window* win) {
    return getCursorPosition(win) == getCurrentBuffer(getView(win))->readOnlyData;
}

const char* getStartOfViewport(const Window* win, int * col) {
    if (col) {
        *col = win->viewportCol;
    }

    const Buffer* currentBuffer = getCurrentBuffer(getView(win));
    const char* pos = getPositionFromEffectiveOffset(win, win->viewportOffset);
    if (isInBuffer(currentBuffer, pos)) {
        pos = currentBuffer->readOnlyData + clampToViewBounds(win, pos - currentBuffer->readOnlyData);
    }
    return pos;
}

void setStartOfViewport(Window* win, const char * c, int col) {
    win->viewportOffset = getEffectiveOffsetOfPosition(win, c);
    assert(col >=0);
    win->viewportCol = col;
}

void setAutoAudjustViewportForCursor(Window* win, int enable) {
    win->disableAutoAdjustForCursor = !enable;
}

int isAutoAudjustViewportForCursor(Window* win) {
    return !win->disableAutoAdjustForCursor;
}

int replaceAtCurrentPosition(Window* win, const char* data, int len, int deletionLen) {
    if (deletionLen >= len) {
        deletionLen = clampToViewBounds(win, getCursorOffset(win) + deletionLen) - getCursorOffset(win);
    }
    return replaceInView(win->view, data, getCursorOffset(win), len, deletionLen);
}

int insertAtCurrentPosition(Window* win, const char* data, int len) {
    return insertInView(win->view, data, getCursorOffset(win), len);
}

int appendEffectiveRangeToInsertionBuffer(Window * win, unsigned long startOffset, unsigned long endOffset) {
    const char* start, * end, *s = getPositionFromEffectiveOffset(win, startOffset), * startOfNextSection;
    int ret = 0;
    while (getSectionBounds(win, s, &start, &end, &startOfNextSection) != -1) {
        unsigned long endOfSectionOffset = getEffectiveOffsetOfPosition(win, end);
        unsigned long startOfSectionOffset = getEffectiveOffsetOfPosition(win, s);
        int r = appendToInsertionBuffer(win, s, MIN(endOfSectionOffset, endOffset)  - startOfSectionOffset);
        if (r == -1)
            return r;
        ret += r;
        if (endOffset <= endOfSectionOffset) {
            break;
        }
        s = startOfNextSection;
    }
    return ret;
}

int removeAtCurrentPosition(Window* win, int len) {
    Buffer* buffer = &win->insertionBuffer;
    if (len < 0) {
        if(buffer->size) {
            len += removeFromBuffer(buffer, buffer->size + len, -len);
        }
    }
    if (len < 0) {
        int offset = getCursorOffset(win);
        setCursorOffset(win, MAX(0, offset + len));
        len = offset - getCursorOffset(win);
        assert(len >= 0);
    }
    if (len) {
        len =  clampToViewBounds(win, getCursorOffset(win) + len) - getCursorOffset(win);
        // sync cursor col
        len = deleteFromView(win->view, getCursorOffset(win), len);
    }
    return len;
}

int saveWindow(Window* win, const char* name) {
    return saveView(win->view, name);
}

static int onReadComplete(CompletionStatus* status) {
    commitInsertion(status->userData);
    return 0;
}

int readIntoCurrentPosition(Window* win, int run, const char* str) {
    if (run) {
        return readFromCmd(str, &win->insertionBuffer, win->insertionBuffer.size, NULL, onReadComplete, win);
    } else {
        int fd = openFDForView(getView(win), str, OPEN_IO_READ);
        if (fd == -1)
            return -1;
        return readDataFromFd(fd, &win->insertionBuffer, win->insertionBuffer.size, NULL, onReadComplete, win);
    }
}

int commitInsertion(Window* win) {
    View* view = win->view;
    Buffer* insertionBuffer = &win->insertionBuffer;
    int delta = insertionBuffer->size;
    int pos = getCursorOffset(win);
    ALL("Committing insertion '%.*s' at pos %d\n", delta,  insertionBuffer->data, pos);
    insertInView(view, insertionBuffer->data, pos, insertionBuffer->size);
    freeBufferData(insertionBuffer);
    return delta;
}

int appendToInsertionBuffer(Window* win, const char* data, int len) {
    int ret = insertInBuffer(&win->insertionBuffer, data, getInsertionBuffer(win)->size, len);
    if (ret != -1)
        runAutoCommands(getPointerSettings(getWindowSettings(win), autocommand_index), AUTO_CMD_INSERTION_BUFFER_POST_APPEND_CHAR, (AutocommandParams){.windowParams = {.win = win}});
    return ret;
}

const Buffer* getInsertionBuffer(const Window* win) {
    return &win->insertionBuffer;
}

const char* getWindowName(const Window* win) {
    return getView(win) ? getName(getView(win)) : NULL;
}

typedef struct {
    const char* start;
    const char* end;
} DataRange;

typedef DataRange DataRangeArr[3];
static inline int isInRange(const DataRange * range, const void * pos) {
    return range->start <= pos && pos < range->end;
}

static void initDataBuffersForWindow(const Window* win, DataRangeArr buffers) {
    const Buffer* currentBuffer = getCurrentBuffer(getView(win));
    const Buffer* insertionBuffer = getInsertionBuffer(win);
    const char* cursor = getCursorPosition(win);
    buffers[0] = (DataRange) {currentBuffer->readOnlyData, cursor};
    buffers[1] = (DataRange) {insertionBuffer->readOnlyData, getEnd(insertionBuffer)};
    buffers[2] = (DataRange) {cursor, getEnd(currentBuffer)};
}

unsigned long getWindowDataLen(const Window* win) {
    DataRangeArr buffers = {};
    initDataBuffersForWindow(win, buffers);
    unsigned long size = 0;
    for (int i = 0; i < LEN(buffers); i++) {
        size += buffers[i].end - buffers[i].start;
    }
    return size;
}

const char* getPositionFromEffectiveOffset(const Window* win, unsigned long effectiveOffset) {
    size_t cursorOffset = getCursorOffset(win);
    DataRangeArr buffers = {};
    initDataBuffersForWindow(win, buffers);
    unsigned long offset = 0;
    for (int i = 0; i < LEN(buffers); i++) {
        long size = buffers[i].end - buffers[i].start;
        if (offset + size > effectiveOffset) {
            return buffers[i].start + effectiveOffset - offset;
        }
        offset += size;
    }
    return buffers[LEN(buffers) - 1].end;
}

unsigned long getEffectiveOffsetOfPosition(const Window* win, const char* pos) {
    DataRangeArr buffers = {};
    initDataBuffersForWindow(win, buffers);
    unsigned long offset = 0;
    for (int i = 0; i < LEN(buffers); i++) {
        if (!isInRange(&buffers[i], pos)) {
            offset += buffers[i].end - buffers[i].start;
        } else {
            offset += pos - buffers[i].start;
            break;
        }
    }
    // assert(getPositionFromEffectiveOffset(win, offset) == pos);
    return offset;
}


int getSectionBounds(const Window* win, const char* pos, const char** start, const char** end, const char** next) {
    DataRangeArr buffers = {};
    initDataBuffersForWindow(win, buffers);
    *start = NULL;
    *end = NULL;
    *next = NULL;
    for (int i = 0; i < LEN(buffers); i++) {
        if (isInRange(&buffers[i], pos)) {
            *start = buffers[i].start;
            *end = buffers[i].end;
            while (i + 1 < LEN(buffers) && !buffers[i + 1].start) i++;
            *next = i + 1 < LEN(buffers) ? buffers[i + 1].start : NULL;
            return 0;
        }
    }
    return -1;
}

const char* getEffectiveEndPosition(const Window* win) {
    const Buffer* buffer = getCurrentBuffer(getView(win));
    return buffer->readOnlyData + clampToViewBounds(win, buffer->size + 1);
}

Label getLabelAtPosition(const Window* win, const char* pos) {
    LabelRange range;
    getLabelsAtPosition(win, pos, 1, &range);
    return range.label;
}

int getLabelsAtPosition(const Window* win, const char* pos, int n, LabelRange ranges[n]) {
    if (isInBuffer(getCurrentBuffer(getView(win)), pos)) {
        return findLabels(getViewStateBufferConst(getView(win), LABEL_INDEX), pos - getCurrentBuffer(getView(win))->readOnlyData, n, ranges);
    } else {
        ranges[0] = (LabelRange) {LABEL_INSERTION_BUFFER, 0, getInsertionBuffer(win)->size};
        return 1;
    }
}

int getLabelsAtEffectiveOffset(const Window* win, unsigned long effectiveOffset, int n, LabelRange ranges[n]) {
    return getLabelsAtPosition(win, getPositionFromEffectiveOffset(win, effectiveOffset), n, ranges);
}

const Buffer* getSearchBuffer(const Window* win) {
    return getCurrentBuffer(getView(win));
}
