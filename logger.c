#define _POSIX_C_SOURCE 200808L
#include "logger.h"
#include "settings.h"
#include "system.h"
#include <errno.h>
#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

// TODO allow customizing log file
int loggingFD = STDOUT_FILENO;
void setLoggingFd(int fd) {
    loggingFD = fd;
}

const char* getErrorString() {
    return strerror(errno);
}

enum LogLevel currentLogLevel = LOG_LEVEL_TRACE;

void setLogLevel(enum LogLevel logLevel) {
    currentLogLevel = logLevel;
}
void logMsg(enum LogLevel logLevel, const char* fmt, ...) {
    if (logLevel < currentLogLevel)
        return;
    va_list args;
    va_start(args, fmt);
    vdprintf(loggingFD, fmt, args);
    va_end(args);
}
