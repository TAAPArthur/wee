#ifndef DEBUG_H
#define DEBUG_H


#define _CONCAT(A, B) A # B
#define CONCAT(A, B) _CONCAT(A, B)

enum LogLevel {
    LOG_LEVEL_ALL,
    LOG_LEVEL_TRACE,
    LOG_LEVEL_VERBOSE,
    LOG_LEVEL_INFO,
    LOG_LEVEL_WARN,
    LOG_LEVEL_SYS_ERR
};
void logMsg(enum LogLevel logLevel, const char* fmt, ...);

void setLoggingFd(int fd);

const char* getErrorString();

#define LABEL  __FILE__ CONCAT(":", __LINE__) " "
#define LOG(LEVEL, MSG...) logMsg(LEVEL, MSG)

#define SYS_TRACE(MSG...) do { LOG(LOG_LEVEL_SYS_ERR, LABEL "Error: %s\n", getErrorString()); LOG(LOG_LEVEL_SYS_ERR, LABEL MSG); } while(0)
#define ALL(MSG...) LOG(LOG_LEVEL_ALL, LABEL MSG)
#define TRACE(MSG...) LOG(LOG_LEVEL_TRACE, LABEL MSG)
#define VERBOSE(MSG...) LOG(LOG_LEVEL_VERBOSE, LABEL MSG)
#define INFO(MSG...) LOG(LOG_LEVEL_INFO, MSG)
#define WARN(MSG...) LOG(LOG_LEVEL_WARN, MSG)

void setLogLevel(enum LogLevel logLevel);


#endif

