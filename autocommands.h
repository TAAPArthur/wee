#ifndef AUTOCOMMANDS_H
#define AUTOCOMMANDS_H

#include "rect.h"
#include "typedefs.h"

typedef struct {
    union {
        const char* fileName;
        const char* cmd;
    };
    union {
        int fd;
        int ret;
    };
} ViewFileAUParams;

typedef struct {
    unsigned int offset;
    unsigned int len;
    const char* data;
} ViewBufferModAUParams;

typedef struct {
    const char c;
} WindowInsertionBufferAUParams;

typedef enum AutoCommandType {
    // View based Auto commands

    // Run after a view changes name
    AUTO_CMD_SET_NAME,

    // Uses ViewFileAUParams
    AUTO_CMD_READ_END,
    AUTO_CMD_READ_FILE_START,
    AUTO_CMD_READ_CMD_START,
    AUTO_CMD_WRITE_END,
    AUTO_CMD_WRITE_FILE_START,
    AUTO_CMD_WRITE_CMD_START,
    AUTO_CMD_SAVE,

    // Uses ViewBufferModAUParams
    AUTO_CMD_BUFFER_PRE_DELETE_CHAR,
    AUTO_CMD_BUFFER_POST_APPEND_CHAR,
    AUTO_CMD_BUFFER_POST_CHANGE_CHAR,
    LAST_VIEW_AUTO_CMD = AUTO_CMD_BUFFER_POST_CHANGE_CHAR,

    // WindowInsertionBufferAUParams
    AUTO_CMD_INSERTION_BUFFER_POST_APPEND_CHAR,
    AUTO_CMD_POST_SET_CURSOR,

    // Context based Auto commands
    AUTO_CMD_ON_RESIZE,

    AUTO_CMD_PRE_QUIT,
    AUTO_CMD_PRE_EXIT,
    AUTO_CMD_PRE_SUSPEND,
    AUTO_CMD_POST_SUSPEND,
    AUTO_CMD_IDLE,
    NUM_AUTO_COMMAND_TYPES
} AutoCommandType;

typedef struct ViewAutoParams ViewAutoParams;
typedef struct WindowAutoParams WindowAutoParams;
typedef struct ContextAutoParams ContextAutoParams;
typedef struct {
    union {
        struct ViewAutoParams {
            View* view;
            union {
                ViewFileAUParams fileParams;
                ViewBufferModAUParams bufferParams;
            };
        } viewParams;
        struct WindowAutoParams {
            Window* win;
            union {
                WindowInsertionBufferAUParams insertionBufferParams;
            };
        } windowParams;
        struct ContextAutoParams {
            Context* context;
            struct {
                Rect rect;
            } resizeParams;
        } contextParams;
    };
} AutocommandParams;

typedef struct {
    AutoCommandType type;
    union {
        int (*viewFunc)(AutoCommandType type, const ViewAutoParams* params);
        int (*windowFunc)(AutoCommandType type, const WindowAutoParams* params);
        int (*contextFunc)(AutoCommandType type, const ContextAutoParams* params);
    };
} AutocommandCallback;

int runAutoCommands(const AutoCommandArray* arr, AutoCommandType type, AutocommandParams params);

void enableAutocommands(int enable);
#endif
