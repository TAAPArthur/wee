#include "addresses.h"
#include "autocommands.h"
#include "commands.h"
#include "context.h"
#include "events.h"
#include "functions.h"
#include "logger.h"
#include "utility.h"
#include "view.h"
#include "wee_string.h"
#include "window.h"

#include <stdlib.h>
#include <string.h>

int insertCommand(Context * context, Arg arg) {
    setMode(context, (Arg){.m = MODE_EX});
    Window* win = getFocusedWindow(context);
    if (!isCursorAtStart(win)) {
        if (arg.i == -1) {
            updateCursorPosition(win, 1);
        }
    }
    return 0;
}

enum {
    CMD_FLAGS_PLUS,
    CMD_FLAGS_MINUS,
    CMD_FLAGS_NUMBER,
    CMD_FLAGS_PRINT,
    CMD_FLAGS_LIST,
};

static char flagsToChar[] = {
    [CMD_FLAGS_PLUS]   = '+',
    [CMD_FLAGS_MINUS]  = '-',
    [CMD_FLAGS_NUMBER] = '#',
    [CMD_FLAGS_PRINT]  = 'p',
    [CMD_FLAGS_LIST]   = 'l',
};

static int getFlags(const char* flags) {
    int mask = 0;
    for (const char* s = flags; *s; s++) {
        for (int i = 0; i < LEN(flagsToChar); i++) {
            if (flagsToChar[i] == *s) {
                mask |= 1<<i;
                break;
            }
        }
    }
    return mask;
}

static void updateSettingBasedOnFlags(Settings* settings, char flags, int set) {
    if (flags & (1 << CMD_FLAGS_NUMBER)) {
        setEnabledForced(settings, number_index, set);
    }
    if (flags & (1 << CMD_FLAGS_LIST)) {
        setEnabledForced(settings, list_index, set);
    }
}

int printCommand(Context * context, Arg arg) {
    long len = arg.i;
    Window* win = allocWindow(getWindowSettings(getFocusedWindow(context)));
    setViewForWindow(win, getFocusedView(context));
    setSubViewForWindow(win, getCursorOffset(win), getCursorOffset(win) + len);
    //setCursorOffset(win, getCursorOffset(src));
    drawToWindow(context, win);
    freeWindow(win);
    return 0;
}

typedef struct CommandOptions {
    const char* suffix;
    int flag;
} CommandOptions;

static int noop() {return 0;}

static int runFunctionWithoutAutocommands(Context* context, Arg arg) {
    enableAutocommands(0);
    int ret = runCommand(context, arg.s);
    enableAutocommands(1);
    return ret;
}

static int setOption(Context* context, Arg arg) {
    if (!arg.s || !*arg.s) {
        // TODO show settings whose defaults have been changed
        return -1;
    } else if (strcmp(arg.s, "all") == 0) {
        // TODO show all settings
        return -1;
    } else if (arg.s[strlen(arg.s) - 1] == '?') {
        int ret = parseAndSetSetting(getWindowSettings(getFocusedWindow(context)), arg.s);
        if (ret != -1) {
            // TODO show setting value
        }
        return ret;
    } else {
        return parseAndSetSetting(getWindowSettings(getFocusedWindow(context)), arg.s);
    }
}

static CommandOptions optionsList[] = {
    {"!",   ALT_FLAG},
    {"all", ALL_FLAG},
    {}
};

static Command defaultCommands[] = {
    {"" ,      moveVert,               .flags = USE_ARG_FLAG, .arg.i = DOWN},
    {"" ,      noop,                   .flags = ADDR1_FLAG | USE_ARG_FLAG, .arg.i = DOWN},
    {"print",  printCommand,           .flags = ADDR2_FLAG | USE_ARG_FLAG | FLAGS_FLAG, },
    {"list",   printCommand,           .flags = ADDR2_FLAG | USE_ARG_FLAG | FLAGS_FLAG, .arg.i = 1 << CMD_FLAGS_LIST},
    {"number", printCommand,           .flags = ADDR2_FLAG | USE_ARG_FLAG | FLAGS_FLAG, .arg.i = 1 << CMD_FLAGS_NUMBER},

    {"append" , insertCommand,         .flags = ADDR1_FLAG, .arg.i = -1},
    {"insert" , insertCommand,         .flags = ADDR1_FLAG, .arg.i = 0},
    {"edit" , openFileInCurrentWindow,  },
    {"read" , loadIntoWindow,    },
    {"read" , loadCommandIntoWindow,    .flags = ALT_FLAG},
    //{"quit" , closeWindowSafe,   },
    {"quit" , closeWindow,              .flags = ALT_FLAG|ALL_FLAG},
    {"set"  , setOption,                .flags = ALL_FLAG}, {"write", save              },
    {"echo", echoInfo},
    {"echom", echoMessage},
    {"tabedit", openFileInNewWindow},
    {"split", openFileInSplitWindow},
    {"vsplit", openFileInVSplitWindow},
    {"xit"  , saveAndCloseWindow,       .flags = ALT_FLAG | ALL_FLAG},
    {"noau" , runFunctionWithoutAutocommands},
};
STATIC_BUFFER(defaultCommandsBuffer, defaultCommands);

const Buffer * getDefaultCommandsBuffer() {
    return &defaultCommandsBuffer;
}

const Buffer* getDefaultCommands() {
    return &defaultCommandsBuffer;
}

static const char* getFlag(const CommandOptions* optionEntry, const char* cmd, int* cmdLen, int * flagsp, const char** argStart) {
    cmd = skipBlanks(cmd);
    const char* firstSpace = strchr(cmd, ' ');
    *cmdLen = firstSpace ? firstSpace - cmd : strlen(cmd);
    while(firstSpace && *firstSpace == ' ') firstSpace++;
    *argStart = firstSpace ? firstSpace : NULL;
    int flags = 0;

    while (optionEntry->suffix) {
        int entryLen = strlen(optionEntry->suffix);
        if (strncmp(optionEntry->suffix, cmd + *cmdLen - entryLen, entryLen) == 0) {
            flags |= optionEntry->flag;
            *cmdLen -= entryLen;
        }
        optionEntry++;
    }
    *flagsp = flags;
    return cmd;
}

static int runCommandFunc(Context* context, const Command* cmdEntry, Arg arg, const char* addr1, const char* addr2) {
    Window* win = getFocusedWindow(context);

    if (cmdEntry->flags & FLAGS_FLAG) {
        updateSettingBasedOnFlags(getWindowSettings(win), arg.i, 1);
    }
    if (addr1) {
        // If this isn't a 2addr command and addr2 is non-null
        if (!(cmdEntry->flags & ADDR2_FLAG) && addr2) {
            addr1 = addr2;
            addr2 = NULL;
        }
        setCursorPosition(win, addr1);
    }
    if (addr2) {
        arg.i = getEffectiveOffsetOfPosition(win, addr2) - getEffectiveOffsetOfPosition(win, addr1);
    }
    int ret = cmdEntry->func(context, arg);
    if (cmdEntry->flags & FLAGS_FLAG) {
        updateSettingBasedOnFlags(getWindowSettings(win), arg.i, 0);
    }
    return ret;
}

static int runFirstMatchCmd(Context* context, const char* cmd, const char* args, int cmdLen, int flags, const char* addr1, const char* addr2) {
    FOR_EACH_BUFFER(const Command*, cmdEntry, getStateBufferConst(context, COMMAND_INDEX)) {
        if ((cmdLen == 0 && !*cmdEntry->name || cmdLen && strncmp(cmdEntry->name, cmd, cmdLen) == 0) && (cmdEntry->flags & flags) == flags ) {
            TRACE("Running command '%s' args '%s' flags %x\n", cmdEntry->name, args, flags);
            Arg arg = cmdEntry->flags & USE_ARG_FLAG ? cmdEntry->arg : (Arg){.s = args};
            if (flags & FLAGS_FLAG) {
                arg.i = (cmdEntry->flags & USE_ARG_FLAG ? arg.i : 0 ) | getFlags(args);
            }
            if (flags & ALL_FLAG) {
                int ret = 0;
                Window* win;
                FOR_EACH_WINDOW(win, context) {
                    setFocusedWindowTransient(context, win);
                    int r = runCommandFunc(context, cmdEntry, arg, addr1, addr2);
                    if (ret == 0)
                        ret = r;
                }
                restoreFocus(context);
                return ret;
            } else {
                return runCommandFunc(context, cmdEntry, arg, addr1, addr2);
            }
        }
    }
    return -1;
}

static const char* drainAddress(const Window* win, const char* cmdStr, const char** addr1, const char** addr2) {
    const char* a1 = NULL, * a2 = NULL;
    while (1) {
        cmdStr = parseAddresses(win, cmdStr, &a1, &a2);
        if (a2) {
            *addr1 = a1;
            *addr2 = a2;
            continue;
        } else if(a1) {
            *addr1 = *addr2;
            *addr2 = a1;
        }
        break;
    }
    return cmdStr;
}

int runCommand(Context* context, const char* cmd) {
    Window* win = getFocusedWindow(context);
    const char* addr1 = NULL, * addr2 = NULL;
    cmd = parseAddresses(win, cmd, &addr1, &addr2);
    if (addr2) {
        cmd = drainAddress(win, cmd, &addr1, &addr2);
    }

    int cmdLen;
    const char* s;
    int flags;
    cmd = getFlag(optionsList, cmd, &cmdLen, &flags, &s);
    TRACE("Parsed command named '%.*s' args '%s' flags %x\n", cmdLen, cmd, s, flags);

    int ret = -1;
    if (cmd && cmd[0])
        ret = runFirstMatchCmd(context, cmd, s, cmdLen, flags, addr1, addr2);

    return ret;
}

int submitCommand(Context* context, Arg arg) {
    Window* win = getFocusedWindow(context);

    setMode(context, (Arg){.m = MODE_NORMAL});
    unsigned long lineLen;
    const char * line = getCursorLine(win, &lineLen);
    if (lineLen == 0)
        return -1;
    if (!getNextLineAfterCursor(win, 1)) {
        lineLen++;
    }

    Buffer temp = {};
    if (appendToBuffer(&temp, NULL, lineLen) == -1)
        return -1;
    replaceInBuffer(&temp, line, 0, lineLen - 1, lineLen - 1);
    replaceInBuffer(&temp, "\0", lineLen - 1, 1, 1);
    unfocusCmdWindow(context, arg);

    TRACE("Submitting command '%s'\n", temp.readOnlyData);
    int ret = runCommand(context, temp.readOnlyData);

    replaceInBuffer(&temp, "\n", lineLen - 1, 1, 1);
    appendToCommandHistory(context, temp.readOnlyData, temp.size);
    freeBufferData(&temp);
    return ret;
}
