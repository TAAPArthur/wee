#ifndef FORMAT_H
#define FORMAT_H

#include "typedefs.h"
int wee_format(const Window* win, char* buffer, int n, const char* fmt);

typedef enum Tokens {
    TOKEN_NAME,
    TOKEN_FOCUSED,
    TOKEN_SIZE,
    TOKEN_LINENO,
    TOKEN_PERCENT_LINES,
    TOKEN_COLNO,
    TOKEN_LABEL,
    NUM_TOKENS,
} Tokens;

#endif

