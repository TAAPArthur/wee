#ifndef FUNCTIONS_H
#define FUNCTIONS_H

#include "events.h"
#include "typedefs.h"

/**
 * Sets the current mode.
 * If the mode changes to NORMAL, then the insertion buffer of the focused
 * window will be committed
 * arg - should contain a valid mode
 */
int setMode(Context* context, Arg arg);

int enterNormalModeIf(Context* context, Arg arg);

/**
 * Opens a file in a new View and assigns it to a new window
 */
int openFileInNewWindow(Context* context, Arg arg);

/**
 * Opens a file in a new View and assigns it to the focused window
 */
int openFileInCurrentWindow(Context* context, Arg arg);

int openFileInSplitWindow(Context* context, Arg arg);
int openFileInVSplitWindow(Context* context, Arg arg);


/**
 * Closes the focused window
 */
int closeWindow(Context* context, Arg arg);
/**
 * Like closeWindow but fails if the file hasn't been saved
 */
int closeWindowSafe(Context* context, Arg arg);

/**
 * Saves the focused window
 */
int save(Context* context, Arg arg);
/**
 * Combination of save and closeWindow
 */
int saveAndCloseWindow(Context* context, Arg arg);

/**
 * Appends the file denoted by arg.s into the current view
 */
int loadIntoWindow(Context* context, Arg arg);

/**
 * Runs the command denoted by arg.s and appends the output into the current view
 */
int loadCommandIntoWindow(Context* context, Arg arg);

// Motion commands

/**
 * Jumps to the line denoted by arg.i
 */
int gotoLine(Context* context, Arg arg);

int movePage(Context* context, Arg arg);
/*
 * Moves the next arg.i line.
 */
int moveVert(Context* context, Arg arg);

/*
 * Moves the next arg.i column. This function won't cross line boundaries
 */
int moveHor(Context* context, Arg arg);

int moveCursor(Context* context, Arg arg);

// Edit Commands
/**
 * Appends arg.c to the insertion buffer
 */
int appendChar(Context * context, Arg arg);

/**
 * Removes arg.i characters from the current position.
 */
int deleteChar(Context * context, Arg arg);
int deleteCharWithinLine(Context * context, Arg arg);

int replaceChar(Context * context, Arg arg);

/**
 * Removes arg.i lines from the current position.
 */
int deleteCurrentLine(Context * context, Arg arg);

// Simulation function
/*
 * Simulates the following arg.s being typed
 */
int typeText(Context* context, Arg arg);
/*
 * Simulates the following char arg.c being received
 */
int typeChar(Context* context, Arg arg);

// Command windows
/**
 * Runs the current line of the focused window to runCommand
 *
 * If the command window was focused it will be unfocused before the runCommand is called
 */
int submitCommand(Context* context, Arg arg);

/**
 * Assigns focus to the command window
 */
int focusCmdWindow(Context* context, Arg arg);

/**
 * Unassign focus to the command window. This function is a no-op if the command window isn't already focused
 */
int unfocusCmdWindow(Context* context, Arg arg);

// MISC

int jumpToCol(Context* context, Arg arg);

int jumpToCharInLine(Context* context, Arg arg);
int jumpToCharInLineBackwards(Context* context, Arg arg);

int jumpToBeforeCharInLine(Context* context, Arg arg);

int jumpToEndOfLine(Context* context, Arg arg);
int jumpToLastNonNewLineCharInLine(Context* context, Arg arg);

int jumpToAfterCharInLineBackwards(Context* context, Arg arg);


/**
 * Moves the cursor to the start/end of the boundary depending on the values of arg
 *
 * arg.dir the direction to jump to (-1 or 1)
 * arg.index the SettingsIndex that defines the pattern that will define the boundary
 * arg.end controls whether to job to the beginning or end of the boundary
 *
 */
int jumpToBoundary(Context* context, Arg arg);

int echoMessage(Context* context, Arg arg);
int echoInfo(Context* context, Arg arg);


int undo(Context* context);
int redo(Context* context);

int yankCurrentLine(Context * context, Arg arg);
int yankRange(Context * context, Arg arg);
int pasteText(Context * context, Arg arg);
#endif
