#ifndef SYSTEM_H
#define SYSTEM_H

#include "buffer.h"

typedef enum {
    EVENT_READ,
    EVENT_WRITE,
    EVENT_CHILD,
} IOEventType;

#define MAX_SYNC_IO_TRANSFER_BYTES (1 << 12)

// List of flags to control how a file will be opened
// Used with readopenflags and writeopenflags
#define OPEN_IO_READ          (1<<0)
#define OPEN_IO_WRITE         (1<<1)
#define OPEN_IO_READ_WRITE    (OPEN_IO_READ | OPEN_IO_WRITE)
#define OPEN_IO_NON_BLOCKING  (1<<2)
#define OPEN_IO_CLOSE_ON_EXEC (1<<3)
#define OPEN_IO_CREATE        (1<<4)
#define OPEN_IO_CREATE_DIR    (1<<5)
#define OPEN_IO_APPEND        (1<<6)
#define OPEN_IO_TRUNCATE      (1<<7)

/**
 * name - path name to write to. This can be absolute or relative
 * openFlags - controls how the file is opened (blocking, auto-create, auto-create dir etc)
 * statusFlags - If the file was successfully opened, describes its stats (if it was newly created, async etc)
 */
int openFile(const char* name, int openFlags);

/*
* Unlinks the file with the given name
*/
int removeFile(const char* name);


typedef struct ProgressStatus {
    // the user data specified
    void* userData;
    // is the position of the start of the last transfer
    unsigned long offset;
    // is the length of the last transfer
    unsigned long len;
} ProgressStatus;

typedef struct CompletionStatus {
    // the user data specified
    void* userData;
    // If there was an IO error, this value is -1
    // if no child process spawned or the child finished successfully, then this is 0
    // Otherwise this field indicates the return value of the child.
    int exitStatus;
} CompletionStatus;

/*
 * Data type is used to report progress to the caller as data is transferred.
 * If this function returns -1, then we act as if no more data can be read from the file descriptor
 */
typedef int (*OnProgressFunc)(ProgressStatus*);

/*
 * Data type is used to report the completion of an IO/child event
 * To known how much data was transferred, see the onProgressFunc
 */
typedef int (*OnCompletionFunc)(CompletionStatus*);

/**
 * name - path name to write to. This can be absolute or relative
 * data - data to write
 * len - amount of data to write
 * returns -1 on error or 0 if the IO started even if it went async
 */
int writeDataToFd(int fd, const Buffer* buffer, int offset, int len, OnProgressFunc onProgress, OnCompletionFunc onCompletion, void* userData);

/**
 * name - path name to read from. This can be absolute or relative
 * buffer - where to store data
 * offset - offset into the buffer to store data. Note that if this isn't buffer->size, then data will be overwritten
 * returns -1 on error or 0 if the IO started even if it went async
 */
int readDataFromFd(int fd, Buffer* buffer, int offset, OnProgressFunc onProgress, OnCompletionFunc onCompletion, void* userData);

// Wrapper around readDataFromFd to read from stdin at into buffer starting at offset 0
int readDataFromStdin(Buffer* buffer, OnProgressFunc onProgress, OnCompletionFunc onCompletion, void* userData);
/**
 * Reads the output of cmd into the given buffer at the given offset.
 * returns -1 on error or 0 if the IO started even if it went async
 */
int readFromCmd(const char* cmd, Buffer* buffer, unsigned long offset, OnProgressFunc onProgress, OnCompletionFunc onCompletion, void* userData);
/**
 * Writes len bytes of buffer starting at the specified offset into a process running cmd
 * returns -1 on error or 0 if the IO started even if it went async
 */
int writeToCmd(const char* cmd, Buffer* buffer, unsigned long offset, unsigned long len, OnProgressFunc onProgress, OnCompletionFunc onCompletion, void* userData);
/*
 * Writes data to and reads from a process running cmd
 * returns -1 on error or 0 if the IO started even if it went async
 */
int filterWithCmd(const char* cmd, Buffer* bufferSend, unsigned long offsetSend, unsigned long lenSend, Buffer* bufferReceive, unsigned long offsetReceive, OnProgressFunc onProgress, OnCompletionFunc onCompletion, void* userData);
/*
 * Runs the specified command
 * Calls callback(userData, exitStatus) when finished
 * returns -1 on error or 0 if the IO started even if it went async
 */
int runCmd(const char* cmd, OnCompletionFunc onCompletion, void* userData);

/**
 * Adds an fd to listen too
 * return 0 on success
 */
int addPollFd(int fd, IOEventType mode, int(*callback)(), int (*oncompletion)(), void* userData);

/**
 * Returns an index for the given fd that has been registered with addPollFd(fd, mode). This index is
 * invalidated whenever a new fd is added/removed
 */
int findPollFd(int fd, IOEventType mode);

/*
 * Removes the index fd descriptor from the list of fds to listen to
 * i - value returned from findPollFd
 */
int removePollFd(int i);

/**
 * Waits at most timeout miliseconds for an event to occur.
 * Returns the number of pending events.
 * If 0 is returned then that means we successfully waited on a child process and there may or may not be IO outstanding events.
 * Also in this case, the completion function may have been run.
 * A return value of -1 indicates a critical error
 */
int waitForEvent(int timeout);

/*
 * Processes pending events
 * Should only be called after a non-zero return of waitForEvent
 */
int processEvents();

/*
 * Changes the current working directory to path
 */
int changeWorkingDirectory(const char* path);

/*
 * Suspends the process
 */
int suspendProcess();

/**
 * Returns true if stdin is tty
 */
int isTTY();

/*
 * Returns 0 iff the are no even sources
 */
int isIdle();

#endif
