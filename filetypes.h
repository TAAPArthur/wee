#ifndef FILETYPES_H
#define FILETYPES_H

#include "typedefs.h"

typedef enum FileType {
    FT_ANY,
    FT_WEE_CMD,
    FT_C,
    FT_CPP,
    FT_PLAIN_TEXT,
    FT_MARKDOWN,
    NUM_FT
} FileType;

#define C_STYLE_COMMENTS   (1 << 0)
#define CPP_STYLE_COMMENTS (1 << 1)
#define C_FAMILY           (1 << 2)

int doesFileTypeMatch(FileType type, FileType target, int group);

const Buffer* getDefaultFileTypeAutoCommands();

#endif
