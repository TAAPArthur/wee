#ifndef REGISTERS_H
#define REGISTERS_H

#include "buffer.h"
typedef enum Register {
    REGISTER_UNAMED = '"',
    REGISTER_0 = '0',
    REGISTER_1 = '1',
    // REGISTER_2..REGISTER_9
    REGISTER_9 = '9',
    REGISTER_NUM_LAST = REGISTER_9,
    REGISTER_A = 'a',
    REGISTER_Z =  'z',
    REGISTER_NAMED_LAST = REGISTER_Z,
    // REGISTER_B..REGISTER_Z
    REGISTER_A_APPEND = 'A',
    // REGISTER_B_APPEND..REGISTER_Z_APPEND
    REGISTER_Z_APPEND = 'Z',
    REGISTER_NAMED_APPEND_LAST = REGISTER_Z_APPEND,

    REGSITER_LASTER_INSERTED_TEXT = ':',
    REGSITER_FILE_NAME = '%',
    REGSITER_BLACKHOLE = '_',

    REGISTER_DEFAULT = REGISTER_UNAMED,
} Register;

int yankText(Register reg, const char* buffer, unsigned long len);

int storeDeletedText(Register reg, const char* buffer, unsigned long len);

const Buffer* getRegisterBufferConst(Register reg);
const Buffer* getRegistersAutocommands();

void clearAllRegisterData();

#endif
