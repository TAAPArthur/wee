#ifndef TYPEDEFS_H
#define TYPEDEFS_H

/**
 * Forward declaration of core private structs to avoid header pollution/duplicate code
 */
typedef struct Context Context;
typedef struct Buffer Buffer;
typedef struct View View;
typedef struct Settings Settings;
typedef Settings ViewSettings;
typedef Settings WindowSettings;
typedef struct Window Window;
typedef struct Rect Rect;
typedef Buffer AutoCommandArray;

#endif
