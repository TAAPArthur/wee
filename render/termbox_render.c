#include "../autocommands.h"
#include "../context.h"
#include "../events.h"
#include "../functions.h"
#include "../memory.h"
#include "../render.h"
#include "../screen.h"
#include "../system.h"

#define TB_OPT_TRUECOLOR
#define TB_IMPL
#define tb_malloc  wee_third_party_malloc
#define tb_realloc wee_third_party_realloc
#define tb_free    wee_third_party_free
#include "../Thirdparty/termbox.h"

static int process_tb_event(Context* context) {
    struct tb_event ev;
    while (tb_peek_event(&ev, 0) == 0) {
        if(ev.type == TB_EVENT_KEY || ev.type == TB_EVENT_MOUSE) {
            Event event = {.key = ev.key | ev.ch};
            processEvent(context, &event, 0);
        }
        if(ev.type == TB_EVENT_RESIZE) {
            setDimensions(context, tb_width(), tb_height());
        }
    }
    return 0;
}

static AutocommandCallback termboxCallbacks[] = {
    {AUTO_CMD_PRE_SUSPEND, .contextFunc = tb_pre_suspend},
    {AUTO_CMD_POST_SUSPEND, .contextFunc = tb_post_suspend},
};
STATIC_BUFFER(termboxCallbackBuffer, termboxCallbacks);

static int init_termbox(Context* context) {
    if (tb_init() != 0) {
        INFO("Error in tb_init %s\n", tb_strerror(tb_last_errno()));
        return -1;
    }
    if (tb_set_output_mode(TB_OUTPUT_TRUECOLOR)) {
        return -1;
    }

    int ttyfd, resizefd;
    tb_get_fds(&ttyfd, &resizefd);
    addPollFd(ttyfd, EVENT_READ, process_tb_event, NULL, context);
    addPollFd(resizefd, EVENT_READ, process_tb_event, NULL, context);
    setDimensions(context, tb_width(), tb_height());

    addStateBuffer(context, AUTOCOMMAND_INDEX, &termboxCallbackBuffer);
    return 0;
}

static int shutdown_termbox(Context* context) {
    int ttyfd, resizefd;
    tb_get_fds(&ttyfd, &resizefd);
    removePollFd(findPollFd(ttyfd, EVENT_READ));
    removePollFd(findPollFd(resizefd, EVENT_READ));
    return tb_shutdown();
}

static int getStyle(int style) {
    int s = 0;
    if (style & STYLE_BOLD)
        s |= TB_BOLD;
    if (style & STYLE_UNDERLINE)
        s |= TB_UNDERLINE;
    if (style & STYLE_REVERSE)
        s |= TB_REVERSE;
    if (style & STYLE_ITALIC)
        s |= TB_ITALIC;
    if (style & STYLE_BLINK)
        s |= TB_BLINK;
    return s;
}

static int getColor(int color) {
    if (color == 0)
        return 0;
    if (color == COLOR_BLACK) {
        return TB_HI_BLACK;
    }
    return color & 0xFFFFFF;
}

int termbox_set_cell(int x, int y, const char *ch, size_t n, RenderTuple tuple) {
    // Yes, this is undefined behavior if ch isn't int aligned and there's no grantee it would be
    return tb_set_cell(x, y, *ch, getColor(tuple.fg) | getStyle(tuple.style), getColor(tuple.bg));
}
void enableTermboxImpl() {
    static RenderBackend backend = {
        .init = init_termbox,
        .shutdown = shutdown_termbox,
        .set_cell = termbox_set_cell,
        .clear = tb_clear,
        .set_cursor = tb_set_cursor,
        .present = tb_present,
    };
    setRenderBackend(&backend);
}
