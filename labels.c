#include "autocommands.h"
#include "buffer.h"
#include "enum_conversion.h"
#include "labels.h"
#include "logger.h"
#include "memory.h"
#include "search.h"
#include "settings.h"
#include "view.h"

#include <assert.h>

#define C_KEYWORDS "auto|break|case|char|const|continue|default|do|double|else|enum|extern|float|for|goto|if|int|long|register|return|short|signed|sizeof|static|struct|switch|typedef|union|unsigned|void|volatile|while|__\\w*|_[A-Z]\\w*"
#define C99_KEYWORDS "_Bool|_Complex|_Imaginary|inline|restrict"
#define C11_KEYWORDS "_Alignas|_Alignof|_Atomic|_Generic|_Noreturn|_Static_assert|_Thread_local"
#define C23_KEYWORDS "_BitInt|_Decimal128|_Decimal32|_Decimal64|alignas|alignof|bool|constexpr|false|nullptr|static_assert|thread_local|true|typeof|typeof_unqual"

#define PROCESSOR "define|defined|elif|else|endif|error|if|ifdef|ifndef|include|line|pragma|undef"
#define PROCESSOR99 "pragma"
#define PROCESSOR23 "__has_c_attribute|__has_embed|__has_include|elifdef|elifndef|embed|warning"

#define MATCH_ANY(WORDS) "("  WORDS ")+"
#define MATCH_ANY_WORDS(WORDS) "(^|[^[:alnum:]_])" MATCH_ANY(WORDS) "($|[^[:alnum:]_])"

#define MATCH_UNTIL_UNESCAPED_NEWLINE "([^\n]*(\\\\\n)?)*"
#define MATCH_ANY_WITH_CONT(WORDS) MATCH_ANY(WORDS) MATCH_UNTIL_UNESCAPED_NEWLINE

#define MATCH_NUMERIC_LITERAL "(0b[0-9]+|0x[[:xdigit:]]+|[0-9]+)"

#define MATCH_UNTIL_DOUBLE_NEWLINE "([^\n]*\n)*"


static Rule defaultRules[] = {
    {LABEL_INCLUDE,    {FT_ANY, C_FAMILY},         .pattern = "#include[[:blank:]]+[<\"](.*)[>\"]", .matchIndexes = 0x1, .cflags = SEARCH_FLAG_NEWLINE},

    {LABEL_MACRO, {FT_ANY, C_FAMILY},                .pattern = "#" MATCH_ANY_WITH_CONT(PROCESSOR)},
    {LABEL_MACRO, {FT_ANY, C_FAMILY, .dialect = 99}, .pattern = "#" MATCH_ANY_WITH_CONT(PROCESSOR99)},
    {LABEL_MACRO, {FT_ANY, C_FAMILY, .dialect = 23}, .pattern = "#" MATCH_ANY_WITH_CONT(PROCESSOR23)},

    {LABEL_COMMENT, {FT_ANY, C_STYLE_COMMENTS},   .pattern = "//[[:blank:]]*([^\\]+(\\\n)?)*$",      .matchIndexes = 0x1, .cflags = SEARCH_FLAG_NEWLINE},
    {LABEL_COMMENT, {FT_ANY, CPP_STYLE_COMMENTS}, .pattern = "/\\*[[:blank:]]*([^\\]+(\\\n)?)*\\*/", .matchIndexes = 0x1},
    {LABEL_COMMENT, {FT_C, .dialect = 99},        .pattern = "/\\*[[:blank:]]*([^\\]+(\\\n)?)*\\*/", .matchIndexes = 0x1},

    {LABEL_LITERAL, {FT_ANY, C_FAMILY},         .pattern = MATCH_NUMERIC_LITERAL},
    {LABEL_LITERAL, {FT_ANY, C_FAMILY},         .pattern = "'(\\\\?.)'", .matchIndexes = 0x1},

    {LABEL_STRING,  {FT_ANY},                   .pattern = "\"(" MATCH_UNTIL_UNESCAPED_NEWLINE ")\"",            .matchIndexes = 0x1},

    {LABEL_KEYWORD, {FT_ANY, C_FAMILY},                     .pattern = MATCH_ANY_WORDS(C_KEYWORDS),   .matchIndexes = 0x2 },
    {LABEL_KEYWORD, {FT_ANY, C_FAMILY, .dialect = 99},      .pattern = MATCH_ANY_WORDS(C99_KEYWORDS), .matchIndexes = 0x2 },
    {LABEL_KEYWORD, {FT_ANY, C_FAMILY, .dialect = 11},      .pattern = MATCH_ANY_WORDS(C11_KEYWORDS), .matchIndexes = 0x2 },
    {LABEL_KEYWORD, {FT_ANY, C_FAMILY, .dialect = 23},      .pattern = MATCH_ANY_WORDS(C23_KEYWORDS), .matchIndexes = 0x2 },

    {LABEL_HEADING,      {FT_MARKDOWN},          .pattern = "^#{,6} (.*)$", .cflags = SEARCH_FLAG_NEWLINE},
    {LABEL_HEADING,      {FT_MARKDOWN},          .pattern = "^.*$\n[=-]+", .cflags = SEARCH_FLAG_NEWLINE},
    {LABEL_BLOCKQUOTE,   {FT_MARKDOWN},          .pattern = "^>" MATCH_UNTIL_DOUBLE_NEWLINE},
    {LABEL_ORDERED_LIST, {FT_MARKDOWN},          .pattern = "^[0-9]+\\." MATCH_UNTIL_DOUBLE_NEWLINE},
};


STATIC_BUFFER(defaultRulesBuffer, defaultRules);

const Buffer* getDefaultRules() {
    return &defaultRulesBuffer;
}

void addLabel(Buffer* buffer, const LabelRange* range) {
    appendToBuffer(buffer, range, sizeof(LabelRange));
}

static void* getRegex(Rule* rule) {
    if (!rule->_regex_metadata) {
        rule->_regex_metadata = allocRegex(rule->pattern, rule->cflags);
    }
    return rule->_regex_metadata;
}

void freeRule(Rule* rule) {
    if (rule->_regex_metadata) {
        freeRegex(getRegex(rule));
        rule->_regex_metadata = NULL;
    }
}

void freeRules(const Buffer* rules) {
    FOR_EACH_BUFFER(Rule*, rule, rules) {
        freeRule(rule);
    }
}
static void labelCodeHelper(Buffer* ranges, const Buffer* rules, const char* data, FileType type, int dialect, int start, int end);

#define TRACE_ADD_RULE(data, r) do {if ((r)->end > (r)->start || 1) TRACE("Adding label %s '%.*s'\n", ((r)->label < NUM_FT ? toFileTypeStr((r)->label) : toLabelStr((r)->label)), (r)->end -(r)->start, data + (r)->start); } while(0)

static void processMatch(Buffer* ranges, const Buffer* rules, const char* data, FileType type, int dialect, Rule* rule, int nmatch, RegexMatch matches[nmatch]) {
    for (int i = 0; i < nmatch; i++) {
        if (!((1<<i) & rule->matchIndexes << 1 || !rule->matchIndexes && !i))
            continue;
        TRACE("Found match [%d, %d), '%.*s' from %s\n", matches[i].start, matches[i].end, matches[i].end - matches[i].start, data, rule->pattern);
        const LabelRange r = {rule->label, matches[i].start, matches[i].end};
        if (rule->label < NUM_FT) {

            LabelRange prefixLabelRange = {LABEL_BOUNDARY, matches[0].start, r.start};
            TRACE_ADD_RULE(data, &prefixLabelRange);
            addLabel(ranges, &prefixLabelRange);
            LabelRange suffixLabelRange = {LABEL_BOUNDARY, r.end, matches[0].end};
            TRACE_ADD_RULE(data, &suffixLabelRange);
            addLabel(ranges, &suffixLabelRange);

            // TRACE("Found match [%d, %d) %s\n", matches[i].start, matches[i].end, rule->pattern);
            // TODO make more efficient
            labelCodeHelper(ranges, rules, data, type, dialect, matches[0].start, r.start);
            labelCodeHelper(ranges, rules, data, r.label, dialect, r.start, r.end);
            labelCodeHelper(ranges, rules, data, type, dialect, r.end, matches[0].end);

            TRACE_ADD_RULE(data, &r);
            addLabel(ranges, &r);
            return;
        } else {
            TRACE_ADD_RULE(data, &r);
            addLabel(ranges, &r);
        }
    }
}

static void labelCodeHelper(Buffer* ranges, const Buffer* rules, const char* data, FileType type, int dialect, int start, int end) {
    FOR_EACH_BUFFER(Rule*, rule, rules) {
        if (!(doesFileTypeMatch(type, rule->ft, rule->group) && (!rule->dialect || dialect == rule->dialect)))
            continue;

        const int maxMatches = rule->matchIndexes ? sizeof(rule->matchIndexes) : 1;
        RegexMatch matches[maxMatches];
        int offset = start;
        while (offset < end) {
            int nmatch = regexMatch(getRegex(rule), data, offset, end, maxMatches, matches, 0);
            if (nmatch == 0) {
                break;
            }

            processMatch(ranges, rules, data, type, dialect, rule, nmatch, matches);
            assert(matches[0].end != -1);
            offset = matches[0].end;
        }
    }
}

void labelCode(Buffer* ranges, const Buffer* rules, const Buffer* buffer, const ViewSettings* settings) {
    FileType type = getNumericSettings(settings, filetype_index);
    int dialect = getNumericSettings(settings, filetype_dialect_index);
    labelCodeHelper(ranges, rules, buffer->readOnlyData, type, dialect, 0, buffer->size);
}

void labelCodeOfView(Buffer* labels, const Buffer* rules, View* view) {
    labelCode(labels, rules, getCurrentBuffer(view), getSettings(view));

}

int updateLabelsOfView(View* view) {
    Buffer* labels = getViewStateBuffer(view, LABEL_INDEX);
    const Buffer* rules = getRulesBuffer(view);
    freeBufferData(labels);
    labelCodeOfView(labels, rules, view);
    return 0;
}

static int labelBufferOnChange(AutoCommandType type, const ViewAutoParams * params) {
    return updateLabelsOfView(params->view);
}

static AutocommandCallback defaultLabelCallbacks[] = {
    {AUTO_CMD_BUFFER_POST_CHANGE_CHAR, labelBufferOnChange},
};
STATIC_BUFFER(defaultLabelCallbacksBuffer, defaultLabelCallbacks);

const Buffer* getLabelAutocommands() {
    return &defaultLabelCallbacksBuffer;
}

int findLabels(const Buffer* labelBuffer, int offset, int n, LabelRange ranges[n]) {
    int i = 0;
    ranges[0] = (LabelRange) {LABEL_UNKNOWN, offset, offset + 1};
    FOR_EACH(LabelRange*, rule, labelBuffer, 0) {
        if (rule->start <= offset && offset < rule->end) {
            ranges[i++] = *rule;
            if (i == n)
                break;
        }
    }
    return i;
}

Label findLabel(const Buffer* ranges, int offset) {
    LabelRange range;
    findLabels(ranges, offset, 1, &range);
    return range.label;
}
