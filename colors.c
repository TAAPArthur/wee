#include "colors.h"

static color_t colors_8bit[] = {
    COLOR_BLACK,      // BLACK
    COLOR_MAROON,     // RED
    COLOR_GREEN,      // GREEN
    COLOR_BROWN,      // YELLOW
    COLOR_NAVY_BLUE,  // BLUE
    COLOR_PURPLE,     // MAGENTA
    COLOR_TEAL,       // CYAN
    COLOR_LIGHT_GRAY, // WHITE
    COLOR_GRAY,       // BRIGHT_BLACK
    COLOR_RED,        // BRIGHT_RED
    COLOR_LIME_GREEN, // BRIGHT_GREEN
    COLOR_YELLOW,     // BRIGHT_YELLOW
    COLOR_BLUE,       // BRIGHT_BLUE
    COLOR_MAGENTA,    // BRIGHT_MAGENTA
    COLOR_CYAN,       // BRIGHT_CYAN
    COLOR_WHITE,      // BRIGHT_WHITE
};

void copyStyle(RenderTuple * dest, const StyleTuple* src) {
    if (!dest->fg && src->fg)
        dest->fg = src->fg;
    if (!dest->bg && src->bg)
        dest->bg = src->bg;
    if (src->style)
        dest->style |= src->style;
}

int get8BitColor(color_t color) {
    int min_distance = 1 << 18;
    color_t min_color = -1;

    for (int i = 0; i < sizeof(colors_8bit) / sizeof(colors_8bit[0]); i++) {
        int product_sum = 0;
        for (int b = 0; b < 3; b++) {
            int delta = ((char*)&color)[b] - ((char*)&colors_8bit[i])[b];
            product_sum += delta * delta;
        }
        if (product_sum < min_distance) {
            min_distance = product_sum;
            min_color = i;
        }
    }
    return min_color;
}
