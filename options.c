#include "autocommands.h"
#include "buffer.h"
#include "context.h"
#include "logger.h"
#include "settings.h"
#include "utility.h"
#include "view.h"

#include <assert.h>
#include <ctype.h>
#include <stddef.h>

/*
 * Returns the amount of white
 */
static int getIndent(Window* win, const char* pos, unsigned long* startOffset, unsigned long* endOffset) {
    unsigned long len;
    const char* start, *end, *startOfNextSection;
    const char* s = getLine(win, pos, &len);
    int i = 0;
    *startOffset = getEffectiveOffsetOfPosition(win, s);
    *endOffset = *startOffset;
    while (getSectionBounds(win, s, &start, &end, &start) != -1) {
        for (; i < len && s != end; i++, s++) {
            *endOffset = getEffectiveOffsetOfPosition(win, s);
            if (!isspace(*s)) {
                break;
            }
        }
        s = start;
    }
    return *endOffset - *startOffset;
}

static int autoindentAutoCommand(AutoCommandType type, const WindowAutoParams * params) {
    Window* win = params->win;
    if (!isEnabled(getWindowSettings(win), autoindent_index) || getCursorCol(win))
        return 0;
    const char* prevLine = getNextLineAfterCursor(win, -1);
    if (prevLine) {
        unsigned long startOffset, endOffset;
        int ret = getIndent(win, prevLine, &startOffset, &endOffset);
        if (ret > 0) {
            appendEffectiveRangeToInsertionBuffer(win, startOffset, endOffset);
        }
    }
    return 0;
}

static int snapToVisibleAutoCommand(AutoCommandType type, const WindowAutoParams * params) {
    Window* win = params->win;
    WindowSettings* settings = getWindowSettings(win);
    const char* pos = getCursorPosition(win);

    if (isCompitableMode(settings, MODE_INSERT) || !isEnabled(settings, snap_to_visible_index) || !pos)
        return 0;

    CountCharResult result;
    if (getCursorOffset(win) == getWindowDataLen(win) || !getNumCharsUsed(pos, getEffectiveEndPosition(win), settings, &result) && getCursorOffset(win) && getNumCharsUsed(pos - 1, getEffectiveEndPosition(win), settings, &result)) {
        updateCursorPosition(win, -1);
        return -1;
    }
    return 0;
}

static void autoWriteHelper(View* view) {
    Settings* settings = getSettings(view);
    if (isEnabled(settings, autowrite_index) && isEnabled(settings, modified_index) && getName(view)) {
        saveView(view, NULL);
    }
}

static int autowriteAutoCommand(AutoCommandType type, const ContextAutoParams * params) {
    forEachView(params->context, autoWriteHelper);
    return 0;
}

static AutocommandCallback optionCallbacks[] = {
    {AUTO_CMD_INSERTION_BUFFER_POST_APPEND_CHAR, .windowFunc = autoindentAutoCommand},
    {AUTO_CMD_POST_SET_CURSOR, .windowFunc = snapToVisibleAutoCommand},
    {AUTO_CMD_IDLE, .contextFunc = autowriteAutoCommand},
};

STATIC_BUFFER(optionCallbackBuffer, optionCallbacks);

const Buffer* getOptionAutocommands() {
    return &optionCallbackBuffer;
}

int appendTab(Context* context) {
	Window* win = getFocusedWindow(context);
    Settings* settings = getWindowSettings(win);
	if (isEnabled(settings, expandtab_index)) {
        int num = getNumericSettings(settings, expandtab_index);
        char arr[num + 1];
        for (int i = 0; i < num; i++)
            arr[i] = ' ';
		return appendToInsertionBuffer(win, arr, num);
	} else {
		return appendToInsertionBuffer(win, "\t", 1);
	}
}
