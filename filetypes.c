#include "autocommands.h"
#include "context.h"
#include "filetype_ext.h"
#include "filetypes.h"
#include "logger.h"
#include "settings.h"
#include "view.h"

#include <string.h>

static int fileTypeMapping[NUM_FT] = {
    [FT_C]   = C_STYLE_COMMENTS | C_FAMILY,
    [FT_CPP] = CPP_STYLE_COMMENTS | C_STYLE_COMMENTS | C_FAMILY,
};

int doesFileTypeMatch(FileType type, FileType target, int group) {
    return (target == FT_ANY || type == target) && (!group || fileTypeMapping[type] & group);
}

static int autoDetectFileTypeByName(AutoCommandType type, const ViewAutoParams * params) {
    const char* name = getName(params->view);
    name = strrchr(name, '.');
    if (name) {
        name++;
        for (int i = 0; i < LEN(fileTypeToExtMapping); i++) {
            if (fileTypeToExtMapping[i] && strcasecmp(fileTypeToExtMapping[i], name) == 0) {
                setNumericSettings(getSettings(params->view), filetype_index, i);
                return i;
            }
        }
    }
    return FT_ANY;
}

static AutocommandCallback defaultAutoFileTypeDetectionAutocommands[] = {
    {AUTO_CMD_SET_NAME, autoDetectFileTypeByName},
};
STATIC_BUFFER(defaultAutoFileTypeDetectionAutocommandsBuffer, defaultAutoFileTypeDetectionAutocommands);

const Buffer* getDefaultFileTypeAutoCommands() {
    return &defaultAutoFileTypeDetectionAutocommandsBuffer;
}
