#!/bin/sh -ex

[ -n "$1" ]
echo "// Generated file; Do not manually edit"
printf "%s\n%s\n\n" '#include "enum_conversion.h"'  '#include "enum_conversion_helper.h"'
printf "%s" "$@" | sed 's/\(\w*.h\)\s*/\#include "\1"\n/g' | sort
echo
sed -En '/enum.*\{/,/}/ {
    /^\s*(ALL|ANY|NUM|LAST)_/d;
    /}/ {
        s/\}.*/};/p;
        x
        s/^.*$/& strTo&(const char* s) { return getEnumFromString(enumInfo&, LEN(enumInfo&), s); }\nconst char* to&Str(& val) { return getStrFromEnum(enumInfo&, LEN(enumInfo&), val); }/p
        s/^.*$//
        x
    }
    /enum/{
        s/.*enum *([^ ]*) *\{/\1/
        h
        s/^.*$/static const EnumInfo enumInfo&[] = {/p;
    }
    s/^\s*([A-Z]+_([A-Z_]+)).*/    {\1,  "\2"},/p;
}' "$@";
