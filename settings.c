#include "enum_conversion.h"
#include "filetypes.h"
#include "memory.h"
#include "settings.h"
#include "wee_string.h"

#include <assert.h>
#include <ctype.h>

#define SETTING_FLAG_ENABLED     0x01
#define SETTING_FLAG_ALT         0x02
#define SETTING_FLAG_NEEDS_FREE  0x10
#define SETTING_FLAG_FIXED       0x20
#define SETTING_FORCE_ENABLED    0x40

typedef enum {
    SETTINGS_GLOBAL,
    SETTINGS_WINDOW,
    SETTINGS_VIEW,
} SettingsScope;

typedef struct {
    SettingType type;
    const char* name;
} SettingsList;

#define INIT_SETTING(T, N, ...) [N ## _index] = {T, # N},
static SettingsList list[] = {
    SETTINGS_LIST
    {0}
};
#undef INIT_SETTING

#define INIT_SETTING(T, N, C, ...) case N ## _index: return C;
static SettingsScope getSettingsGlobalChar(SettingsIndex index) {
    switch (index) {
        default:
        SETTINGS_LIST
    }
}
#undef INIT_SETTING

#define INIT_SETTING(T, N, ...) T##_t N;
typedef struct Settings {
    struct Settings* globalSettings;
    struct Settings* const * viewSettings;
    SETTINGS_LIST;
    SettingsScope scope;
    unsigned char flags[NUM_SETTINGS];

} Settings;
#undef INIT_SETTING

static int verifyAccess(const Settings* settings, SettingsIndex index) {
    assert(settings);
    if (settings->scope == SETTINGS_GLOBAL)
        return 0;

    ALL("Verifying access to setting: '%s' (%d)\n", getSettingsName(index), index);

    switch(getSettingsGlobalChar(index)) {
        default:
        case SETTINGS_GLOBAL:
            break;
        case SETTINGS_WINDOW:
            assert(settings->scope == SETTINGS_WINDOW);
            break;
        case SETTINGS_VIEW:
            assert(settings->scope == SETTINGS_VIEW || settings->scope == SETTINGS_WINDOW && !!settings->viewSettings);
    }
    return 0;
}

#define getSettingsToModify(settings, index) (getSettingsGlobalChar(index) == SETTINGS_GLOBAL & verifyAccess(settings, index) == 0 ? settings->globalSettings : getSettingsGlobalChar(index) == SETTINGS_VIEW && settings->scope == SETTINGS_WINDOW && settings->viewSettings ? *settings->viewSettings : settings)

void disableAllSettings(Settings * settings) {
    TRACE("Disabling all settings\n");
    for (int i = 0; i < NUM_SETTINGS; i++)
        setEnabled(settings->globalSettings, i, 0);
}

static unsigned char getMask(const Settings* settings, SettingsIndex index) {
    return getSettingsToModify(settings, index)->flags[index];
}

static int setMask(Settings* settings, SettingsIndex index, int mask, int set) {
    settings = getSettingsToModify(settings, index);
    if ((getMask(settings, index) & SETTING_FLAG_FIXED) && (set || !(mask & SETTING_FLAG_FIXED)))
        return -1;
    if (set) {
        settings->flags[index] |= mask;
    } else {
        settings->flags[index] &= ~mask;
    }
    return 0;
}

int setEnabled(Settings* settings, SettingsIndex index, int set) {return setMask(settings, index, SETTING_FLAG_ENABLED, set);}
char isEnabled(const Settings* settings, SettingsIndex index) {return getMask(settings, index) & (SETTING_FORCE_ENABLED | SETTING_FLAG_ENABLED);}
char isAltVersion(const Settings* settings, SettingsIndex index){return getMask(settings, index) & SETTING_FLAG_ALT;}
int setAltVersion(Settings* settings, SettingsIndex index, int set) {return setMask(settings, index, SETTING_FLAG_ALT, set);}
int setFixed(Settings* settings, SettingsIndex index, int set) {return setMask(settings, index, SETTING_FLAG_FIXED, set);}
int setEnabledForced(Settings* settings, SettingsIndex index, int set) {return setMask(settings, index, SETTING_FORCE_ENABLED, set);}

static inline int shouldFreeSettings(Settings* settings, SettingsIndex index) {
    return ((settings->scope == SETTINGS_GLOBAL || settings->scope == getSettingsGlobalChar(index)) && (getMask(settings, index) & SETTING_FLAG_NEEDS_FREE));
}

void freeSettings(Settings* settings) {
    for (int i = 0; i < NUM_SETTINGS; i++) {
        if (shouldFreeSettings(settings, i)) {
            setStringSettings(settings, i, NULL);
        }
    }
    wee_free(settings);
};

struct DefaultSettingsInfo {
    char flags;
    union {
        const char * s;
        int i;
    };
};

static void setDefaults(Settings* settings, SettingsIndex index, struct DefaultSettingsInfo info) {
    if (getSettingsGlobalChar(index) == SETTINGS_GLOBAL && settings->scope != SETTINGS_GLOBAL) {
        return;
    }
    setEnabled(settings, index, !!(info.flags & 0b01));
    setAltVersion(settings, index, !!(info.flags & 0b10));
    if (getSettingsType(index) == SETTING_STR) {
        setStringSettings(settings, index, info.s);
    } else if (getSettingsType(index) != SETTING_BOOL) {
        setNumericSettings(settings, index, info.i);
    }
}

void setDefaultSettings(Settings * settings) {
#define INIT_SETTING(T, N, C, ALT, ...) setDefaults(settings, N ## _index, (struct DefaultSettingsInfo) {__VA_ARGS__});
    SETTINGS_LIST
#undef INIT_SETTING
}

static Settings* allocSettings(SettingsScope scope, const Settings* src, ViewSettings* const * viewSettings ) {
    Settings* settings = wee_malloc(ALLOCATION_VIEW_SETTINGS, sizeof(Settings));
    if (src) {
        const Settings* ref = src;
        if (src->scope == SETTINGS_WINDOW && scope == SETTINGS_VIEW && src->viewSettings && *src->viewSettings)
            ref = *src->viewSettings;
        *settings = *ref;
        settings->scope = scope;
        for (int i = 0; i < NUM_SETTINGS; i++) {
            if (shouldFreeSettings(settings, i)) {
                setMask(settings, i, SETTING_FLAG_NEEDS_FREE, 0);
                setSettings(settings, i, getStringSettings(src, i));
            }
        }
    } else {
        settings->scope = SETTINGS_GLOBAL;
        settings->globalSettings = settings;
        setDefaultSettings(settings);
        settings->scope = scope;
    }
    settings->viewSettings = viewSettings;
    return settings;
}

Settings* allocWindowSettings(const Settings* src, ViewSettings* const* viewSettings ) {
    return allocSettings(SETTINGS_WINDOW, src, viewSettings );
}

Settings* allocViewSettings(const Settings* src) {
    return allocSettings(SETTINGS_VIEW, src, NULL);
}

Settings* allocGlobalSettings() {
    return allocSettings(SETTINGS_GLOBAL, NULL, NULL);
}

#define INIT_SETTING(T, N, ...) case N ## _index: return &settings->N;
static const void* getSettingsField(const Settings* settings, SettingsIndex index) {
    switch(index) {
        SETTINGS_LIST
    }
    assert(0);
    return NULL;
}
#undef INIT_SETTING

#define INIT_SETTING(T, N, X, ALT, ...) if (strncmp(#N, str, len) == 0 || strncmp(#ALT, str, len) == 0) return N ## _index;
#define ALIAS_SETTING(N, S, M) INIT_SETTING(_, N, _, S);
SettingsIndex getSettingsIndexFromString(const char* str, int len) {
    SETTINGS_LIST;
    ALIAS_LIST;
    return NUM_SETTINGS;
}
#undef INIT_SETTING

const char* getSettingsName(SettingsIndex index) {
    return list[index].name;
}

SettingType getSettingsType(SettingsIndex index) {
    return list[index].type;
}

int isNumericSetting(SettingsIndex index) {
    return list[index].type != SETTING_STR && list[index].type != SETTING_ENV_STR && list[index].type != SETTING_POINTER;
}

static const char* getEnvName(SettingsIndex index) {
    const char* name = getSettingsName(index);
    static char buffer[64];
    assert(strlen(name) < sizeof(buffer));
    for (int i = 0; i < strlen(name); i++) {
        buffer[i] = toupper(name[i]);
    }
    buffer[strlen(buffer)] = 0;
    TRACE("HERE %s\n", buffer);
    return buffer;
}

char* getEnvSettingsStr(const Settings* settings, SettingsIndex index) {
    return getenv(getEnvName(index));
}

int setEnvSettings(Settings* settings, SettingsIndex index, const char* s) {
    assert(list[index].type == SETTING_ENV_STR || list[index].type == SETTING_ENV_INT);
    return setenv(getEnvName(index), s, 1);
}

typedef union SettingsValue {
    SETTING_LONG_t n;
    SETTING_STR_t s;
    SETTING_POINTER_t ptr;
} SettingsValue;

static SettingsValue getSettings(const Settings* settings, SettingsIndex index) {
    settings = getSettingsToModify(settings, index);
    verifyAccess(settings, index);
    const void* dest = getSettingsField(settings, index);
    SettingsValue value = {};
    switch(list[index].type) {
        case SETTING_BOOL:
            value.n = isEnabled(settings, index);
            break;
        case SETTING_CHAR:
            value.n = *((SETTING_CHAR_t*)dest);
            break;
        case SETTING_ENUM:
        case SETTING_INT:
            value.n = *((SETTING_INT_t*)dest);
            break;
        case SETTING_LONG:
            value.n = *((SETTING_LONG_t*)dest);
            break;
        case SETTING_STR:
            value.s = *((SETTING_STR_t*)dest);
            break;
        case SETTING_ENV_STR:
            value.s = getEnvSettingsStr(settings, index);
            break;
        case SETTING_ENV_INT:
            parseAsNumber(getEnvSettingsStr(settings, index), &value.n);
            break;
        case SETTING_POINTER:
            value.ptr = *((SETTING_POINTER_t*)dest);
            break;
    }
    return value;
}

long getNumericSettings(const Settings* settings, SettingsIndex index) {
    assert(isNumericSetting(index));
    return getSettings(settings, index).n;
}

SETTING_STR_t getStringSettings(const Settings* settings, SettingsIndex index) {
    assert(!isNumericSetting(index) && list[index].type != SETTING_POINTER);
    return getSettings(settings, index).s;
}

const void* getPointerSettings(Settings* settings, SettingsIndex index) {
    assert(list[index].type == SETTING_POINTER);
    return getSettings(settings, index).ptr;
}

void setNumericSettings(Settings* settings, SettingsIndex index, int i) {
    assert(list[index].type != SETTING_STR);
    settings = getSettingsToModify(settings, index);
    const void* dest = getSettingsField(settings, index);
    switch(list[index].type) {
        default:
        case SETTING_BOOL:
            setEnabled(settings, index, !!i);
            break;
        case SETTING_CHAR:
            *((SETTING_CHAR_t*)dest) = i & 255;
            break;
        case SETTING_ENUM:
        case SETTING_INT:
            *((SETTING_INT_t*)dest) = i;
            break;
        case SETTING_LONG:
            *((SETTING_LONG_t*)dest) = i;
            break;
    }
}

void setPointerSettings(Settings* settings, SettingsIndex index, const void* ptr) {
    assert(list[index].type == SETTING_POINTER);
    settings = getSettingsToModify(settings, index);
    const void* dest = getSettingsField(settings, index);
    *((SETTING_POINTER_t*)dest) = ptr;
}


int setStringSettings(Settings* settings, SettingsIndex index, const char* s) {
    assert(list[index].type == SETTING_STR);
    settings = getSettingsToModify(settings, index);
    SETTING_STR_t* dest = (SETTING_STR_t*)getSettingsField(settings, index);
    if (*dest && (getMask(settings, index) & SETTING_FLAG_NEEDS_FREE)) {
        wee_free((char*)*dest);
    }
    *dest = s;
    setMask(settings, index, SETTING_FLAG_NEEDS_FREE, 0);
    return 0;
}

int setSettings(Settings* settings, SettingsIndex index, const char* s) {
    if (list[index].type == SETTING_STR) {
        settings = getSettingsToModify(settings, index);
        int ret = setStringSettings(settings, index, s ? wee_strdup(s) : NULL);
        setMask(settings, index, SETTING_FLAG_NEEDS_FREE, 1);
        return ret;
    } else if (list[index].type == SETTING_ENV_STR || list[index].type == SETTING_ENV_INT) {
        setEnvSettings(settings, index, s);
    } else if (list[index].type == SETTING_ENUM) {
            int value = -1;
            switch(index){
                case filetype_index :
                    value = strToFileType(s);
                    break;
                case default_fg_index:
                case default_bg_index:
                case default_style_index:
                    value = strToStyle(s);
                    break;
                case show_cmd_bar_index:
                case show_info_bar_index:
                    value = strToDockType(s);
                    break;
                case mode_index:
                    value = strToMode(s);
                    break;
                default:
                    assert(0);
            }
            if(value == -1) {
                return -1;
            }
            setNumericSettings(settings, index, value);
    } else {
        setNumericSettings(settings, index, strtol(s, NULL, 0));
    }
    return 0;
}

int parseAndSetSetting(Settings* settings, const char* s) {
    const char* settingName = NULL;
    const char* settingValue = NULL;
    int len = 0;
    int enable = 1;
    while (*s) {
        if (*s == '=' || *s == ':') {
            settingValue = s + 1;
            break;
        } if (*s != ' ') {
            if (settingName == NULL) {
                settingName = s;
            }
            len++;
        }
        s++;
    }

    TRACE("Parsing settings '%.*s'; value = '%s'\n", len, settingName, settingValue);
    SettingsIndex index = getSettingsIndexFromString(settingName, len);
    if (index == NUM_SETTINGS) {
        if(strncmp(settingName, "no", 2) == 0) {
            index = getSettingsIndexFromString(settingName + 2, len - 2);
            enable = 0;
        } else if(strncmp(settingName, "inv", 3) == 0) {
            index = getSettingsIndexFromString(settingName + 3, len - 3);
            enable = !isEnabled(settings, index);
        } else if (settingName[len - 1] == '!') {
            index = getSettingsIndexFromString(settingName, len - 1);
            enable = !isEnabled(settings, index);
        } else if (settingName[len - 1] == '?') {
            index = getSettingsIndexFromString(settingName, len - 1);
        }
    }
    if (index == NUM_SETTINGS) {
        return -1;
    }
    TRACE("Parsed settings %s (%d) settingValue '%s' enable %d\n", getSettingsName(index), index, settingValue, enable);
    if (!settings) {
        return 0;
    } else if(settingValue) {
        return setSettings(settings, index, settingValue);
    } else {
        setEnabled(settings, index, enable);
        return 0;
    }
}

void setupBatchMode(Settings* settings) {
    setEnabled(settings, autoindent_index, 0);
    setFixed(settings, autoindent_index, 1);
    setEnabled(settings, exinit_index, 0);
    setEnabled(settings, interactive_index, 0);
    setEnabled(settings, show_info_bar_index, 0);
    setEnabled(settings, term_index, 0);
    setEnabled(settings, window_index, 0);
}
