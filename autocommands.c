#include "autocommands.h"
#include "buffer.h"
#include "enum_conversion.h"
#include "logger.h"
#include <assert.h>
#include <stdlib.h>

static char autoCommandsDisabled;
void enableAutocommands(int enable) {
    autoCommandsDisabled = !enable;
}

int runAutoCommands(const AutoCommandArray* arr, AutoCommandType type, AutocommandParams params) {
    if (!arr || autoCommandsDisabled) {
        return 0;
    }
    FOR_EACH_BUFFER(const AutocommandCallback*, elem, arr) {
        if (elem->type == type) {
            ALL("Running command type %s (%d)\n", toAutoCommandTypeStr(elem->type), elem->type);
            int ret;
            if (type <= LAST_VIEW_AUTO_CMD) {
                assert(params.viewParams.view);
                ret = elem->viewFunc(elem->type, &params.viewParams);
            } else {
                assert(params.contextParams.context);
                ret = elem->contextFunc(elem->type, &params.contextParams);
            }
            if (ret < 0)
                return ret;
        }
    }
    return 0;
}
