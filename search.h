#ifndef REGEX_H
#define REGEX_H

#include "typedefs.h"
typedef struct RegexMatch {
    int start;
    int end;
} RegexMatch;

typedef enum RegexImpl {
    // Standard POSIX regex impl. This does not support embedded new lines
    REGEX_IMPL_POSIX,
    // Standard POSIX regex impl with BSD extensions. This does not support embedded new lines
    REGEX_IMPL_BSD,
    // https://laurikari.net/tre/
    REGEX_IMPL_TRE,
    NUM_REGEX_IMPL
} RegexImpl;

/*
 * Checks to see if impl i is the current regex engine
 */
int isRegexBackendEnabled(RegexImpl i);
/*
 * Changes the current regex backend to i. This should not be used if there are any allocated regexes
 */
int setRegexBackend(RegexImpl i);
// Returns the index of the active backend
RegexImpl getRegexBackend();

// See REG_NEWLINE
#define SEARCH_FLAG_NEWLINE      0x1
// See REG_ICASE
#define SEARCH_FLAG_IGNORE_CASE  0x2
// Disables REG_EXTENDED
#define SEARCH_FLAG_BASIX_REGEX  0x4

// See REG_NOTBOL
#define SEARCH_NOTBOL      1
// See REG_NOTEOL
#define SEARCH_NOTEOL      2

/*
 * Allocates memory to create a regex of pattern with the corresponding search flags
 */
void * allocRegex(const char * pattern, int flags);
/*
 * Frees memory assoicated with regex
 * regex should be a result of allocRegex
 */
void freeRegex(void * regex);

int regexMatch(void* regex, const char * data, int offset, int end, int nmatch, RegexMatch matches[nmatch], int eflags);
int regexFindAll(void * regex, const char* data, int start, int end, int nmatch, RegexMatch matches[nmatch], int eflags);
int regexFindMatchContainingPosition(void * regex, const char* data, int start, int end, int target, int dir, int snapToEnd, int eflags);

#endif
